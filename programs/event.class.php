<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * The crm_Event class
 */
class crm_Event extends crm_Object
{
	/**
	 * this method can be called when an addon with crm classes is upgraded
	 * @return void
	 */
	public function onUpgrade($addonname)
	{
		$path = $this->Crm()->getPath();
		$Crm = "bab_functionality::get('$path')->Event()";

		bab_addEventListener('bab_eventGroupDeleted', $Crm.'->onGroupDeleted', '', $addonname);
		bab_addEventListener('bab_eventUserModified', $Crm.'->onUserModified', '', $addonname);
		bab_addEventListener('bab_eventUserDeleted', $Crm.'->onUserDeleted', '', $addonname);
		bab_addEventListener('bab_eventBeforeSiteMapCreated', $Crm.'->onBeforeSiteMapCreated', '', $addonname);
		bab_addEventListener('libpayment_EventPaymentSuccess', $Crm.'->onPaymentSuccess', '', $addonname);
		bab_addEventListener('libpayment_EventPaymentUserReturn', $Crm.'->onPaymentUserReturn', '', $addonname);
		bab_addEventListener('libpayment_EventPaymentError', $Crm.'->onPaymentError', '', $addonname);
		bab_addEventListener('libpayment_EventPaymentCancel', $Crm.'->onPaymentCancel', '', $addonname);
		bab_addEventListener('LibTimer_eventDaily', $Crm.'->onDaily', '', $addonname);
	}

	/**
	 * this method can be called when an addon with crm classes is deleted
	 * @return void
	 */
	public function onDelete($addonname)
	{
		$path = $this->Crm()->getPath();
		$Crm = "bab_functionality::get('$path')->Event()";

		bab_removeEventListener('bab_eventGroupDeleted', $Crm.'->onGroupDeleted', '');
		bab_removeEventListener('bab_eventUserModified', $Crm.'->onUserModified', '');
		bab_removeEventListener('bab_eventUserDeleted', $Crm.'->onUserDeleted', '');
		bab_removeEventListener('bab_eventBeforeSiteMapCreated', $Crm.'->onBeforeSiteMapCreated', '');
		bab_removeEventListener('libpayment_EventPaymentSuccess', $Crm.'->onPaymentSuccess', '');
		bab_removeEventListener('libpayment_EventPaymentUserReturn', $Crm.'->onPaymentUserReturn', '');
		bab_removeEventListener('libpayment_EventPaymentError', $Crm.'->onPaymentError', '');
		bab_removeEventListener('libpayment_EventPaymentError', $Crm.'->onPaymentCancel', '');
		bab_removeEventListener('LibTimer_eventDaily', $Crm.'->onDaily', '');
	}


	/**
	 *
	 * @param bab_eventGroupDeleted $event
	 * @return void
	 */
	public function onGroupDeleted(bab_eventGroupDeleted $event)
	{

	}


	/**
	 *
	 * @param bab_eventUserModified $event
	 * @return void
	 */
	public function onUserModified(bab_eventUserModified $event)
	{
		$Crm = $this->Crm();
		
		if (isset($Crm->Contact))
		{
		
			$set = $Crm->ContactSet();
			$set->address();
	
			$res = $set->select($set->user->is($event->id_user));
	
			foreach($res as $contact)
			{
				/* @var $contact crm_Contact */
				$contact->onUserModified();
			}
		}
	}


	/**
	 *
	 * @param bab_eventUserDeleted $event
	 * @return void
	 */
	public function onUserDeleted(bab_eventUserDeleted $event)
	{
		$Crm = $this->Crm();
		
		if (isset($Crm->Contact))
		{
		
			$set = $Crm->ContactSet();
	
			$contact = $set->get($set->user->is($event->id_user));
			if (isset($contact))
			{
				$contact->user = 0;
				$contact->save();
			}
		}
	}


	/**
	 * Add nodes to sitemap
	 * @param	bab_eventBeforeSiteMapCreated	$event
	 */
	public function onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
	{


		bab_functionality::includefile('Icons');
		$Crm = $this->Crm();

		if (!$Crm->onlineShop || !isset($Crm->Catalog))
		{
			return;
		}

		$link = $event->createItem($Crm->classPrefix.'OnlineShop');
		$link->setLabel($Crm->translate('Online shop'));
		$link->setPosition(array('root', 'DGAll'));
		$link->addIconClassname(Func_Icons::PLACES_FOLDER);

		$event->addFolder($link);
		
		$link = $event->createItem($Crm->classPrefix.'Catalogs');
		$link->setLabel($Crm->translate('Categories'));
		$link->setLink($Crm->Controller()->CatalogItem()->catalog(0)->url());
		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		$link->addIconClassname('objects-product-category'); // // Func_Icons::OBJECTS_PRODUCT_CATEGORY 7.8.95
		$event->addFunction($link);

		$set = $Crm->CatalogSet();
		$set->addSitemapNodes($event, 0, array('root', 'DGAll', $Crm->classPrefix.'OnlineShop', $Crm->classPrefix.'Catalogs'));
		
		if ($Crm->Access()->canSearchCatalogItems()) {
    		$link = $event->createItem($Crm->classPrefix.'Search');
    		$link->setLabel($Crm->translate('Advanced search'));
    		$link->setLink($Crm->Controller()->CatalogItem()->displayList(null, 1, 1)->url());
    		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
    		$link->addIconClassname(Func_Icons::ACTIONS_EDIT_FIND);
    		$event->addFunction($link);
		}
		
		/*
		$link = $event->createItem($Crm->classPrefix.'Rss.xml');
		$link->setLabel($Crm->translate('Products RSS feed'));
		$link->setLink($Crm->Controller()->CatalogItem()->rss()->url());
		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		$event->addFunction($link);
		*/
		$link = $event->createItem($Crm->classPrefix.'PaymentUserReturn');
		$link->setLabel($Crm->translate('Payment confirmation'));
		$link->setLink($Crm->Controller()->ShoppingCart()->paymentUserReturn()->url());
		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		$link->addIconClassname(Func_Icons::ACTIONS_DIALOG_OK);
		$event->addFunction($link);
		
		$link = $event->createItem($Crm->classPrefix.'AccountCreated');
		$link->setLabel($Crm->translate('Your account has been created'));
		$link->setDescription($Crm->translate('Account creation confirmation page'));
		$link->setLink($Crm->Controller()->ShoppingCart()->paymentUserReturn()->url());
		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		$link->addIconClassname(Func_Icons::ACTIONS_DIALOG_OK);
		$event->addFunction($link);
		
		
		$link = $event->createItem($Crm->classPrefix.'TermsOfSale');
		$link->setLabel($Crm->translate('Terms of sale'));
		$link->setLink($Crm->Controller()->ShoppingCart()->termsOfSale()->url());
		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		$link->addIconClassname(Func_Icons::OBJECTS_PUBLICATION_ARTICLE);
		$event->addFunction($link);
		
		$link = $event->createItem($Crm->classPrefix.'ShoppingCart');
		$link->setLabel($Crm->translate('Shopping cart'));
		$link->setLink($Crm->Controller()->ShoppingCart()->edit()->url());
		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		$link->addIconClassname(Func_Icons::OBJECTS_SHOPPING_CART);
		$event->addFunction($link);
		
		$link = $event->createItem($Crm->classPrefix.'LoginCreate');
		$link->setLabel($Crm->translate('Login or create an account'));
		$link->setLink($Crm->Controller()->MyContact()->loginCreate()->url());
		$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		$link->addIconClassname(Func_Icons::ACTIONS_USER_NEW);
		$event->addFunction($link);
		
		
		if (bab_isUserLogged())
		{
			$link = $event->createItem($Crm->classPrefix.'MyAccount');
			$link->setLabel($Crm->translate('My account'));
			$link->setLink($Crm->Controller()->MyContact()->edit()->url());
			$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
			$link->addIconClassname(Func_Icons::ACTIONS_USER_PROPERTIES);
			$event->addFunction($link);
			
			$link = $event->createItem($Crm->classPrefix.'MyShoppingCarts');
			$link->setLabel($Crm->translate('My saved shopping carts'));
			$link->setLink($Crm->Controller()->MyContact()->displayCartList()->url());
			$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
			$link->addIconClassname(Func_Icons::ACTIONS_VIEW_LIST_DETAILS);
			$event->addFunction($link);
			
			$link = $event->createItem($Crm->classPrefix.'MyOrders');
			$link->setLabel($Crm->translate('My orders'));
			$link->setLink($Crm->Controller()->MyContact()->orderList()->url());
			$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
			$link->addIconClassname(Func_Icons::ACTIONS_VIEW_LIST_DETAILS);
			$event->addFunction($link);
		}

		if (isset($Crm->PrivateSell))
		{
			$set = $Crm->PrivateSellSet();
			$set->addSitemapNodes($event, array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
		}

		if ($Crm->Access()->viewGiftCard())
		{
			$link = $event->createItem($Crm->classPrefix.'GiftCard');
			$link->setLabel($Crm->translate('Gift card'));
			$link->setDescription($Crm->translate('A coupon to use in our online shop'));
			$link->setLink($Crm->Controller()->GiftCard()->display()->url());
			$link->setPosition(array('root', 'DGAll', $Crm->classPrefix.'OnlineShop'));
			$link->addIconClassname('objects-product'); // Func_Icons::OBJECTS_PRODUCT 7.8.95

			$event->addFunction($link);
		}
	}
	
	
	private function getUserShoppingCart(libpayment_EventPayment $event)
	{
	    $Crm = $this->Crm();
	    
	    if (!$Crm->onlineShop || !isset($Crm->ShoppingCart))
	    {
	        return null;
	    }
	    
	    $payment = $event->getPayment();
	    $paymentToken = $payment->getToken();
	    
	    $set = $Crm->ShoppingCartSet();
	    $cart = $set->get($set->paymentToken->is($paymentToken));
	    
	    if (null === $cart) {
	        bab_debug(sprintf($Crm->translate('%s ; Shopping cart not found for payment token %s'), get_class($Crm), $paymentToken));
	        return null;
	    }
	    
	    
	    
	    
	    return $cart;
	}
	
	
	/**
	 * When the user get back to the site after a payment
	 */
	public function onPaymentUserReturn(libpayment_EventPaymentUserReturn $event)
	{
	    $cart = $this->getUserShoppingCart($event);
	    
	    if (!isset($cart)) {
	        return;
	    }

	    
	    $Crm = $this->Crm();
		
		// on retrouve bien le panier, pas plus de verification car la confirmation du paiement 
		// peut etre en cours de traitement (onPaymentSuccess peut etre appelle avant ou apres)
		// afficher la page de confirmation, utiliser l'url reecrite
		
		
		$action = $Crm->Controller()->ShoppingCart()->paymentUserReturn($cart->id, 0);
		/*@var $action Widget_Action */
		$action->location();
	}
	
	/**
	 * When the user get back to the site without payment (cancel button)
	 */
	public function onPaymentCancel(libpayment_EventPaymentCancel $event)
	{
	    $cart = $this->getUserShoppingCart($event);
	    
	    if (!isset($cart)) {
	        return;
	    }

	    
	    $Crm = $this->Crm();
		
	    $action = $Crm->Controller()->ShoppingCart()->paymentUserReturn($cart->id, 1);
		/*@var $action Widget_Action */
		$action->location();
	}


	/**
	 * Manages a successful payment.
	 *
	 * @param	libpayment_EventPaymentSuccess	$event
	 */
	public function onPaymentSuccess(libpayment_EventPaymentSuccess $event)
	{
		$Crm = $this->Crm();
		
		if (!$Crm->onlineShop || !isset($Crm->ShoppingCart))
		{
			return;
		}
		
		$payment = $event->getPayment();

		$paymentToken = $payment->getToken();

		
		bab_debug(sprintf('%s ; received payment confirmation URL : %s', get_class($Crm), $_SERVER['REQUEST_URI']));


		$set = $Crm->ShoppingCartSet();
		$cart = $set->get($set->paymentToken->is($paymentToken));

		if (null === $cart) {
			bab_debug(sprintf($Crm->translate('%s ; Shopping cart not found for payment token %s'), get_class($Crm), $paymentToken));
			return false;
		}

		/*@var $cart crm_ShoppingCart */
		
		if (isset($Crm->ShopTrace))
		{
			$shopTraceSet = $Crm->ShopTraceSet();
			$shopTraceSet->cartLog($cart, 'Return from payment gateway');
		}


		if (crm_ShoppingCart::CONFIRMED !== (int) $cart->status)
		{
			bab_debug($Crm->translate('Your shopping cart is not confirmed'));
			return false;
		}

		$total = $cart->getTotalTI();

		$paymentResponseAmount = $event->getResponseAmount();



		if (round($paymentResponseAmount * 100) !== round($total * 100))
		{
			if (isset($shopTraceSet))
			{
				$shopTraceSet->cartLog($cart, sprintf($Crm->translate('The billing amount is incorrect, shopping cart : %d; payment : %d'), $total, $paymentResponseAmount));
			}
			bab_debug(sprintf($Crm->translate('The billing amount is incorrect, shopping cart : %d; payment : %d'), $total, $paymentResponseAmount));
			return false;
		}


		// create order

		$authorization = $event->getResponseAuthorization();
		$transaction = $event->getResponseTransaction();
		$order = $cart->createOrder($authorization, $transaction);
		
		if (false === $order)
		{
			// do not notify if the order is not correct because the PDF of the order in attached in notifications
			return $cart->id;
		}
		
		
		if (isset($shopTraceSet))
		{
			$shopTraceSet->cartLog($cart, sprintf('Order %s created. Send order notification', $order->name));
		}
		
		
		$set = $Crm->ShoppingCartSet();
		$set->paymentorder();
		$set->paymentorder->contact();
		$set->paymentorder->deliveryaddress();
		$set->paymentorder->billingaddress();
		$set->paymentorder->shippingaddress();
		
		$shoppingCart = $set->get($cart->id);
		
		$this->onPaymentSuccessEmail($shoppingCart);

		return $shoppingCart->id;
	}
	
	/**
	 * Manage send mail on payement sucess
	 * @param crm_ShoppingCart $shoppingCart
	 */
	protected function onPaymentSuccessEmail($shoppingCart)
	{
		$Crm = $this->Crm();
		$Notify = $Crm->Notify();

		$email = $Notify->shoppingCart_creditCardResponseCustomer($shoppingCart);
		$email->send();
		
		$email = $Notify->shoppingCart_creditCardResponseManager($shoppingCart);
		$email->send();
	}
	
	
	/**
	 * Manages a failed payment.
	 *
	 * @param	libpayment_EventPaymentSuccess	$event
	 */
	public function onPaymentError(libpayment_EventPaymentError $event)
	{
		$Crm = $this->Crm();
		bab_debug(__FUNCTION__);
		
		$payment = $event->getPayment();
		$paymentToken = $payment->getToken();
		
		$SCset = $Crm->ShoppingCartSet();
		$cart = $SCset->get($SCset->paymentToken->is($paymentToken));
		
		if (null === $cart) {
			bab_debug(sprintf($Crm->translate('Shopping cart not found for payment token %s'), $paymentToken));
			return false;
		}
		
		// add a PaymentError
		
		
		$set = $Crm->PaymentErrorSet();
		$paymenterror = $set->newRecord();
		
		/*@var $paymenterror crm_PaymentError */
		
		$paymenterror->cart 				= $cart->id;
		
		$paymenterror->code 				= $event->response_code;
		$paymenterror->message				= $event->errorMessage;
		$paymenterror->paymentToken			= $paymentToken;
		$paymenterror->createdOn			= date('Y-m-d H:i:s');
		$paymenterror->bank_response_code	= $event->bank_response_code;
		$paymenterror->payment_means		= $event->payment_means;
		$paymenterror->card_number			= $event->card_number;
		
		if (isset($event->transmission_date))
		{
			$paymenterror->transmission_date	= $event->transmission_date->getIsoDateTime();
		}
		
		
		$paymenterror->save();
		
		if (isset($Crm->ShopTrace))
		{
			$shopTraceSet = $Crm->ShopTraceSet();
			$shopTraceSet->cartLog($cart, 'Payment error created');
		}

	}
	
	


	/**
	 * Get the list of items to notify for each contact
	 * @return Array
	 */
	private function getCommentRequestContactItems()
	{

		$Crm = $this->Crm();
		
		if (!isset($Crm->OrderItem) || !isset($Crm->Comment))
		{
			return array();
		}

		$set = $Crm->OrderItemSet();
		$set->parentorder();
		$set->parentorder->contact();
		$set->catalogitem();
		$set->catalogitem->article();

		$date = BAB_DateTime::now();
		$date->less(60, BAB_DATETIME_DAY);

		$res = $set->select(
				$set->commentrequest->is(0)
				->_AND_($set->parentorder->createdOn->lessThan($date->getIsoDateTime()))
		);

		$comSet = $Crm->CommentSet();
		$contactItems = array();

		foreach($res as $orderItem)
		{
			if (!$orderItem->catalogitem->id)
			{
				// product not in catalog or deleted product 
				$orderItem->commentrequest = 1;
				$orderItem->save();
				continue;
			}
			
			
			
			
			if (!$orderItem->parentorder->contact || !$orderItem->parentorder->contact->id || !$orderItem->parentorder->contact->getMainEmail())
			{
				// no contact found on this order or the contact does not has an email address
				$orderItem->commentrequest = 1;
				$orderItem->save();
				continue;
			}



			$id_user = $orderItem->parentorder->contact->user;
			if (null !== $comSet->get($comSet->createdBy->is($id_user)->_AND_($comSet->article->is($orderItem->catalogitem->article->id))))
			{
				// a comment allready exist on this product, du not notify the customer for this comment
				$orderItem->commentrequest = 1;
				$orderItem->save();
				continue;
			}


			if (!isset($contactItems[$id_user]))
			{
				$contactItems[$id_user] = array(
					'contact' => $orderItem->parentorder->contact,
					'items' => array()
				);
			}

			$contactItems[$id_user]['items'][] = $orderItem;

		}

		return $contactItems;
	}




	/**
	 * Method called every days base on LibTimer event
	 *
	 */
	public function onDaily(LibTimer_eventDaily $event)
	{
		$Crm = $this->Crm();
		

		// comment request notifications

		if ($Crm->onlineShop)
		{
			if ($contactItems = $this->getCommentRequestContactItems())
			{
				foreach($contactItems as $arr)
				{
					if (!$arr['items'])
					{
						continue;
					}
					
					$notif = $Crm->Notify()->commentRequest_contact($arr['contact'], $arr['items']);
					
					$status = $notif->send();
					
					if (!isset($status)) {
					    // notification disabled or no recipient
					    continue;
					}
					
					if ($status) {
						
						// mark items as notified
						
						foreach($arr['items'] as $orderItem)
						{
							$orderItem->commentrequest = 1;
							$orderItem->save();
						}
						
						$message = $Crm->translate('A comment request notification has been sent successfully to %s');
						
					} else {
						$message = $Crm->translate('A comment request notification to %s has failed');
					}
	
					$event->log($Crm->getAddonName(), sprintf($message, $arr['contact']->getMainEmail()));
				}
			}
		}

		
		if (isset($Crm->MailBox))
		{
			$set = $Crm->MailBoxSet();
			$n = $set->importResponses();
			if ($n > 0)
			{
				$event->log($Crm->getAddonName(), sprintf('%d email responses imported', $n));
			}
		}

	}

}
