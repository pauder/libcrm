<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


bab_functionality::includefile('Ovml/Container');
bab_functionality::includefile('Ovml/Function');



/**
 * 
 * @param string $name
 * @return Func_Crm
 */
function crm_getOvmlCrm($name)
{
	$Crm = bab_functionality::get('Crm/'.$name);
	/**@var $Crm Func_Crm */
	
	if (false === $Crm)
	{
		return false;
	}
	
	if (!$Crm->isAddonsValid())
	{
		return false;
	}
	
	return $Crm;
}



/**
 * The crm attribute is a valid Crm name
 * 
 * <OCCrmShoppingCart crm="">
 * 		<OVNbItems>
 * 		<OVCartUrl>
 * </OCCrmShoppingCart>
 *
 */
class Func_Ovml_Container_CrmShoppingCart extends Func_Ovml_Container
{
	/**
	 * 
	 * @var Func_Crm
	 */
	private $Crm = false;
	
	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);
		
		$this->Crm = crm_getOvmlCrm($ctx->get_value('crm'));
		
		$this->ctx->curctx->push('CCount', 1);
		
	}
	
	
	/**
	 *
	 */
	public function getnext()
	{
		if (false === $this->Crm)
		{
			return false;
		}
		
		
	
		if($this->idx  == 0)
		{
			$set = $this->Crm->ShoppingCartSet();
			try {
				$shoppingCart = $set->getMyCart(false);
			} catch (ORM_BackEndSelectException $e)
			{
				bab_debug($e->getMessage());
				return false;
			}
			
			
			if (null === $shoppingCart)
			{
				$nbitems = 0;
				$this->ctx->curctx->push('NbItems', 0);
				
				
			} else {
				try {
					$nbitems = $shoppingCart->getCount();
				} catch(ORM_BackEndSelectException $e)
				{
					bab_debug($e->getMessage());
					$nbitems = 0;
				}
				$this->ctx->curctx->push('NbItems', $nbitems);
			}
			
			if (0 === $nbitems)
			{
				$description = $this->Crm->translate('Your shopping cart is empty');
			}
			else if (1 === (int) $nbitems)
			{
				$description = $this->Crm->translate('1 product');
			} else {
				$description = sprintf($this->Crm->translate('%d products'), $nbitems);
			}
			
			
			$this->ctx->curctx->push('Description', $description);
			
			$action = $this->Crm->Controller()->ShoppingCart()->edit();
			$this->ctx->curctx->push('CartUrl', $action->url());
			
			
			
			$this->idx++;
			$this->index = $this->idx;
			return true;
		}
		else
		{
			$this->idx = 0;
			return false;
		}
	}
}






/**
 * Container for shopping cart content
 * The crm attribute is a valid Crm name
 *
 * <OCCrmShoppingCartItems crm="">
 *      <OVShoppingCartItemId>
 *      <OVCatalogItemId>
 *      <OVCatalogItemUrl>
 *      <OVArticleReference>
 * 		<OVArticleName>
 *      <OVArticleDescription>
 * </OCCrmShoppingCartItems>
 *
 */
class Func_Ovml_Container_CrmShoppingCartItems extends Func_Ovml_Container
{
    /**
     *
     * @var Func_Crm
     */
    private $Crm = false;
    
    /**
     * @var crm_ShoppingCartItem[]
     */
    private $items;
    
    
    private $count;

    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);

        $this->Crm = crm_getOvmlCrm($ctx->get_value('crm'));
        $set = $this->Crm->ShoppingCartSet();
        $shoppingCart = $set->getMyCart(false);
        
        if (isset($shoppingCart)) {
            $this->items = $shoppingCart->getItems();
            $sciSet = $this->items->getSet();
            /*@var $sciSet crm_ShoppingCartItemSet */
            $this->items->orderAsc($sciSet->catalogitem->article->name);
        }
        
        if (!isset($this->items)) {
            $this->ctx->curctx->push('CCount', 0);
            return;
        }

        

        $this->items->rewind();
        $this->count = $this->items->count();
        $this->ctx->curctx->push('CCount', $this->count);
    }


    /**
     *
     */
    public function getnext()
    {
        if (false === $this->Crm || !isset($this->items)) {
            return false;
        }



        if ($this->items->valid() && $this->idx < $this->count) {
			$record = $this->items->current();
			/*@var $record crm_ShoppingCartItem */
				
			$this->ctx->curctx->push('ShoppingCartItemId', $record->id);
			$this->ctx->curctx->push('CatalogItemId', $record->id);
			$this->ctx->curctx->push('CatalogItemUrl', $record->catalogitem->getRewritenUrl());
			$this->ctx->curctx->push('ArticleReference', $record->catalogitem->article->reference);
			$this->ctx->curctx->push('ArticleName', $record->catalogitem->article->name);
			$this->ctx->curctx->push('ArticleDescription', $record->catalogitem->article->description);
			
			
			
			$this->idx++;
			$this->index = $this->idx;
			$this->items->next();
			return true;
			
		} else {
			$this->idx = 0;
			return false;
		}
    }
}






/**
 *
 * <OCCrmCatalog crm="" parent="0" column="1/2" sitemapnode="" width="" height="">
 *
 * </OCCrmCatalog>
 *
 */
class Func_Ovml_Container_CrmCatalog extends Func_Ovml_Container
{
	/**
	 *
	 * @var Func_Crm
	 */
	private $Crm = false;

	private $count;
	private $res;
	
	private $width;
	private $height;

	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);

		$this->Crm = crm_getOvmlCrm($ctx->get_value('crm'));
		
		if (!$this->Crm)
		{
			return;
		}
		
		$parent = $ctx->get_value('parent');
		$sitemapnode = $ctx->get_value('sitemapnode');
		$this->width = $ctx->get_value('width');
		$this->height = $ctx->get_value('height');
		
		$set = $this->Crm->CatalogSet();
		
		$criteria = $set->getAccessibleCatalogs();
		
		if (null !== $parent && false !== $parent)
		{
			$criteria = $criteria->_AND_($set->parentcatalog->is($parent));
		}
		
		if ($sitemapnode)
		{
			$sitemap = bab_sitemap::getFromSite();
			$node = $sitemap->getNodeById($sitemapnode);
			$sitemapItem = $node->getData();
			/*@var $sitemapItem bab_sitemapItem */
			$target = $sitemapItem->getTarget();
			
			
		
			$prefix = $this->Crm->classPrefix.'Catalog';
			
			if (0 === mb_strpos($target->id_function, $prefix))
			{
				$id = (int) mb_substr($target->id_function, mb_strlen($prefix));
				$criteria = $criteria->_AND_($set->id->is($id));
			} else {
				// ne corespond pas a un noeud de type catalog fourni par le crm
				$this->Crm = false;
				return;
			}
		}
		
		
		$this->res = $set->select($criteria);
		$this->count = $this->res->count();
		

		$this->idx = 0;
		
		$this->res->rewind();
		
		if ($column = $ctx->get_value('column'))
		{
			list($col, $nb_cols) = explode('/', $column);
			$all_cols = array();
			$sum = 0;
			for($i = 0; $i < $nb_cols; $i++)
			{
				if (($i+1) === $nb_cols)
				{
					$all_cols[$i] = ($this->count - $sum);
				} else {
					$all_cols[$i] = (int) round($this->count / $nb_cols);
				}
				
				if (($col - 1) === $i)
				{
					$this->res->seek($sum);
				}
				
				$sum += $all_cols[$i];
				
				
			}
			
			$this->count = $all_cols[($col - 1)];
			
			
			
		} else {
			$this->res->rewind();
		}
		
		
		$this->ctx->curctx->push('CCount', $this->count);
		
	}


	/**
	 *
	 */
	public function getnext()
	{
		if (false === $this->Crm)
		{
			return false;
		}

		if($this->res->valid() && $this->idx < $this->count)
		{
			$record = $this->res->current();
			/*@var $record crm_Catalog */
				
			$this->ctx->curctx->push('CatalogName', $record->name);
			$this->ctx->curctx->push('CatalogDescription', $record->description);
			$this->ctx->curctx->push('CatalogId', $record->id);
			$this->ctx->curctx->push('SitemapNode', $this->Crm->classPrefix.'Catalog'.$record->id);
			$this->ctx->curctx->push('CatalogUrl', $record->getRewritenUrl());
			
			if ($this->width && $this->height && $photopath = $record->getPhotoPath())
			{
				$T = bab_functionality::get('Thumbnailer');
				/* @var $T Func_Thumbnailer */
				$T->setSourceFile($photopath);
				$this->ctx->curctx->push('ImageUrl', $T->getThumbnail($this->width, $this->height));
			} else {
				$this->ctx->curctx->push('ImageUrl', null);
			}
			
			$this->idx++;
			$this->index = $this->idx;
			$this->res->next();
			return true;
		}
		else
		{
			$this->idx = 0;
			return false;
		}
	}
}




/**
 * 
 * 
 * crm :
 *  Nom de l'objet CRM a utiliser
 * 
 * type :
 *  unavailable : tous les produits y compris ceux qui ne sont pas accessibles dans la boutique
 *  all 		: tout les produits le plus recent en premier
 *  homepage 	: produits selectionnes pour la page d'acceuil le plus recent en premier
 *  reduction 	: produits beneficiant d'un pourcentage de reduction le plus recent en premier
 *  bestsales	: tout les produits, dans l'ordre du plus vendu au moins vendu
 *  linked		: produits attaches, dans l'ordre alphabetique
 * 
 * articletype :
 * 	un ou plusieurs nom de types de produit separes par des virgules
 * 
 * limit :
 *  combien de produit il faut afficher, n'est pas actif si random=1
 *  
 * random :
 *  reduit la liste a un produit selectione alleatoirement parmis les resultat
 *  
 * linkedto :
 *  article id, only for type=linked
 *  
 * linktype :
 *  I : Up-sell				//ventes incitatives
	C : Cross-sell			//ventes croisees
	A : Related product		//apparentes
 * 
 * 
 * <OCCrmArticle crm="" type="all|homepage|reduction|bestsales|linked" articletype="" limit="" random="1|0" template="buyframe|shoplistframe|shopcardframe">
 * 
 * </OCCrmArticle>
 *
 */
class Func_Ovml_Container_CrmArticle extends Func_Ovml_Container
{
	/**
	 * 
	 * @var Func_Crm
	 */
	private $Crm = false;
	
	private $articletype;
	private $type;
	private $limit;
	private $count;
	private $template;
	
	private $iterator;
	
	private $linkedto;
	private $linktype;
	
	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);
	
		$this->Crm = crm_getOvmlCrm($ctx->get_value('crm'));
		
		if (false === $this->Crm)
		{
			return;
		}
		
		$this->ctx->curctx->push('CCount', 1);
		
		$this->type = $ctx->get_value('type');
		if (empty($this->type))
		{
			$this->type = 'all';
		}
		
		if ($articletype = $ctx->get_value('articletype'))
		{
			$this->articletype = preg_split('/\s*,\s*/', $articletype);
		}
		
		$this->linkedto = (int) $ctx->get_value('linkedto');
		if (empty($this->linkedto))
		{
			$this->linkedto = null;
		}
		
		$this->linktype = $ctx->get_value('linktype');
		if (empty($this->linktype))
		{
			$this->linktype = null;
		}
		
		
		$I = $this->getIterator();
		$this->limit = (int) $ctx->get_value('limit');
		
		if ($I->count() < $this->limit)
		{
			$this->limit = $I->count();
		} else if (0 === $this->limit)
		{
			$this->limit = $I->count();
		}
		
		$random = $ctx->get_value('random');
		if (empty($random))
		{
			$random = 0;
		}
		
		if ($random)
		{
			$I->seek(rand(0, $this->limit -1));
			$this->count = 1;
		} else {
			$I->seek(0);
			$this->count = $this->limit;
		}
		
		$this->template = $ctx->get_value('template');
		if (empty($this->template))
		{
			$this->template = 'shopcardframe';
		}
		

	}
	
	
	
	private function getCatalogItemSet()
	{
		$this->Crm->includeArticleSet();
		$set = $this->Crm->CatalogItemSet();
		$set->article();
		$set->catalog();
		$set->article->joinHasOneRelations();
		

		return $set;
	}
	
	
	protected function getTypeCriteria($set, $type)
	{
		switch($type)
		{
			default:
				trigger_error(sprintf('wrong parameter type="%s" in ovml container OCCrmArticle in %s', $this->type, (string) $this->ctx->debug_location));
				
			case 'unavailable':
			    $criteria = new ORM_TrueCriterion();
			    break;
		
			case 'all':
			case 'bestsales':
				$criteria = $set->article->disabled->is(0)
				->_AND_($set->catalog->getAccessibleCatalogs());
				break;
		
			case 'homepage':
				$criteria = $set->article->disabled->is(0)
				->_AND_($set->catalog->getAccessibleCatalogs())
				->_AND_($set->article->homepage->is(1));
				break;
		
			case 'reduction':
				$subset = $this->Crm->ArticlePackagingSet();
				$subcriteria = $subset->reduction->greaterThan(0)->_AND_(
						$subset->reduction_end->greaterThan(date('Y-m-d'))
						->_OR_($subset->reduction_end->is('0000-00-00'))
				);
				$criteria =
				$set->article->disabled->is(0)
				->_AND_($set->catalog->getAccessibleCatalogs())
				->_AND_($set->article->id->in($subcriteria));
				break;
				
			case 'linked':
				$articleLinkSet = $this->Crm->ArticleLinkSet();
				$subcriteria = $articleLinkSet->article_from->is($this->linkedto)->_AND_($articleLinkSet->type->is($this->linktype));
				$criteria = $set->article->disabled->is(0)
				->_AND_($set->catalog->getAccessibleCatalogs())
				->_AND_($set->article->id->in($subcriteria, 'article_to'));
				break;
		}
		
		
		
		
		return $criteria;
	}
	
	
	
	protected function setTypeOrder($res, $type)
	{
		$set = $res->getSet();
		
		switch($this->type)
		{
		
			case 'all':
			case 'homepage':
			case 'reduction':
		
				$res->groupBy($set->article->id);
				$res->orderDesc($set->article->createdOn);
				break;
		
		
			case 'bestsales':
				$res->groupBy($set->article->id);
				$res->orderAsc($set->article->sales_count);
				break;
		
			case 'linked':
				$res->groupBy($set->article->id);
				$res->orderAsc($set->article->name);
				break;
		
		}
		
		
	}
	
	
	
	private function getIterator()
	{
		if (null === $this->iterator)
		{
			
			$set = $this->getCatalogItemSet();
			
			
			$criteria = $this->getTypeCriteria($set, $this->type);
			
			if (null !== $this->articletype)
			{
				$criteria = $criteria->_AND_($set->article->articletype->name->in($this->articletype));
			}
			
			$res = $set->select($criteria);
			
			$this->setTypeOrder($res, $this->type);
			
			$this->iterator = $res;
		}
		
		return $this->iterator;
	}
	
	
	
	
	
	
	

	public function getnext()
	{
		if (false === $this->Crm)
		{
			return false;
		}
		
		$res = $this->getIterator();

		if($res->valid() && $this->idx < $this->count)
		{
			$catalogItem = $res->current();
				
			/*@var $catalogItem crm_CatalogItem */
			
			$W = bab_Widgets();
			
			$display = $this->Crm->Ui()->CatalogItemDisplay($catalogItem);
			switch($this->template)
			{
				case 'buyframe':
					$frame = $display->getBuyFrame();
					break;
					
				case 'shoplistframe':
					$frame = $display->getShopListFrame();
					break;
				
				case 'shopcardframe':
					$frame = $display->getShopCardFrame();
					break;
			}
			
			$html = $frame->display($W->HtmlCanvas());
			
			$this->ctx->curctx->push('ArticleDisplay', $html);
			$this->ctx->curctx->push('ArticleReference', $catalogItem->article->reference);
			$this->ctx->curctx->push('ArticleName', $catalogItem->article->name);
			$this->ctx->curctx->push('ArticleSubtitle', $catalogItem->article->subtitle);
			$this->ctx->curctx->push('ArticleDescription', $catalogItem->article->description);
			$this->ctx->curctx->push('ArticleUrl', $catalogItem->getRewritenUrl());
			
			$photoPath = $catalogItem->article->getPhotoPath();
			if (isset($photoPath)) {
			    $this->ctx->curctx->push('ArticlePhotoPath', $photoPath->tostring());
			} else {
			    $this->ctx->curctx->push('ArticlePhotoPath', null);
			}
			
			$this->idx++;
			$this->index = $this->idx;
			$res->next();
			return true;
		}
		else
		{
			$this->idx = 0;
			return false;
		}
	}
}





/**
 * Get files in the attachements subfolder
 * 
 * 
 * <OCCrmArticleAttachments crm="" reference="reference">
 *     <OVFilePath>
 *     <OVFileUrl>
 *     <OVFileName>
 * </OCCrmArticleAttachments>
 * 
 */
class Func_Ovml_Container_CrmArticleAttachments extends Func_Ovml_Container
{
    /**
     *
     * @var Func_Crm
     */
    private $Crm = false;
    
    /**
     * @var Widget_FilePickerIterator
     */
    private $files;

    
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);
    
        $this->Crm = crm_getOvmlCrm($ctx->get_value('crm'));
    
        if (false === $this->Crm)
        {
            return;
        }
        
        $reference = $ctx->get_value('reference');
        
        $set = $this->Crm->ArticleSet();
        $article = $set->get($set->reference->is($reference));
        
        if (!isset($article)) {
            trigger_error(sprintf('OCCrmArticleAttachments : Article not found from reference %s %s', $reference, (string) $this->gctx->debug_location));
            return;
        }
        
        $this->catalogItem = $article->getCatalogItem();
        
        if (!isset($this->catalogItem)) {
            trigger_error(sprintf('OCCrmArticleAttachements : The article found from reference %s has no accessible category %s', $reference, (string) $this->gctx->debug_location));
            return;
        }
        
        
        $this->files = $article->attachments(new bab_Path('attachments'));
        
        if (!$this->files) {
            return;
        }
    
        $this->ctx->curctx->push('CCount', 1);
    
        
        $this->files->rewind();
        
    }
    
    
    /**
     *
     */
    public function getnext()
    {
        if (false === $this->Crm || !$this->files)
        {
            return false;
        }
        
        if($this->files->valid())
        {
            $file = $this->files->current();
            
            /*@var $file Widget_FilePickerItem */
            
            
            $action = $this->Crm->Controller()->CatalogItem()->downloadAttachment($this->catalogItem->id, $file->getFileName());
            
            $this->ctx->curctx->push('FilePath', $file->getFilePath());
            $this->ctx->curctx->push('FileUrl', $action->url());
            $this->ctx->curctx->push('FileName', $file->toString());
            
            
            
            $this->idx++;
            $this->index = $this->idx;
            $this->files->next();
            return true;
        }
        
        $this->idx = 0;
        $this->files->rewind();
        return false;
    }
}





/**
 *
 * <OCCrmIfShopManager crm="">
 *
 * </OCCrmIfShopManager>
 *
 */
class Func_Ovml_Container_CrmIfShopManager extends Func_Ovml_Container
{
	/**
	 *
	 * @var Func_Crm
	 */
	private $Crm = false;


	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);

		$Crm = crm_getOvmlCrm($ctx->get_value('crm'));
		$this->idx = 0;
		
		if ($Crm)
		{
			$manager = $Crm->Access()->viewShopAdmin();
			
			if ($manager)
			{
				$this->ctx->curctx->push('CCount', 1);
				$this->idx = 1;
			} else {
				$this->ctx->curctx->push('CCount', 0);
			}
		}
	}


	/**
	 *
	 */
	public function getnext()
	{
		if (0 < $this->idx)
		{
			$this->idx--;
			return true;
		}
		
		return false;
	}
}











/**
 *
 * <OCCrmIfContactLogged crm="">
 *
 * 	<OVTitle>
 *  <OVFullTitle>
 *  <OVGender>
 *  <OVLastname>
 *  <OVFirstname>
 *  
 * </OCCrmIfContactLogged>
 *
 */
class Func_Ovml_Container_CrmIfContactLogged extends Func_Ovml_Container
{
	/**
	 *
	 * @var Func_Crm
	 */
	private $Crm = false;


	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);

		$this->Crm = crm_getOvmlCrm($ctx->get_value('crm'));
		$this->idx = 0;

		if ($this->Crm)
		{
			if ($this->Crm->isContactLogged())
			{
				$this->ctx->curctx->push('CCount', 1);
				$this->idx = 1;
			} else {
				$this->ctx->curctx->push('CCount', 0);
			}
		}
	}


	/**
	 *
	 */
	public function getnext()
	{
		if (0 < $this->idx)
		{
			$contact = $this->Crm->getMyContact();
			
			$this->ctx->curctx->push('Title', $contact->getParentSet()->title->output($contact->title));
			$this->ctx->curctx->push('FullTitle', $contact->getFullTitle());
			$this->ctx->curctx->push('Gender', $contact->getParentSet()->gender->output($contact->gender));
			$this->ctx->curctx->push('Lastname', $contact->lastname);
			$this->ctx->curctx->push('Firstname', $contact->firstname);
			
			$this->idx--;
			return true;
		}

		return false;
	}
}











/**
 * Test if the current page is a page from online shop (product, category, shopping cart, myContact pages)
 *
 * <OCCrmIfShopPage crm="">
 *
 * </OCCrmIfShopPage>
 *
 */
class Func_Ovml_Container_CrmIfShopPage extends Func_Ovml_Container
{

	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);

		$Crm = @crm_getOvmlCrm($ctx->get_value('crm'));
		$this->idx = 0;

		if ($Crm)
		{
			if ($sitemapItem = bab_siteMap::getRealPosition())
			{
				$len = strlen($Crm->classPrefix);
				if (substr($sitemapItem->id_function, 0, $len) === $Crm->classPrefix)
				{
					$this->ctx->curctx->push('CCount', 1);
					$this->idx = 1;
					return;
				}
			}
		}
		
		$this->ctx->curctx->push('CCount', 0);
	}


	/**
	 *
	 */
	public function getnext()
	{
		if (0 < $this->idx)
		{
			$this->idx--;
			return true;
		}

		return false;
	}
}















/**
 * Set page into breadcumbs
 * <OFCrmBreadCrumbs crm="" title="page title">
 *
 */
class Func_Ovml_Function_CrmBreadCrumbs extends Func_Ovml_Function
{
	
	/**
	 * @return string
	 */
	public function toString()
	{
		$args = $this->args;
		$Crm = null;
		$title = null;
	
		if(count($args))
		{
			foreach( $args as $p => $v)
			{
				switch(mb_strtolower(trim($p)))
				{
					case 'crm':
						$Crm = crm_getOvmlCrm($v);
						if (false === $Crm)
						{
							return 'wrong crm parameter';
						}
						unset($args[$p]);
						break;
						
						
					case 'title':
						$title = $v;
						unset($args[$p]);
						break;
				}
			}
		}
		
		if (null === $Crm)
		{
			return 'missing crm parameter';
		}
		
		if (null === $title)
		{
			return 'missing title parameter';
		}
		
		// create action from current URL
		
		$W = bab_Widgets();
		$action = $W->Action();
		$action->fromRequest();
		
		crm_BreadCrumbs::addPage($action);
		crm_BreadCrumbs::setCurrentPosition($action, $title);
		
		return '';
	}
}