<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



class crm_ShoppingCartNotify extends crm_NotifyMessage
{
	
	/**
	 * 
	 * @var crm_ShoppingCart
	 */
	protected $shoppingCart;
	
	public function __construct(Func_Crm $Crm, crm_shoppingCart $cart)
	{
		$this->shoppingCart = $cart;
		parent::__construct($Crm);
	}
	
	
	protected function linkRecords(crm_Email $email)
	{
		parent::linkRecords($email);
		
		$email->linkTo($this->shoppingCart, 'referencedBy');
		if ($order = $this->shoppingCart->getOrder())
		{
			$email->linkTo($order, 'referencedBy');
		}
	}
}



class crm_shoppingCart_creditCardResponseCustomer extends crm_ShoppingCartNotify
{
	public function __construct(Func_Crm $Crm, crm_shoppingCart $cart)
	{
		parent::__construct($Crm, $cart);
		global $babUrl;
		
		$order = $cart->getOrder();

		$this->addContactRecipient($order->contact);
		$sitename = $_SERVER['HTTP_HOST'];
		$this->setSubject(sprintf($Crm->translate('Your order on site %s'), $sitename));
		
		$order_number 	= $order->name;
		
		$line1 = sprintf($Crm->translate('Your order has been registered as number %s and vour payment confirmed.'), $order_number);
		$line2 = $Crm->translate('A notification will be sent back to you when the order is beeing processed.');
		$link = sprintf('<a href="%s">%s</a>', bab_toHtml($babUrl.$Crm->Controller()->MyContact()->orderList()->url()), $Crm->translate('your personal space'));
		$line3 = sprintf($Crm->translate('You can connect yourself on the website to follow the list of ongoing orders in %s'), $link);
		
		
		$body = bab_toHtml($line1, BAB_HTML_ALL);
		
		if (true !== $cart->paymentorder->isGiftCardOnly())
		{
			$body .= bab_toHtml($line2, BAB_HTML_ALL);
			$body .= '<p>'.$line3.'</p>';
		}
		
		
		if ($giftcards = $this->getGiftCards($order))
		{
			$body .= $giftcards;
		}
		
		
		$this->setBody($body);
	}
	
	
	
	/**
	 * Get the list of gift card in the order
	 * @return string HTML
	 */
	protected function getGiftCards(crm_Order $order)
	{
		$Crm = $this->Crm();
		$cSet = $Crm->CouponSet();
		$cSet->orderitem();
		
		$res = $cSet->select($cSet->orderitem->parentorder->is($order->id));
		
		if (0 === $res->count())
		{
			return null;
		}
		
		$ul = '<ul>';
		foreach($res as $coupon)
		{
			$value = $Crm->numberFormat($coupon->value).bab_nbsp().$Crm->Ui()->Euro();
			$ul .= sprintf('<li><strong>%s</strong> %s</li>', bab_toHtml($coupon->code), bab_toHtml(sprintf($Crm->translate('worth %s'), $value)));
		}
		$ul .= '</ul>';
		
		if (1 === $res->count())
		{
			$line1 = $Crm->translate('Below is the coupon code purchased when ordering:');
		} else {
			$line1 = $Crm->translate('Below is the list of coupon codes purchased when ordering:');
		}
		
		
		
		return bab_toHtml($line1, BAB_HTML_ALL).$ul;
	}
	
	
	protected function attachFiles(crm_Email $email)
	{
		
		$tempPath = $this->shoppingCart->getOrder()->exportPdf();
		$email->attachFile($tempPath);
		
	}

}






class crm_shoppingCart_creditCardResponseManager extends crm_ShoppingCartNotify
{
	public function __construct(Func_Crm $Crm, crm_shoppingCart $cart)
	{
		parent::__construct($Crm, $cart);
		global $babUrl;

		$managers = $Crm->Access()->getOnlineShopManagers();
		
		foreach($managers as $id_user)
		{
			$this->addUserRecipient($id_user);
		}
		
		$sitename = $_SERVER['HTTP_HOST'];
		$this->setSubject(sprintf($Crm->translate('New order received on %s'), $sitename));

		$order_number 	= $cart->getOrder()->name;

		$line1 = sprintf($Crm->translate('A new order has been registered as number %s and the payment received.'), $order_number);
		$line2 = $Crm->translate('The shopping cart is waiting to be processed.');
		$link = sprintf('<a href="%s">%s</a>', bab_toHtml($babUrl.$Crm->Controller()->ShoppingCartAdm()->display($cart->id)->url()), $Crm->translate('View shopping cart'));

		$body = bab_toHtml($line1, BAB_HTML_ALL);
		$body .= bab_toHtml($line2, BAB_HTML_ALL);
		$body .= '<p>'.$link.'</p>';

		$this->setBody($body);
	}

}





class crm_shoppingCart_WishList extends crm_ShoppingCartNotify
{
	public function __construct(Func_Crm $Crm, crm_shoppingCart $cart, $recipients, $message)
	{
		parent::__construct($Crm, $cart);
		global $babUrl;
		
		$arr = preg_split('/[\R\s,]+/m', $recipients);
		foreach($arr as $email)
		{
			$email = trim($email, '<> ');
			if (preg_match("/^[a-zA-Z0-9+_\.-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$/", $email))
			{
				$this->addRecipient($email);
			}
		}
	
		$sitename = $_SERVER['HTTP_HOST'];
		$this->setSubject(sprintf('%s : %s', $sitename, $cart->name));
	
		$link = sprintf('<a href="%s">%s</a>', bab_toHtml($babUrl.$Crm->Controller()->ShoppingCart()->display($cart->uuid)->url()), $Crm->translate('View the wishlist'));
	
		$body = bab_toHtml($message, BAB_HTML_ALL);
		$body .= '<p>'.$link.'</p>';
	
		$this->setBody($body);
	}
}







class crm_shoppingCartAdm_setDeliveryStatus extends crm_ShoppingCartNotify
{
	public function __construct(Func_Crm $Crm, crm_shoppingCart $cart)
	{
		parent::__construct($Crm, $cart);
		global $babUrl;

		$this->addContactRecipient($cart->contact);
		$sitename = $_SERVER['HTTP_HOST'];
		$this->setSubject(sprintf($Crm->translate('Your order on site %s'), $sitename));

		$order_number 	= $cart->getOrder()->name;

		$line1 = sprintf($Crm->translate('Your order number %s is now beeing processed.'), $order_number);
		$line2 = $Crm->translate('A notification will be sent back to you when the package is ready for shippment.');
		$link = sprintf('<a href="%s">%s</a>', bab_toHtml($babUrl.$Crm->Controller()->MyContact()->orderList()->url()), $Crm->translate('your personal space'));
		$line3 = sprintf($Crm->translate('You can connect yourself on the website to follow the list of ongoing orders in %s'), $link);


		$body = bab_toHtml($line1, BAB_HTML_ALL);
		$body .= bab_toHtml($line2, BAB_HTML_ALL);
		$body .= '<p>'.$line3.'</p>';

		$this->setBody($body);
	}
}





class crm_shoppingCartAdm_setShippedStatus extends crm_ShoppingCartNotify
{
	public function __construct(Func_Crm $Crm, crm_shoppingCart $cart)
	{
		parent::__construct($Crm, $cart);
		global $babUrl;

		$this->addContactRecipient($cart->contact);
		$sitename = $_SERVER['HTTP_HOST'];
		$this->setSubject(sprintf($Crm->translate('Your order on site %s'), $sitename));

		$order_number = $cart->getOrder()->name;

		$line1 = sprintf($Crm->translate('Your order number %s is now shipped.'), $order_number);
		$link = sprintf('<a href="%s">%s</a>', bab_toHtml($babUrl.$Crm->Controller()->MyContact()->orderList()->url()), $Crm->translate('your personal space'));
		$line2 = sprintf($Crm->translate('You can connect yourself on the website to follow the list of ongoing orders in %s'), $link);


		$body = bab_toHtml($line1, BAB_HTML_ALL);
		$body .= '<p>'.$line2.'</p>';
		$body .= bab_toHtml($cart->shipped_comment, BAB_HTML_ALL);

		$this->setBody($body);
	}

}