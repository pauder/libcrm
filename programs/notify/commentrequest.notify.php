<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


class crm_CommentRequestNotify_Contact extends crm_NotifyMessage
{
	/**
	 * @var array
	 */
	protected $orderItems;
	
	/**
	 * 
	 * @var string
	 */
	protected $html_products;

	public function __construct(Func_Crm $Crm, crm_Contact $contact, Array $items)
	{
		parent::__construct($Crm);
		global $babUrl;
		
		$this->orderItems = $items;
		$this->addContactRecipient($contact);
		$this->setSubject(sprintf($Crm->translate('Your opinion matters'), $_SERVER['HTTP_HOST']));
		
		$body = bab_toHtml($Crm->translate('Following your last order, you can leave an opinion on the products:'));
		$body .= "<br />";
		
		$this->html_products = '<ul>';
		
		foreach($items as $orderItem)
		{
			$this->html_products.= sprintf('<li><a href="%s">%s</a></li>', bab_toHtml($orderItem->catalogitem->getRewritenUrl()), bab_toHtml($orderItem->catalogitem->article->name));
		}
		$this->html_products .= '</ul>';
		
		$this->setBody($body. $this->html_products);
	}
	
	
	
	protected function linkRecords(crm_Email $email)
	{
		parent::linkRecords($email);
		
		$linkedOrders = array();
	
		foreach($this->orderItems as $orderItem)
		{
			if (!isset($linkedOrders[$orderItem->parentorder->id]))
			{
				$email->linkTo($orderItem->parentorder, 'referencedBy');
				$linkedOrders[$orderItem->parentorder->id] = 1;
			}
		}
	}
	
}