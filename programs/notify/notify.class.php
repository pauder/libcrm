<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * The crm_Notify class
 */
class crm_Notify extends crm_Object
{
	/**
	 * Includes crm Ui helper functions definitions.
	 */
	public function includeBase()
	{
		require_once FUNC_CRM_NOTIFY_PATH . 'base.notify.php';
	}
	
	/**
	 * include notifications from the shopping cart controller
	 */
	public function includeShoppingCart()
	{
		require_once FUNC_CRM_NOTIFY_PATH . 'shoppingcart.notify.php';
	}

	
	/**
	 * include notifications from the my contact controller
	 */
	public function includeMyContact()
	{
		require_once FUNC_CRM_NOTIFY_PATH . 'mycontact.notify.php';
	}
	
	/**
	 * include notifications from the lost password controller
	 */
	public function includeLostPassword()
	{
		require_once FUNC_CRM_NOTIFY_PATH . 'lostpassword.notify.php';
	}
	
	/**
	 * include notifications for the comments requests
	 */
	public function includeCommentRequest()
	{
		require_once FUNC_CRM_NOTIFY_PATH . 'commentrequest.notify.php';
	}
	
	
	/**
	 * Notify the customer about his payment
	 * 
	 * @param crm_shoppingCart $cart
	 * @return crm_shoppingCart_creditCardResponseCustomer
	 */
	public function shoppingCart_creditCardResponseCustomer(crm_shoppingCart $cart)
	{
		$this->includeShoppingCart();
		return new crm_shoppingCart_creditCardResponseCustomer($this->Crm(), $cart);
	}
	
	
	
	
	/**
	 * Notify the managers about the new received payement
	 * 
	 * @param crm_shoppingCart $cart
	 * @return crm_shoppingCart_creditCardResponseManager
	 */
	public function shoppingCart_creditCardResponseManager(crm_shoppingCart $cart)
	{
		$this->includeShoppingCart();
		return new crm_shoppingCart_creditCardResponseManager($this->Crm(), $cart);
	}
	
	
	/**
	 * 
	 * @param crm_shoppingCart $cart
	 * @param string $recipients
	 */
	public function shoppingCart_WishList(crm_shoppingCart $cart, $recipients, $message)
	{
		$this->includeShoppingCart();
		return new crm_shoppingCart_WishList($this->Crm(), $cart, $recipients, $message);
	}
	
	
	
	/**
	 * Notify the customer about his order beeing processed
	 * @see crm_ShoppingCart::DELIVERY
	 * @param crm_shoppingCart $cart
	 * @return crm_shoppingCartAdm_setDeliveryStatus
	 */
	public function shoppingCartAdm_setDeliveryStatus(crm_shoppingCart $cart)
	{
		$this->includeShoppingCart();
		return new crm_shoppingCartAdm_setDeliveryStatus($this->Crm(), $cart);
	}
	
	
	/**
	 * Notify the customer about his package beeing shipped (or ready for shippment)
	 * @see crm_ShoppingCart::SHIPPED
	 * @param crm_shoppingCart $cart
	 * @return crm_shoppingCartAdm_setDeliveryStatus
	 */
	public function shoppingCartAdm_setShippedStatus(crm_shoppingCart $cart)
	{
		$this->includeShoppingCart();
		return new crm_shoppingCartAdm_setShippedStatus($this->Crm(), $cart);
	}
	
	/**
	 * 
	 * @param crm_Contact $contact
	 * @param string $password
	 * @return crm_MyContact_createAccount
	 */
	public function myContact_createAccount(crm_Contact $contact, $password)
	{
		$this->includeMyContact();
		return new crm_MyContact_createAccount($this->Crm(), $contact, $password);
	}
	
	
	/**
	 * 
	 * @param crm_Contact $contact
	 * @param string $password
	 * @return crm_MyContact_newPassword
	 */
	public function myContact_newPassword(crm_Contact $contact, $password)
	{
		$this->includeMyContact();
		return new crm_MyContact_newPassword($this->Crm(), $contact, $password);
	}
	
	
	/**
	 * 
	 * @param crm_Contact $contact
	 */
	public function lostPassword_sendToken(crm_Contact $contact)
	{
		$this->includeLostPassword();
		return new crm_lostPassword_sendToken($this->Crm(), $contact);
	}
	
	
	/**
	 * Notify a contact about the items he can comment on the online shop
	 * @param crm_Contact	$contact	the customer
	 * @param Array			$items		list of items purshased by the customer not notified and not commented
	 * @return crm_CommentRequestNotify_Contact
	 */
	public function commentRequest_contact(crm_Contact $contact, Array $items)
	{
		$this->includeCommentRequest();
		return new crm_CommentRequestNotify_Contact($this->Crm(), $contact, $items);
	}
}