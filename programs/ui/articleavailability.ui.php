<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of articleavailability of articles from back office
 *
 */
class crm_ArticleAvailabilityTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();
		$Ui = $Crm->Ui();

		/*@var $Ui crm_Ui */

		$editAction = $Crm->Controller()->ArticleAvailability()->edit($record->id);

		switch ($fieldPath) {

			case '_edit_':
				return $W->Link($W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $editAction);
				break;
				
			case 'can_be_purchased':
				if ($record->can_be_purchased)
				{
					return $W->Label($Crm->translate('Yes'));
				} else {
					return $W->Label($Crm->translate('No'));
				}
				break;
				
			case 'name':
				$label = $W->Label(bab_nbsp())->addClass('crm-circle');
				$label->setCanvasOptions($label->Options()->backgroundColor('#'.$record->color));
				return $W->HBoxItems($label, parent::computeCellContent($record, $fieldPath))->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle');
				break;
		}
		
		

		return parent::computeCellContent($record, $fieldPath);
	}







}







/**
 * 
 */
class crm_ArticleAvailabilityEditor extends crm_Editor
{
	/**
	 * 
	 * @var crm_ArticleAvailability
	 */
	protected $articleavailability = null;
	
	
	public function __construct(Func_Crm $Crm, crm_ArticleAvailability $articleavailability = null, $id = null, Widget_Layout $layout = null)
	{
		$this->articleavailability = $articleavailability;
		
		parent::__construct($Crm, $id, $layout);
		$this->setName('articleavailability');
		
		$this->addFields();
		$this->addButtons();
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		if (isset($articleavailability)) {
			$this->setHiddenValue('articleavailability[id]', $articleavailability->id);
			$this->setValues($articleavailability->getValues(), array('articleavailability'));
		}
	}
	
	
	protected function addFields()
	{
		$this->addItem($this->name());
		$this->addItem($this->description());
		$this->addItem($this->can_be_purchased());
		$this->addItem($this->color());
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$id = null !== $this->articleavailability ? $this->articleavailability->id : null;
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->ArticleAvailability()->save())
				->setSuccessAction(crm_BreadCrumbs::getPosition(-1))
				->setFailedAction($Crm->Controller()->ArticleAvailability()->edit($id))
		);
		
		
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction(crm_BreadCrumbs::getPosition(-1))
		);
	}
	
	
	protected function name()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Name'),
				$W->LineEdit()->setSize(70)->setMaxSize(100)->setMandatory(true, $Crm->translate('The name is mandatory')),
				__FUNCTION__
		);
	}
	
	
	protected function description()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Description'),
				$W->TextEdit(),
				__FUNCTION__
		);
	}
	
	
	protected function can_be_purchased()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		return $this->labelledField(
				$Crm->translate('Products with this status can be purchased'),
				$W->Checkbox(),
				__FUNCTION__
		);
	}
	
	
	protected function color()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Color'),
				$W->ColorPicker(),
				__FUNCTION__
		);
	}
	
}



