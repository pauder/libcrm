<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');





/**
 * Editor used to create or modifiy a comment
 * a comment can be created associated to an article
 * or modified by online shop manager
 * @return crm_Editor
 */
class crm_CommentEditor extends crm_Editor
{
	const ACCESS_PUBLIC		= 1;
	const ACCESS_PRIVATE	= 2;
	
	/**
	 * 
	 * @var crm_Comment
	 */
	protected $comment = null;
	
	
	/**
	 * @var crm_Record
	 */
	protected $targetRecord = null;
	
	/**
	 * 
	 * @var int
	 */
	protected $access_type = null;
	


	public function __construct($crm, $access_type, crm_Record $targetRecord, crm_Comment $comment = null, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($crm, $id, $layout);

		$this->access_type = $access_type;
		$this->setComment($comment);
		$this->targetRecord = $targetRecord;
		
		if (null === $comment)
		{
			$this->setHiddenValue('comment[for]', $targetRecord->getRef());
		}
		
		$W = $this->widgets;

		$this->colon();
		$this->setName('comment');
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		$this->addFields();
		$this->addButtons();
	}
	
	

	
	

	/**
	 * Add fields into form
	 * @return crm_CommentEditor
	 */
	public function addFields()
	{
		$W = $this->widgets;
		$Access = $this->Crm()->Access();

		$this->addItem($this->rating());
		$this->addItem($this->Summary());
		
		if ($Access->setPrivateCommentOn($this->targetRecord))
		{
			$this->addItem($this->privateComment());
		}
		return $this;
	}
	
	
	protected function addButtons()
	{
		$W = $this->widgets;
		$controller = $this->Crm()->Controller();
		
		$button = $W->SubmitButton()
					->setLabel($this->Crm()->translate('Save the comment'))
					->setAction($controller->Comment()->save());
		
		switch($this->access_type)
		{
			case self::ACCESS_PRIVATE:	
				// $button->setSuccessAction(crm_BreadCrumbs::getPosition(-2));
				// $button->setFailedAction(crm_BreadCrumbs::getPosition(-1));
				break;
				
			case self::ACCESS_PUBLIC:
				 
				break;
		
		}
		
		$this->addItem($button);
	}
	

	
	
	protected function setComment(crm_Comment $comment = null)
	{
		if (isset($comment)) {
			$this->comment = $comment;
			$commentValues = $comment->getValues();
			$this->setValues(array('comment' => $commentValues));
	
			if (!empty($comment->id)) {
				$this->setHiddenValue('comment[id]', $comment->id);
			}
		}
	
	}
	
	
	
	protected function Summary()
	{
		$summaryFormItem = $this->widgets->TextEdit()
			->setLines(6)
			->setName('summary')
			->setColumns(70)
			->addClass('widget-fullwidth');
		$summaryFormItem->setMandatory(true, $this->Crm()->translate('The comment must not be empty.'));
	
		return $summaryFormItem;
	}
	
	
	
	protected function privateComment()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		
		return $this->labelledField(
				$Crm->translate('This comment is private'),
				$W->Checkbox(),
				'private'
		);
	}


	protected function rating()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();

		return $this->labelledField(
				$Crm->translate('Your rating'),
				$W->RatingEdit(),
				__FUNCTION__
			);
	}




}









class crm_WaitingCommentsEditor extends crm_Editor {
	
	public function __construct($crm, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($crm, $id, $layout);
	
		$this->colon();
		$this->setName('comments');
		$this->addClass('crm-waiting-comments-editor');
		$this->setHiddenValue('tg', bab_rp('tg'));
	
		$this->addFields();
		$this->addButtons();
	}
	
	
	
	
	
	
	/**
	 * Add fields into form
	 *
	 */
	public function addFields()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		
		
		$layout = $W->VBoxLayout()->setVerticalSpacing(0, 'em');
		
		
		
		$Crm->includeArticleSet();
		
		$set = $Crm->CommentSet();
		$set->article();
		
		$res = $set->select($set->confirmed->is(0));
		
		if ($res->count() == 0)
		{
			$layout->addItem($W->Label($Crm->translate('No comments to confirm')));
		}
		
		foreach($res as $comment)
		{
			$layout->addItem($this->articleComment($comment));
		}
		
		$this->addItem($layout);
	}
	
	
	protected function addButtons()
	{
		$W = $this->widgets;
		$controller = $this->Crm()->Controller();
	
		$button = $W->SubmitButton()
		->setLabel($this->Crm()->translate('Confirm checked comments'))
		->setAction($controller->Comment()->confirm())
		->setSuccessAction($controller->Comment()->displayList())
		->setFailedAction($controller->Comment()->displayList())
		;
	
		$this->addItem($button);
	}


	
	
	
	protected function articleComment(crm_Comment $comment)
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$article = $comment->article;
		$articleDisplay = $Crm->Ui()->ArticleDisplay($article);
		
		$visible = $W->HBoxLayout()->setHorizontalSpacing(1, 'em')->setSizePolicy(Widget_SizePolicy::MAXIMUM);
		$visible->addItem($articleDisplay->Photo(32, 32));
		$visible->addItem($W->Label(bab_abbr($comment->summary, BAB_ABBR_FULL_WORDS, 170))->setSizePolicy(Widget_SizePolicy::MAXIMUM));
		$visible->addItem($W->CheckBox()->setName(array('comments', $comment->id))->setValue(true));
		
		$section = $W->Section($visible, $W->DelayedItem($Crm->Controller()->Comment()->loadWaitingComment($comment->id)))->setFoldable(true, true);
		
		return $section;
	}
	
	
}











/**
 * Display a comment as seen in history frames.
 *
 *
 */
class crm_HistoryComment extends crm_HistoryElement
{


	public function __construct(crm_Comment $comment, $mode = null, $id = null)
	{
		parent::__construct($comment->Crm(), $mode, $id);

		$this->setComment($comment);
 		$this->addClass('crm-comment');
	}

	/**
	 * Set Comment image
	 * @param	Widget_Image	$image
	 * @return	crm_historyComment
	 */
	public function setImage(Widget_Image $image)
	{
		$this->addImage($image);
	}


	/**
	 * Set Comment content
	 * @return crm_historyComment
	 */
	public function setComment(crm_Comment $comment, $label = null, Widget_Action $link = null)
	{
		$W = bab_Widgets();
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$this->setElement($comment);

		$Crm = $this->Crm();


		$Crm->Ui()->includeContact();

		$access = $Crm->Access();

		$contactSet = $Crm->ContactSet();


		if ($comment->createdBy) {
			$creator = $contactSet->get($contactSet->user->is($comment->createdBy));
			if ($creator && $access->viewContact($creator)) {
				$creatorName = $creator->getFullName();
				$this->addIcon(
					$W->Link(
						$Crm->Ui()->ContactPhoto($creator, 32, 32, 0),
						$Crm->Controller()->Contact()->display($creator->id)
					)
				);
			} else {
				$creatorName = bab_getUserName($comment->createdBy, true);
			}
		} else {
			$creatorName = '';
		}


		$subtitleLayout = $W->FlowLayout()
			->setHorizontalSpacing(1, 'ex')
			->addClass(Func_Icons::ICON_LEFT_24)
			->addClass('crm-small');


		if (!$comment->confirmed)
		{
			$this->addTitle(
				$W->Title(
					$Crm->translate('Awaiting approval'),
					4
				)
			);
			
			$this->addClass('crm-comment-waiting');
		}
		
		if ($comment->private)
		{
			$subTitle = sprintf($Crm->translate('Private comment by %s'),  $creatorName);
			$this->addClass('crm-comment-private');
		} else {
			$subTitle = sprintf($Crm->translate('Comment by %s'),  $creatorName);
		}

		$commentIcon = $W->Icon($subTitle . "\n" . BAB_DateTimeUtil::relativePastDate($comment->createdOn, true), Func_Icons::ACTIONS_VIEW_PIM_NEWS);
		$subtitleLayout->addItem($commentIcon);

		$this->addTitle($subtitleLayout);



		switch ($this->getMode()) {

			case self::MODE_COMPACT :
				break;

			case self::MODE_SHORT :
				$this->addContent($W->Html(bab_toHtml(bab_abbr(strip_tags($comment->summary), BAB_ABBR_FULL_WORDS, 200))));
				break;

			case self::MODE_FULL :
			default:
				$this->addContent($W->RichText($comment->summary));
				break;
		}
		
		if ($comment->rating)
		{
			$this->addLinkedElements($W->RatingEdit()->setValue($comment->rating)->setDisplayMode());
		}
		
		return $this;
	}



	/**
	 * Add a contextual menu with several actions applicable
	 * to the comment.
	 *
	 * @return crm_historyComment
	 */
	public function addActions()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		if ($this->Crm()->Access()->updateComment($this->element)) {
			$this->addAction(
				$W->Link(
					$W->Icon($Crm->translate('Edit this comment'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
					$Crm->Controller()->Comment()->edit($this->element->id)
				)
			);
		}

		if ($this->Crm()->Access()->deleteComment($this->element)) {
			$this->addAction(
				$W->Link(
					$W->Icon($Crm->translate('Delete this comment'), Func_Icons::ACTIONS_EDIT_DELETE),
					$Crm->Controller()->Comment()->delete($this->element->id)
				)->setConfirmationMessage($Crm->translate('Are you sure you want to delete this comment? It will be definitely lost!'))
			);
		}

		return $this;
	}




}




/**
 * Get objects linked to comment
 * @param crm_Comment $comment
 *
 * @return widget_Frame
 */
function crm_commentLinkedObjects(crm_Comment $comment)
{
	$W = bab_Widgets();
	$Crm = $comment->Crm();


	$objectsFrame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');

	$orgs = array();

	if ($article = $comment->article()) {

		$Crm->Ui()->includeArticle();

		$objectsFrame->addItem(
			$W->Title($Crm->translate('Article linked to this comment'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);

	
		$articleFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
		$objectsFrame->addItem($articleFrame);
		/* @var $article crm_Article */
		$card = $Crm->Ui()->articleCardFrame($article);
		$articleFrame->addItem($card);

		$displayLink = $W->Link($W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Article()->display($article->id));
	
	}

	return $objectsFrame;
}





/**
 * Comment history for a record, as displayed in online shop public access
 * example : under a product page
 *
 */
class crm_CommentHistory extends crm_UiObject
{
	public function __construct(Func_Crm $Crm, crm_Record $record)
	{
		parent::__construct($Crm);
		$W = bab_Widgets();
		$this->setInheritedItem($W->Frame());
		$this->addClass('crm-comment-history');
		
		// get linked comments
		
		$res = $record->selectComments();
		$res->orderDesc($res->getSet()->createdOn);
		
		foreach($res as $comment)
		{
			$this->addComment($comment);
		}
	}
	
	
	
	protected function addComment(crm_Comment $comment)
	{
		$this->addItem($this->Crm()->Ui()->HistoryElement($comment, crm_HistoryElement::MODE_FULL));
	}
}

