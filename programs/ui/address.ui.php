<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');




/**
 *
 */
class crm_AddressFrame extends Widget_Frame
{
    const MAPS_URL = '//maps.google.com/?q=%s';

    const STATIC_MAPS_URL = '//maps.googleapis.com/maps/api/staticmap?';

    protected $address;


    public function __construct(crm_Address $address, $id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout();
        }
        parent::__construct($id, $layout);

        $this->address = $address;

        if (!empty($address->recipient))
        {
            $this->addItem(
                $W->RichText($address->recipient)
                ->setRenderingOptions(BAB_HTML_BR)
                ->addClass('crm-strong')
                ->addClass('fn')
                ->addClass('recipient')
            );
        }
        $this->addItem($W->RichText($address->street)->setRenderingOptions(BAB_HTML_BR)->addClass('street-address'));
        $this->addItem(
            $W->FlowItems(
                $W->Label($address->postalCode)
                    ->addClass('postal-code'),
                $W->Label($address->city)
                    ->addClass('locality'),
                $W->Label($address->cityComplement)
            )->setHorizontalSpacing(1, 'ex')
        );


        $this->addClass('adr');

        $this->addCountry();
        $this->addMapLink();
        $this->addInstruction();
    }


    public function addInstruction()
    {
        $W = bab_Widgets();

        if ($this->address->instructions) {
            $this->addItem($W->VBoxItems(
                $W->Label(crm_translate('Additional instructions'))->colon(),
                $W->RichText($this->address->instructions)
                ->setRenderingOptions(BAB_HTML_BR)
            )->addClass('widget-small widget-bordered'));
        }

        return $this;
    }

    /**
     * Add country
     *
     * @return crm_AddressFrame
     */
    public function addCountry()
    {
        $W = bab_Widgets();

        if ($country = $this->address->getCountry()) {

            $label = $W->Label($country->getName())->addClass('country');

            if ($country->code)
            {
                if ($flag = $W->CountryFlag($country->code))
                {
                    $label = $W->FlowItems($flag, $label)->setHorizontalSpacing(.3, 'em')->setVerticalAlign('middle');
                }
            }

            $this->addItem($label);
        }

        return $this;
    }


    /**
     * Adds a popup menu containing a static map of the address.
     *
     * @return crm_AddressFrame
     */
    public function addMapLink()
    {
        $W = bab_Widgets();
        if ($maplink = $this->MapLink(null, $this->address)) {

            $menu = $W->Menu(null, $W->FlowLayout())
                    ->attachTo($this)
                    ->addClass('icon-left-16 icon-16x16 icon-left')
                    ->addItem($maplink);

            $this->addItem($menu);
        }

        return $this;
    }

    /**
     * Adds a popup menu containing a static map of the address.
     *
     * @return crm_AddressFrame
     */
    public function addMapPreview($zoom = 15, $width = 200, $height = 150)
    {
        $W = bab_Widgets();
        if ($maplink = $this->MapLink(null, $this->address, $zoom, $width, $height)) {
            $this->addItem($maplink);
        }

        return $this;
    }



    /**
     * @return Widget_Link | null
     */
    public function MapLink($name = null, crm_Address $address, $zoom = 15, $width = 400, $height = 300)
    {
        if (empty($address->city)) {
            return null;
        }

        $query = '';

        if (!empty($address->street)) {
            $query = str_replace("\n", ' ', $address->street);
            $query = str_replace("\r", ' ', $address->street);
        }

        if (isset($name)) {
            $query = $name.', '. $query;
        }

        $query .= $address->postalCode ? ', '.$address->postalCode : '';
        $query .= ', '.$address->city;
//		$query .= $address->cityComplement ? ' '.$address->cityComplement : '';
        if ($country = $address->getCountry()) {
            $query .= ', '.$country->getName();
        }

        $url = sprintf(self::MAPS_URL, urlencode($query));
        $W = bab_Widgets();

        return $W->Link($this->MapLinkDisplay($address, $zoom, $width, $height), $url)->setOpenMode(Widget_Link::OPEN_POPUP);
    }




    /**
     * Returns an url to an image of a map corresponding to the specified address.
     *
     * @param crm_Address $address The address to display on a map.
     * @param int $zoom            Zoom level.
     * @param int $width           Image width in pixels.
     * @param int $height          Image height in pixels.
     *
     * @return string
     */
    protected function staticMapUrl(crm_Address $address, $zoom = 15, $width = 400, $height = 300)
    {
        if (empty($address->city)) {
            return null;
        }

        $query = '';

        if (empty($address->street)) {
            $zoom = 11;
        } else {
            $query = str_replace("\n", ' ', $address->street);
            $query = str_replace("\r", ' ', $address->street);
        }
        $query .= $address->postalCode ? ', '.$address->postalCode : '';
        $query .= ', '.$address->city;
//		$query .= $address->cityComplement ? ' '.$address->cityComplement : '';
        if ($country = $address->getCountry()) {
            $query .= ', '.$country->getName();
        }

        $url = self::STATIC_MAPS_URL . 'zoom=' . $zoom . '&size=' . $width . 'x' . $height . '&maptype=roadmap&sensor=false&markers=color:green|' . urlencode(bab_removeDiacritics($query));
        $url .= isset($GLOBALS['babGoogleApiKey']) ? '&key='.$GLOBALS['babGoogleApiKey'] : '';
        return $url;
    }




    /**
     * @return Widget_Image
     */
    protected function MapLinkDisplay(crm_Address $address, $zoom = 15, $width = 400, $height = 300)
    {
        $W = bab_Widgets();

        return $W->Image(
            $this->staticMapUrl($address, $zoom, $width, $height)
        );

//		return $W->Icon($this->Crm()->translate('Display on a map'), Func_Icons::STATUS_DIALOG_INFORMATION);
    }

}






class crm_AddressCardFrame extends crm_CardFrame
{

    /**
     *
     * @param Func_Crm $Crm
     * @param crm_Address $address
     * @param string $title
     * @param string $recipient			Deprecated parameter, if specified, overwrite the recipient stored in address object
     */
    public function __construct(Func_Crm $Crm, crm_Address $address, $title = null, $recipient = null)
    {
        $W = bab_Widgets();
        $layout = $W->VBoxLayout()->SetVerticalSpacing(.3, 'em');
        /*
        $layout =
            $W->HBoxLayout()
            ->setVerticalAlign('top')
            ->setHorizontalSpacing(8, 'px')
            ->addItem($vbox);
        */

        if ($title) {
            $layout = $this->LabelStr($title, $layout);
        }

        parent::__construct($Crm, null, $layout->addClass('crm-titled-address'));


        if ($recipient) {
            $address->recipient = $recipient;
        }

        $addressFrame = new crm_AddressFrame($address);
        //$addressFrame->addClass('widget-nowrap'); //SHOULD NOT BE FORCE BECAUSE WE CANNOT REMOVE IT
        $addressFrame->setSizePolicy(Widget_SizePolicy::MINIMUM);


        $layout->addItem($addressFrame);

        if ($maplink = $this->MapLink(null, $address)) {

            $menu = $W->Menu(null, $W->FlowLayout())
            ->attachTo($addressFrame)
            ->addClass('icon-left-16 icon-16x16 icon-left')
            ->addItem($maplink);

            //$maplink->addClass('crm-address-map');
            $layout->addItem($menu);
        }


    }
}



/**
 * crm_AddressEdit is a compound input widget usable for editing a postal address.
 */
class crm_AddressEdit extends crm_UiObject
{
    protected $showRecipient = false;
    protected $showInstructions = false;

    protected $showCountry = true;
    protected $showComplement = true;
    protected $showMapPreview = true;
    protected $showCoordones = false;

    protected $mandatory = false;
    protected $mandatoryMessage = '';

    protected $mandatoryStreet = false;
    protected $mandatoryPostalCode = false;
    protected $mandatoryCity = false;
    protected $mandatoryComplement = false;
    protected $mandatoryCountry = false;

    protected $defaultCountry = null;

    protected $convertCase = null;

    /**
     * @param Func_Crm $Crm
     * @param string   $id
     */
    public function __construct(Func_Crm $Crm, $id = null)
    {
        parent::__construct($Crm);

        $W = bab_Widgets();

        $this->setInheritedItem(
            $W->NamedContainer(null, $id)
                ->addClass('adr')
        );
    }

    /**
     * Defines if the recipient field should be visible or not
     *
     * @param bool $showRecipient
     * @return crm_AddressEdit
     */
    public function showRecipient($showRecipient = true)
    {
        $this->showRecipient = $showRecipient;
        return $this;
    }

    /**
     * Definie if the instructions field should be visible or not
     *
     * @param bool $showInstructions
     * @return crm_AddressEdit
     */
    public function showInstructions($showInstructions = true)
    {
        $this->showInstructions = $showInstructions;
        return $this;
    }


    /**
     * Defines if the country selector should be visible or not.
     *
     * @param bool $showCountryField True to have the country selector in the form.
     *
     * @return crm_AddressEdit
     */
    public function showCountry($showCountryField = true)
    {
        $this->showCountry = $showCountryField;
        return $this;
    }

    public function setDefaultCountry($defaultCountry)
    {
        $this->defaultCountry = $defaultCountry;
        return $this;
    }

    /**
     * Defines if the address complement field should be visible or not.
     * The complement field is a short line edit after the city field wihich typically
     * contains the optional CEDEX information in France.
     *
     * @param bool $showComplementField True to have the postal code complement field (CEDEX...) in the form.
     *
     * @return crm_AddressEdit
     */
    public function showComplement($showComplementField = true)
    {
        $this->showComplement = $showComplementField;
        return $this;
    }


    /**
     * Defines if an automatic map preview should be visible or not.
     *
     * @param bool $showMapPreview True to have the map preview in the form.
     *
     * @return crm_AddressEdit
     */
    public function showMapPreview($showMapPreview = true)
    {
        $this->showMapPreview = $showMapPreview;
        return $this;
    }


    /**
     * Defines if an automatic map preview should be visible or not.
     *
     * @param bool $showMapPreview True to have the map preview in the form.
     *
     * @return crm_AddressEdit
     */
    public function showCoordones($showCoordones = true)
    {
        $this->showCoordones = $showCoordones;
        return $this;
    }




    /**
     * Makes address fields mandatory.
     *
     * @param bool $mandatory True to make address fields mandatory.
     * @param string $message
     *
     * @return crm_AddressEdit
     */
    public function setMandatory($mandatory = true, $message = '')
    {
        $this->mandatory = $mandatory;
        $this->mandatoryStreet = $mandatory;
        $this->mandatoryPostalCode = $mandatory;
        $this->mandatoryCity = $mandatory;

        return $this;
    }

    /**
     * @param bool $mandatory
     * @return crm_AddressEdit
     */
    public function setCountryMandatory($mandatory = true)
    {
        $this->mandatoryCountry = $mandatory;
        return $this;
    }

    /**
     * @param bool $mandatory
     * @return crm_AddressEdit
     */
    public function setStreetMandatory($mandatory = true)
    {
        $this->mandatoryStreet = $mandatory;
        return $this;
    }

    /**
     * @param bool $mandatory
     * @return crm_AddressEdit
     */
    public function setPostalCodeMandatory($mandatory = true)
    {
        $this->mandatoryPostalCode = $mandatory;
        return $this;
    }

    /**
     * @param bool $mandatory
     * @return crm_AddressEdit
     */
    public function setCityMandatory($mandatory = true)
    {
        $this->mandatoryCity = $mandatory;
        return $this;
    }

    /**
     * @param bool $mandatory
     * @return crm_AddressEdit
     */
    public function setComplementMandatory($mandatory = true)
    {
        $this->mandatoryComplement = $mandatory;
        return $this;
    }

    /**
     * @param Widget_Canvas $canvas
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->initialize();
        return parent::display($canvas);
    }


    /**
     * Sets the value of the text transformation for the city field.
     * Currently only uppercase / lowercase transformation.
     *
     * @param int  $convertCase          One of MB_CASE_LOWER, MB_CASE_UPPER or MB_CASE_TITLE.
     */
    public function setConvertCase($convertCase)
    {
        $this->convertCase = $convertCase;
        return $this;
    }


    /**
     * Initializes the widget before display().
     *
     * @ignore
     */
    protected function initialize()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        if ($this->showRecipient)
        {
            $recipientFormItem = crm_FormField(
                    $Crm->translate('Recipient name'),
                    $W->TextEdit()
                    ->setColumns(54)
                    ->setLines(2)
                    ->setName('recipient')
                    ->addClass('fn widget-fullwidth')
            );

        } else {
            $recipientFormItem = null;

        }

        $streetFormItem = crm_FormField(
            $Crm->translate('Number / pathway'),
            $W->TextEdit()
                ->setColumns(58)
                ->setLines(2)
                ->setName('street')
                ->setMandatory($this->mandatoryStreet, $Crm->translate('The address street is mandatory'))
                ->addClass('street-address widget-fullwidth widget-autoresize')
        );

        $postalCodeInput = $Ui->SuggestPostalCode()
            ->setSize(4)
            ->setMandatory($this->mandatoryPostalCode, $Crm->translate('The address postal code is mandatory'));
        $cityInput = $Ui->SuggestPlaceName()
            ->setSize(24)
            ->setMandatory($this->mandatoryCity, $Crm->translate('The address city is mandatory'));
        if (isset($this->convertCase)) {
            $cityInput->setConvertCase($this->convertCase);
        }
        $postalCodeInput->setRelatedPlaceName($cityInput);
        $cityInput->setRelatedPostalCode($postalCodeInput);

        $postalCodeFormItem = crm_FormField(
            $Crm->translate('Postal Code'),
            $postalCodeInput
                ->addClass('postal-code')
                ->setName('postalCode')
        );
        $cityFormItem = crm_FormField(
            $Crm->translate('City'),
            $cityInput->setName('city')
                ->addClass('locality')
            );
        $cityComplementFormItem = null;
        if ($this->showComplement) {
            $cityComplementFormItem = crm_FormField(
                $Crm->translate('Complement'),
                $W->LineEdit()
                    ->setSize(6)
                    ->setName('cityComplement')
                    ->setMandatory($this->mandatoryComplement, $Crm->translate('The complement is mandatory'))
            );
        }


        $this->setLayout(
            $W->HBoxItems(
                $innerAddressFrame = $W->VBoxItems(
                    $recipientFormItem,
                    $streetFormItem,
                    $W->HBoxItems(
                        $postalCodeFormItem,
                        $cityFormItem,
                        $cityComplementFormItem
                    )->setHorizontalSpacing(1, 'em')
                    ->setVerticalAlign('bottom')
                )->setVerticalSpacing(1, 'em'),
                ($this->showMapPreview ?
                    crm_LabelledWidget(
                        $Crm->translate('Map preview'),
                        $W->Image()
                            ->addClass('crm-address-map')
                            ->setCanvasOptions(Widget_Item::Options()->width(150, 'px')->height(128, 'px'))
                            ->setSizePolicy(Widget_SizePolicy::MINIMUM)
                    ) :
                    null
                )
            )
            ->setVerticalAlign('top')
            ->setHorizontalSpacing(1, 'em')
            ->addClass('crm-address-form')
        );

        if ($this->showCountry) {

            $countrySet = $Crm->CountrySet();
            $countryInput = $W->Select();
            $options = $countrySet->getOptions();

            if ($this->mandatoryCountry) {
                $countryInput->setMandatory(true, $Crm->translate('The country is mandatory'));
                unset($options[0]);
            }

            $countryInput->setOptions($options);

            if (null !== $this->defaultCountry) {
                $countryInput->setValue($this->defaultCountry);
            }

            $countryFormItem = crm_FormField(
                $Crm->translate('Country'),
                $countryInput->setName('country')
                    ->addClass('country')
            );

            $innerAddressFrame->addItem($countryFormItem);
        }


        if ($this->showCoordones) {
            $coordonesFormItem = $W->HBoxItems(
                crm_FormField(
                    $Crm->translate('Latitude'),
                    $W->LineEdit()->setSize(20)->setName('latitude')->addClass('latitude')->setTitle($Crm->translate('Automatically fill if empty with address coordinates'))
                ),
                crm_FormField(
                    $Crm->translate('Longitude'),
                    $W->LineEdit()->setSize(20)->setName('longitude')->addClass('longitude')->setTitle($Crm->translate('Automatically fill if empty with address coordinates'))
                )
            )->setHorizontalSpacing(1,'em');
            $innerAddressFrame->addItem($coordonesFormItem);
        }


        if ($this->showInstructions) {
            $innerAddressFrame->addItem(
                $W->LabelledWidget(
                    $Crm->translate('Additional instructions'),
                    $W->TextEdit()
                    ->setColumns(54)
                    ->setLines(2)
                    ->setName('instructions')
                    ->addClass('widget-fullwidth')
                )
            );
        }

    }


    public function test()
    {
        return $this->item;
    }


}

/**
 * Creates a frame with fields to enter an address
 *
 * @param Func_Crm 	$Crm
 * @param bool		$withCountry		false
 * @param bool		$mandatory			false
 * @param bool		$withComplement		true
 * @param bool		$withMapPreview		false
 * @param bool		$mandatoryCountry	false
 * @param int		$defaultCountry		null
 * @param bool		$gps				false
 * @return crm_AddressEdit
 */
function crm_addressEditorFrame(Func_Crm $Crm, $withCountry = false, $mandatory = false, $withComplement = true, $withMapPreview = false, $mandatoryCountry = false, $defaultCountry = null, $gps = false)
{


    $editor = $Crm->Ui()->AddressEdit();
    $editor->showCountry($withCountry)
        ->setMandatory($mandatory)
        ->showComplement($withComplement)
        ->showMapPreview($withMapPreview)
        ->setCountrymandatory($mandatoryCountry)
        ->setDefaultCountry($defaultCountry)
        ->showCoordones($gps)
    ;


    return $editor;


}



