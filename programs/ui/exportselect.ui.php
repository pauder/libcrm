<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


class crm_ExportSelectEditor extends crm_Editor
{
    private $tableview;
    
    private $select;
    
    public function __construct(Func_Crm $Crm, $id, crm_TableModelView $tableview, array $filter = null) {
        parent::__construct($Crm, $id);
        
        $this->setReadOnly(false);
        
        $this->tableview = $tableview;
        
        if (isset($filter)) {
            $this->setHiddenValues('filter', $filter);
        }
        $this->setHiddenValue('tg', $Crm->controllerTg);
        
        $this->addItem($this->format());
        $this->addItem($this->charset());
        $this->addItem($this->columns());
    }
    
    
    protected function format()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        
        $this->select = $W->Select()
            ->setName('format')
            ->addOption('xlsx', $Crm->translate('Microsoft Excel 2007-2013 XML (.xlsx)'))
            ->addOption('xls', $Crm->translate('Microsoft Excel 97-2003 (.xls)'))
            ->addOption('csv', $Crm->translate('CSV (.csv)'))
            ->addOption('ssv', $Crm->translate('Microsoft CSV (.csv)'));
        
        if ($this->tableview instanceof crm_ContactTableView) {
            $this->select->addOption('outlookcsv', $Crm->translate('Csv for outlook contacts'));
        }
        
        return $W->Section(
            $Crm->translate('Format'),
            $this->select,
            4
        );
    }
    
    
    /**
     * @return Widget_LabelledWidget
     */
    protected function charset()
    {
        $W = $this->widgets;
    
        $charsetSelector = $W->LabelledWidget(
            crm_translate('Content charset'),
            $W->Select()
            ->addOption('UTF-8', 'UTF-8')
            ->addOption('UTF-8 BOM', crm_translate('UTF-8 (with BOM)'))
            ->addOption('CP1252', 'CP1252 (Windows-1252)')
            ->addOption('ISO-8859-15', 'ISO-8859-15'),
            __FUNCTION__
            );
        
        $this->select->setAssociatedDisplayable($charsetSelector, array('csv'));
        
        return $charsetSelector;
    }
    
    
    protected function columns()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        
        $checkAll = $W->CheckBoxAll();
        $columnsBox = $W->FlowItems();
        $columnsSection = $W->Section(
            $Crm->translate('Exported columns'),
            $W->VBoxItems(
                $W->LabelledWidget(
                    $Crm->translate('Select all / none'),
                    $checkAll
                    ),
                $columnsBox->setSizePolicy('widget-list-element')
                )->setVerticalSpacing(2, 'em'),
            4
            );
        $columnsSection->setName('columns');
        
        $columns = $this->tableview->getVisibleColumns();
        
        foreach ($columns as $columnId => $column) {
            if (!$column->isExportable()) {
                continue;
            }
            $checkbox = $W->CheckBox()->setName($columnId);
            $columnsBox->addItem(
                $W->LabelledWidget(
                    $column->getSelectableName(),
                    $checkbox
                    )->setSizePolicy('widget-33pc')
                );
            $checkAll->addCheckBox($checkbox);
        }
        
        $this->select->setAssociatedDisplayable($columnsSection, array('xlsx', 'xls', 'csv', 'ssv'));
        
        return $columnsSection;
    }
}