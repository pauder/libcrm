<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




require_once dirname(__FILE__).'/../set/base.class.php';






/**
 * A Ui class used to display an element (note/task...) as seen in history frames.
 *
 *
 */
class crm_HistoryElement extends crm_UiObject // Widget_Frame
{
    protected	$leftBox = null;
    protected	$mainBox = null;

    private		$headerBox = null;
    private		$contentBox = null;
    private		$attachmentsBox = null;
    private		$commentsBox = null;
    private		$titleBox = null;
    private		$linkedElementsBox = null;

    protected	$menu = null;

    protected	$element = null;


    /**
     * Constants usable for crm_HistoryElement::setMode().
     */
    const MODE_COMPACT	= 'compact';
    const MODE_SHORT	= 'short';
    const MODE_FULL		= 'full';

    const LINKED_ELEMENT_IMAGE_WIDTH = 56;
    const LINKED_ELEMENT_IMAGE_HEIGHT = 32;

    private $mode = crm_HistoryElement::MODE_SHORT;




    /**
     * @param Func_Crm $crm
     * @param string   $mode One of crm_HistoryElement::MODE_SHORT or crm_HistoryElement::MODE_FULL
     * @param string   $id
     */
    public function __construct(Func_Crm $crm, $mode = null, $id = null)
    {
        parent::__construct($crm);

        if (isset($mode)) {
            $this->setMode($mode);
        }

        bab_functionality::includefile('Icons');

        $W = bab_Widgets();
        $layout = $W->HBoxLayout();

        $this->leftBox = $W->VBoxLayout()
        ->setSizePolicy(Widget_SizePolicy::MINIMUM)
        ->addClass('crm-icons')
        ->addClass(Func_Icons::ICON_TOP_32);

        $this->mainBox = $W->VBoxItems();
        $this->mainBox
        ->setVerticalSpacing(0.5, 'em')
        ->setSizePolicy(Widget_SizePolicy::MAXIMUM);

        $layout->addItem($this->leftBox);
        $layout->addItem($this->mainBox);

        $this->setInheritedItem($W->Frame($id, $layout));
        $this->addClass('crm-history-element');
    }


    /**
     * Returns the box where the title and linked elements will appear.
     *
     * @see crm_HistoryElement::addTitle(), crm_HistoryElement::addLinkedElements()
     * @return Widget_HBoxLayout
     */
    protected function headerBox()
    {
        if (!isset($this->headerBox)) {
            $W = bab_Widgets();
            $this->headerBox = $W->HBoxItems();
            $this->headerBox
            ->addClass('crm-header')
            ->setHorizontalSpacing(1, 'em');
        }
        return $this->headerBox;
    }


    /**
     * Returns the box where the title will be displayed.
     *
     * @see crm_HistoryElement::addTitle()
     * @return Widget_VBoxLayout
     */
    protected function titleBox()
    {
        if (!isset($this->titleBox)) {
            $W = bab_Widgets();
            $this->titleBox = $W->VBoxLayout();
            $this->titleBox
            ->setSizePolicy(Widget_SizePolicy::MAXIMUM);
        }
        return $this->titleBox;
    }


    /**
     * Returns the box where the linked elements will be displayed.
     *
     * @see crm_HistoryElement::addLinkedElements(), crm_HistoryElement::addDefaultLinkedElements()
     * @return Widget_HBoxLayout
     */
    protected function linkedElementsBox()
    {
        if (!isset($this->linkedElementsBox)) {
            $W = bab_Widgets();
            $this->linkedElementsBox = $W->HBoxLayout();
            $this->linkedElementsBox
            ->setHorizontalSpacing(4, 'px')
            ->setVerticalAlign('middle')
            ->addClass('crm-linked-elements');
        }
        return $this->linkedElementsBox;
    }


    /**
     * Returns the main content box.
     *
     * @see crm_HistoryElement::addContent()
     * @return Widget_VBoxLayout
     */
    protected function contentBox()
    {
        if (!isset($this->contentBox)) {
            $W = bab_Widgets();
            $this->contentBox = $W->VBoxLayout();
            $this->contentBox->setVerticalSpacing(1, 'em');
        }
        return $this->contentBox;
    }



    /**
     * Returns the box where comments will appear.
     *
     * @see crm_HistoryElement::addComments()
     * @return Widget_VBoxLayout
     */
    protected function commentsBox()
    {
        if (!isset($this->commentsBox)) {
            $W = bab_Widgets();
            $this->commentsBox = $W->VBoxLayout();
            $this->commentsBox->setVerticalSpacing(1, 'em');
        }
        return $this->commentsBox;
    }


    /**
     * Returns the box where attached files will appear.
     *
     * @see
     * @return Widget_FlowLayout
     */
    protected function attachmentsBox()
    {
        if (!isset($this->attachmentsBox)) {
            $W = bab_Widgets();
            $this->attachmentsBox = $W->FlowLayout();
            $this->attachmentsBox->setHorizontalSpacing(1, 'em')
            ->addClass(Func_Icons::ICON_LEFT_32 . ' crm-attachments');
        }
        return $this->attachmentsBox;
    }




    public function setLastModified($datetime)
    {
        if ('0000-00-00 00:00:00' === $datetime) {
            return;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

        $this->item->setCacheHeaders($this->item->CacheHeaders()->setLastModified(BAB_DateTime::fromIsoDateTime($datetime)));
    }



    /**
     * Returns the display mode (short or full) for the element.
     *
     * @return string One of crm_HistoryElement::MODE_COMPACT, crm_HistoryElement::MODE_SHORT or crm_HistoryElement::MODE_FULL
     */
    public function getMode()
    {
        return $this->mode;
    }




    /**
     * Selects the display mode (short or full) for the element.
     *
     * @param string $mode One of crm_HistoryElement::MODE_COMPACT, crm_HistoryElement::MODE_SHORT or crm_HistoryElement::MODE_FULL
     *
     * @return crm_HistoryElement
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
        return $this;
    }




    /**
     * Adds comments associated to the displayed element.
     *
     * @return crm_HistoryElement
     */
    public function addComments()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $element = $this->element;

        $commentsBox = $this->commentsBox();


        $noteEditor = $Crm->Ui()->AddNoteEditor($element, false, true)
        ->setTitle($Crm->translate('Add a comment'))
        ->addClass('widget-instant-form');

        $noteEditor->setName('data');
        
        $addNoteButton = $W->Link($Crm->translate('Add comment'), $Crm->Controller()->Note()->edit(null, $element->getRef()))
        ->addClass('widget-instant-button widget-actionbutton');


        $instantContainer = $W->VboxItems(
            $addNoteButton,
            $noteEditor
        )->addClass('widget-instant-container');


        $subNotes = $element->selectNotes();

        if ($subNotes->count() > 0) {
            $subNotesSection = $W->Section(
                sprintf($Crm->translate('%d comments'), $subNotes->count()),
                $W->VBoxItems()->setVerticalSpacing(1, 'em'),
                7
            )->setFoldable(true, true);
            $menu = $subNotesSection->addContextMenu();
            $menu->addItem($instantContainer);
            $commentsBox->addItem($subNotesSection);

            foreach ($subNotes as $subNote) {
                $historyNote = new crm_HistoryNote($subNote->targetId, $this->getMode());
                $historyNote->addActions();
                $subNotesSection->addItem($historyNote);
            }

        } else {

            $commentsBox->addItem(
                $W->HBoxItems(
                    $W->Label('')->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                    $instantContainer->setSizePolicy(Widget_SizePolicy::MINIMUM)
                )->setSizePolicy(Widget_SizePolicy::MAXIMUM)
            );

        }

        return $this;
    }




    /**
     *
     * @param crm_Record $element
     * @return crm_HistoryElement
     */
    public function setElement(/*crm_Record*/ $element)
    {
        $this->element = $element;
        return $this;
    }




    /**
     * Adds an image to the element
     * @param	Widget_Image	$image
     * @return	crm_HistoryElement
     */
    public function addImage(Widget_Image $image)
    {
        $image->setSizePolicy(Widget_SizePolicy::MINIMUM);
        $this->leftBox->addItem($image);
        return $this;
    }




    /**
     * Adds an icon to the element
     * @param	Widget_Icon|string	$icon
     * @return	crm_HistoryElement
     */
    public function addIcon($icon)
    {
        if (is_string($icon)) {
            $W = bab_Widgets();
            $icon = $W->Icon('', $icon);
        }
        $icon->setSizePolicy(Widget_SizePolicy::MINIMUM);
        $this->leftBox->addItem($icon);
        return $this;
    }




    /**
     * Adds the item to the title box.
     *
     * @return crm_HistoryElement
     */
    public function addTitle(Widget_Item $item)
    {
        $this->titleBox()->addItem($item);

        return $this;
    }




    /**
     * Manually add items to the linked elements box.
     *
     * @param Widget_Displayable_Interface $linkedItem
     *
     * @return crm_HistoryElement
     */
    public function addLinkedElements(Widget_Displayable_Interface $linkedItem)
    {
        $this->linkedElementsBox()->addItem($linkedItem);

        return $this;
    }




    /**
     * Adds default images of linked elements (contacts, organizations, deals and teams).
     *
     * @return crm_HistoryElement
     */
    public function addDefaultLinkedElements()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();

        $maxLinkedElements = 10;
        $nbLinkedElements = 0;

        if (isset($Crm->Team) && ($nbLinkedElements <= $maxLinkedElements))
        {
            $teamSet = $Crm->TeamSet();
            $teams = $teamSet->selectLinkedFrom($this->element);

            foreach ($teams as $team) {
                $nbLinkedElements++;
                if ($nbLinkedElements > $maxLinkedElements) {
                    break;
                }
                $this->addLinkedElements(
                    $W->Link(
                        $Ui->TeamPhoto($team->sourceId, crm_HistoryElement::LINKED_ELEMENT_IMAGE_WIDTH, crm_HistoryElement::LINKED_ELEMENT_IMAGE_HEIGHT),
                        $Crm->Controller()->Team()->display($team->sourceId->id)
                    )
                );
            }
        }

        if (isset($Crm->Contact) && ($nbLinkedElements <= $maxLinkedElements))
        {
            $contactSet = $Crm->ContactSet();
            $contacts = $contactSet->selectLinkedFrom($this->element);

            foreach ($contacts as $contact) {
                $nbLinkedElements++;
                if ($nbLinkedElements > $maxLinkedElements) {
                    break;
                }
                $this->addLinkedElements(
                    $W->Link(
                        $Ui->ContactPhoto($contact->sourceId, crm_HistoryElement::LINKED_ELEMENT_IMAGE_WIDTH, crm_HistoryElement::LINKED_ELEMENT_IMAGE_HEIGHT),
                        $Crm->Controller()->Contact()->display($contact->sourceId->id)
                    )
                );
            }
        }

        if (isset($Crm->Organization) && ($nbLinkedElements <= $maxLinkedElements))
        {
            $organizationSet = $Crm->OrganizationSet();
            $organizations = $organizationSet->selectLinkedFrom($this->element);

            foreach ($organizations as $organization) {
                $nbLinkedElements++;
                if ($nbLinkedElements > $maxLinkedElements) {
                    break;
                }
                $this->addLinkedElements(
                    $W->Link(
                        $Ui->OrganizationLogo($organization->sourceId, crm_HistoryElement::LINKED_ELEMENT_IMAGE_WIDTH, crm_HistoryElement::LINKED_ELEMENT_IMAGE_HEIGHT),
                        $Crm->Controller()->Organization()->display($organization->sourceId->id)
                    )
                );
            }
        }

        if (isset($Crm->Deal) && ($nbLinkedElements <= $maxLinkedElements))
        {
            $dealSet = $Crm->DealSet();
            $deals = $dealSet->selectLinkedFrom($this->element);

            foreach ($deals as $deal) {
                $nbLinkedElements++;
                if ($nbLinkedElements > $maxLinkedElements) {
                    break;
                }
                $this->addLinkedElements(
                    $W->Link(
                        $Ui->DealPhoto($deal->sourceId, crm_HistoryElement::LINKED_ELEMENT_IMAGE_WIDTH, crm_HistoryElement::LINKED_ELEMENT_IMAGE_HEIGHT, false, true),
                        $Crm->Controller()->Deal()->display($deal->sourceId->id)
                    )
                );
            }
        }

        return $this;
    }



    /**
     * Sets the element content.
     *
     * @param Widget_Displayable_Interface $item
     *
     * @return crm_HistoryElement
     */
    public function addContent(Widget_Displayable_Interface $item)
    {
        $contentBox = $this->contentBox();
        $contentBox->addItem($item);

        return $this;
    }


    /**
     * Returns a widget corresponding to the specified attached file.
     * Should be overridden to customize for example the download link.
     *
     * @param Widget_FilePickerItem $file
     *
     * @return Widget_Item
     */
    protected function getAttachmentWidget(Widget_FilePickerItem $file)
    {
        $W = bab_Widgets();

        return crm_fileAttachementIcon($file, 28, 28);
    }


    /**
     * @param Widget_FilePickerIterator $files
     * @return crm_HistoryElement
     */
    public function setAttachments(Widget_FilePickerIterator $files)
    {
        $attachmentsBox = $this->attachmentsBox();

        foreach ($files as $file) {
            $attachmentsBox->addItem($this->getAttachmentWidget($file));
        }

        return $this;
    }


    /**
     *
     * @param Widget_Link	$link
     * @return crm_HistoryElement
     */
    public function addAction($link)
    {
        if (!isset($this->menu)) {
            $W = bab_Widgets();
            $this->menu = $W->Menu()
            ->setLayout($W->FlowLayout())
            ->addClass(Func_Icons::ICON_LEFT_16);

        }
        $this->menu->addItem($link);
        return $this;
    }

    /**
     * Add default actions.
     *
     * @return crm_HistoryElement
     */
    public function addActions()
    {
        return $this;
    }



    public function display(Widget_Canvas $canvas)
    {
        if (isset($this->titleBox)) {
            $this->headerBox()->addItem($this->titleBox);
        }
        if (isset($this->linkedElementsBox)) {
            $this->headerBox()->addItem($this->linkedElementsBox);
        }
        if (isset($this->menu)) {
            $this->headerBox()->addItem($this->menu);
        }

        if (isset($this->headerBox)) {
            $this->mainBox->addItem($this->headerBox);
        }
        if (isset($this->contentBox)) {
            $this->mainBox->addItem($this->contentBox);
        }
        if (isset($this->attachmentsBox)) {
            $this->mainBox->addItem($this->attachmentsBox);
        }
        if (isset($this->commentsBox)) {
            $this->mainBox->addItem($this->commentsBox);
        }

        return $this->item->display($canvas);
    }
}









/**
 * Displays a chronological list of tasks, notes and emails associated to the
 * a specific crm_Record $record.
 */
class crm_History extends crm_UiObject {

    /**
     *
     * @var int
     */
    private $maxDisplayedElements = 15;

    private $skipDisplayedElements = 0;

    private $remainingToDisplay = null;

    private $objectIsSet = false;

    /**
     *
     * @var crm_Record
     */
    private $record;


    /**
     * @param crm_Record	$record		The record (contact, organization, deal...) for which the history must be created
     * @param string		$id
     * @param Widget_Layout $layout
     */
    public function __construct(crm_Record $record, $id = null, Widget_Layout $layout = null)
    {
        $Crm = $record->Crm();
        parent::__construct($Crm);
        $W = bab_Widgets();

        if (null === $layout) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(4, 'px');
        }

        // Recent general history
        $layout->addClass('crm-recent-history');



        $this->setInheritedItem($W->Frame($id, $layout));


        $this->record = $record;

    }

    /**
     * Sets the maximum number of elements displayed.
     *
     * @param	int		$max
     * @return 	crm_History
     */
    public function setMaxDisplayedElements($max)
    {
        $this->maxDisplayedElements = $max;
        return $this;
    }

    /**
     * Gets the maximum number of elements displayed.
     *
     * @return 	int
     */
    public function getMaxDisplayedElements()
    {
        return $this->maxDisplayedElements;
    }


    /**
     * Sets the number of elements to skip.
     *
     * @param	int		$max
     * @return 	crm_History
     */
    public function setSkipDisplayedElements($start)
    {
        $this->skipDisplayedElements = $start;
        return $this;
    }

    /**
     * Gets the number of elements to skip.
     *
     * @return 	int
     */
    public function getSkipDisplayedElements()
    {
        return $this->skipDisplayedElements;
    }



    /**
     * Gets the number of elements of the history remaining to display.
     *
     * @return 	int
     */
    public function getRemainingElementsToDisplay()
    {
        $this->setObject($this->record);
        return $this->remainingToDisplay;
    }



    /**
     * /**
     *
     * @return Widget_Item | null
     */
    protected function HistoryElement(crm_Record $element)
    {
        $historyElement = $this->Crm()->Ui()->HistoryElement($element, crm_HistoryElement::MODE_FULL);

        if (null === $historyElement)
        {
            return null;
        }

        if ($element instanceof crm_TraceableRecord)
        {
            $historyElement->setLastModified($element->modifiedOn);
        } else {
            bab_debug(sprintf('Cache headers : %s this is not a tracable record!', get_class($element)));
        }


        $historyElement
        ->addActions()
        ->addComments()
        ;

        return $historyElement;
    }



    /**
     *
     * @return array
     */
    protected function getElements(crm_Record $record, $pinned = true)
    {
        $Crm = $this->Crm();

        $Access = $Crm->Access();

        $Crm->includeNoteSet();
        $Crm->Ui()->includeContact();
        $linkSet = $Crm->LinkSet();

        $noteLinks = $record->selectNotes();
        $noteLinks->orderDesc($noteLinks->getSet()->targetId->createdOn);

        if (isset($Crm->Email)) {
            $emailLinks = $record->selectEmails();
            $emailLinks->orderDesc($emailLinks->getSet()->targetId->createdOn);
        } else {
            $emailLinks = array();
        }

        if (isset($Crm->Task)) {
            $taskLinks = $record->selectTasks();
            $taskLinks->setCriteria($taskLinks->getCriteria()->_AND_($taskLinks->getSet()->targetId->isCompleted()));
            $taskLinks->orderDesc($taskLinks->getSet()->targetId->completedOn);
        } else {
            $taskLinks = array();
        }


        if (isset($Crm->Order)) {
            $orderLinks = $record->selectOrders();
        }
        if (isset($orderLinks)) {
            $orderLinks->orderDesc($orderLinks->getSet()->createdOn);
        } else {
            $orderLinks = array();
        }


        $elements = array();
        $elementsFirst = array();

        foreach ($noteLinks as $noteLink) {
            if (!$Access->readNote($noteLink->targetId)) {
                continue;
            }
            if ($noteLink->targetId->pinned && $pinned) {
                $elementsFirst[] = array('date' => $noteLink->targetId->createdOn, 'data' => $noteLink->targetId);
            } else {
                $elements[] = array('date' => $noteLink->targetId->createdOn, 'data' => $noteLink->targetId);
            }
        }

        foreach ($emailLinks as $emailLink) {
            if (!$Access->readEmail($emailLink->targetId)) {
                continue;
            }
            $elements[] = array('date' => $emailLink->targetId->createdOn, 'data' => $emailLink->targetId);
        }

        foreach ($taskLinks as $taskLink) {
            if (!$Access->readTask($taskLink->targetId)) {
                continue;
            }
            $elements[] = array('date' => $taskLink->targetId->completedOn, 'data' => $taskLink->targetId);
        }


        foreach ($orderLinks as $order) {
            if (!$Access->viewOrder($order)) {
                continue;
            }
            $elements[] = array('date' => $order->createdOn, 'data' => $order);
        }

        return array($elements, $elementsFirst);
    }



    /**
     * @return crm_History
     */
    public function setObject(crm_Record $record, $pinned = true)
    {
        if ($this->objectIsSet) {
            return $this;
        }
        $this->objectIsSet = true;
        $Crm = $this->Crm();

        list($elements, $elementsFirst) = $this->getElements($record, $pinned);

        if (!$pinned) {
            $elements = array_merge($elements, $elementsFirst);
        }

        bab_Sort::asort($elements, 'date');
        $elements = array_reverse($elements, true);

        $maxDisplayedElements = $this->maxDisplayedElements;
        $skipDisplayedElements = $this->skipDisplayedElements;

        $elementsTotal = count($elementsFirst) + count($elements);

        $this->remainingToDisplay = max(array(0, $elementsTotal - ($skipDisplayedElements + $maxDisplayedElements)));

        foreach ($elementsFirst as $elementFirst) {
            if ($skipDisplayedElements-- > 0) {
                continue;
            }
            if ($maxDisplayedElements-- <= 0) {
                break;
            }

            $historyElement = $this->HistoryElement($elementFirst['data']);
            if (isset($historyElement)) {
                $this->addItem($historyElement);
            }
        }
        foreach ($elements as $element) {
            if ($skipDisplayedElements-- > 0) {
                continue;
            }
            if ($maxDisplayedElements-- <= 0) {
                break;
            }

            $historyElement = $this->HistoryElement($element['data']);
            if (isset($historyElement)) {
                $this->addItem($historyElement);
            }
        }

        return $this;
    }



    public function display(Widget_Canvas $canvas)
    {
        $this->setObject($this->record);
        return parent::display($canvas);
    }

}

