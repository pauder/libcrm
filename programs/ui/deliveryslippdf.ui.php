<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__).'/orderpdf.ui.php';


class crm_DeliverySlipPdf extends crm_OrderPdf 
{
	
	
	public function init() {
	
	
		$this->SetTitle($this->order->name);
	
	
		$this->SetSubject($this->getSubject());
		$this->SetAuthor(bab_convertStringFromDatabase($GLOBALS['BAB_SESS_USER']										, 'UTF-8'));
		$this->SetCreator(bab_convertStringFromDatabase($GLOBALS['babUrl']												, 'UTF-8'));
	
		$this->AddPage();
	
		$this->addSellerContactInfos();
		$this->setY($this->getY() - 10);
		$this->setX(70);
	
		$this->getBillingDelivery()->createMultiCells($this);
		
		
		$this->addGiftComment();
	
		$this->Ln();
		$this->addTitle();
		$this->addHeaderText();
		$this->getItemTable()->createMultiCells($this);
		$this->Ln();
		$this->addFooterText();
		$this->Ln();
	
	}
	
	
	
	protected function delivery()
	{
	    $coordinates = $this->getCoodinates('delivery');
	
	    if ('' === $coordinates)
	    {
	        return '';
	    }
	
	    $return = '<em color="#999">'.$this->Crm()->translate('Delivery').'</em><br /><br />';
	    $return .= $coordinates;
	
	
	    $deliveryAddress = $this->order->deliveryaddress;
	
	    if ($deliveryAddress->instructions) {
	        $return .= "<br />";
	        $return .= bab_toHtml($deliveryAddress->instructions, BAB_HTML_BR);
	    }
	
	    return $return;
	}
	
	
	
	protected function addGiftComment()
	{
		$Crm = $this->Crm();
		
		
		if (!$this->order->gift_comment)
		{
			return;
		}
		
		$this->setY($this->getY() + 5);
		
		$margins = $this->getMargins();
		$width = $this->getPageWidth() - $margins['left'] - $margins['right'];
		
		
		
		$Crm->includeContactSet();
		$contact = $this->order->contact();
		
		// back to default style
		$this->SetFont(crm_OrderPdf::FONT_FAMILY, 'B', crm_OrderPdf::FONT_SIZE);
		
		$this->MultiCell($width, 4, bab_convertStringFromDatabase(sprintf($Crm->translate('Message from %s %s'), $contact->firstname, $contact->lastname).$this->colon(), 'UTF-8'), 0, 1, 'L');
		
		// back to default style
		$this->SetFont(crm_OrderPdf::FONT_FAMILY, crm_OrderPdf::FONT_STYLE, crm_OrderPdf::FONT_SIZE);
		
		$this->MultiCell($width, 4, bab_convertStringFromDatabase($this->order->gift_comment, 'UTF-8'), 1, 1, 'L');
	}
	
	
	
	/**
	 *
	 * @return unknown_type
	 */
	protected function getItemTable() {
	
	
		$table = bab_functionality::get('TcPdf')
		->getTableHelper()
		->setBorder(1);
	
		$this->itemTableAddHeader($table);
		$this->itemTableAddBody($table);
	
		return $table;
	}
	
	
	
	/**
	 * add header to table
	 * @param LibPdf_table $table
	 * @return unknown_type
	 */
	protected function itemTableAddHeader(LibPdf_table $table)
	{
		$Crm = $this->Crm();
		$this->reference	= $table->createColumn()->setCellWith(-1);
		$this->name 		= $table->createColumn()->setCellWith(0);
		$this->quantity		= $table->createColumn()->setAlignment('R')->setCellWith(8, '%');
	
	
	
	
		$r = $table->createRow()->setBackgroundColor(240, 240, 240);
	
		$r->createCell()->inColumn($this->reference)	->setData(bab_convertStringFromDatabase($Crm->translate('Reference')				, 'UTF-8'));
		$r->createCell()->inColumn($this->name)			->setData(bab_convertStringFromDatabase($Crm->translate('Product description')		, 'UTF-8'));
		$r->createCell()->inColumn($this->quantity)		->setData(bab_convertStringFromDatabase($Crm->translate('Quantity')					, 'UTF-8'));
	
	
	
	}
	
	
	
	
	/**
	 * add products body
	 * @param LibPdf_table $table
	 * @return unknown_type
	 */
	protected function itemTableAddBody(LibPdf_table $table)
	{
		$items = $this->order->getItems();
	
		if (0 == $items->count())
		{
			return null;
		}
	
	
		foreach ($items as $item) {
	
			$r = $table->createRow()->setBorder('LR');
	
			$r->createCell()->inColumn($this->reference)	->setData(bab_convertStringFromDatabase($this->referenceCell($item)					, 'UTF-8'))->isHtml(true);
			$r->createCell()->inColumn($this->name)			->setData(bab_convertStringFromDatabase($this->itemNameCell($item)					, 'UTF-8'))->isHtml(true);
			$r->createCell()->inColumn($this->quantity)		->setData(bab_convertStringFromDatabase($this->quantityCell($item)					, 'UTF-8'));
		}
	
		$r->setBorder('LRB');
	
	}
}