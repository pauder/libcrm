<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




bab_Widgets()->includePhpClass('widget_TableView');

class crm_DealTableView extends crm_TableModelView
{
    /**
     *
     */
    protected function getDisplayAction(crm_Deal $record)
    {
        return $this->Crm()->Controller()->Deal()->display($record->id);
    }

    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {

            case 'image':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link($this->Crm()->Ui()->DealPhoto($record, 22, 22), $displayAction);

            case 'name':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link($record->name, $displayAction);

            case 'status':
                $this->Crm()->includeStatusSet();
                $status = $record->status();
                if (isset($status)) {
                    $statusName = $status->name;
                } else {
                    $statusName = '';
                }
                return $W->Label($statusName);
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}



class crm_DealCardFrame extends crm_CardFrame
{
    /**
     * @var crm_Deal
     */
    protected $deal = null;

    public function __construct(Func_Crm $crm, crm_Deal $deal, $id = null)
    {
        $W = bab_Widgets();
        $cardLayout = $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
        parent::__construct($crm, $id, $cardLayout);

        $Crm = $this->Crm();
        $Crm->includeOrganizationSet();
        $lead = $deal->lead();

        $access = $Crm->Access();

        $this->deal = $deal;


        $lead = $deal->lead();
        if (!$lead) {
            $leadLink = $W->Label('-');
        } else {
            $leadLink = $W->Link($lead->name, $Crm->Controller()->Organization()->display($lead->id));
        }


        $cardLayout->addItem(
            $W->VBoxItems(
                $W->Link(
                    $deal->name,
                    $Crm->Controller()->Deal()->display($deal->id)
                )->addClass('crm-card-title'),
                $leadLink,
                $W->Label($deal->getFullNumber())->addClass('crm-small'),
                $this->status(),
                $W->Label(bab_abbr($deal->description, BAB_ABBR_FULL_WORDS, 100))
            )->setSizePolicy(Widget_SizePolicy::MAXIMUM)
        );


        if ($deal->getPhotoPath() || (!$lead) || (!$lead->getLogoPath())) {
            $image = $Crm->Ui()->DealPhoto($deal, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
        } else {
            $image = $Crm->Ui()->OrganizationLogo($lead, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
        }

        if ($access->viewDeal($this->deal)) {
            $image = $W->Link(
                $image,
                $Crm->Controller()->Deal()->display($this->deal->id)
            );
        }
        $cardLayout->addItem($image->setSizePolicy(Widget_SizePolicy::MINIMUM));

    }


    protected function status()
    {
        if (!$this->deal->status) {
            return null;
        }

        $W = bab_Widgets();
        $Crm = $this->Crm();

        return $W->Label($Crm->translate('Status:') . ' ' . $this->deal->getStatusName())->addClass('crm-small');
    }

}


/**
 * Full deal frame
 */
class crm_DealFullFrame extends crm_CardFrame
{

    public function __construct(Func_Crm $crm, crm_Deal $deal, $id = null)
    {
        $W = bab_Widgets();
        $cardLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        parent::__construct($crm, $id, $cardLayout);

        $this->deal = $deal;

        $this->init();
    }


    protected function init()
    {
        $W = bab_Widgets();
        $crm = $this->Crm();
        $deal = $this->deal;

        $this->addItem(
            $W->HBoxItems(
                $this->photo(),
                $vbox = $W->VBoxItems(
                    $W->VBoxItems(
                        $W->Title($deal->name, 1),
                        $W->Title($crm->translate('Number:') . ' ' . $deal->year . '-' . $deal->number, 3)
                    ),
                    $W->HBoxItems(
                        $W->VBoxItems(
                            $this->status(),
                            $this->labelStr($crm->translate('Amount'), $W->Label(number_format((float)$deal->amount, 0, ',', ' ')  . $crm->translate('_euro_')))
                        )->setVerticalSpacing(0.5, 'em'),
                        $this->address()
                    )->setSizePolicy(Widget_SizePolicy::MAXIMUM)->setHorizontalSpacing(2, 'em'),
                    $this->description(),
                    $W->HBoxItems(
                        $this->codifications(),
                        $this->tags()
                    )->setHorizontalSpacing(0.5, 'em')
                    ->setSizePolicy(Widget_SizePolicy::MAXIMUM)->setHorizontalSpacing(2, 'em')
                )->setVerticalSpacing(1, 'em')
            )->setHorizontalSpacing(2, 'em')
        );

        $sections = $this->getSections();
        foreach ($sections as $section) {
            $vbox->addItem($section);
        }
    }


    public function section($title, Widget_Displayable_Interface $content, $level = 5)
    {
        return $this->labelStr(
            $title,
            $content
        );
    }

    protected function status()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->includeStatusSet();

        $status = $this->deal->status();
        if (!$status || empty($status->name)) {
            $statusName = $Crm->translate('Undefined');
        } else {
            $statusName = $status->name;
        }

        return $this->labelStr(
            $Crm->translate('Status'),
            $W->FlowItems(
                $W->Label($statusName),
                $W->Link(
                    $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $Crm->Controller()->Deal()->editStatus($this->deal->id)
                )
            )->setHorizontalSpacing(1, 'em')
            ->addClass('icon-left-16 icon-left icon-16x16')
        );
    }

    protected function photo()
    {
        return $this->Crm()->Ui()->DealPhoto($this->deal, 180, 180)
            ->addClass('crm-deal-image crm-element-image large');
    }


    protected function description()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (empty($this->deal->description)) {
            return $W->Html('');
        }
        return $this->labelStr(
            $Crm->translate('Description'),
            $W->Html(bab_toHtml($this->deal->description, BAB_HTML_ALL))
        );
    }


    protected function address()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->includeAddressSet();

        $address = $this->deal->address();
        if (!($address instanceof crm_Address) || $address->isEmpty()) {
            return $W->Label('');
        } else {
            return $this->section(
                $Crm->translate('Address'),

                $this->TitledAddress($address)
            );
        }
    }


    /**
    * Display address with title and map link in a menu
    *
    * @param	crm_Address	$address
    * @param	string		$title
    * @param	string		$recipient
    *
    * @return Widget_Item
    */
    protected function TitledAddress(crm_Address $address, $title = null, $recipient = null)
    {
        $W = bab_Widgets();

        $addressFrame = $this->AddressFrame($address);
        $addressFrame->addClass('widget-nowrap');
        $addressFrame->setSizePolicy(Widget_SizePolicy::MINIMUM);


        $layout = $W->VBoxLayout()->SetVerticalSpacing(.3, 'em');

        if ($recipient) {
            $layout->addItem(
                $W->Label($recipient)->addClass('crm-address-recipient')
            );
        }

        $layout->addItem($addressFrame);

        $layout = $W->HBoxLayout()
            ->setVerticalAlign('top')
            ->setHorizontalSpacing(8, 'px')
            ->addItem($layout);

        if ($maplink = $this->MapLink(null, $address, 14, 320, 240)) {

            $menu = $W->Menu(null, $W->FlowLayout())
            ->attachTo($addressFrame)
            ->addClass('icon-left-16 icon-16x16 icon-left')
            ->addItem($maplink);

            $layout->addItem($menu);
        }

        if ($title) {
            $layout = $this->LabelStr($title, $layout);
        }
        return $W->Frame(null, $layout->addClass('crm-titled-address'));
    }

    /**
     *
     * @return Widget_FlowLayout
     */
    protected function codifications()
    {
        $W = bab_Widgets();
        $crm = $this->Crm();

        $crm->includeClassificationSet();

        $dealClassificationSet = $crm->DealClassificationSet();
        $dealClassificationSet->join('classification');
        $dealClassifications = $dealClassificationSet->selectForDeal($this->deal->id);

        $classifications = $W->FlowLayout()->setSpacing(3, 'px');

        foreach ($dealClassifications as $dealClassification) {
            /* @var $dealClassification crm_DealClassification */

            $classificationLabel = $W->Label($dealClassification->classification->name)
                                        ->setTitle(implode(' > ', $dealClassification->classification->getPathname()))
                                        ->addClass('crm-codification');
            $classifications->addItem($classificationLabel);
        }

        return $this->section(
            $crm->translate('Codification'),
            $classifications,
            3
        );
    }

    /**
     * Widget_FlowLayout
     */
    protected function tags()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->Ui()->includeTag();

        $tagSet = $Crm->TagSet();
        $tags = $tagSet->selectFor($this->deal);
        $tagsDisplay = new crm_TagsDisplay($Crm, $tags, $this->deal, $Crm->Controller()->Deal()->displayListForTag());

        $addTagForm = $W->Form()
            ->setLayout($W->FlowLayout()->setVerticalAlign('middle')->setSpacing(2, 'px'))
            ->setName('tag');
        $addTagForm->addItem($Crm->Ui()->SuggestTag()->setName('label'));
        $addTagForm->addItem(
            $W->SubmitButton()
                ->setAction($Crm->Controller()->Tag()->link())
                ->setLabel($Crm->translate('Add'))
//				->addClass('crm-hidden')
        );
        $addTagForm->setHiddenValue('tg', bab_rp('tg'));
        $addTagForm->setHiddenValue('to', 'Deal:' . $this->deal->id);

        return $this->labelStr(
            $Crm->translate('Tags'),
            $W->FlowItems(
                $tagsDisplay,
                $addTagForm
            )->setHorizontalSpacing(0.5, 'em')
        );
    }



    public function customFields()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $customFields = $Crm->DealSet()->selectCustomFields();

        if ($customFields->count() != 0) {

            $customFieldsLayout = $W->VBoxItems()->setVerticalSpacing(1,'em');
            foreach ($customFields as $customField) {
                /*@var $customField crm_CustomField */
                $fieldName = $customField->fieldname;
                $customFieldsLayout->addItem(
                    $this->labelStr($customField->name, $this->deal->$fieldName)
                );
            }

            $this->addItem(
                $customFieldsLayout
            );
        }
    }
}


/**
 * @return Widget_Form
 */
class crm_DealEditor extends crm_Editor
{
    protected $deal;

    protected $organization;


    public function __construct(Func_Crm $crm, crm_Deal $deal = null, crm_Organization $organization = null, $id = null, Widget_Layout $layout = null)
    {
        $this->deal = $deal;
        $this->organization = $organization;

        parent::__construct($crm, $id, $layout);

        $this->setName('deal');


        $this->setCancelAction($crm->Controller()->Deal()->cancel());
        $this->setSaveAction($crm->Controller()->Deal()->save());

        $this->setHiddenValue('tg', bab_rp('tg'));

        if (isset($deal)) {
            $this->setValues($deal, array('deal'));
        }
    }


    protected function prependFields()
    {
        $W = crm_widgets();
        $Crm = $this->Crm();

        $Crm->includeEntrySet();

        $nameFormItem = $this->labelledField(
                $Crm->translate('Name'),
                $W->LineEdit()
                    ->setSize(55)
                    ->addClass('widget-fullwidth')
                    ->setMandatory(true, $Crm->translate('You must specify the deal name.')),
                'name'
            );
        $descriptionFormItem = $this->labelledField(
                $Crm->translate('Description'),
                $W->TextEdit()
                    ->setLines(2)
                    ->setColumns(73)
                    ->addClass('widget-fullwidth', 'widget-autoresize'),
                'description'
            );


        $this->imagesFormItem = $W->ImagePicker()
                ->oneFileMode()
                ->setTitle($Crm->translate('Set Photo'));



        $organizationFormItem =
            $this->labelledField(
                $Crm->translate('Client organization'),
                $suggestOrganization = $Crm->Ui()->SuggestOrganization()
                    ->addClass('widget-fullwidth')
                    ->setMinChars(0)
                    ->setSize(35)->setMaxSize(255),
                'lead_name'
            );
        if ($this->organization instanceof crm_Organization) {
            $this->setValue(array('deal', 'lead_name'), $this->organization->name);
        }

        $amountFormItem =
            $this->labelledField(
                $Crm->translate('Amount'),
                $W->LineEdit()
                    ->setSize(8)
                    ->setMaxSize(8),
                'amount'
            );

        $amountIsTaxInclusiveFormItem =
            $this->labelledField(
                $Crm->translate('Taxes'),
                $W->Select()
                    ->addOption('0', $Crm->translate('Exclusive of tax'))
                    ->addOption('1', $Crm->translate('Tax inclusive')),
                'amountIsTaxInclusive'
            );

        $originFormItem =
            $this->labelledField(
                $Crm->translate('Deal origin'),
                $Crm->Ui()->SuggestEntry(crm_Entry::DEAL_ORIGINS)
                    ->addClass('widget-fullwidth')
                    ->setMinChars(0)
                    ->setSize(75),
                'origin'
            );

        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestcontact.class.php';
        $suggestContact = new crm_SuggestContact('suggest_referral_contact');
        $suggestContact->setCrm($this->Crm());

        $referralContactFormItem =
            $this->labelledField(
                $Crm->translate('Organization\'s referral contact'),
                $suggestContact// = $Crm->Ui()->SuggestContact()
                    ->setRelatedOrganization($suggestOrganization)
                    ->addClass('widget-fullwidth')
                    ->setMinChars(0)
                    ->setSize(35),
                'referralContact_name'
            );


        $Crm->Ui()->includeAddress();
        $addressFormItem = crm_addressEditorFrame($Crm, true, false, true, true)
                                ->setName('address');

        $yearFormItem =
            $this->labelledField(
                $Crm->translate('Year'),
                $W->LineEdit()->setSize(4)->setMaxSize(4)->disable(),
                'year'
        );

        $numberFormItem =
            $this->labelledField(
                $Crm->translate('Number'),
                $W->LineEdit()->setSize(4)->setMaxSize(6)->disable(),
                'number'
        );



        $collaboratorFormItem = $this->labelledField(
            $Crm->translate('Responsible collaborator'),
            $Crm->Ui()->SuggestCollaborators(),
            'responsible_name'
        );



        $this->addItem(
            $W->VBoxItems(
                $W->HBoxItems(
                    $nameFormItem,
                    $W->HBoxItems(
                        $yearFormItem,
                        $numberFormItem
                    )->setHorizontalSpacing(1, 'em')
                    ->setSizePolicy(Widget_SizePolicy::MINIMUM)
                )->setHorizontalSpacing(5, 'em')
                ->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                $descriptionFormItem
            )->setVerticalSpacing(1, 'em')
        );


        $this->addItem(
            $W->HBoxItems(
                $organizationFormItem,
                $referralContactFormItem
            )->setSizePolicy(Widget_SizePolicy::MAXIMUM)

            ->setHorizontalSpacing(1, 'em')
        );

        $this->addItem($originFormItem);


        $this->addItem(
            $W->HBoxItems(
                $amountFormItem,
                $amountIsTaxInclusiveFormItem
            )->setHorizontalSpacing(1, 'em')
        );

        $this->addItem(
            $collaboratorFormItem
        );

    //	$dealEditor->addItem($W->Title($Crm->translate('Address'), 2));

        $this->addItem(
            $W->Section(
                $Crm->translate('Address'),
                $W->VBoxItems(
                    $addressFormItem
                )
            )->setFoldable(true)
        );




    }


    /**
     * @param	crm_Deal|array		$deal
     * @param	array 				$namePathBase
     *
     * @return 	crm_DealEditor
     */
    public function setValues($deal, $namePathBase = array())
    {
        if ($deal instanceof crm_Deal) {

            if (!empty($deal->id)) {
                $uploadPath = $deal->getPhotoUploadPath();
                $this->imagesFormItem->setFolder($uploadPath);
            }

            $dealValues = $deal->getValues();
            if ($deal->address) {
                $this->Crm()->includeAddressSet();
                $dealAddress = $deal->address();
                $this->setHiddenValue('deal[address]', $dealAddress->id);
                $dealValues['address'] = $dealAddress->getValues();
            } else {
                $dealValues['address'] = null;
            }

            $this->setValues(array('deal' => $dealValues));

            $referralContact = $deal->referralContact();
            if ($referralContact instanceof crm_Contact) {
                $this->setValue(array('deal', 'referralContact_name'), $referralContact->getFullName());
            }

            $responsibleContact = $deal->responsible();
            if ($responsibleContact instanceof crm_Contact) {
                $this->setValue(array('deal', 'responsible_name'), $responsibleContact->getFullName());
            }

            if (!empty($deal->id)) {
                $this->setHiddenValue('deal[id]', $deal->id);
            }
            if (!empty($deal->lead)) {
                $this->Crm()->includeOrganizationSet();
                $this->setValue(array('deal', 'lead_name'), $deal->lead()->name);
            }
            return $this;
        } else {
            return parent::setValues($deal, $namePathBase);
        }

    }

}




/**
 * Generates and returns an image representing the specified deal.
 *
 * @param crm_Deal $deal              The deal to generate a photo of.
 * @param int      $width             Width in px.
 * @param int      $height            Height in px.
 * @param bool     $border            True to add a 1px light-grey border around the image.
 * @param bool     $defaultToLeadLogo True fallback to the deal's lead logo if the deal has
 *                                    no specific photo.
 *
 * @return Widget_Image
 */
function crm_dealPhoto(crm_Deal $deal, $width, $height, $border = true, $defaultToLeadLogo = false)
{
    $W = bab_Widgets();
    $T = @bab_functionality::get('Thumbnailer');

    $image = $W->Image();

    if ($T) {
        $photoPath = $deal->getPhotoPath();

        $photoDone = false;

        if (isset($photoPath)) {
             $T->setSourceFile($photoPath->toString());
            $photoDone = true;
        } else if ($defaultToLeadLogo) {
            $deal->Crm()->includeOrganizationSet();
            $lead = $deal->lead();
            if (isset($lead)) {
                $logoPath = $lead->getLogoPath();
                if (isset($logoPath)) {
                    $T->setSourceFile($logoPath->toString());
                    $photoDone = true;
                }
            }
        }

        if (!$photoDone) {
            $addon = bab_getAddonInfosInstance('LibCrm');
             $T->setSourceFile($addon->getStylePath() . 'images/no-image.png');
        }

        if ($border) {
            $padWidth = min(array(2, round(min(array($width, $height)) / 24)));
            $T->setBorder(1, '#cccccc', $padWidth, '#ffffff');
        }
        $imageUrl = $T->getThumbnail($width, $height);
        $image->setUrl($imageUrl);
    }

    $image->setTitle($deal->name);

    $image->addClass('crm-deal-image crm-element-image small');

    return $image;
}




/**
 *
 * @param 	crm_Deal 	$deal
 * @param	string		$taskurl
 * @param	string		$taskparam			name of parameter in url for task ID
 *
 * @return Widget_Timeline
 */
function crm_DealTimeline(crm_Deal $deal, $taskurl, $taskparam)
{
    $Crm = $deal->Crm();
    $Access = $Crm->Access();
    $Ui = $Crm->Ui();
    $W = bab_Widgets();

    $Ui->includeTask();

    $taskSet = $Crm->TaskSet();
    $tasks = $taskSet->selectLinkedTo($deal);

    $timeline = $W->Timeline();

    $timeline->addBand(new Widget_TimelineBand('10%',  Widget_TimelineBand::INTERVAL_DAY, 36, Widget_TimelineBand::TYPE_OVERVIEW));
    $timeline->addBand(new Widget_TimelineBand('80%', Widget_TimelineBand::INTERVAL_WEEK, 36 * 7, Widget_TimelineBand::TYPE_ORIGINAL));
    $timeline->addBand(new Widget_TimelineBand('10%', Widget_TimelineBand::INTERVAL_MONTH, 150, Widget_TimelineBand::TYPE_OVERVIEW));


    require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';

    $widgetAddonInfo = bab_getAddonInfosInstance('widgets');
    $baseIconPath = $GLOBALS['babUrl'] . $widgetAddonInfo->getTemplatePath() . 'timeline/timeline_js/images/';
    $myNormalIcon = $baseIconPath . 'dark-blue-circle.png';
    $otherNormalIcon = $baseIconPath . 'dull-blue-circle.png';
    $myOverdueIcon = $baseIconPath . 'dark-red-circle.png';
    $otherOverdueIcon = $baseIconPath . 'dull-red-circle.png';
    $myCompleteIcon = $baseIconPath . 'dark-green-circle.png';
    $otherCompleteIcon = $baseIconPath . 'dull-green-circle.png';

    $now = BAB_DateTime::now();
    $nowIso = $now->getIsoDate();

    $timeline->addMonthSpans($now->getYear() - 1);
    $timeline->addMonthSpans($now->getYear());
    $timeline->addMonthSpans($now->getYear() + 1);

    foreach ($tasks as $task) {
        $task = $task->targetId;
        /* @var $task crm_Task */

        $responsibleName = bab_getUserName($task->responsible, true);


        if ($task->scheduledStart != '0000-00-00 00:00:00') {
            $start = BAB_DateTime::fromIsoDateTime($task->scheduledStart);
            $periodStart = substr($task->scheduledStart, 0, 16);
        } else {
            $start = BAB_DateTime::fromIsoDateTime($task->dueDate);
            $periodStart = $task->dueDate . ' 12:00';
        }
        $period = $timeline->createMilestone($start);
        $period->start = $periodStart;

        $period->setBubbleContent(
            $W->VBoxItems(
                $W->Link($W->Title($task->summary, 2), $GLOBALS['babUrl'] . $Crm->Controller()->Task()->display($task->id)->url()),
                $W->Html($Crm->translate('Responsible:') . ' <b>' . bab_toHtml($responsibleName) . '</b>'),
                $W->Html($task->description),
                $taskActions = $W->FlowItems(
                    $W->Link($Crm->translate('Details...'), $Crm->Controller()->Task()->display($task->id))
                    ->addClass('widget-actionbutton')
                )
                ->setHorizontalSpacing(1, 'em')
            )->setVerticalSpacing(0.5, 'em')
        );

        if ($Access->updateTask($task)) {
            $taskActions->addItem(
                $W->Link($Crm->translate('Edit...'), $Crm->Controller()->Task()->edit($task->id))
                ->addClass('widget-actionbutton')
            );
        }



        if ($task->responsible == $GLOBALS['BAB_SESS_USERID']) {
            $period->classname = 'crm-personal-task';
            if ($task->completion >= 100) {
                $period->icon = $myCompleteIcon;
            } else  {
                if ($nowIso > $task->dueDate) {
                    $period->icon = $myOverdueIcon;
                } else {
                    $period->icon = $myNormalIcon;
                }
            }
        } else {
            $period->classname = 'crm-other-task';
            if ($task->completion >= 100) {
                $period->icon = $otherCompleteIcon;
            } else  {
                if ($nowIso > $task->dueDate) {
                    $period->icon = $otherOverdueIcon;
                } else {
                    $period->icon = $otherNormalIcon;
                }
                $period->setTitle($task->summary);
            }
        }
        $period->setTitle($task->summary);

        $timeline->addPeriod($period);
    }

    $dealStatusHistorySet = $Crm->DealStatusHistorySet();
    $dealStatusHistorySet->status();

    $dealStatusHistories = $dealStatusHistorySet->select(
        $dealStatusHistorySet->deal->is($deal->id)
    )->orderDesc($dealStatusHistorySet->date);

    $currentStatusDone = false;
    foreach ($dealStatusHistories as $dealStatusHistory) {

        $start = BAB_DateTime::fromIsoDateTime($dealStatusHistory->date);
        $period = $timeline->createMilestone($start);

        // We set a special css class on the current status.
        if ($dealStatusHistory->status->id == $deal->status()->id) {
            $period->classname = 'crm-current-status';
            $period->start = $dealStatusHistory->date . ' 12:01'; // To ensure it will be above other statuses at the same date
            $currentStatusDone = true;
        } else {
            $period->start = $dealStatusHistory->date . ' 12:00';
        }

        $period->setTitle($Crm->translate('Status:') . ' ' . $dealStatusHistory->status->name);
        $period->setBubbleContent(
            $W->VBoxItems(
                $W->Title($Crm->translate('Status:') . ' ' . $dealStatusHistory->status->name, 2),
                $W->Html($Crm->translate('On') . ' <b>' . $dealStatusHistorySet->date->output($dealStatusHistory->date) . '</b>'),
                $W->Html($Crm->translate('Updated by:') . ' <b>' . bab_toHtml(bab_getUserName($dealStatusHistory->modifiedBy, true)) . '</b>'),
                $W->Html(bab_toHtml($dealStatusHistory->comment)),
                $taskActions = $W->FlowItems()
                    ->setHorizontalSpacing(1, 'em')
            )->setVerticalSpacing(0.5, 'em')
        );

        if ($Access->updateDeal($deal)) {
            $taskActions->addItem(
                $W->Link($Crm->translate('Edit'), $Crm->Controller()->Deal()->editStatusHistory($dealStatusHistory->id))
                ->addClass('widget-actionbutton')
            );
            $taskActions->addItem(
                $W->Link($Crm->translate('Delete'), $Crm->Controller()->Deal()->deleteStatusHistory($dealStatusHistory->id))
                ->setConfirmationMessage($Crm->translate('Are you sure you want to delete this status?'))
                ->addClass('widget-actionbutton')
            );
        }

        $timeline->addPeriod($period);
    }

    return $timeline;
}



function crm_dealPlanningList(crm_Deal $deal, $taskurl, $taskparam)
{
    $Crm = $deal->Crm();
    $W = bab_Widgets();

    $taskSet = $Crm->TaskSet();
    $tasks = $taskSet->selectLinkedTo($deal);

    $elements = array();

    foreach ($tasks as $task) {
        $task = $task->targetId;
        /* @var $task crm_Task */

        if ($task->scheduledStart != '0000-00-00 00:00:00') {
            $start = substr($task->scheduledStart, 0, 10);
        } else {
            $start = $task->dueDate;
        }

        $elements[] = array(
            'date' => $start,
            'items' => array(
                $W->Label(bab_shortDate(bab_mktime($task->dueDate), false))
                    ->addClass('widget-strong'),
                $W->Link($task->summary, $Crm->Controller()->Task()->display($task->id)),
                $W->FlowItems(
                    $W->Link(
                        $Crm->translate('Edit'),
                        $Crm->Controller()->Task()->edit($task->id)
                    )->addClass('widget-small', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                )->addClass(Func_Icons::ICON_LEFT_SYMBOLIC)
            )
        );
    }

    $dealStatusHistorySet = $Crm->DealStatusHistorySet();
    $dealStatusHistorySet->status();

    $dealStatusHistories = $dealStatusHistorySet->select(
        $dealStatusHistorySet->deal->is($deal->id)
    )->orderDesc($dealStatusHistorySet->date)
    ->orderDesc($dealStatusHistorySet->id);

    foreach ($dealStatusHistories as $dealStatusHistory) {

        $elements[] = array(
            'date' => $dealStatusHistory->date,

            'items' => array(
                $W->Label(bab_shortDate(bab_mktime($dealStatusHistory->date), false))
                    ->addClass('widget-strong'),
                $W->Label($dealStatusHistory->status->name)
                    ->addClass('widget-strong', 'icon', $dealStatusHistory->status->icon),
                $W->FlowItems(
                    $W->Link(
                        $Crm->translate('Edit'),
                        $Crm->Controller()->Deal()->editStatusHistory($dealStatusHistory->id)
                    )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $W->Link(
                        $Crm->translate('Delete'),
                        $Crm->Controller()->Deal()->deleteStatusHistory($dealStatusHistory->id)
                    )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setConfirmationMessage($Crm->translate('Are you sure you want to delete this status?'))
                )->addClass('icon-left-symbolic icon-left icon-symbolic')
            )
        );

    }

    $tableView = $W->TableView();
    $tableView->addClass('crm-list', Func_Icons::ICON_LEFT_16);

    $tableView->addSection('body', null, 'widget-table-body');
    $tableView->setCurrentSection('body');

    bab_Sort::asort($elements, 'date');
    $elements = array_reverse($elements);

    $row = 0;
    foreach ($elements as $element) {
        $tableView->addRowClass($row, 'crm-list-element');
        $items = $element['items'];
        for ($i = 0; $i < count($items); $i++) {
            $tableView->addItem($items[$i], $row, $i);
        }
        $row++;

    }

    $tableView->addColumnClass(0, 'widget-col-10em');
    $tableView->addColumnClass(2, 'widget-column-right');

    return $tableView;
}



/**
 * @return Widget_Select
 */
function crm_DealStatusSelector(Func_Crm $crm, $includeRoot = true)
{
    $W = bab_Widgets();
    $statusSelector = $W->Select();

    $statusLevels = array('1' => 0);
    if ($includeRoot) {
        $statusSelector->addOption('1', $crm->translate('All status'));
        $offset = 0;
    } else {
        $offset = 1;
    }


    $statusSet = $crm->StatusSet();
    $statuses = $statusSet->select($statusSet->id->greaterThan(1))->orderAsc($statusSet->lf);
    foreach ($statuses as $status) {
        if ($status->name == '') {
            continue;
        }
        if ($status->id_parent) {
            $statusLevel = $statusLevels[$status->id_parent] + 1;
        } else {
            $statusLevel = 0;
        }
        $statusLevels[$status->id] = $statusLevel;
        $statusName = str_repeat(bab_nbsp(), 4 * ($statusLevel - $offset)) . $status->name;
        $statusSelector->addOption($status->id, $statusName);
        $statusSelector->addOptionClass($status->id, 'crm-status-level' . $statusLevel);
    }

    return $statusSelector;
}





/**
 * Creates a generic form fragment for the specified set.
 *
 * @return Widget_Item
 */
function crm_dealFilterForm(Func_Crm $Crm, $classifications = array())
{
    $W = bab_Widgets();

//	require_once dirname(__FILE__) . '/classification.ui.php';

    $layout = $W->FlowLayout();
    $layout->setVerticalSpacing(1, 'em')->setHorizontalSpacing(1, 'em');

//	$layout->addItem(
//		$W->VBoxItems(
//			$W->Label($Crm->translate('Search')),
//			$W->LineEdit()->setSize(20)->setName('_general_')
//		)
//	);

    $layout->addItem(
        $W->VBoxItems(
            $W->Label($Crm->translate('Deal')),
            $W->LineEdit()->setSize(20)->setName('name')
        )
    );

    $layout->addItem(
        $W->VBoxItems(
            $W->Label($Crm->translate('Status')),
            crm_DealStatusSelector($Crm)->setName('status')
        )
    );

    /* Statutes are from now on in a table mbd_status and mbd_dealstatus: several statutes can be linked to a deal */
//	$statusSelect = $W->Select()
//		->addOption('_ALL_', '');
//	$values = $Crm->DealSet()->status->getValues();
//	foreach ($values as $key => $value) {
//		$statusSelect->addOption($key, $value);
//	}
//	$statusSelect->setValue('_ALL_');
//	$layout->addItem(
//		$W->VBoxItems(
//			$W->Label($Crm->translate('Status')),
//			$statusSelect->setName('status')
//		)
//	);
    $layout->addItem(
        $W->VBoxItems(
            $W->Label($Crm->translate('Client organization')),
            $Crm->Ui()->SuggestOrganization()->setSize(20)->setName('lead/name')
        )
    );

//	$layout->addItem(
//		$W->Frame(null,
//			$W->VBoxItems(
//				$W->Label($Crm->translate('Status')),
//				$Crm->Ui()->SuggestStatus()->setMinChars(0)->setName('status')->setSize(50)
//			)
//		)
//	);

    $layout->addItem(
        $W->Frame(null,
            $W->VBoxItems(
                $W->Label($Crm->translate('Classification')),
                $Crm->Ui()->SuggestClassification()->setMinChars(0)->setName('classification')->setSize(50)
            )
        )
    );

    $layout->addItem(
        $W->Frame(
            null,
            $W->VBoxItems(
                $W->Label($Crm->translate('Tag')),
                $Crm->Ui()->SuggestTag()->setName('tag')
            )
        )
    );

    return $layout;
}


/**
 * Creates a specific filter panel containing the table and an auto-generated form.
 *
 * @param widget_TableModelView $table
 * @param array					$filter
 * @param string				$name
 * @return Widget_Filter
 */
function crm_dealFilteredTableView(Func_Crm $Crm, crm_DealTableView $table, $filter = null, $name = null)
{
    $W = bab_Widgets();

    $filterPanel = $W->Filter();
    $filterPanel->setLayout($W->VBoxLayout());
    if (isset($name)) {
        $filterPanel->setName($name);
    }

    if ($table->getPageLength() === null) {
        $table->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : 12);
    }

     if (isset($filter['pageNumber'])) {
        $table->setCurrentPage($filter['pageNumber']);
     } elseif ($page = $W->getUserConfiguration($table->getId() . '/page', 'widgets', true)) {
         $table->setCurrentPage($page);
     } elseif ($table->getCurrentPage() === null) {
         $table->setCurrentPage(0);
     }

    $table->sortParameterName = $name . '[filter][sort]';

    if (isset($filter['sort'])) {
        $table->setSortField($filter['sort']);
    } elseif ($sort = $W->getUserConfiguration($table->getId() . '/sort', 'widgets', true)) {
        $table->setSortField($sort);
    } elseif ($table->getSortField() === null) {
        $columns = $table->getVisibleColumns();
        foreach ($columns as $sort => $column) {
            if ($column->getField()) {
                $table->setSortField($sort);
                break;
            }
        }
    }


    $classifications = isset($filter['classification']) ? $filter['classification'] : array();


    $layout = crm_dealFilterForm($Crm, $classifications);

    $form = $W->Form();
    $form->setReadOnly(true);
    $form->setName('filter');
    $form->setLayout($layout);
    $form->colon();

    $form->addItem($W->SubmitButton()->setLabel($Crm->translate('Filter')));

    if (isset($filter) && is_array($filter)) {
        $form->setValues($filter, array($name, 'filter'));
    }
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', bab_rp('idx'));

    $filterPanel->setFilter($form);
    $filterPanel->setFiltered($table);

    return $filterPanel;
}
