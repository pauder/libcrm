<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





bab_Widgets()->includePhpClass('Widget_Frame');
bab_Widgets()->includePhpClass('Widget_Link');
bab_Widgets()->includePhpClass('widget_TableView');




class crm_ContactTableView extends crm_TableModelView
{

    /**
     * @var crm_ContactOrganizationSet
     */
    protected $contactOrganizationSet;

    /**
     * @var crm_TagSet
     */
    protected $tagSet;


    /**
     * @param Func_Crm $Crm
     * @param string   $id
     */
    public function __construct(Func_Crm $Crm, $id = null)
{
        parent::__construct($Crm, $id);

        $Crm = $this->Crm();

        if (isset($Crm->Organization) && isset($Crm->ContactOrganization)) {
            $this->contactOrganizationSet = $Crm->ContactOrganizationSet();
            $this->contactOrganizationSet->organization();
        }

        if (isset($Crm->Tag)) {
            $this->tagSet = $Crm->TagSet();
        }
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $contactSet)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        $contactSet->address();
        $contactSet->address->country();

        $this->addColumn(widget_TableModelViewColumn('image', ' ')
                ->setSortable(false)->setExportable(false)->setSelectable(true, $Crm->translate('Photo'))
                ->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($contactSet->lastname, $Crm->translate('Last name')));
        $this->addColumn(widget_TableModelViewColumn($contactSet->firstname, $Crm->translate('First name')));
        $this->addColumn(widget_TableModelViewColumn($contactSet->address->street, $Crm->translate('Number / pathway'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($contactSet->address->postalCode, $Crm->translate('Postal code')));
        $this->addColumn(widget_TableModelViewColumn($contactSet->address->city, $Crm->translate('City')));
        $this->addColumn(widget_TableModelViewColumn($contactSet->address->cityComplement, $Crm->translate('Complement'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($contactSet->address->country->getNameField(), $Crm->translate('Country'))->setSearchable(true)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($contactSet->email, $Crm->translate('Email'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($contactSet->phone, $Crm->translate('Phone'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($contactSet->mobile, $Crm->translate('Mobile phone'))->setSearchable(false)->setVisible(false));
        if (isset($Crm->Tag)) {
            $this->addColumn(widget_TableModelViewColumn('tag', $Crm->translate('Tags'))->setSearchable(true)->setVisible(true));
        }

        if (isset($contactSet->deliveryaddress) && $contactSet->deliveryaddress instanceof crm_AdressSet) {
            $contactSet->deliveryaddress();
            $contactSet->deliveryaddress->country();

            $this->addColumn(widget_TableModelViewColumn($contactSet->deliveryaddress->street, $Crm->translate('Number / pathway (delivery)'))->setSearchable(false)->setVisible(false));
            $this->addColumn(widget_TableModelViewColumn($contactSet->deliveryaddress->postalCode, $Crm->translate('Postal code (delivery)'))->setSearchable(false)->setVisible(false));
            $this->addColumn(widget_TableModelViewColumn($contactSet->deliveryaddress->city, $Crm->translate('City (delivery)'))->setSearchable(false)->setVisible(false));
            $this->addColumn(widget_TableModelViewColumn($contactSet->deliveryaddress->cityComplement, $Crm->translate('Complement (delivery)'))->setSearchable(false)->setVisible(false));
            $this->addColumn(widget_TableModelViewColumn($contactSet->deliveryaddress->country->getNameField(), $Crm->translate('Country (delivery)'))->setSearchable(false)->setVisible(false));
        }

        if ($access->manageMailingLists())
        {
            $this->addColumn(widget_TableModelViewColumn('_mailinglists_', $Crm->translate('Mailing Lists'))->setExportable(false)->setVisible(false));
        }

        $this->addColumn(widget_TableModelViewColumn($contactSet->createdOn, $Crm->translate('Created on'))->setSearchable(false)->setVisible(false));
    }


    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	crm_ContactSet	$contactSet        Deprecated argument, use setRecordSet()
     * @param	array			$filter
     * @return ORM_Criteria
     */
    public function getFilterCriteria($filter = null, crm_ContactSet $contactSet = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if (!isset($contactSet)) {
            $contactSet = $this->getRecordSet();
        }

        // Initial conditions are base on read access rights.
        $conditions = $Access->canPerformActionOnSet($contactSet, 'contact:read');

        if (isset($filter['import']) && !empty($filter['import'])) {
            $importRecord = $Crm->ImportSet()->get($filter['import']);
            if ($importRecord) {
                $linkSet = $Crm->LinkSet();

                $linkSet->joinTarget($Crm->ContactClassName());

                $conditions = $conditions->_AND_(
                    $contactSet->id->in(
                        $linkSet->sourceId->is($importRecord->id)->_AND_(
                            $linkSet->sourceClass->is(get_class($importRecord))
                        )->_AND_(
                            $linkSet->targetClass->is($contactSet->getRecordClassName())
                        ),
                        'targetId'
                    )
                );
            }
            unset($filter['import']);
        }


        if (isset($filter['organization/name']) && !empty($filter['organization/name'])) {
            $organizationNames = explode(',', $filter['organization/name']);
            $organizationNameConditions = array();
            $contactOrganizationSet = $Crm->ContactOrganizationSet();
            $contactOrganizationSet->organization();
            if (count($organizationNames) > 1) {
                foreach ($organizationNames as $organizationName) {
                    $organizationNameConditions[] = $contactSet->id->in(
                        $contactOrganizationSet->organization->name->like($organizationName)
                        ->_AND_($contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan(date('Y-m-d'))))
                        ->_AND_($contactOrganizationSet->organization->deleted->is(0))
                    );
                }
                $conditions = $conditions->_AND_(ORM_Or($organizationNameConditions));

            } else {
                $conditions = $conditions->_AND_(
                    $contactSet->id->in(
                        $contactOrganizationSet->organization->name->contains($organizationNames[0])
                        ->_AND_($contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan(date('Y-m-d'))))
                        ->_AND_($contactOrganizationSet->organization->deleted->is(0))
                    )
                 );
            }
            unset($filter['organization/name']);
        }


        if (isset($filter['address/country_ID']) && !empty($filter['address/country_ID'])) {
            $conditions = $conditions->_AND_(
                $contactSet->address->country()->code->is($filter['address/country_ID'])
            );
        } elseif (isset($filter['address/country']) && !empty($filter['address/country'])) {
            $conditions = $conditions->_AND_(
                $contactSet->address->country()->name_fr->startsWith($filter['address/country'])
            );
        }


        if (isset($filter['classification']) && !empty($filter['classification'])) {
            $classificationSet = $Crm->ClassificationSet();
            $classification = $classificationSet->get($classificationSet->name->is($filter['classification']));
            if (isset($classification)) {
                $parent = $classification->id_parent();
                $conditions = $conditions
                    ->_AND_(
                        $contactSet->hasImpliedClassification($filter['classification'])
                        ->_OR_($contactSet->hasImpliedClassification($parent->name))
                    );
            }
        }

        if (isset($Crm->Tag) && isset($filter['tag']) && !empty($filter['tag'])) {
            $conditions = $conditions->_AND_($contactSet->haveTagLabels($filter['tag']));
        }

        if (isset($filter['acquaintance']) && !empty($filter['acquaintance'])) {
            if ($filter['acquaintance'] === 'all') {
                $filter['acquaintance'] = array('1', '2', '3');
            }
            $conditions = $conditions->_AND_($contactSet->isKnownBy($Access->currentContact()->id, $filter['acquaintance']));
        }

        if (isset($filter['position']) && !empty($filter['position'])) {
            if (!isset($contactOrganizationSet)) {
                $contactOrganizationSet = $Crm->ContactOrganizationSet();
            }
            $conditions = $conditions->_AND_(
                $contactSet->id->in(
                    $contactOrganizationSet->position->contains($filter['position'])
                )
            );
        }

        if ($Access->manageMailingLists() && isset($filter['_mailinglists_']) && !empty($filter['_mailinglists_'])) {
            /* @var $ML Func_MailingList */
            $ML = crm_MailingList();
            $contacts = $ML->getListSubscriptions($filter['_mailinglists_']);
            $filterContact = array();
            $this->columns['_mailinglists_']->setVisible(true);
            foreach ($contacts as $contact) {
                $filterContact[] = $contact->getEmail();
            }
            $conditions = $conditions->_AND_($contactSet->email->in($filterContact));
        }

        $conditions = $conditions->_AND_(parent::getFilterCriteria($filter));

        return $conditions;
    }




    /**
     * @return Widget_Action
     */
    protected function getDisplayAction(crm_Contact $record)
    {
        return $this->Crm()->Controller()->Contact()->display($record->id);
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::computeCellTextContent()
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        if ($record instanceOf crm_Link) {
             $contact = $record->targetId;
             $fieldPath = mb_substr($fieldPath, 9);
        } else {
             $contact = $record;
        }

        static $ML = null;
        $access = $this->Crm()->Access();
        if ($access->manageMailingLists() && $ML === null)
        {
            /* @var $ML Func_MailingList */
            $ML = crm_MailingList();
        }

        switch ($fieldPath) {
            case '_mailinglists_':
                if ($ML !== null && $record->email) {
                    $c = $ML->newContact();
                    $c->setEmail($record->email);
                    $lists = $ML->getContactSubscriptions($c);
                    return implode(', ', $lists);
                }
                return '';
            case 'position':
                $contactOrganization = $this->getContactOrganization($contact);
                if (isset($contactOrganization)) {
                    return $contactOrganization->position;
                }
                return '';

            case 'organization/name':
                $contactOrganization = $this->getContactOrganization($contact);
                if (isset($contactOrganization)) {
                    return $contactOrganization->organization->getName();
                }
                return '';

            case 'tag':
                $tags = $this->tagSet->selectFor($contact);
                $values = array();
                foreach ($tags as $tag) {
                    $values[] = $tag->targetId->label;
                }
                return implode(', ', $values);

            case 'classification':
                $classifications = $contact->getClassifications();
                return implode(', ', $classifications);
        }

        return parent::getRecordFieldValue($record, $fieldPath);
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::computeCellContent($record, $fieldPath)
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        if ($record instanceOf crm_Link) {
            $contact = $record->targetId;
            $fieldPath = mb_substr($fieldPath, 9);
        } else {
            $contact = $record;
        }

        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();

        static $ML = null;
        $access = $this->Crm()->Access();
        if ($access->manageMailingLists() && $ML === null)
        {
            /* @var $ML Func_MailingList */
            $ML = crm_MailingList();
        }

        $displayAction =  $this->getDisplayAction($record);

        switch ($fieldPath) {
            case '_mailinglists_':
                if ($ML !== null && $record->email) {
                    $layout = $W->FlowLayout()->setHorizontalSpacing(.25, 'em')->addClass(Func_Icons::ICON_LEFT_16);

                    $c = $ML->newContact();
                    $c->setEmail($record->email);
                    $lists = $ML->getContactSubscriptions($c);

                    foreach ($lists as $id => $list) {
                        $layout->addItem(
                            $W->Link(
                                $W->Icon(
                                    $list,
                                    Func_Icons::ACTIONS_EDIT_DELETE
                                )->setTitle($Crm->translate('Remove from this mailing list.')),
                                $Crm->Controller()->Contact()->deleteFromMailinglist($record->id, $id)
                            )->setAjaxAction(
                                $Crm->Controller()->Contact()->deleteFromMailinglist($record->id, $id)
                            )->setConfirmationMessage($Crm->translate('This user will be removed from this mailing list ?'))
                        );
                    }
                    return $layout;
                }
                return $W->Label('');

            case 'lastname':
                return $W->Link(
                    $this->computeCellTextContent($contact, $fieldPath),
                    $displayAction
                );

            case 'image':
                return $W->Link(
                    $Ui->ContactPhoto($contact, 22, 22, false),
                    $displayAction
                );

            case 'address/country/name_fr':
            case 'address/country/name_en':
                $flag = $W->CountryFlag($contact->address->country->code);
                if ($flag) {
                    return $W->HBoxItems(
                        $flag,
                        $W->Label($contact->address->country->getName())
                    )->setHorizontalSpacing(.3,'em');
                }
                break;

            case 'organization/name':
                $contactOrganizationSet = $this->contactOrganizationSet;

                $contactOrganizations = $contactOrganizationSet->select(
                    $contactOrganizationSet->contact->is($contact->id)
                        ->_AND_($contactOrganizationSet->organization->deleted->is(0))
                );
                $contactOrganizations
                    ->orderDesc($contactOrganizationSet->main);	;

                $cellContent = $W->VBoxLayout();//->setHorizontalSpacing(1, 'em');

                $otherOrganizations = $W->FlowLayout()->setHorizontalSpacing(1, 'em');

                if ($contactOrganizations->count() > 1) {
                    $otherOrganizations->addItem($W->Label('et')->addClass('crm-small'));
                }

                $first = true;
                foreach ($contactOrganizations as $contactOrganization) {
                    $organizationLink = $W->Link(
                        $contactOrganization->organization->name,
                        $Crm->Controller()->Organization()->display($contactOrganization->organization->id)
                    );

                    if ($first) {
                        $cellContent->addItem($organizationLink);
                    } else {
                        $otherOrganizations->addItem($organizationLink->addClass('crm-small'));
                    }
                    $first = false;
                    if ($contactOrganization->history_to != '0000-00-00') {
                        if ($contactOrganization->history_to < date('Y-m-d')) {
                            $organizationLink->addClass('crm-deleted');
                        }
                        if ($contactOrganization->history_from != '0000-00-00') {
                            $position = sprintf(
                                $Crm->translate('%s from %s until %s'),
                                $contactOrganization->position,
                                bab_shortDate(bab_mktime($contactOrganization->history_from), false),
                                bab_shortDate(bab_mktime($contactOrganization->history_to), false)
                            );
                        } else {
                            $position = sprintf(
                                $Crm->translate('%s until %s'),
                                $contactOrganization->position,
                                bab_shortDate(bab_mktime($contactOrganization->history_to), false)
                            );
                        }
                    } else {
                        $position = $contactOrganization->position;
                    }
                    $organizationLink->setTitle($position);
                }
                $cellContent->addItem($otherOrganizations);
                return $cellContent;
        }

        return parent::computeCellContent($contact, $fieldPath);
    }




    /**
     * Main contact organization
     * @param crm_Contact $contact
     * @param crm_ContactOrganization
     */
    protected function getContactOrganization(crm_Contact $contact)
    {
        $contactOrganizationSet = $this->contactOrganizationSet;
        $contactOrganizations = $contactOrganizationSet->select(
            $contactOrganizationSet->contact->is($contact->id)
                ->_AND_($contactOrganizationSet->organization->deleted->is(0))
        );
        $contactOrganizations->orderDesc($contactOrganizationSet->main);

        foreach ($contactOrganizations as $contactOrganization) {
            return $contactOrganization;
        }

        return null;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleFilterInputWidget()
     */
    protected function handleFilterInputWidget($name, ORM_Field $field = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        if ($name === 'tag') {
            if (isset($Crm->Tag)) {
                return $Ui->SuggestTag();
            }
        }
        if ($name === '_mailinglists_') {
            $W = bab_Widgets();
            /* @var $select Widget_Select */
            $select = $W->Select()
                ->addOption('', '')
                ->setName('_mailinglists_');

            $ML = crm_MailingList();
            $lists = $ML->getLists();

            foreach($lists as $id => $name) {
                $subs = $ML->getListSubscriptions($id);
                $select->addOption(
                    $id,
                    $name. ' (' . sprintf($this->Crm()->translate('%s contacts'),count($subs)).')'
                );
            }

            return $select;
        }
        return parent::handleFilterInputWidget($name, $field);
    }
}





/**
 * Contact table view, list contacts with column name defined to match outlook
 *
 */
class crm_OutlookContactTableView extends crm_ContactTableView
{


    /**
     * @return array
     */
    protected function getOutlookColumns()
    {
        return array(

            0  => 'First Name',
            1  => 'Middle Name',
            2  => 'Last Name',
            3  => 'Title',
            4  => 'Suffix', 				// Vide
            5  => 'Initials', 				// Vide
            6  => 'Web Page', 				// Vide
            7  => 'Gender', 				// Vide
            8  => 'Birthday',				// Vide
            9  => 'Anniversary',			// Vide
            10  => 'Location',				// Vide
            11  => 'Language',				// Vide
            12  => 'Internet Free Busy',	// Vide
            13  => 'Notes',					// Vide
            14  => 'E-mail Address',
            15  => 'E-mail 2 Address', 		// Vide
            16  => 'E-mail 3 Address', 		// Vide
            17  => 'Primary Phone',
            18  => 'Home Phone',
            19  => 'Home Phone 2',			// Vide
            20  => 'Mobile Phone',
            21  => 'Pager',					// Vide
            22  => 'Home Fax',				// Vide
            23  => 'Home Address',
            24  => 'Home Street',
            25  => 'Home Street 2',			// Vide
            26  => 'Home Street 3',			// Vide
            27  => 'Home Address PO Box',	// Vide
            28  => 'Home City',
            29  => 'Home State',
            30  => 'Home Postal Code',
            31  => 'Home Country',
            32  => 'Spouse',				// Vide
            33  => 'Children',				// Vide
            34  => 'Manager\'s Name', 		// Vide
            35  => 'Assistant\'s Name',		// Vide
            36  => 'Referred By',			// Vide
            37  => 'Company Main Phone',	// Vide
            38  => 'Business Phone',
            39  => 'Business Phone 2',		// Vide
            40  => 'Business Fax',
            41  => 'Assistant\'s Phone',	// Vide
            42  => 'Company',
            43  => 'Job Title',
            44  => 'Department',
            45  => 'Office Location',		// Vide
            46  => 'Organizational ID Number', // Vide
            47  => 'Profession',			// Vide
            48  => 'Account',				// Vide
            49  => 'Business Address',
            50  => 'Business Street',
            51  => 'Business Street 2', 	// Vide
            52  => 'Business Street 3',		// Vide
            53  => 'Business Address PO Box',	// Vide
            54  => 'Business City',
            55  => 'Business State',
            56  => 'Business Postal Code',
            57  => 'Business Country',
            58  => 'Other Phone',			// Vide
            59  => 'Other Fax',				// Vide
            60  => 'Other Address',			// Vide
            61  => 'Other Street',			// Vide
            62  => 'Other Street 2',	 	// Vide
            63  => 'Other Street 3',		// Vide
            64  => 'Other Address PO Box',	// Vide
            65  => 'Other City',			// Vide
            66  => 'Other State',			// Vide
            67  => 'Other Postal Code',		// Vide
            68  => 'Other Country',			// Vide
            69  => 'Callback',				// Vide
            70  => 'Car Phone',				// Vide
            71  => 'ISDN',					// Vide
            72  => 'Radio Phone',			// Vide
            73  => 'TTY/TDD Phone',			// Vide
            74  => 'Telex',					// Vide
            75  => 'User 1',
            76  => 'User 2',
            77  => 'User 3',
            78  => 'User 4',				// Vide
            79  => 'Keywords',				// Vide
            80  => 'Mileage',				// Vide
            81  => 'Hobby',					// Vide
            82  => 'Billing Information',	// Vide
            83  => 'Directory Server',		// Vide
            84  => 'Sensitivity',			// Vide
            85  => 'Priority',				// Ce champ aura toujours la valeur "Normal"
            86  => 'Private', 				// Vide
            87  => 'Categories'
        );
    }


    protected function getFieldFromName(crm_ContactSet $contactSet, $pos, $name)
    {
        switch($name) {
            case 'First Name':          return $contactSet->firstname;
            case 'Last Name':           return $contactSet->lastname;
            case 'Title':               return $contactSet->title;
            case 'E-mail Address':      return $contactSet->email;
            case 'Primary Phone':       return $contactSet->phone;
            case 'Mobile Phone':        return $contactSet->mobile;
            case 'Business Phone':      return $contactSet->phone;
            case 'Business Fax':        return $contactSet->fax;
            case 'Company':             return 'organization/name';
            case 'Job Title':           return 'mainPosition';
            case 'Business Address':    return $contactSet->address;
            case 'Business Street':     return $contactSet->address->street;
            case 'Business City':       return $contactSet->address->city;
            case 'Business State':      return $contactSet->address->state;
            case 'Business Postal Code':return $contactSet->address->postalCode;
            case 'Business Country':    return $contactSet->address->country->name_fr;
        }

        return '_empty'.$pos;
    }


    /**
     * {@inheritDoc}
     * @see crm_ContactTableView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $contactSet)
    {
        $cols = $this->getOutlookColumns();

        foreach($cols as $pos => $name) {
            $this->addColumn(widget_TableModelViewColumn($this->getFieldFromName($contactSet, $pos, $name), $name)->setSearchable(false));
        }
    }
}








/**
 * List of contacts for knowing a specified contact
 */
class crm_contactAcquaintances extends crm_UiObject
{

    protected function Title()
    {
        $W = bab_Widgets();
        return $W->Title($this->Crm()->translate('Who knows this contact'), 4)
            ->setSizePolicy(Widget_SizePolicy::MAXIMUM);
    }


    protected function ContactTitle(crm_OrganizationPerson $organizationPerson)
    {
        $return = $organizationPerson->getContact()->getFullName();

        $position = $organizationPerson->getMainPosition();
        if (!empty($position)) {
            $return  .= ' - ' . $this->Crm()->translate($position);
        }

        return $return;
    }





    public function organizationsBox($contact)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $box = $W->VBoxLayout();

        $contactOrganizationSet = $this->contactOrganizationSet;
        $contactOrganizationSet->contact();
        $contactOrganizationSet->organization();

        $contactOrganizations = $contactOrganizationSet->select(
            $contactOrganizationSet->contact->is($contact->id)
            ->_AND_($contactOrganizationSet->organization->deleted->is(0))
        );
        $contactOrganizations->orderDesc($contactOrganizationSet->main);	;

        $otherOrganizations = $W->FlowLayout()->setHorizontalSpacing(1, 'em');

        if ($contactOrganizations->count() > 1) {
            $otherOrganizations->addItem($W->Label('et')->addClass('crm-small'));
        }

        $first = true;
        foreach ($contactOrganizations as $contactOrganization) {
            $organizationLink = $W->Link(
                $contactOrganization->organization->name,
                $Crm->Controller()->Organization()->display($contactOrganization->organization->id)
            );


            if ($first) {
                $box->addItem($organizationLink);
            } else {
                $otherOrganizations->addItem($organizationLink->addClass('crm-small'));
            }
            $first = false;
            if ($contactOrganization->history_to != '0000-00-00') {
                if ($contactOrganization->history_to < date('Y-m-d')) {
                    $organizationLink->addClass('crm-deleted');
                }
                if ($contactOrganization->history_from != '0000-00-00') {
                    $position = sprintf(
                        $Crm->translate('%s from %s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->history_from), false),
                        bab_shortDate(bab_mktime($contactOrganization->history_to), false)
                    );
                } else {
                    $position = sprintf(
                        $Crm->translate('%s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->history_to), false)
                    );
                }
            } else {
                $position = $contactOrganization->position;
            }
            $organizationLink->setTitle($position);
        }
        $box->addItem($otherOrganizations);

        return $box;
    }







    /**    /**
     *
     * @param crm_Contact   $contact
     * @param   int             $limit          Maximum contact to display (default 50)
     *
     * @return widget_Frame
     */
    public function getFrame(crm_Contact $contact, $limit = 50)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Crm->Ui()->includeContact();

        if (!isset($Crm->ContactAcquaintance)) {
            return null;
        }

        $contactFrame = $W->Section(
            $Crm->translate('Who knows this contact'),
            $W->VBoxLayout(),
            4
        );

        $contactAquaintances = $contact->selectContactsKnowing();

        $max = $limit;

        $level = null;
        $nb = 0;
        foreach ($contactAquaintances as $contactAquaintance) {
            $organizationPerson = $contactAquaintance->contact();
            if (!isset($organizationPerson)) {
                continue;
            }

            $contact = $organizationPerson->getContact();

            $contactCard = $Crm->Ui()->ContactCardFrame(
                $organizationPerson,
                crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_ADDRESS)
            );
            $contactCard->setImageWidth(24);
            $contactCard->setImageHeight(24);

            if ($level != $contactAquaintance->level) {
                $level = $contactAquaintance->level;
                $levelSection = $W->Section(
                    sprintf($this->Crm()->translate('Level %s'), $level),
                    $contactBox = $W->VBoxItems(),
                    6
                )->setFoldable(true);
                $contactFrame->addItem($levelSection);
            }
            $contactCard->setSizePolicy('widget-list-element');
            $contactBox->addItem($contactCard);

            $nb++;
            $max--;

            $remains = $contactAquaintances->count() - $limit;

            if ($max <= 0 && $remains > 1) {

                $item = $this->Remains($contact, $remains);

                if (null !== $item) {
                    $contactFrame->addItem($item);
                }

                break;
            }
        }

        if ($nb == 0) {
            return null;
        }

        return $contactFrame;
    }






    /*
     * Called if the getFrame method return not all the contacts
     *
     * @param	crm_Contact			$contact
     * @param 	int 				$remains
     * @return Widget_Item
     */
    protected function Remains(crm_Contact $contact, $remains)
    {
        $W = bab_Widgets();
        return $W->Label(sprintf($this->Crm()->translate('And %d contacts not displayed'), $remains));
    }

}



/**
 * Contact card frame
 * Display a contact in organization
 *
 * @see Func_Crm::ContactCardFrame
 */
class crm_ContactCardFrame extends crm_CardFrame
{
    const SHOW_PHOTO = 1;
    const SHOW_NAME = 2;
    const SHOW_ORGANIZATION = 4;
    const SHOW_POSITION = 8;
    const SHOW_DEPARTEMENT = 16;
    const SHOW_ADDRESS = 32;
    const SHOW_EMAIL = 64;
    const SHOW_PHONE = 128;
    const SHOW_MOBILE = 256;
    const SHOW_FAX = 512;

    const SHOW_ALL = 0xFFFF;

    const SHOW_MINIMUM = 0x1F;

    /**
     *
     * @var crm_ContactOrganization
     */
    protected $contactOrganization;

    /**
     * @var crm_Contact
     */
    protected $contact;

    protected $attributes;

    private $imageWidth = null;
    private $imageHeight = null;


    /**
     * @param Func_Crm $crm
     * @param crm_OrganizationPerson $organizationPerson
     * @param unknown $attributes
     * @param unknown $id
     */
    public function __construct(Func_Crm $crm, crm_OrganizationPerson $organizationPerson, $attributes = crm_ContactCardFrame::SHOW_ALL, $id = null)
    {
        parent::__construct($crm, $id);
        if ($organizationPerson instanceOf crm_contactOrganization) {
            $this->contactOrganization = $organizationPerson;
        }

        $this->contact = $organizationPerson->getContact();
        $this->attributes = $attributes;

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $this->item->setCacheHeaders(Widget_Item::CacheHeaders()->setLastModified(BAB_DateTime::fromIsoDateTime($this->contact->modifiedOn)));
    }


    protected function getController()
    {
        return $this->Crm()->Controller()->Contact();
    }


    /**
     * {@inheritDoc}
     * @see crm_UiObject::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if ($this->contact->id) {
            $this->init($this->Crm(), $this->getId());
        }
        return parent::display($canvas);
    }


    /**
     * @return Widget_VBoxLayout
     */
    protected function organizationsBox()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $contact = $this->contact;

        $box = $W->VBoxLayout();

        $contactOrganizationSet = $Crm->ContactOrganizationSet();
        $contactOrganizationSet->contact();
        $contactOrganizationSet->organization();

        $contactOrganizations = $contactOrganizationSet->select(
            $contactOrganizationSet->contact->is($contact->id)
            ->_AND_($contactOrganizationSet->organization->deleted->is(0))
        );
        $contactOrganizations->orderDesc($contactOrganizationSet->main);

        $otherOrganizationsBox = $W->VBoxLayout()->setHorizontalSpacing(1, 'em');

        $currentBox = $box;
        foreach ($contactOrganizations as $contactOrganization) {
            $organizationLink = $W->FlowItems(
                $W->Link(
                    $contactOrganization->organization->name,
                    $Crm->Controller()->Organization()->display($contactOrganization->organization->id)
                )
            );
            $organizationLink->addClass('widget-small');

            $currentBox->addItem($organizationLink);
            $currentBox = $otherOrganizationsBox;
            if ($contactOrganization->history_to != '0000-00-00') {
                if ($contactOrganization->history_to < date('Y-m-d')) {
                    $organizationLink->addClass('crm-deleted');
                }
                if ($contactOrganization->history_from != '0000-00-00') {
                    $position = sprintf(
                        $Crm->translate('%s from %s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->history_from), false),
                        bab_shortDate(bab_mktime($contactOrganization->history_to), false)
                    );
                } else {
                    $position = sprintf(
                        $Crm->translate('%s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->history_to), false)
                    );
                }
            } else {
                $position = $contactOrganization->position;
            }
            if (trim($position) !== '') {
                $position .=  ',' . bab_nbsp();
            }
            $organizationLink->addItem($W->Label($position)->addClass('widget-strong'), 0);
        }
        $box->addItem($otherOrganizationsBox);

        return $box;
    }


    /**
     * @param Func_Crm $crm
     * @param string $id
     */
    protected function init($crm, $id)
    {
        $W = bab_Widgets();

        $frameLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        $dataLayout = $W->VBoxLayout();

        if ($icon = $this->Icon()) {
            $cardLayout = $W->HBoxItems(
                $dataLayout->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                $icon->setSizePolicy(Widget_SizePolicy::MINIMUM)
            )->setHorizontalSpacing(1, 'em');
        } else {
            $cardLayout = $dataLayout;
        }

        if ($fullName = $this->Fullname()) {
            $dataLayout->addItem($fullName);
        }

        if (isset($crm->Organization)) {
            $dataLayout->addItem($this->organizationsBox());
        }

        $dataLayout->addItem($this->handleContent());
        $frameLayout->addItem($cardLayout);

        $this->setLayout($frameLayout);

        $this->addClass('vcard');
    }



    protected function getAction()
    {
        return $this->getController()->display($this->contact->id);
    }


    /**
     * @return Widget_Item
     */
    protected function Fullname()
    {
        if (!($this->attributes & self::SHOW_NAME)) {
            return null;
        }

        $W = bab_Widgets();
        $access = $this->Crm()->Access();
        $action = $this->getAction();

        if (!isset($action) || !$access->viewContact($this->contact)) {
            return $W->Label($this->contact->getFullName())
                ->addClass('fn crm-card-title');
        }

        return $W->Link(
            $this->contact->getFullName(),
            $action
        )->addClass('fn crm-card-title');
    }


    /**
     * Data under fullname
     * @return Widget_Item
     */
    protected function handleContent()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $layout = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');

        if ($address = $this->Address()) {
            $layout->addItem($address);
        }

        if ($Crm->onlineShop && !$Crm->noDelivery && $deliveryaddress = $this->DeliveryAddress()) { // online shop
            $layout->addItem($deliveryaddress);
        }

        if ($web = $this->Web()) {
            $layout->addItem($web);
        }

        if ($phones = $this->Phones()) {
            $layout->addItem($phones);
        }

        return $layout;
    }



    protected function PasswordToken()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $set = $Crm->PasswordTokenSet();
        $token = $set->get($set->contact->is($this->contact->id));

        if (null === $token) {
            if ($this->contact->user) {
                return $W->Link(
                    $W->Icon($Crm->translate('Send a notification to set a new password'), Func_Icons::APPS_PREFERENCES_AUTHENTICATION),
                    $this->getController()->sendPasswordToken($this->contact->id)
                )->addClass(Func_Icons::ICON_LEFT_16);

            } else {

                return $W->Link(
                    $W->Icon($Crm->translate('Send a notification to create a new account'), Func_Icons::APPS_PREFERENCES_AUTHENTICATION),
                    $this->getController()->sendPasswordToken($this->contact->id)
                )->setConfirmationMessage(sprintf($Crm->translate('An email will be sent to %s with a link allowing this person to create an account linked to this contact, do you really want to continue?'), $this->contact->email))->addClass(Func_Icons::ICON_LEFT_16);
            }
        }


        $infos = $W->VBoxLayout()->addClass(Func_Icons::ICON_LEFT_16)->addClass('crm-contact-password-token')->setVerticalSpacing(.5,'em');
        $infos->addItem($W->Title($Crm->translate('A password token exists for contact'), 5));
        $infos->addItem($W->Label(sprintf($Crm->translate('Creation date : %s'), bab_shortDate(bab_mktime($token->createdOn)))));
        $infos->addItem($W->Link($Crm->translate('Link sent to user'), $token->getUrl()));
        $infos->addItem(
            $W->HBoxItems(
                $W->Link(
                    $W->Icon($Crm->translate('Send link by email'), Func_Icons::ACTIONS_MAIL_SEND),
                    $this->getController()->sendPasswordToken($this->contact->id)
                )->addClass('crm-dialog-button'),
                $W->Link(
                    $W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    $this->getController()->deletePasswordToken($this->contact->id)
                )->addClass('crm-dialog-button')
            )->setSpacing(0,'em',1,'em')
        );

        return $infos;
    }



    /**
     * Placeholder for organization and position informations
     *
     * @return Widget_Item | null
     */
    protected function OrganizationPosition()
    {
        $showOrganization = $this->attributes & self::SHOW_ORGANIZATION;
        $showPosition = $this->attributes & self::SHOW_POSITION;
        $W = bab_Widgets();

        $organization = $this->contact->getMainOrganization();
        $orgCtrl = $this->Crm()->Controller()->Organization();

        if (!$organization || !$orgCtrl) {
            $showOrganization = false;
        }


        if ($showOrganization) {
            $label = $W->Label($organization->name)->addClass('org');
            $orgLink = $W->Link($label, $orgCtrl->display($organization->id));
        }

        if ($showPosition) {
            $positionLabel = $W->Label($this->contact->getMainPosition());
        }

        if ($showOrganization && $showPosition) {
            return $W->VBoxItems(
                $orgLink,
                $positionLabel->addClass('crm-small')
            );
            //return $W->FlowItems($positionLabel, $W->Label($this->Crm()->translate('at')), $orgLink)->setHorizontalSpacing(0.4, 'em');
        } else if ($showOrganization) {
            return $orgLink;
        } else if ($showPosition) {
            return $positionLabel;
        }

        return null;
    }


    /**
     * @return string
     */
    protected function AddressTitle()
    {
        return $this->Crm()->translate('Address');
    }

    protected function OrganizationAddressTitle($organization)
    {
        return $this->Crm()->translate('Address') . ' (' .  $organization->name . ')';
    }



    /**
     * Placeholder for address
     *
     * @return Widget_Item | null
     */
    protected function Address()
    {
        if (!($this->attributes & self::SHOW_ADDRESS)) {
            return null;
        }

        if (($mainAddress = $this->contact->getMainAddress()) && !$mainAddress->isEmpty()) {
            return $this->TitledAddress($mainAddress, $this->AddressTitle());
        } else if (($mainOrganization = $this->contact->getMainOrganization()) && ($mainAddress = $mainOrganization->getMainAddress()) && !$mainAddress->isEmpty()) {
            return $this->TitledAddress($mainAddress, $this->OrganizationAddressTitle($mainOrganization));
        }

        return null;
    }





    /**
     * Placeholder for address
     *
     * @return Widget_Item | null
     */
    protected function DeliveryAddress()
    {
        $Crm = $this->Crm();

        if (!($this->attributes & self::SHOW_ADDRESS)) {
            return null;
        }

        if (($deliveryAddress = $this->contact->getDeliveryAddress()) && !$deliveryAddress->isEmpty()) {

            return $this->TitledAddress($deliveryAddress, $Crm->translate('Delivery address'));
        }

        return null;
    }



    /**
     *
     * @return array	label1 => array(phone1,phone2,...), label2 => array(phone3,...)
     */
    protected function getPhones()
    {
        $Crm = $this->Crm();

        $phone = $this->contact->getMainPhone();
        $phone2 = $this->contact->getAlternatePhone();

        if ($phone || $phone2) {

            if ($phone && $phone2) {
                return array($Crm->translate('Phone') => array($phone, $phone2));
            } else if ($phone2) {
                $phone = $phone2;
            }
            return array($Crm->translate('Phone') => array($phone));

        } else if (($mainOrganization = $this->contact->getMainOrganization()) && ($phone = $mainOrganization->getMainPhone())) {

            return array($Crm->translate('Phone') . ' (' .  $mainOrganization->name . ')' => array($phone));
        }

        return array();

    }

    /**
     * Placeholder for phone numbers, mobile, fax ...
     *
     * @return Widget_Item | null
     */
    protected function Phones()
    {
        $W = bab_Widgets();

        $phonesLayout = $W->FlowLayout()->setVerticalAlign('top')->setHorizontalSpacing(2, 'em')->setVerticalSpacing(0.25, 'em');

        if (($this->attributes & self::SHOW_PHONE)) {

             $phones = $this->getPhones();

            foreach ($phones as $phoneLabel => $phoneNumbers) {
                $phoneNumbersVBox = $W->VBoxLayout();
                foreach ($phoneNumbers as $phoneNumber) {
                    $numberTrimmed = str_replace(array(' ', '-'), array('', ''), $phoneNumber);
                    $phoneNumbersVBox->addItem($W->Link($phoneNumber, 'tel:' . $numberTrimmed));
                }
                $phonesLayout->addItem(
                    $this->labelStr($phoneLabel, $phoneNumbersVBox)
                );
            }

        }

        if (($this->attributes & self::SHOW_MOBILE) && ($mobile = $this->contact->getMainMobilePhone())) {
            $numberTrimmed = str_replace(array(' ', '-'), array('', ''), $mobile);
            $mobile = $W->Link($mobile, 'tel:' . $numberTrimmed);
            if ($mobile2 = $this->contact->getAlternateMobilePhone()) {
                $numberTrimmed = str_replace(array(' ', '-'), array('', ''), $mobile2);
                $mobile = $W->VBoxItems($mobile, $W->Link($mobile2, 'tel:' . $numberTrimmed));
            }
            $phonesLayout->addItem(
                $this->labelStr($this->Crm()->translate('Mobile'), $mobile)
            );
        }

        if (($this->attributes & self::SHOW_FAX)) {
            if ($fax = $this->contact->getMainFax()) {
                $phonesLayout->addItem(
                    $this->labelStr($this->Crm()->translate('Fax'), $fax)
                );
            } else if (($mainOrganization = $this->contact->getMainOrganization()) && ($fax = $mainOrganization->getMainFax())) {
                $phonesLayout->addItem(
                    $this->labelStr($this->Crm()->translate('Fax') . ' (' .  $mainOrganization->name . ')', $fax)
                );
            }
        }

        return $phonesLayout;
    }


    /**
     * Placeholder for email address, web url, IM ...
     *
     * @return Widget_Item | null
     */
    protected function Web()
    {
        $W = bab_Widgets();

        if (($this->attributes & self::SHOW_EMAIL) && ($email = $this->contact->getMainEmail())) {
//			return $W->Link($email, $this->Crm()->mailTo($email));
            return $this->labelStr($this->Crm()->translate('Email'), $W->Link($email, $this->Crm()->mailTo($email)));
        }

        return null;
    }



    /**
     * Display online status
     * @return Widget_Item
     */
    protected function Online()
    {
        $W = bab_Widgets();

        if ($time = $this->contact->isOnline()) {

            $duration = time() - $time;

            return $W->Frame()
                ->addClass('icon-left-16 icon-16x16 icon-left')
                ->addItem(
                    $W->Icon($this->Crm()->translate('This contact is online'), Func_Icons::STATUS_DIALOG_INFORMATION)
                    ->setTitle(sprintf($this->Crm()->translate('Last click %d seconds ago'), $duration))
                );
        }

        return null;
    }


    public function setImageWidth($imageWidth)
    {
        $this->imageWidth = $imageWidth;
        return $this;
    }

    public function setImageHeight($imageHeight)
    {
        $this->imageHeight = $imageHeight;
        return $this;
    }


    public function getImageWidth()
    {
        if (isset($this->imageWidth)) {
            return $this->imageWidth;
        }
        return self::IMAGE_WIDTH;
    }

    public function getImageHeight()
    {
        if (isset($this->imageHeight)) {
            return $this->imageHeight;
        }
        return self::IMAGE_HEIGHT;
    }


    /**
     * @return Widget_Item | null
     */
    protected function Icon()
    {
        if (!($this->attributes & self::SHOW_PHOTO)) {
            return null;
        }

        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $Ui = $Crm->Ui();

        $photo = $Ui->ContactPhoto($this->contact, $this->getImageWidth(), $this->getImageHeight());

        if (!$Access->viewContact($this->contact)) {
            return $photo;
        }
        return $W->Link(
            $photo,
            $this->getController()->display($this->contact->id)
        );
    }



    /**
     * Display account informations
     * @return Widget_Item | null
     */
    protected function User()
    {
        if (!$this->contact->user) {
            return null;
        }

        $nickname = bab_getUserNickname($this->contact->user);
        if (!$nickname) {
            return null;
        }

        $W = bab_Widgets();

        $entry = bab_getUserInfos($this->contact->user);
        $link = bab_getUserDirEntryLink($this->contact->user);

        if ($link) {
            $item = $W->Link($W->Icon($nickname, Func_Icons::ACTIONS_USER_PROPERTIES), $link)->setOpenMode(Widget_Link::OPEN_POPUP);
        } else {
            $item = $W->Icon($nickname, Func_Icons::ACTIONS_USER_PROPERTIES);
        }

        if ($entry['disabled']) {
            $labelStr = $this->labelStr($this->Crm()->translate('The associated user account is disabled'), $item);
        } else {
            $labelStr = $this->labelStr($this->Crm()->translate('This contact can login with nickname'), $item);
        }


        $labelStr->setVerticalAlign('middle')->setSpacing(.5,'em')
            ->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);

        return $labelStr;
    }


    /**
     * @return Widget_Section
     */
    protected function Sessions()
    {
        if (!$this->contact->user) {
            return null;
        }

        // require ovidentia 8.5.96
        @include_once $GLOBALS['babInstallPath'].'utilit/statsessionsincl.php';
        if (!class_exists('bab_StatSessions')) {
            return null;
        }



        $sessions = new bab_StatSessions();
        $sessions->idUser = (int) $this->contact->user;

        $widget = $sessions->getWidget();

        if (!isset($widget)) {
            return null;
        }

        $W = bab_Widgets();
        $section = $W->Section(crm_translate('User sessions tracking'));
        $section->setFoldable(true, true);

        $section->addItem($widget);

        return $section;
    }




    /**
     * Widget_FlowLayout
     */
    protected function Tags()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (!isset($Crm->Tag)) {
            return null;
        }

        $Crm->Ui()->includeTag();

        $tagSet = $Crm->TagSet();
        $tags = $tagSet->selectFor($this->contact);
        $tagsDisplay = new crm_TagsDisplay($Crm, $tags, $this->contact, $this->getController()->displayListForTag());

        $addTagForm = $W->Form()
            ->setLayout($W->FlowLayout()->setVerticalAlign('middle')->setHorizontalSpacing(0.5, 'em'))
            ->setName('tag');
        $addTagForm->addItem($Crm->Ui()->SuggestTag()->setName('label'));
        $addTagForm->addItem($W->SubmitButton()->setAction($Crm->Controller()->Tag()->link())->setLabel($Crm->translate('Add')));
        $addTagForm->setHiddenValue('tg', bab_rp('tg'));
        $addTagForm->setHiddenValue('to', 'Contact:' . $this->contact->id);

        return $W->FlowItems(
            $W->Label($Crm->translate('Tags'))->colon()->addClass('crm-display-label'),
            $tagsDisplay,
            $addTagForm
        )->setHorizontalSpacing(0.5, 'em');
    }
}




/**
 * Full contact frame
 * all contact infos and link to organization(s)
 */
class crm_ContactFullFrame extends crm_ContactCardFrame
{

    public function __construct(Func_Crm $crm, crm_OrganizationPerson $organizationPerson, $attributes = crm_ContactCardFrame::SHOW_ALL, $id = null)
    {
        parent::__construct($crm, $organizationPerson, $attributes, $id);
    }


    protected function init($crm, $id)
    {
        $W = bab_Widgets();

        $frameLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        $cardLayout = $W->HBoxLayout()->setHorizontalSpacing(3, 'em');
        $dataLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

        if ($icon = $this->Icon()) {
            $cardLayout = $W->HBoxItems($icon, $dataLayout)->setHorizontalSpacing(1, 'em');
        } else {
            $cardLayout = $dataLayout;
        }

        $headerBox = $W->VBoxItems();
        $dataLayout->addItem($headerBox);

        if ($fullName = $this->Fullname()) {
            $headerBox->addItem($fullName);
        }

        if (isset($this->Crm()->Organization) && $org = $this->OrganizationPosition()) {
            $headerBox->addItem($org);
        }

        $dataLayout->addItem($this->handleContent());
        $frameLayout->addItem($cardLayout);

        $this->setLayout($frameLayout);

        $this->addClass('vcard');
    }

    /**
     * @return Widget_Item
     */
    protected function Fullname()
    {
        $W = bab_Widgets();
        return $W->Title($this->contact->getFullName(), 1)->addClass('fn');
    }



    /**
     * @return Widget_Item | null
     */
    protected function OrganizationPosition()
    {
        $W = bab_Widgets();

        $contactOrganizations = $this->contact->selectOrganizations(false);

        $vbox = $W->VBoxLayout();

        if (!isset($contactOrganizations)) {
            return $vbox;
        }


        foreach ($contactOrganizations as $contactOrganization) {
            $vbox->addItem(
                $W->FlowItems(
                    $W->Label($contactOrganization->position)->addClass('position')->addClass('role'),
                    $W->Label(empty($contactOrganization->position) ? '' : ',' . bab_nbsp()),
                    $W->Link(
                        $contactOrganization->organization->name,
                        $this->Crm()->Controller()->Organization()->display($contactOrganization->organization->id)
                    )
                        //->addClass('org')
                )//->setHorizontalSpacing(0.5, 'em')
            );
        }

        return $vbox;
    }


    /**
     * @return Widget_Item | null
     */
    protected function Icon()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $QR = @bab_functionality::get('QRCode');
        $T = @bab_functionality::get('Thumbnailer');
        if ($QR instanceof Func_QRCode && $T instanceof Func_Thumbnailer) {
            $QR->setModuleSize(1);
            $QR->setEccLevel(Func_QRCode::ECC_L);
            $image = $QR->getImage('QRCode');
            $qrPath = $this->contact->uploadPath();
            $qrPath->createDir();
            $qrPath->push('qricon.png');
            imagepng($image, $qrPath->toString());

            $T->setSourceFile($qrPath->toString());
            $T->setResizeMode(Func_Thumbnailer::KEEP_ASPECT_RATIO);

            $imageUrl = $T->getThumbnail(24, 24);

            $qrimage = $W->Image()->setUrl($imageUrl);

            $icon = $W->VBoxItems(
                $this->Crm()->Ui()->ContactPhoto($this->contact, 150, 150),
                $qrimage,
                $menu = $W->Menu()
            )->setTitle($Crm->translate('Click to get this contact card in QRCode.'));
            $menu->setLayout($W->VBoxLayout());
            $menu->addClass(Func_Icons::ICON_LEFT_16);
            $menu->addItem($this->qrCode(800));
            $menu->addEntry($Crm->translate('Download full vCard'), $Crm->Controller()->Contact()->vCard($this->contact->id), Func_Icons::OBJECTS_CONTACT);

            $menu->attachTo($qrimage);
        } else {
            $icon = $this->Crm()->Ui()->ContactPhoto($this->contact, 150, 150);
        }

        return $icon;
    }


    /**
    * @return Widget_Item | null
    */
    protected function qrCode($size = 150)
    {
        /* @var $QR Func_QRCode */
        $QR = bab_functionality::get('QRCode');
        $T = @bab_functionality::get('Thumbnailer');

        if (isset($QR) && isset($T)) {

            require_once dirname(__FILE__).'/../vcard.class.php';
            $W = bab_Widgets();

            $vCard = new crm_VCard();
            $vCard->setName($this->contact->lastname, $this->contact->firstname);


             if ($title = $this->contact->getMainPosition()) {
                 $vCard->addProperty('TITLE', $title);
             }

             $phones = $this->getPhones();
             if (count($phones) > 0) {
                 foreach ($phones as $phoneNumbers) {
                     $vCard->addTel('WORK', $phoneNumbers[0]);
                     break;
                 }
             }
             $vCard->addTel('WORK,CELL', $this->contact->getMainMobilePhone());
             $vCard->addEmail('WORK', $this->contact->getMainEmail());


             $addressDone = false;
             if ($organization = $this->contact->getMainOrganization()) {
                 $vCard->setOrganization($organization->name);

                 if ($workaddress = $organization->getMainAddress()) {
 //					$vCard->addAddress('WORK', $workaddress->street, null, $workaddress->postalCode, $workaddress->city, $workaddress->state, $workaddress->getCountry()->getName());
                     $addressDone = true;
                 }
             }
             if (!$addressDone) {
                 if ($workaddress = $this->contact->getMainAddress()) {
                     $vCard->addAddress('WORK', $workaddress->street, null, $workaddress->postalCode, $workaddress->city, $workaddress->state, $workaddress->getCountry()->getName());
                     $addressDone = true;
                 }
             }

//			bab_debug($vCard->toString());

             $QR->setModuleSize(3);
// 			$QR->setEccLevel(Func_QRCode::ECC_L);
             $image = $QR->getImage($vCard->toString());
            $qrPath = $this->contact->uploadPath();
            $qrPath->createDir();
            $qrPath->push('qrcode.png');
            imagepng($image, $qrPath->toString());

            $T->setSourceFile($qrPath->toString());
            $T->setResizeMode(Func_Thumbnailer::KEEP_ASPECT_RATIO);

            $imageUrl = $T->getThumbnail($size, $size);

            $image = $W->Image();
            $image->setUrl($imageUrl);
            return $image;
        }

        return null;
    }


    /**
     * Data under fullname
     * @return Widget_Item
     */
    protected function handleContent()
    {
        $Crm = $this->Crm();
        $layout = parent::handleContent();

        if ($classification = $this->Classification()) {
            $layout->addItem($classification);
        }

        if ($user = $this->User()) {
            $layout->addItem($user);
        }

        if ($online = $this->Online()) {
            $layout->addItem($online);
        }

        if ($tags = $this->Tags()) {
            $layout->addItem($tags);
        }

        if (isset($Crm->PasswordToken) && $passwordtoken = $this->PasswordToken()) {
            $layout->addItem($passwordtoken);
        }

        if ($newsletter = $this->Newsletter()) {
            $layout->addItem($newsletter);
        }

        if ($gender = $this->gender()) {
            $layout->addItem($gender);
        }

        if ($birthday = $this->birthday()) {
            $layout->addItem($birthday);
        }

        if ($sessions = $this->Sessions()) {
            $layout->addItem($sessions);
        }

        return $layout;
    }


    protected function gender()
    {
        if (empty($this->contact->gender))
        {
            return null;
        }

        $Crm = $this->Crm();
        $set = $this->contact->getParentSet();

        return $this->labelStr($Crm->translate('Gender'), $set->gender->output($this->contact->gender));
    }

    /**
     *
     */
    protected function birthday()
    {
        if (empty($this->contact->birthday) || $this->contact->birthday = '0000-00-00')
        {
            return null;
        }

        $Crm = $this->Crm();

        return $this->labelStr($Crm->translate('Birthday'), bab_shortDate(bab_mktime($this->contact->birthday), false));
    }


    /**
     *
     */
    protected function Newsletter()
    {
        $ML = crm_MailingList();
        if (false === $ML) {
            return null;
        }

        /*if (!$ML->isConfigured()) {
            return null;
        }*/

        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $W->Section(
            $Crm->translate('Newsletters subscriptions'),
            $W->DelayedItem($Crm->Controller()->Contact()->newsletterSubscriptions($this->contact->id))->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_ON_APPEAR),
            5
        )/*->setSubHeaderText($ML->getDescription(), 6)*/
        ->addClass('crm-contact-newsletter-subscriptions');


    }


    /**
     * @return Widget_FlowLayout
     */
    protected function Classification()
    {
        $W = bab_Widgets();

        $Crm = $this->Crm();

        if (!isset($Crm->Classification)) {
            return null;
        }

        $Crm->includeClassificationSet();

        $contactClassificationSet = $Crm->ContactClassificationSet();
        $contactClassificationSet->join('classification');
        $contactClassifications = $contactClassificationSet->selectForContact($this->contact->id, 'defined');

        if ($contactClassifications->count() === 0) {
            return $W->Label($Crm->translate('No codification'))->addClass('crm-display-label');
        }


        $classifications = $W->FlowLayout()->setSpacing(4, 'px');

        foreach ($contactClassifications as $contactClassification) {
            /* @var $dealClassification crm_DealClassification */

            $classificationLabel = $W->Label($contactClassification->classification->name)
                                        ->setTitle(implode(' > ', $contactClassification->classification->getPathname()))
                                        ->addClass('crm-codification');
            $classifications->addItem($classificationLabel);
        }

        return $this->labelStr($Crm->translate('Codification'), $classifications);
//		return $classifications;

    }



    /**
    * Display address with title and map link in a menu
    *
    * @param	crm_Address	$address
    * @param	string		$title
    * @param	string		$recipient
    *
    * @return Widget_Item
    */
    protected function TitledAddress(crm_Address $address, $title = null, $recipient = null)
    {
        $Ui = $this->Crm()->Ui();
        return $Ui->AddressCardFrame($address, $title, $recipient);
    }
}









/**
 *
 *
 */
class crm_ContactOrganizationTableView extends crm_TableModelView
{

    /**
     *
     * @var Widget_RadioSet
     */
    protected $radioSet;

    /**
     * @param	crm_Contact	$contact
     *
     */
    public function setContact(crm_Contact $contact = null) {

        $this->addClass('icon-16x16 icon-left icon-left-16');
        $W = bab_Widgets();

        if ($contact) {
            $I = $contact->selectOrganizations(true);
        } else {
            $oSet = $this->Crm()->OrganizationSet();
            $I = $oSet->select(new ORM_FalseCriterion());
        }


        $this->setDataSource($I);

        $cols = array(
                'organization/name' => $this->Crm()->translate('Organization'),
                'position' => $this->Crm()->translate('Position'),
                'history_to' => $this->Crm()->translate('Until')
        );


        if ($I->count() > 1) {
            $cols['main'] = $this->Crm()->translate('Main');
        }

        $cols['delete'] = $this->Crm()->translate('');

        $this->setVisibleColumns($cols);
        $this->setName('contactOrganization');


        $this->radioSet = $W->RadioSet()->setRadioNamePath(array('contact','contactOrganization','main'));
        if ($contact && ($contactOrganization = $contact->getMainContactOrganization())) {
            $this->radioSet->setValue($contactOrganization->id);
        }

        return $this;
    }



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleCell()
     */
    protected function handleCell(ORM_Record $record, $fieldPath, $row, $col, $name = null, $rowSpan = null, $colSpan = null)
    {
        return parent::handleCell($record, $fieldPath, $row, $col, $record->id);
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::computeCellContent()
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();

        $this->Crm()->Ui()->includeOrganization();

        if ($fieldPath === 'organization/name') {

            $access = $Crm->Access();
            if ($access->viewOrganization($record->organization)) {
                return $W->FlowItems(
                    $W->Link($Ui->OrganizationLogo($record->organization, 16, 16), $Crm->Controller()->Organization()->display($record->organization->id)),
                    $W->Link($record->organization->name, $Crm->Controller()->Organization()->display($record->organization->id))
                )->setVerticalAlign('middle')->setHorizontalSpacing(4, 'px');
            } else {
                return $W->Icon($record->organization->name, Func_Icons::ACTIONS_USER_GROUP_PROPERTIES);
            }
        }

        if ($fieldPath === 'position') {
            return $W->NamedContainer()
                ->addItem($W->Hidden()->setName('organization')->setValue($record->organization->id))
                ->addItem($Ui->SuggestPosition()
                    ->setMinChars(0)->setName('position')->setValue($record->position));
        }

        if ($fieldPath === 'history_from') {
            return $W->DatePicker()->setName('history_from')->setValue($record->history_from);
        }

        if ($fieldPath === 'history_to') {
            return $W->DatePicker()->setName('history_to')->setValue($record->history_to);
        }

        if ($fieldPath === 'main') {
            return $W->Radio(null, $this->radioSet)->setCheckedValue($record->id);
        }

        if ($fieldPath === 'delete') {
            return $W->Link('', $Crm->Controller()->Contact()->removeOrganization($record->id))
                ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                ->setConfirmationMessage($this->Crm()->translate('Are you sure you want to remove the contact position in this organization ?'));
        }

        return parent::computeCellContent($record, $fieldPath);
    }


    protected function addOrganization()
    {
        $W = bab_Widgets();
        return $W->Link('+', $this->Crm()->Controller()->Organization()->create())
            ->setOpenMode(Widget_Link::OPEN_DIALOG);
    }


    protected function initFooterRow($row)
    {
        parent::initFooterRow($row);

        static $newId = 0;
        $newId--;

        $W = bab_Widgets();

        $col = 0;
        foreach ($this->columns as $columnId => $column) {


            if ($columnId === 'organization/name') {

                $suggestOrganization = $this->Crm()->Ui()->SuggestOrganization('new_organization')
                    ->setIdName('organization')
                    ->setName('organizationname')
                    ->setSize(35)
                    ->setMinChars(0);
                $this->addItem(
                    $W->NamedContainer()->setName('' . $newId)
                        ->addItem($suggestOrganization)
                        ->addItem($this->addOrganization()),
                    0,
                    $col
                );


            } else if ($columnId === 'position') {

                $this->addItem($W->NamedContainer()->setName('' . $newId)->addItem($this->Position()), 0, $col);

            } else if ($columnId === 'history_to') {

                $this->addItem($W->NamedContainer()->setName('' . $newId)->addItem($W->DatePicker()->setName('history_to')), 0, $col);

            } else {

                $this->addItem($W->Label(''), 0, $col);

            }

            $col++;
        }

    }




    protected function Position()
    {

        return $this->Crm()->Ui()->SuggestPosition()
            ->setMinChars(0)
            ->setName('position');
    }

}



















/**
 *
 * @param crm_Contact 	$contact
 * @param int			$width
 * @param int			$height
 * @param bool			$border
 * @return string
 */
function crm_contactPhotoUrl(crm_Contact $contact, $width, $height, $border = true)
{
    /* @var $T Func_Thumbnailer */
    $T = @bab_functionality::get('Thumbnailer');

    if (!$T) {
        return null;
    }

    $photoPath = $contact->getPhotoPath();

    if (isset($photoPath)) {
         $T->setSourceFile($photoPath->toString());
         $T->setResizeMode(Func_Thumbnailer::CROP_CENTER);
    } else {
        $addon = bab_getAddonInfosInstance('LibCrm');
         $T->setSourceFile($addon->getStylePath() . 'images/contact-default.png');
    }

    if ($border) {
        $padWidth = min(array(2, round(min(array($width, $height)) / 24)));
        $T->setBorder(1, '#cccccc', $padWidth, '#ffffff');
    }

    $imageUrl = $T->getThumbnail($width, $height);

    return $imageUrl;
}


/**
 *
 * @param crm_Contact 	$contact
 * @param int			$width
 * @param int			$height
 * @param bool			$border
 * @return Widget_Displayable_Interface
 */
function crm_contactPhoto(crm_Contact $contact = null, $width = 32, $height = 32, $border = true)
{
    $W = bab_Widgets();

    /* @var $T Func_Thumbnailer */
    $T = @bab_functionality::get('Thumbnailer');

    if (!$T) {
        return null;
    }



    if (!isset($contact) || ($path = $contact->getPhotoPath()) === null)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        $addon = bab_getAddonInfosInstance('LibCrm');
        $path = new bab_Path($addon->getStylePath() . 'images/contact-default.png');
    }

    $image = $W->ImageThumbnail($path)->setThumbnailSize($width, $height);

    $image->setResizeMode(Func_Thumbnailer::CROP_CENTER);

    if (isset($contact)) {
        $image->setTitle($contact->getFullName());
    }
    $image->addClass('crm-contact-image crm-element-image small');

    return $image;
}




class crm_ContactExportEditor extends crm_Editor
{
    protected $contacts;

    protected $charset;

    public function __construct(Func_Crm $Crm, Array $contacts = null)
    {
        parent::__construct($Crm);
        $this->setName('export');

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setHiddenValue('export[contacts]', serialize($contacts));

        $this->contacts = $contacts;

        $this->colon();

        $this->addFields();
        $this->addButtons();
    }

    protected function addFields()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $this->charset = $this->charset();

        $this->addItem($this->format());
        $this->addItem($this->charset);
    }

    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $contactCtrl = $Crm->Controller()->Contact();

        $this->addButton($W->SubmitButton()
            ->setLabel(crm_translate('Export'))
            ->setAction($contactCtrl->export())
            ->setSuccessAction($contactCtrl->displayList($this->contacts))
            ->setFailedAction($contactCtrl->exportOptions($this->contacts))
        );
    }


    protected function format()
    {
        $W = $this->widgets;

        return $W->LabelledWidget(
            crm_translate('Format'),
            $W->Select()
                ->addOption('xlsx', crm_translate('XLSX (Excel 2007)'))
                ->addOption('xls', crm_translate('XLS (Excel 97, 2000, XP, 2003)'))
                ->addOption('csv', crm_translate('Csv (Coma separated)'))
                ->addOption('outlookcsv', crm_translate('Csv for outlook contacts'))
                ->setAssociatedDisplayable($this->charset, array('csv')),
            __FUNCTION__
        );
    }

    /**
     * @return Widget_LabelledWidget
     */
    protected function charset()
    {
        $W = $this->widgets;

        return $W->LabelledWidget(
            crm_translate('Content charset'),
            $W->Select()
            ->addOption('UTF-8', 'UTF-8')
            ->addOption('UTF-8 BOM', crm_translate('UTF-8 (with BOM)'))
            ->addOption('CP1252', 'CP1252 (Windows-1252)')
            ->addOption('ISO-8859-15', 'ISO-8859-15'),
            __FUNCTION__
        );
    }
}






/**
 * @return crm_Editor
 */
class crm_ContactEditor extends crm_Editor
{
    /**
     * @var crm_ContactSet
     */
    protected $contactSet;

    /**
     * @var crm_SuggestContact
     */
    protected $suggestContact = null;

    /**
     * @var crm_SuggestOrganization
     */
    protected $suggestOrganization = null;

    /**
     *
     * @var crm_Contact
     */
    protected $contact;

    protected $organization;


    protected $firstnameLineEdit;
    protected $lastnameLineEdit;


    public function __construct(Func_Crm $crm, crm_Contact $contact = null, crm_Organization $organization = null, $id = null, Widget_Layout $layout = null)
    {

        $this->contact = $contact;
        $this->organization = $organization;

        parent::__construct($crm, $id, $layout);
        $this->setName('contact');

        $this->colon();

        $this->addFields();
        $this->addButtons();

        $this->setHiddenValue('tg', bab_rp('tg'));

        if (isset($contact)) {
            if ($contact->id) {
                $this->setHiddenValue('contact[id]', $contact->id);
            }
            $this->setValues($contact, array('contact'));
        }
    }


    protected function addButtons()
    {
        $Crm = $this->Crm();

        $contactCtrl = $Crm->Controller()->Contact();

        if (null !== $contactCtrl)
        {
            // in some cases, the MyContact controller extend Contact controller,
            // mycontact is a restricted version of contact, in this case, the contact controller may be inaccessible

//            $this->setCancelAction($Crm->Controller()->Contact()->cancel());
            $this->setSaveAction($Crm->Controller()->Contact()->save());
            if (!empty($this->contact->id)) {
                $this->setFailedAction($Crm->Controller()->Contact()->edit());
            } else {
                $this->setFailedAction($Crm->Controller()->Contact()->add());
            }
        }
    }

    protected function addFields()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;


        $Crm->includeAddressSet();
        $Crm->Ui()->includeAddress();

        $this->contactSet = $Crm->ContactSet();
        $this->contactSet->join('address');

        $this->addItem($this->InformationsSection());

        if (isset($Crm->Organization))
        {
            $this->addItem($this->OrganizationsSection());
        }

        $this->addresses();

        if ($Crm->Access()->associateUserAccount()) {
            $this->addItem(
                $W->Section(
                    $Crm->translate('User account'),
                    $this->User($this->contact)
                )->setFoldable(true)
            );
        }
    }


    protected function addresses()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;


        $this->addItem(
            $W->Section(
                $Crm->translate('Address'),
                $W->VBoxItems(
                    $this->Address('address')
                )
            )->setFoldable(true)
        );


        if ($Crm->onlineShop && !$Crm->noDelivery)
        {
            $this->addItem(
                $delivery = $W->Section(
                    $Crm->translate('Delivery address'),
                    $W->VBoxItems(
                        $this->Address('deliveryaddress', null, false, true)
                    )
                )
            );

            if (($this->contact instanceof crm_Contact) && ($this->contact->deliveryaddress instanceof crm_Address) && $this->contact->deliveryaddress->isEmpty())
            {
                $delivery->setFoldable(true, true);
            } else {
                $delivery->setFoldable(true);
            }

        }
    }


    /**
     * @return Widget_Section
     */
    protected function InformationsSection()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $section = $this->createSection($Crm->translate('Contact information'));
        $section->addItem(
                $W->FlowItems(
                    $this->Title(),
                    $W->HBoxItems($this->Firstname(), $this->Lastname())->setHorizontalSpacing(1, 'em')
            )->setHorizontalSpacing(1, 'em')->setVerticalAlign('top')
        );
        $section->addItem($this->Phones());
        $section->addItem($this->Email());
        return $section;
    }


    /**
     * @return Widget_Section
     */
    protected function OrganizationsSection()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();

        if (isset($this->organization)) {
            $section = $this->createSection($Crm->translate('Organization'));
            $section->addItem(
                $W->NamedContainer('contactOrganization')->addItem(
                        $W->NamedContainer('-1')
                            ->addItem($this->Position())
                            ->addItem($W->Hidden()->setName('organization')->setValue($this->organization->id))
                )
            );
            $section->addItem(
                    $this->Assistant()
            );
        } else {
            $section = $this->createSection($Crm->translate('Organizations'));
            $section->addItem(
                $this->Organizations($this->contact)
            );
        }

        return $section;
    }



    protected function Collaborator()
    {
        $Crm = $this->Crm();

        $orgs = array();
        if ($currentContact = $Crm->Access()->currentContact()) {

            if ($mainOrganization = $currentContact->getMainOrganization()) {
                $orgs[$mainOrganization->id] = $mainOrganization->name;

                $descendantIds = array();
                $mainOrganization->getDescendants($descendantIds);
                $organizationSet = $Crm->OrganizationSet();
                $descendants = $organizationSet->select($organizationSet->id->in($descendantIds));

                foreach ($descendants as $descendant) {
                    $orgs[$descendant->id] = $descendant->name;
                }
            }
        }

        return
            $this->labelledField(
                $Crm->translate('Responsible collaborator'),
                $Crm->Ui()->SuggestContact()
                    ->setRelatedOrganization($orgs)
                    ->addClass('widget-fullwidth')
                    ->setMinChars(0)
                    ->setSize(75),
                'responsible_name'
            );
    }


    protected function Phones()
    {
        $W = bab_Widgets();

        return $W->FlowItems(
            $this->BusinessPhone(),
            $this->MobilePhone(),
            $this->Fax()
        )->setHorizontalSpacing(1, 'em');

    }

    protected function Email()
    {
        $W = bab_Widgets();
        return $this->labelledField(
            $this->Crm()->translate('Email address'),
            $W->EmailLineEdit()
                    ->addClass('widget-fullwidth')
                    ->setSize(50),
            'email'
        );
    }


    /**
     * @return Widget_LabelledWidget
     */
    protected function BusinessPhone()
    {
        $W = $this->widgets;

        return $this->labelledField(
            $this->Crm()->translate('Phone'),
            $W->TelLineEdit()->setSize(20)->setTelType(Widget_TelLineEdit::BUSINESS),
            'phone'
        );
    }

    protected function MobilePhone()
    {
        $W = $this->widgets;

        return $this->labelledField(
            $this->Crm()->translate('Mobile'),
            $W->TelLineEdit()->setSize(20)->setTelType(Widget_TelLineEdit::MOBILE),
            'mobile'
        );
    }

    protected function Fax()
    {
        $W = $this->widgets;

        return $this->labelledField(
            $this->Crm()->translate('Fax'),
            $W->TelLineEdit()->setSize(20)->setTelType(Widget_TelLineEdit::FAX),
            'fax'
        );
    }




    protected function gender()
    {
        return $this->labelledField(
                $this->Crm()->translate('Gender'),
                crm_OrmWidget($this->contactSet->gender),
                __FUNCTION__
        );
    }

    protected function Title()
    {
        return $this->labelledField(
            $this->Crm()->translate('Title'),
            crm_OrmWidget($this->contactSet->title),
            'title'
        );
    }



    /**
     * Returns an array containing field that can be used to search google for an image.
     */
    public function getGoogleSearchFields()
    {
        return array($this->firstnameLineEdit, $this->lastnameLineEdit);
    }



    protected function Firstname()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $this->firstnameLineEdit = $W->LineEdit()->setSize(25)->setMaxSize(255);

        if ($Crm->Access()->associateUserAccount()) {

            $this->firstnameLineEdit->setMandatory(true, $this->Crm()->translate('The firstname is mandatory'));
        }



        return $this->labelledField(
            $this->Crm()->translate('First name'),
            $this->firstnameLineEdit,
            'firstname'
        );
    }




    protected function Lastname()
    {
        $W = $this->widgets;
        $this->lastnameLineEdit = $W->LineEdit()->setSize(25)->setMaxSize(255)->setMandatory(true, $this->Crm()->translate('The lastname is mandatory'));
        return $this->labelledField(
            $this->Crm()->translate('Last name'),
            $this->lastnameLineEdit,
            'lastname'
        );
    }




    /**
     * Titled address
     * @param	string	$name 			prefix field name for address
     * @param	string	$title			optional title for address
     * @param	bool	$mandatory		set this parameter to true for mandatory address (fields : street, postal code, city)
     *
     * @return Widget_VBoxLayout
     */
    protected function Address($name, $title = null, $mandatory = false, $recipient = false)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(0.5,'em');

        if (isset($title)) {
            $layout->addItem($W->Title($title, 5));
        }

        $Ui = $this->Crm()->Ui();
        $editor = $Ui->AddressEditor(true, true, true);
        $editor->setMandatory($mandatory);
        $editor->showRecipient($recipient);
        $editor->showInstructions(true);

        $editor->setName($name);

        $layout->addItem($editor);

        return $layout;

    }




    /**
     *
     * @return Widget_ImagePicker
     */
    protected function Photo()
    {
        /* @var $W Func_Widgets */
        $W = $this->widgets;

        $addon = bab_getAddonInfosInstance('LibCrm');

        $imagePicker = $W->ImagePicker()
            ->setName('photo')
            ->oneFileMode()
            ->setDefaultImage(new bab_Path($addon->getStylePath() . 'images/contact-default.png'))
            ->setTitle($this->Crm()->translate('Set Photo'));

        return $imagePicker;
    }




    protected function Organization(crm_Contact $contact = null)
    {
        $W = bab_Widgets();

         $field = $this->labelledField(
                $this->Crm()->translate('Add a link with an organization'),
                $this->suggestOrganization = $this->Crm()->Ui()->SuggestOrganization()
                     ->setIdName('organization')
                    ->setSize(50)
                    ->setMinChars(0),
                'organizationname'
            );



        $layout = $W->FlowLayout()->setSpacing(.5,'em')->additem($field);

        if (null !== $contact && null !== $contact->id) {
            $layout->addItem($W->SubmitButton()->setLabel($this->Crm()->translate('Add this organization'))->setAction($this->Crm()->Controller()->Contact()->addOrganization()));
        }

        return $layout;
    }




    protected function contactOrganizationTableView()
    {
        return new crm_ContactOrganizationTableView($this->Crm());
    }




    protected function Organizations($contact)
    {
        $W = bab_Widgets();

        $organizations = $this->contactOrganizationTableView();

        $organizations->setContact($contact);


        $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        $layout->addItem($organizations);

//		$layout->addItem($this->Organization($contact));

        return $layout;
    }




    public function Position()
    {
        $Crm = $this->Crm();
        $Crm->includeEntrySet();
        return $this->labelledField(
                $Crm->translate('Position:'),
                $Crm->Ui()->SuggestEntry(crm_Entry::CONTACT_POSITIONS)
                    ->setMinChars(0)
                    ->setSize(25),
                'position');
    }




    protected function Assistant()
    {
        return $this->labelledField(
                $this->Crm()->translate('Assistant'),
                $this->suggestContact = $this->Crm()->Ui()->SuggestContact()
//					->setRelatedOrganization($this->suggestOrganization)
                    ->setSize(50)
                    ->setMinChars(0),
                'assistant_name'
            );
    }



    /**
     * Ovidentia user account field
     *
     * if the user is linked propose a user picker field
     * if no user linked, propose a user cration form or a user picker
     */
    protected function User(crm_Contact $contact = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $user = null;

        if (isset($contact) && !empty($contact->user)) {
            $username = bab_getUserName($contact->user);

            if (!empty($username)) {
                $user = $contact->user;
            }
        }


        $layout = $W->VBoxLayout()
            ->addClass('icon-left-16 icon-left icon-16x16')
            ->setVerticalSpacing(1,'em')
            ;

        $extra = $W->Frame('crm_contact_user_extra', $W->VBoxLayout()->setSpacing(.5,'em'))->setName('user_extra');

        if (isset($user)) {

            $layout->addItem($this->UserPicker($user));
            $layout->additem($extra);

            $entry = bab_getUserInfos($user);

            if ($Crm->loginByEmail) {
                $nickname = $W->FlowItems(
                    $W->Label($this->Crm()->translate('User email'))->colon(),
                    $W->Label(bab_getUserEmail($user))
                )->setHorizontalSpacing(.5,'em');
            } else {
                $nickname = $this->labelledField(
                    $this->Crm()->translate('User login'),
                    $W->LineEdit()->setSize(30),
                    'nickname'
                );
            }

            $changepassword = $this->labelledField(
                $this->Crm()->translate('Set new password for account'),
                $W->LineEdit()->setAutoComplete(false)->setSize(30)->obfuscate(true),
                'new_password'
            );

            $disabled = $this->labelledField(
                $this->Crm()->translate('User account disabled'),
                $W->Checkbox()->setValue($entry['disabled'] ? true : false),
                'disabled'
            );


            $extra->addItem($nickname);

            $extra->addItem($disabled);
            $extra->addItem($changepassword);

            if (isset($this->Crm()->Role)) {
                $extra->addItem($this->UserRoles());
            }

        } else {
            $userType = $this->UserType();
            $userPicker = $this->UserPicker($user);
            $layout->addItem($userType);
            $layout->addItem($userPicker);
            $layout->additem($extra);

            $userType->setAssociatedDisplayable($userPicker, array('2'));
            $userType->setAssociatedDisplayable($extra, array('3'));

            // user creation form

            $loginFormItem = $this->labelledField(
                $this->Crm()->translate('User login'),
                $W->LineEdit()->setSize(30)->setAutoComplete(false),
                'nickname'
            );

            $passwordFormItem = $this->labelledField(
                $this->Crm()->translate('Password'),
                $W->LineEdit()->setSize(30)->obfuscate(true)->setAutoComplete(false),
                'password'
            );

            $notify = $this->labelledField(
                $this->Crm()->translate('Notify the user about is account'),
                $W->CheckBox(),
                'notify'
            );

            $extra
                ->addItem($loginFormItem)
                ->addItem($passwordFormItem)
                ->addItem($notify);
        }


        return $layout;
    }



    protected function UserType()
    {
        $W = bab_Widgets();

        return $W->RadioSet() //$W->RadioSet('crm_usertype')
            ->addOption('1', $this->Crm()->translate('No user account'))
            ->addOption('2', $this->Crm()->translate('Link an existing user account to contact'))
            ->addOption('3', $this->Crm()->translate('Create a new user account'))
            ->setName('usertype')
            ->setValue('1');
    }


    protected function UserPicker($user = null)
    {
        $W = bab_Widgets();

        if (empty($user)) {
            $label = $this->Crm()->translate('Attach a user account');
            $description = $this->Crm()->translate('An existing user account can be attached to this contact');
        } else {
            $label = $this->Crm()->translate('User account');
            $description = $this->Crm()->translate('The contact can login into the application with this account');
        }


        return $this->labelledField(
            $label,
            $W->UserPicker(),
            'user',
            $description
        );
    }



    protected function UserRoles()
    {
        $W = bab_Widgets();

        $rolesBox = $W->Frame()
            ->setName('roles')
            ->setLayout($W->VBoxLayout())
            ->addItem($W->Title($this->Crm()->translate('Role'), 6));
        $roleSet = $this->Crm()->RoleSet();
        $roles = $roleSet->select();
        foreach ($roles as $role) {
            $rolesBox->addItem(
                $this->labelledField(
                    $role->name,
                    $W->CheckBox(),
                    $role->id
                )
            );
        }

        return $rolesBox;
    }


    /**
     * Captcha field tu use for registration form of a contact from a disconnected user
     * @return Widget_Frame
     */
    protected function Captcha()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $captcha = bab_functionality::get('Captcha');
        $layout = $W->HBoxItems(
            $W->Html($captcha->getGetSecurityHtmlData()),
            $this->labelledField(
                $Crm->translate('Copy the security code'),
                $W->LineEdit()
                    ->setSize(15)
                    ->setMandatory(true, $Crm->translate('You must fill the security code')),
                'captcha'
            )
        )->setHorizontalSpacing(1,'em');

        return $W->Frame(null, $layout)->addClass('crm-captcha');
    }


    /**
     *
     */
    public function birthday()
    {
        $W = $this->widgets;

        return $this->labelledField(
            $this->Crm()->translate('Birthday'),
            $W->DatePicker()->setMaxDate(BAB_DateTime::now())->setSelectableYear(),
            __FUNCTION__
        );
    }



    public function setValues($contact, $namePathBase = array())
    {
        if ($contact instanceof crm_Contact) {

            $contactValues = $contact->getFormOutputValues();

            if ($contact->address) {
                $this->Crm()->includeAddressSet();
                $contactAddress = $contact->address();
                if ($contactAddress->id) {
                    $this->setHiddenValue('contact[address]', $contactAddress->id);
                }
                $contactValues['address'] = $contactAddress->getValues();
            } else {
                $contactValues['address'] = null;
            }

            $this->setValues(array('contact' => $contactValues));

            if ($contact->responsible && ($responsible = $contact->responsible()) instanceof crm_Contact) {
                $this->setValue(array('contact', 'responsible_name'), $responsible->getFullName());
            }

            if ($contact->assistant && ($assistant = $contact->assistant())) {
                $this->setValue(array('contact', 'assistant_name'), $assistant->getFullName());
            }

            if (!empty($contact->user)) {
                $this->setValue(array('contact', 'usertype'), 2);

                $nickname = bab_getUserNickname($contact->user);

                // this value is used by the mycontact editor
                $this->setValue(array('contact', 'nickname'), $nickname);

                // this value is used in the contact editor if associated to user
                $this->setValue(array('contact', 'user_extra', 'nickname'), $nickname);
            }

            if (isset($this->Crm()->ContactRole)) {
                $contactRoleSet = $this->Crm()->ContactRoleSet();
                $contactRoles = $contactRoleSet->select($contactRoleSet->contact->is($contact->id));
                foreach ($contactRoles as $contactRole) {
                    $this->setValue(array('contact', 'user_extra', 'roles', $contactRole->role), true);
                }
            }

            if ($this->suggestContact) {

                if ($this->organization) {
                    $this->suggestContact->setRelatedOrganization(array($this->organization->name));
                }
                if (!empty($contact->id)) {
                    $this->setHiddenValue('contact[id]', $contact->id);
//					$contactSet = $this->suggestContact->getContactSet();
//					$suggestContactCriteria = new ORM_TrueCriterion();
//					$suggestContactCriteria = $suggestContactCriteria ->_AND_($contactSet->id->notIn($contact->id));
//					$this->suggestContact->setCriteria($suggestContactCriteria);
                    if (!$this->organization) {
                        if ($mainOrganization = $contact->getMainOrganization()) {
                            $this->suggestContact->setRelatedOrganization(array($mainOrganization->name));
                        } else {
                            $this->suggestContact->setRelatedOrganization($this->suggestOrganization);
                        }
                    }
                }

            }

        } else {

            parent::setValues($contact, $namePathBase);

        }
    }

}




/**
 * @return crm_Editor
 */
class crm_MailinglistEditor extends crm_Editor
{

    public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($crm, $id, $layout);
        $this->setName('contacts');

        $this->colon();

        $this->addFields();
        $this->addButtons();

        $this->setHiddenValue('tg', bab_rp('tg'));



        if (bab_pp('contacts', false) !== false) {
            $this->setValues(bab_pp('contacts'), array('contacts'));
        }
    }


    protected function addButtons()
    {
        $Crm = $this->Crm();

        $contactCtrl = $Crm->Controller()->Contact();

        if (null !== $contactCtrl)
        {
            $this->setCancelAction($Crm->Controller()->Contact()->cancel());
            $this->setSaveAction($Crm->Controller()->Contact()->saveMailinglist());
            $this->setFailedAction($Crm->Controller()->Contact()->addMailinglist());
            $this->setSuccessAction($Crm->Controller()->Contact()->displayList());
        }
    }

    protected function addFields()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $addLayout = $this->addMailingList();
        $removeLayout = $this->removeMailingList();

        $this->addItem(
            $W->VBoxItems(
                $W->RadioSet()
                    ->setHorizontalView()
                    ->setName('action')
                    ->addOption('1', $this->Crm()->translate('Add to the following mailing list'))
                    ->addOption('2', $this->Crm()->translate('Remove from the following mailing list'))
                    ->setValue('1')
                    ->setAssociatedDisplayable($addLayout, array('1'))
                    ->setAssociatedDisplayable($removeLayout, array('2')),
                $W->VBoxItems(
                    $addLayout,
                    $removeLayout
                )
            )->setVerticalSpacing(1, 'em')
        );
    }


    protected function newMailingListName()
    {
        $W = $this->widgets;
        return $this->labelledField(
            $this->Crm()->translate('Name (must be unique)'),
            $W->LineEdit()->setMandatory(true, $this->Crm()->translate('The name is mandatory.')),
            'name'
        );
    }


    protected function newMailingListType()
    {
        $W = $this->widgets;
        $options = array();

        /* @var $func Func_Newsletter */
        $funcs = @bab_functionality::getFunctionalities('MailingList');
        foreach($funcs as $func) {
            $functionnality = @bab_functionality::get('MailingList/'.$func);
            $options[get_class($functionnality)] = $functionnality->getDescription();
        }

        return $W->LabelledWidget(
            $this->Crm()->translate('Type'),
            $W->Select()->setOptions($options)->setValue('Func_MailingList_Ovidentia')->setMandatory(true, LibMailingList_translate('The type can not be empty')),
            'type'
        );
    }


    protected function newMailingList()
    {
        $W = $this->widgets;

        return $W->VBoxItems(
            $this->newMailingListName(),
            $this->newMailingListType()
        )->setVerticalSpacing(1,'em');
    }


    /**
     * @return Widget_Section
     */
    protected function addMailingList()
    {
        $W = $this->widgets;

        $layout = $W->VBoxItems()->setVerticalSpacing(1,'em');

        $displayable = $this->newMailingList();

        /* @var $select Widget_Multiselect */
        $select = $W->Select()
            ->setMandatory(true, $this->Crm()->translate('A mailing list is mandatory.'))
            ->setName('addmailinglist')
            ->addOption('0', '')
            ->setAssociatedDisplayable($displayable, array('-1'))
            ->setValue(0);

        $layout->addItem($select);

        /* @var $func Func_Newsletter */
        $ML = crm_MailingList();
        $lists = $ML->getLists();

        foreach($lists as $id => $name) {
            $subs = $ML->getListSubscriptions($id);
            $select->addOption(
                $id,
                $name. ' (' . sprintf($this->Crm()->translate('%s contacts'),count($subs)).')',
                $this->Crm()->translate('Existing mailing list')
            );
        }

        $select->sortOptions();
        $select->addOption('-1', $this->Crm()->translate('Add a mailing list'), $this->Crm()->translate('New mailing list'));

        return $layout->addItem($displayable);
    }


    /**
     * @return Widget_Section
     */
    protected function removeMailingList()
    {
        $W = $this->widgets;

        $layout = $W->VBoxItems()->setVerticalSpacing(1,'em');

        /* @var $select Widget_Multiselect */
        $select = $W->Select()
            ->setMandatory(true, $this->Crm()->translate('A mailing list is mandatory.'))
            ->setName('removemailinglist')
            ->addOption('0', '')
            ->setValue(0);

        $layout->addItem($select);

        /* @var $func Func_Newsletter */
        $ML = crm_MailingList();
        $lists = $ML->getLists();

        foreach($lists as $id => $name) {
            $subs = $ML->getListSubscriptions($id);
            $select->addOption(
                $id,
                $name. ' (' . sprintf($this->Crm()->translate('%s contacts'),count($subs)).')'
            );
        }

        $select->sortOptions();

        return $layout;
    }
}





class crm_ContactLink extends Widget_Link
{
    public function __construct(Func_Crm $Crm, $contact, $id = null)
    {

        if (!($contact instanceof crm_Contact)) {
            $contact = $Crm->ContactSet()->get($contact);
        }
        parent::__construct($contact->getFullName(), $Crm->Controller()->Contact()->display($contact->id), $id);

        $this->addClass('crm-contact-link');
    }
}




class crm_ContactMergeEditor extends crm_MergeEditor
{

}



/**
 * Creates a generic form fragment for the specified set.
 *
 * @return Widget_Item
 */
function crm_contactFilterForm(Func_Crm $Crm, $filter, $name)
{
    $W = bab_Widgets();

    $layout = $W->VBoxLayout()
        ->setVerticalAlign('bottom')
        ->setVerticalSpacing(1, 'em');



    $advancedLayout = $W->FlowLayout();
    $advancedLayout->setVerticalSpacing(1, 'em')->setHorizontalSpacing(3, 'em');

    $advancedLayout->addItem(
        $W->HBoxItems(
            $W->VBoxItems(
                $W->Label($Crm->translate('Last name')),
                $W->LineEdit()->setSize(20)->setName('lastname')
            ),
            $W->VBoxItems(
                $W->Label($Crm->translate('First name')),
                $W->LineEdit()->setSize(20)->setName('firstname')
            )
        )->setHorizontalSpacing(1, 'em')
    );

    if (isset($Crm->Organization))
    {
        $advancedLayout->addItem(
            $W->HBoxItems(
                $W->VBoxItems(
                    $W->Label($Crm->translate('Organization')),
                    $Crm->Ui()->SuggestOrganization()
//				        ->setSuggestAction($Crm->Controller()->Organization()->suggest(), 'search')
                        ->setMinChars(0)
                        ->setSize(20)
                        ->setName('organization/name')
                ),
                $W->VBoxItems(
                    $W->Label($Crm->translate('Position')),
                    $Crm->Ui()->SuggestPosition()
                        ->setMinChars(0)
                        ->setSize(20)
                        ->setName('position')
                )
            )->setHorizontalSpacing(1, 'em')
        );
    }

    $advancedLayout->addItem(
        $W->HBoxItems(
            $W->VBoxItems(
                $W->Label($Crm->translate('Postal code')),
                $W->SuggestPostalCode()
                    ->setMinChars(2)
                    ->setSuggestAction($Crm->Controller()->Search()->suggestPostalCode(), 'search')
                    ->setSize(5)
                    ->setName('address/postalCode')
            ),
            $W->VBoxItems(
                $W->Label($Crm->translate('City')),
                $W->SuggestPlaceName()
                    ->setSuggestAction($Crm->Controller()->Search()->suggestPlaceName(), 'search')
                    ->setMinChars(2)
                    ->setSize(20)
                    ->setName('address/city')
            ),
            $W->VBoxItems(
                $W->Label($Crm->translate('Country')),
                $W->SuggestCountry()
                    ->setMinChars(0)
                    ->setSize(20)
                    ->setName('address/country')
            )
        )->setHorizontalSpacing(1, 'em')
    );


    if (isset($Crm->ContactClassification))
    {
        $classification = $W->Frame(null,
                $W->VBoxItems(
                        $W->Label($Crm->translate('Classification')),
                        $Crm->Ui()->SuggestClassification()
//				            ->setSuggestAction($Crm->Controller()->Search()->suggestClassification(), 'search')
                            ->setName('classification')
                            ->setSize(50)
                )
        );

    } else {

        $classification = null;
    }


    if (isset($Crm->Tag))
    {
        $advancedLayout->addItem(
            $W->HBoxItems(
                $classification,
                $W->Frame(
                    null,
                    $W->VBoxItems(
                        $W->Label($Crm->translate('Tag')),
                        $Crm->Ui()->SuggestTag('filter_tag')
                            ->setSuggestAction($Crm->Controller()->Tag()->suggest(), 'search')
                            ->setName('tag')
                    )
                )
            )->setHorizontalSpacing(1, 'em')
        );
    }

    if (isset($Crm->ContactAcquaintance))
    {
        $advancedLayout->addItem(
            $W->VBoxItems(
                $W->Label($Crm->translate('Acquaintance level')),
                $W->Select()
                    ->addOption('', '')
                    ->addOption('1', '1')
                    ->addOption('2', '2')
                    ->addOption('3', '3')
                    ->addOption('all', '1, 2 ou 3')
                    ->setName('acquaintance')
            )
        );
    }

    $form = $W->Form()->setId('crm_search')->setReadOnly(true)->setName('filter')->colon()
        ->setLayout(
            $W->HBoxItems(
                $W->VBoxItems($W->LineEdit()->setSize(30)->setName('search')),
                $W->SubmitButton()->setLabel($Crm->translate('Filter'))
            )->setHorizontalSpacing(2, 'em')->setVerticalAlign('middle')
        );

    if (isset($filter) && is_array($filter)) {
        $form->setValues($filter, array($name, 'filter'));
    }
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', bab_rp('idx'));

    $advancedForm = $W->Form()->setId('crm_advanced_search')->setReadOnly(true)->setName('filter')->colon()
        ->setLayout(
            $W->HBoxItems(
                $advancedLayout,
                $W->SubmitButton()->setLabel($Crm->translate('Filter'))
            )->setVerticalAlign('bottom')
        );

    if (isset($filter) && is_array($filter)) {
        $advancedForm->setValues($filter, array($name, 'filter'));
    }
    $advancedForm->setHiddenValue('tg', bab_rp('tg'));
    $advancedForm->setHiddenValue('idx', bab_rp('idx'));


    $layout->addItem($advancedForm);

    return $layout;
}




/**
 * Creates a specific filter panel containing the table and an auto-generated form.
 *
 * @param widget_TableModelView $table
 * @param array					$filter
 * @param string				$name
 * @return Widget_Filter
 */
function crm_contactFilteredTableView(crm_ContactTableView $table, $filter = null, $name = null)
{
    $W = bab_Widgets();
    $Crm = $table->Crm();

    $filterPanel = $W->Filter();
    $filterPanel->setLayout($W->VBoxLayout());
    if (isset($name)) {
        $filterPanel->setName($name);
    }

    $table->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : 12);

    $table->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : 0);

    $table->sortParameterName = $name . '[filter][sort]';

    if (isset($filter['sort'])) {
        $table->setSortField($filter['sort']);
    } else {
        $columns = $table->getVisibleColumns();
        foreach ($columns as $columnId => $column) {
            /* @var $column widget_TableModelViewColumn */
            if ($column->isSortable()) {
                $table->setSortField($columnId);
                break;
            }
        }
    }


    $layout = crm_contactFilterForm($Crm, $filter, $name);


    $filterPanel->setFilter($layout);
    $filterPanel->setFiltered($table);

    return $filterPanel;
}



/**
 *
 * @param crm_Contact $contact
 *
 * @return widget_Frame
 */
function crm_dealsWithContact(crm_Contact $contact)
{
    $W = bab_Widgets();

    $Crm = $contact->Crm();

    $dealsFrame = $W->VBoxLayout();

    $deals = $contact->getDeals();
    if ($deals->count() == 0) {
        $dealsFrame->addItem($W->Label($Crm->translate('No deal associated with this contact')));
    }
    foreach ($deals as $deal) {
        $dealsFrame->addItem(
            $W->VBoxItems(
                $W->Link($W->Title($deal->name, 5), $Crm->Controller()->Deal()->display($deal->id)),
                $W->Label(number_format((float)$deal->amount, 0, ',', ' ') . $Crm->translate('_euro_'))
            )
        );
    }

    return $dealsFrame;
}




function crm_acquaintanceEditor(Func_Crm $Crm, $contact = null)
{
    $W = bab_Widgets();

    $editor = new crm_Editor($Crm);

    $editor->colon(true);

    $lineBox = $W->VBoxItems(
        $editor->labelledField(
            $Crm->translate('Level'),
            $W->Select()
                ->addOption('', '')
                ->addOption('0', $Crm->translate("I don't know this contact"))
                ->addOption('1', '1')
                ->addOption('2', '2')
                ->addOption('3', '3')
                ->setMandatory(true, $Crm->translate('You must specify the domain')),
            'level'
        )
    )
    ->setVerticalSpacing(1, 'em');

    $editor->addItem($lineBox);

    if (isset($contact)) {
        $editor->setHiddenValue('contact', $contact);
    }
    $editor->setHiddenValue('tg', bab_rp('tg'));

    $editor->setSaveAction($Crm->Controller()->Contact()->saveAcquaintance());
    $editor->setCancelAction($Crm->Controller()->Contact()->cancel());

    return $editor;
}




function crm_contactSectorEditor(Func_Crm $Crm, $contact = null)
{
    $W = bab_Widgets();

    $editor = new crm_Editor($Crm);

    $editor->colon(true);

    $sectorsBox = $W->NamedContainer()
        ->setName('sectors')
        ->setLayout($W->VBoxLayout());

    $sectors = $Crm->SectorSet()->select();


    foreach ($sectors as $sector) {
        $sectorsBox->addItem(
            $editor->labelledField(
                $sector->name,
                $W->CheckBox(),
                '' . $sector->id
            )
        );
    }


    $editor->addItem($sectorsBox);

    if (isset($contact)) {
        $editor->setHiddenValue('contact', $contact->id);
        $contactSectorSet = $Crm->ContactSectorSet();
        $contactSectors = $contactSectorSet->select($contactSectorSet->contact->is($contact->id));
        foreach ($contactSectors as $contactSector) {
            $editor->setValue(array('sectors', $contactSector->sector), true);
        }
    }
    $editor->setHiddenValue('tg', bab_rp('tg'));

    $editor->setCancelAction($Crm->Controller()->Contact()->cancel());

    return $editor;
}






class crm_ContactDirectoryEditor extends crm_Editor
{
    public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
    {

        parent::__construct($crm, $id, $layout);
        $this->setName('contactdirectory');

        $this->colon();

        $this->addFields();
        $this->addButtons();

        $this->setHiddenValue('tg', bab_rp('tg'));
    }


    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $this->addButton(
                $W->SubmitButton()
                ->setLabel($Crm->translate('Save'))
                ->validate(true)
                ->setAction($Crm->Controller()->Admin()->saveContactDirectory())
                ->setFailedAction($Crm->Controller()->Admin()->editContactDirectory())
                ->setSuccessAction($Crm->Controller()->Admin()->administer())
        );



        $this->addButton(
                $W->SubmitButton()
                ->setLabel($Crm->translate('Cancel'))
                ->setAction($Crm->Controller()->Admin()->administer())
        );
    }


    protected function getDirectoryFields()
    {
        $fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);

        if (isset($fields['jpegphoto']))
        {
            unset($fields['jpegphoto']);
        }

        return $fields;
    }


    protected function directoryFieldSelect()
    {
        $W = $this->widgets;

        $fields = $this->getDirectoryFields();
        $select = $W->Select();
        $select->addOption('', '');

        foreach($fields as $name => $arr)
        {
            $select->addOption($name, $arr['name']);
        }

        return $select;
    }


    protected function getContactFields()
    {
        $Crm = $this->Crm();

        return array(

            'Contact.firstname' 			=> $Crm->translate('Firstname'),
            'Contact.lastname'				=> $Crm->translate('Lastname'),
            'Contact.phone'					=> $Crm->translate('Phone'),
            'Contact.mobile'				=> $Crm->translate('Mobile'),
            'Contact.fax'					=> $Crm->translate('Fax'),
            'Contact.email'					=> $Crm->translate('Email'),
            'ContactOrganization.position'	=> $Crm->translate('Position in the main Organization'),
            'Contact.address.street'		=> $Crm->translate('Street'),
            'Contact.address.postalCode'	=> $Crm->translate('Postal code'),
            'Contact.address.city'			=> $Crm->translate('City'),
            'Contact.address.cityComplement'=> $Crm->translate('City complement'),
            'Contact.address.country'		=> $Crm->translate('Country')
        );
    }

    protected function contactFieldSelect()
    {
        $W = $this->widgets;

        $fields = $this->getContactFields();
        $select = $W->Select();
        $select->addOption('', '');
        $select->addOptions($fields);


        return $select;
    }



    protected function addFields()
    {
        $this->addItem($this->userFrame());
        // $this->addItem($this->contactFrame());

    }



    protected function userFrame()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;


        $frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $frame->setName('contact');
        $frame->addItem($W->Title($Crm->translate('Save into contact when a user is modified in Ovidentia')));

        $fields = $this->getContactFields();

        foreach($fields as $name => $text)
        {
            $frame->addItem(
                $this->labelledField($text, $this->directoryFieldSelect(), $name)
            );
        }

        return $frame;
    }


    protected function contactFrame()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;


        $frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $frame->setName('user');
        $frame->addItem($W->Title($Crm->translate('Save into user directory entry when a contact is modified')));


        $fields = $this->getDirectoryFields();

        foreach($fields as $name => $arr)
        {
            $frame->addItem(
                $this->labelledField($arr['name'], $this->contactFieldSelect(), $name)
            );
        }

        return $frame;
    }
}



/**
 * @param Func_Crm	$Crm
 * @param crm_Organization $organization
 * @return Widget_Timeline
 */
function crm_contactTimeline(Func_Crm $Crm, crm_Contact $contact)
{
    $W = bab_Widgets();

    require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';

    $now = BAB_DateTime::now();
    $nowIso = $now->getIsoDate();

    $taskSet = $Crm->TaskSet();
    $contactTasks = $taskSet->selectLinkedTo($contact);

    $timeline = $W->Timeline();

    $timeline->addBand(new Widget_TimelineBand('20%', Widget_TimelineBand::INTERVAL_MONTH, 150, Widget_TimelineBand::TYPE_OVERVIEW));
    $timeline->addBand(new Widget_TimelineBand('80%', Widget_TimelineBand::INTERVAL_WEEK, 150, Widget_TimelineBand::TYPE_ORIGINAL));


    foreach ($contactTasks as $task) {
        $task = $task->targetId;
        $start = BAB_DateTime::fromIsoDateTime($task->dueDate);
        $period = $timeline->createMilestone($start);
        $period->start = $task->dueDate . ' 12:00';
        if ($task->completion >= 100) {
            $period->color = '#282';
            $period->setTitle($task->summary);
        } else  {
            if ($nowIso > $task->dueDate) {
                $period->color = '#b22';
            }
            $period->setTitle($task->summary);
        }
        $period->setBubbleContent(
            $W->VBoxItems(
                $W->Link($W->Title($task->summary, 2), $GLOBALS['babUrl'] . $Crm->Controller()->Task()->display($task->id)->url()),
                $W->Html($Crm->translate('Responsible:') . ' <b>' . bab_toHtml(bab_getUserName($task->responsible, true)) . '</b>'),
                $W->Html(bab_toHtml($task->description, BAB_HTML_ALL))
            )->setVerticalSpacing(0.5, 'em')
        );

        $timeline->addPeriod($period);
    }

    return $timeline;
}
