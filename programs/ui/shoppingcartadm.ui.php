<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/shoppingcart.ui.php';

class crm_ShoppingCartAdmTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_ShoppingCart $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		if ('id' === $fieldPath)
		{
			return $W->Link($record->id, $Crm->Controller()->ShoppingCartAdm()->display($record->id));
		}
		
		
		if ('_items_' === $fieldPath)
		{
			return $W->Label($record->getItems()->count());
		}
		
		
		if ('customer' === $fieldPath)
		{
			if (!empty($record->contact->id))
			{
				return $W->Label($record->contact->getFullName());
			} else if (!empty($record->user))
			{
				return $W->Label(bab_getUserName($record->user));
			} else {
				return $W->Label($Crm->translate('Anonymous user'));
			}
			
		}
		
		if ('paymentorder/name' === $fieldPath)
		{
			return $W->Link($record->paymentorder->name, $Crm->Controller()->Order()->display($record->paymentorder->id));
		}
		
		if ('accountingdocument/name' === $fieldPath)
		{
			return $W->Link($record->accountingdocument->name, $Crm->Controller()->Order()->display($record->accountingdocument->id));
		}
		
		return parent::computeCellContent($record, $fieldPath);
	}
}





class crm_ShoppingCartItemAdmTableView extends crm_ShoppingCartItemTableView
{

	protected function isEditMode()
	{
		return false;
	}
}



class crm_ShoppingCartAdmCardFrame extends crm_CardFrame
{
	public function __construct(Func_Crm $Crm, crm_ShoppingCart $cart)
	{
		$W = bab_Widgets();
		$layout = $W->VBoxLayout()->setVerticalSpacing(.5,'em');
		parent::__construct($Crm, null, $layout);
		$title = $W->Title(sprintf($Crm->translate('Shopping cart %s'), $cart->id), 5);
		$layout->addItem($W->Link($title, $Crm->Controller()->ShoppingCartAdm()->display($cart->id)));
		
		if ($contact = $cart->getContact())
		{
			$layout->addItem($W->FlowItems($W->Label($Crm->translate('Owner'))->colon(), $W->Link($contact->getFullName(), $Crm->Controller()->Contact()->display($contact->id)))->setHorizontalSpacing(.5,'em'));
		}
		
		$set = $cart->getParentSet();
		
		$layout->addItem($W->Label(sprintf($Crm->translate('Created on %s'), $set->createdOn->output($cart->createdOn))));
		if ('0000-00-00 00:00:00' !== $cart->expireOn)
		{
			$layout->addItem($W->Label(sprintf($Crm->translate('Expire on %s'), $set->createdOn->output($cart->expireOn))));
		}
	}
}




class crm_ShoppingCartShippedCommentEditor extends crm_Editor
{
	private $shoppingCart = null;
	
	public function __construct(Func_Crm $Crm, crm_ShoppingCart $cart)
	{
		parent::__construct($Crm);
		$W = $this->widgets;
		$this->addClass('crm-shoppingcart-status-editor');
		
		$this->shoppingCart = $cart;
		
		$this->setName('shoppingcart');
		
		// download delivery slip
		
		if(!$Crm->noDelivery){
			$this->addItem(
				$W->Link($W->Icon($Crm->translate('Download delivery slip PDF'), Func_Icons::MIMETYPES_APPLICATION_PDF), 
				$Crm->Controller()->Order()->deliverySlipPdf($cart->getOrder()->id))
			);
		}

		$this->addFields();
		$this->addButtons();

		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->setHiddenValue('shoppingcart[id]', $cart->id);

		
		$this->setValues($cart->getValues(), array('shoppingcart'));
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$button = $W->SubmitButton()
			->setAction($Crm->Controller()->ShoppingCartAdm()->setShippedStatus())
			->setSuccessAction($Crm->Controller()->ShoppingCartAdm()->display($this->shoppingCart->id))
			->setFailedAction($Crm->Controller()->ShoppingCartAdm()->display($this->shoppingCart->id));
		if($Crm->noDelivery){
			$button->setLabel($Crm->translate('Set this shopping cart available'));
		}else{
			$button->setLabel($Crm->translate('Set this shopping cart shipped'));
		}
		
		$this->addItem($button);
	}
	
	
	
	protected function addFields()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$comment = $W->Frame()->addItem($this->shipped_comment());
		
		$this->addItem($comment);
		$this->addItem($this->noemail($comment));
	}
	
	
	protected function noemail($displayable)
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		
		
		return $this->labelledField(
				$Crm->translate('Do not send the notification to the customer'),
				$W->Checkbox()->setAssociatedDisplayable($displayable, array('0')),
				__FUNCTION__
		);
	}
	
	
	protected function shipped_comment()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		$text = $Crm->translate('Comment on shipping');
		if($Crm->noDelivery){
			$text = $Crm->translate('Comment on this order');
		}
		
		return $this->labelledField(
				$text,
				$W->TextEdit()->setLines(3)->setColumns(80),
				__FUNCTION__,
				$Crm->translate('This information will be sent by email to the customer')
		);
	}
}





class crm_PaymentErrorTableView extends crm_TableModelView
{

	public function __construct(Func_Crm $Crm, crm_ShoppingCart $cart, $id = null)
	{
		parent::__construct($Crm, $id);

		$this->addClass('crm-paymenterror-tableview');
		$this->addClass(Func_Icons::ICON_LEFT_16);

		$this->setDataSource($cart->selectPaymentError());
	}


	/**
	 * Add the default collumns of a widget table modelview
	 *
	 * @see widget_TableModelView::addColumn
	 *
	 * @param	ORM_RecordSet	$set
	 *
	 * @return widget_TableModelView
	 */
	public function addDefaultColumns(ORM_RecordSet $set)
	{
		$Crm = $this->Crm();

		$this->addColumn(widget_TableModelViewColumn($set->paymentToken, $Crm->translate('Payment token')));
		// $this->addColumn(widget_TableModelViewColumn($set->createdOn, $Crm->translate('Created on')));
		$this->addColumn(widget_TableModelViewColumn($set->transmission_date, $Crm->translate('Date'))); // date de la demande
		$this->addColumn(widget_TableModelViewColumn($set->code, $Crm->translate('Code')));
		$this->addColumn(widget_TableModelViewColumn($set->message, $Crm->translate('Message')));
		$this->addColumn(widget_TableModelViewColumn($set->bank_response_code, $Crm->translate('Bank response code')));
		$this->addColumn(widget_TableModelViewColumn($set->payment_means, $Crm->translate('Payment mean')));
		$this->addColumn(widget_TableModelViewColumn($set->card_number, $Crm->translate('Card number')));

		return $this;
	}

}