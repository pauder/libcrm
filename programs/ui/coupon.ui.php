<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/discountbase.ui.php';


/**
 * list of coupon from back office
 *
 */
class crm_CouponTableView extends crm_TableModelView
{
	
	public function addDefaultColumns(crm_CouponSet $set)
	{
		$Crm = $this->Crm();
		
		$this->addColumn(widget_TableModelViewColumn($set->code, $Crm->translate('Code')));
		$this->addColumn(widget_TableModelViewColumn($set->description, $Crm->translate('Description')));
		$this->addColumn(widget_TableModelViewColumn($set->start_date, $Crm->translate('Start date'))->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->end_date, $Crm->translate('Expiration date')));
		$this->addColumn(widget_TableModelViewColumn($set->threshold, $Crm->translate('Threshold')) );
		$this->addColumn(widget_TableModelViewColumn($set->shipping, $Crm->translate('On shipping'))->setVisible(false) );
		$this->addColumn(widget_TableModelViewColumn($set->maxuse, $Crm->translate('Max usage'))->setSearchable(false)->setVisible(false)->addClass('widget-column-thin widget-column-center') );
		
	}
	
	
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_Coupon $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();

		$displayAction = $Crm->Controller()->Coupon()->display($record->id);
		
		if ('code' === $fieldPath) {
			return $W->Link(
				$record->code,
				$displayAction
			);
		}
		
		if ('maxuse' === $fieldPath) {
			if (!$record->maxuse) {
				$icon = Func_Icons::ACTIONS_DIALOG_CANCEL;
				return $W->Icon(
				    '',
				    $icon
				);
			}
		}
		
		
		return parent::computeCellContent($record, $fieldPath);
	}

}




/**
 * @return Widget_Form
 */
class crm_CouponEditor extends crm_DiscountBaseEditor
{
	
	protected $suggestArticlePackaging = null;
	
	/**
	 *
	 * @var crm_Coupon
	 */
	protected $coupon;

	public function __construct(Func_Crm $crm, crm_Coupon $coupon = null)
	{
		$this->coupon = $coupon;


		parent::__construct($crm, $coupon);
		$this->setName('coupon');
		

		if (isset($coupon)) {
			$values = $this->getCouponValues();
			
			if (isset($values['articlepackaging']['id'])) {
			    $this->suggestArticlePackaging->setIdValue($values['articlepackaging']['id']);
			}
			
			$this->setValues($values, array('coupon'));
			$this->setHiddenValue('coupon[id]', $coupon->id);
		}
	}


	/**
	 * @return Array
	 */
	protected function getCouponValues()
	{
		if (null === $this->coupon)
		{
			return array();
		}
		
		
		$values = $this->coupon->getFormOutputValues();
		
		if (empty($values['threshold_type'])) {
		    $values['threshold_type'] = 'amount';
		}
		
		$values['opt'] = 'total';
		
		if ($this->coupon->shipping) {
		    $values['opt'] = 'shipping';
		}
		
		$Crm = $this->Crm();
		
		if (isset($Crm->ArticlePackaging) 
		    && ($this->coupon->articlepackaging instanceof crm_ArticlePackaging) 
		    && $this->coupon->articlepackaging->id)
		{
		    $values['opt'] = 'product';
		    
			$this->Crm()->includePackagingSet();
			$values['articlepackaging']['id'] = $this->coupon->articlepackaging->id;
			$values['articlepackaging']['suggestname'] = $this->coupon->articlepackaging->article->name.$this->suggestArticlePackaging->getSeparator().$this->coupon->articlepackaging->packaging()->name;
		}
		
		if ($values['contact']) {
		    $contact = $Crm->ContactSet()->get($values['contact']);
		    if (isset($contact)) {
		      $values['contactname'] = $contact->getFullName();
		    }
		}

		return $values;
	}



	protected function addFields()
	{
		$W = $this->widgets;

		$this->addItem($this->code());
		$this->addItem($this->description());
		
		$value = $W->FlowItems($this->value(), $this->percentage());
		$product = $W->HBoxItems(
		    $this->articlepackaging(),
		    $this->quantity()
		)->setHorizontalSpacing(1, 'em');
		
		
		$this->addItem($this->opt($value, $product));
		$this->addItem($value);
		$this->addItem($product);
		$this->addItem($this->threshold_type());
		$this->addItem($this->threshold());
		$this->addItem($this->periodeDate());
		$this->addItem($this->maxuse());
		$this->addItem($this->contact());

	}
	
	
	
	protected function opt(Widget_Displayable_Interface $value, Widget_Displayable_Interface $product) 
	{
	    $W = $this->widgets;
	    $Crm = $this->Crm();
	     
	    
	    $opt = $W->RadioSet()
	    ->addOption('total', crm_translate('A discount on total'))
	    ->addOption('shipping', crm_translate('A discount on shipping'))
	    ->addOption('product', crm_translate('A product'))
	    ->setHorizontalView()
	    ->setAssociatedDisplayable($product, array('product'))
	    ->setAssociatedDisplayable($value, array('total', 'shipping'));
	    
	    return $this->labelledField(
	        $Crm->translate('Advantage type offered'),
	        $opt,
	        'opt'
	    );
	    
	}
	
	
	


	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		$id_coupon = null !== $this->coupon ? $this->coupon->id : null;

		$this->addButton(
		$W->SubmitButton()
			->setLabel($Crm->translate('Save'))
			->validate(true)
			->setAction($Crm->Controller()->Coupon()->save())
			->setFailedAction($Crm->Controller()->Coupon()->edit($id_coupon))
		);



		$this->addButton(
			$W->SubmitButton()
			->setLabel($Crm->translate('Cancel'))
			->setAction($Crm->Controller()->Coupon()->cancel())
		);
	}

	/**
	 *
	 * @return Widget_Item
	 */
	protected function code()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
			$Crm->translate('Code'),
			$W->LineEdit()->setSize(70)->setMandatory(true, $Crm->translate('The code is mandatory')),
			'code'
		);
	}


	
	
	/**
	 *
	 * @return Widget_Item
	 */
	protected function articlepackaging()
	{
		$Crm = $this->Crm();
		
		if (!isset($Crm->ArticlePackaging)) {
		    return null;
		}
		
		$W = $this->widgets;

		$suggest = $this->labelledField(
			$Crm->translate('Article and packaging'),
			$this->suggestArticlePackaging = $Crm->Ui()->SuggestArticlePackaging(__FUNCTION__)->setSize(70),
			'suggestname'
		);

		return $W->Frame()->addItem($suggest)->setName(__FUNCTION__);
	}
	
	
	protected function quantity()
	{
	    $Crm = $this->Crm();
	    $W = $this->widgets;
	    
	    if (!isset($Crm->ArticlePackaging)) {
	        return null;
	    }
	    
	    return $this->labelledField(
	        $Crm->translate('Product quantity'),
	        $W->NumberEdit(),
	        __FUNCTION__
	    );
	    
	}
	
	
	
	
	
	/**
	 *
	 * @return Widget_Item
	 */
	protected function unique()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
			$Crm->translate('Unique usage'),
			$W->CheckBox(),
			'unique'
		);
	}
	
	
	
	
	protected function maxuse()
	{
	    $Crm = $this->Crm();
	    $W = $this->widgets;
	     
	    return $this->labelledField(
	        $Crm->translate('Maximal number of uses'),
	        $W->NumberEdit(),
	        __FUNCTION__
	    );
	}
	
	
	
	protected function contact()
	{
	    $Crm = $this->Crm();
	    $Ui = $Crm->Ui();
	    $W = $this->widgets;
	    
	    return $this->labelledField(
	        $Crm->translate('Contact'),
	        $Ui->SuggestContact()->setIdName(__FUNCTION__)->setMinChars(1),
	        'contactname'
	    );
	}
}




/**
 * Display coupon
 *
 */
class crm_CouponDisplay extends crm_Object {


	/**
	 * @var crm_Coupon
	 */
	protected $coupon = null;

	/**
	 *
	 * @param crm_Coupon $coupon
	 *
	 */
	public function __construct(Func_Crm $Crm, crm_Coupon $coupon)
	{
		parent::__construct($Crm);
		$this->coupon = $coupon;
	}
	


	/**
	 * Get coupon full frame visible by coupon manager
	 *
	 * @return Widget_Item
	 */
	public function getFullFrame()
	{
		$W = bab_Widgets();
		
		$frame = $W->Frame();
		
		$frame->addItem($W->Title($this->coupon->code,2));
		$frame->addItem($this->getTopFrame());
		
		if (null !== $usage = $this->usage())
		{
			$frame->addItem($usage);
		}
		
		$frame->addClass('crm-detailed-info-frame');
		
		return $frame;
	}
	/**
	 * Article frame without the main photo, contain information displayed on the right of the main photo
	 * @return Widget_Frame
	 */
	protected function getTopFrame()
	{
		$W = bab_Widgets();
		
		$top = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.4,'em'));
		
		$top->addItem($this->threshold());
		$top->addItem($this->period());
		$top->addItem($this->description());
		
		$top->addItem($W->Title(crm_translate('The discount'), 4));
		$top->addItem($this->discount());
		$top->addItem($this->maxuse());
		$top->addItem($this->contact());
		
		return $top;
	}
	
	
	protected function usage()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$set = $Crm->CouponUsageSet();
		$set->shoppingcart();
		$set->shoppingcart->contact();
		$set->shoppingcart->paymentorder();
		
		$res = $set->select($set->coupon->is($this->coupon->id));
		
		if (0 === $res->count())
		{
			return null;
		}
		
		
		
		$table = $W->TableModelView();
		$table->setDataSource($res);
		$table->addColumn(widget_TableModelViewColumn($set->shoppingcart->createdOn, $Crm->translate('Date')));
		$table->addColumn(widget_TableModelViewColumn($set->shoppingcart->contact->lastname, $Crm->translate('Lastname')));
		$table->addColumn(widget_TableModelViewColumn($set->shoppingcart->contact->firstname, $Crm->translate('Firstname')));
		$table->addColumn(widget_TableModelViewColumn($set->shoppingcart->paymentorder->name, $Crm->translate('Invoice')));
		
		return $W->Section(
			$Crm->translate('List of uses for this code'),
			$table
		);
	}
	
	
	
	protected function threshold()
	{
	    $W = bab_Widgets();
	    $Crm = $this->Crm();
	    
	    if ($this->coupon->threshold > 0) {
	        
	        $set = $this->coupon->getParentSet();
	        $threshold = $set->threshold->output($this->coupon->threshold);
	        
	        return $W->Label(sprintf(crm_translate('For shopping cart with total greater than %s %s'), $threshold, $Crm->Ui()->Euro()));
	    }
	}
	
	protected function period()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$set = $this->coupon->getParentSet();
		
		return $W->FlowItems(
			$W->Label($Crm->translate('Available'))->colon(),
			$W->Label($Crm->translate('From')),
			$W->Label($set->start_date->output($this->coupon->start_date)),
			$W->Label($Crm->translate('to')),
			$W->Label($set->end_date->output($this->coupon->end_date))
		)->setSpacing(0,'em',.5,'em');
	}
	
	
	protected function description()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		if (empty($this->coupon->description))
		{
			return $W->Label('');
		}
	
		return $W->Section(
				$Crm->translate('Description'),
				$W->RichText($this->coupon->description)
		);
	}
	
	
	
	protected function discount()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$coupon = $this->coupon;
		if($coupon->value != 0){
			return $W->Label($coupon->getDiscount());
		}else{
		    
		    if (!isset($Crm->ArticlePackaging)) {
		        return null;
		    }
		    
			$Ui = $Crm->Ui();
			return $W->VBoxItems(
			            $W->Label(sprintf($Crm->translate('Quantity: %d'), $coupon->quantity)),
						$Ui->ArticleCardFrame($coupon->articlepackaging->article), 
						$W->Label($coupon->articlepackaging->packaging->name)
					)->setSpacing(1,'em');
		}
	}
	
	
	
	protected function unique()
	{
		$W = bab_Widgets();
		
		$coupon = $this->coupon;
		if ($coupon->unique) {
			return $W->Label(crm_translate('To unique usage'));
		} else {
			return $W->Label('');
		}
	}
	
	
	protected function maxuse()
	{
	    $W = bab_Widgets();
	
	    $coupon = $this->coupon;
	    if ($coupon->maxuse) {
	        return $W->Label(sprintf(crm_translate('Maximal number of usages: %d'), $coupon->maxuse));
	    }
	    
	    return null;
	}
	
	
	
	protected function contact()
	{
	    $W = bab_Widgets();
	    
	    if ($this->coupon->getFkPk('contact')) {
	        $contact = $this->coupon->contact();
	        /*@var $contact crm_Contact */
	        return $W->Label(sprintf(crm_translate('Usable only by: %s'), $contact->getFullName()));
	    }
	    
	    return null;
	}
	
}