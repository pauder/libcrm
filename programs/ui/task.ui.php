<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');
$W->includePhpClass('Widget_VBoxLayout');



/**
 * list of tasks
 *
 * @method Func_Crm    Crm()
 */
class crm_TaskTableView extends crm_TableModelView
{
    /**
     * @var crm_ContactSet
     */
    protected $contactSet = null;

    /**
     * @param Func_Crm $crm
     * @param string $id
     */
    public function __construct(Func_Crm $crm = null, $id = null)
    {
        parent::__construct($crm, $id);
        $this->contactSet = $crm->ContactSet();
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $Crm = $this->Crm();

        $this->addColumn(
            crm_TableModelViewColumn($recordSet->summary)
        );
        $this->addColumn(
            crm_TableModelViewColumn($recordSet->description)
        );
        $this->addColumn(
            crm_TableModelViewColumn($recordSet->responsible)
        );
        $this->addColumn(
            crm_TableModelViewColumn($recordSet->dueDate)
            ->addClass('widget-10em')
        );
        $this->addColumn(
            crm_TableModelViewColumn($recordSet->createdOn)
            ->addClass('widget-10em')
            ->setVisible(false)
        );
        $this->addColumn(
            crm_TableModelViewColumn($recordSet->completion)
            ->addClass('widget-10em', 'widget-percent')
            ->setVisible(false)
            ->setSearchable(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn('_object_', $Crm->translate('Object'))
            ->addClass('widget-30em')
        );
        $this->addColumn(
            widget_TableModelViewColumn('_done_', $Crm->translate('Done'))
            ->setSortable(false)
            ->addClass('widget-column-thin', 'widget-column-center')
        );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')
            ->setExportable(false)
            ->setSortable(false)
            ->addClass('widget-column-thin', 'widget-column-center')
        );
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleFilterFields()
     */
    protected function handleFilterFields(Widget_Item $form)
    {
        parent::handleFilterFields($form);

        $Crm = $this->Crm();
        $W = bab_Widgets();

        // add a category filter

        $form->addClass(Func_Icons::ICON_LEFT_16);

        $label = $W->Label($Crm->translate('Organization'));
        $input = $Crm->Ui()->SuggestOrganization()->setName('organization');
        $label->setAssociatedWidget($input);

        $form->addItem($this->handleFilterLabel($label, $input));

        $label = $W->Label($Crm->translate('Contact'));
        $input = $Crm->Ui()->SuggestContact()->setName('contact');
        $label->setAssociatedWidget($input);

        $form->addItem($this->handleFilterLabel($label, $input));

        $label = $W->Label($Crm->translate('Deal'));
        $input = $W->LineEdit()->setName('deal');
        $label->setAssociatedWidget($input);

        $form->addItem($this->handleFilterLabel($label, $input));

        $label = $W->Label($Crm->translate('Status'));
        $input = $W->Select()
            ->setName('status')
            ->addOption('', '')
            ->addOption('done', $Crm->translate('Done'))
            ->addOption('notdone', $Crm->translate('Not done'));
        $label->setAssociatedWidget($input);

        $form->addItem($this->handleFilterLabel($label, $input));
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleFilterInputWidget()
     */
    protected function handleFilterInputWidget($name, ORM_Field $field = null)
    {
        $Crm = $this->Crm();
        if ($name === 'responsible') {
            return $Crm->Ui()->SuggestCollaborator()->setMinChars(0);
        }
        return parent::handleFilterInputWidget($name, $field);
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::getFilterCriteria()
     */
    public function getFilterCriteria($filter = null)
    {
        $criteria = parent::getFilterCriteria($filter);

        $Crm = $this->Crm();

        /* @var $recordSet crm_TaskSet */
        $recordSet = $this->getRecordSet();

        if (isset($filter['status']) && $filter['status'] != '') {
            if ($filter['status'] == 'done') {
                $criteria = $criteria->_AND_($recordSet->isCompleted());
            }
            if ($filter['status'] == 'notdone') {
                $criteria = $criteria->_AND_($recordSet->isNotCompleted());
            }
        }

        $organizations = null;
        $organizationSet = $Crm->OrganizationSet();
        if (isset($filter['organization_ID']) && $filter['organization_ID'] != '') {
            $organizations = $organizationSet->select($organizationSet->id->is($filter['organization_ID']));
            unset($filter['organization_ID']);
        } elseif (isset($filter['organization']) && $filter['organization'] != '') {
            $organizations = $organizationSet->select($organizationSet->name->contains($filter['organization']));
            unset($filter['organization']);
        }

        if (isset($organizations)) {
            $criteria = $criteria->_AND_($recordSet->isTargetOfAny($organizations));
        }

        $contacts = null;
        $contactSet = $Crm->ContactSet();
        if (isset($filter['contact_ID']) && $filter['contact_ID'] != '') {
            $contacts = $contactSet->select($contactSet->id->is($filter['contact_ID']));
            unset($filter['contact_ID']);
        } else if (isset($filter['contact']) && $filter['contact'] != '') {
            $contacts = $contactSet->select(
                $contactSet->any(
                    $contactSet->lastname->contains($filter['contact']),
                    $contactSet->firstname->concat(' ')->concat($contactSet->lastname)->like($filter['contact'])
                )
            );
            unset($filter['contact']);
        }

        if (isset($contacts)) {
            $criteria = $criteria->_AND_($recordSet->isTargetOfAny($contacts));
        }


        $deals = null;
        $dealSet = $Crm->DealSet();
        $dealSet->addFullNumberField();
        if (isset($filter['deal_ID']) && $filter['deal_ID'] != '') {
            $deals = $dealSet->select($dealSet->id->is($filter['deal_ID']));
        } else if (isset($filter['deal']) && $filter['deal'] != '') {
            $deals = $dealSet->select(
                $dealSet->name->contains($filter['deal'])->_OR_($dealSet->fullNumber->contains($filter['deal']))
            );
        }

        if (isset($deals)) {
            $criteria = $criteria->_AND_($recordSet->isTargetOfAny($deals));
        }


        $responsibles = null;
        if (isset($filter['responsible_ID']) && $filter['responsible_ID'] != '') {
            $responsibles = $contactSet->select($contactSet->id->is($filter['responsible_ID']));
            unset($filter['responsible_ID']);
        } elseif (isset($filter['responsible']) && $filter['responsible'] != '') {
            $responsibles = $contactSet->select(
                $contactSet->any(
                    $contactSet->lastname->contains($filter['responsible']),
                    $contactSet->firstname->concat(' ')->concat($contactSet->lastname)->like($filter['responsible'])
                )
            );
            if (!isset($responsibles)) {//If no responsible found so it should force empty list
                $responsibles = array();
            }
            unset($filter['responsible']);
        }

        if (isset($responsibles)) {
            $users = array();
            foreach ($responsibles as $responsible) {
                $users[] = $responsible->user;
            }
            $criteria = $criteria->_AND_($recordSet->responsible->in($users));
        }

        return $criteria;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        /* @var $record crm_Task */
        if ($record->isCompleted()) {
            $this->addRowClass($row, 'task-completed');
        } else {
            if ($record->dueDate < date('Y-m-d')) {
                $this->addRowClass($row, 'task-late');
            }
        }


        return parent::handleRow($record, $row);
    }


    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return string
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $this->Crm();

        /* @var $record crm_Task */

        switch ($fieldPath) {
            case '_object_':
                $values = array();
                $organizations = $record->getLinkedOrganizations(null);
                foreach ($organizations as $organization) {
                    $values[] = $Crm->translate('Organization') . ' : ' . $organization->name;
                }

                $contacts = $record->getLinkedContacts(null);
                foreach ($contacts as $contact) {
                    $values[] = $Crm->translate('Contact') . ' : ' . $contact->getFullName();
                }

                $deals = $record->getLinkedDeals(null);
                foreach ($deals as $deal) {
                    $values[] = $Crm->translate('Deal') . ' : ' . $deal->getFullNumber() . " / " . $deal->name;
                }
                return implode("\n", $values);

            case '_done_':
                if ($record->isCompleted()) {
                    return $Crm->translate('Done');
                }
                return $Crm->translate('Not done');
        }

        return parent::computeCellTextContent($record, $fieldPath);
    }

    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        /* @var $record crm_Task */

        switch ($fieldPath) {
            case 'description':
                return $W->Html(nl2br($record->description));
                break;

            case 'responsible':
                $contact = $this->contactSet->get($this->contactSet->user->is($record->responsible));
                if (!$contact) {
                    return null;
                }
                return $W->Link(
                    $contact->getFullName(),
                    $Crm->Controller()->Contact()->display($contact->id)
                );

            case '_object_':
                $box = $W->VBoxItems();
                $organizations = $record->getLinkedOrganizations(null);
                foreach ($organizations as $organization) {
                    $box->addItem(
                        $W->Link($Crm->translate('Organization') . ' : ' . $organization->name, $Crm->Controller()->Organization()->display($organization->id))
                    );
                }

                $contacts = $record->getLinkedContacts(null);
                foreach ($contacts as $contact) {
                    $box->addItem(
                        $W->Link($Crm->translate('Contact') . ' : ' . $contact->getFullName(), $Crm->Controller()->Contact()->display($contact->id))
                    );
                }

                $deals = $record->getLinkedDeals(null);
                foreach ($deals as $deal) {
                    $box->addItem(
                        $W->Link($Crm->translate('Deal') . ' : ' . $deal->getFullNumber() . " / " . $deal->name, $Crm->Controller()->Deal()->display($deal->id))
                    );
                }
                return $box;

            case '_done_':
                $checkbox = $W->CheckBox();
                $checkbox->setname(array('tasks', $record->id));
                $checkbox->setAjaxAction(
                    $Crm->Controller()->Task()->markDone()
                );
                $checkbox->setValue($record->isCompleted());
                if (!$record->isUpdatable() || $record->isPlanned() || $record->responsible != bab_getUserId()) {
                    $checkbox->disable();
                }
                return $checkbox;

            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '', $this->Crm()->Controller()->Task()->edit($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '', $this->Crm()->Controller()->Task()->confirmDelete($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}



/**
 *
 */
class crm_taskPreview extends Widget_Frame
{
    public function __construct(crm_Contact $contact, $id = null)
    {
        $Crm = $contact->Crm();
        $W = bab_Widgets();

        $contactIcon = $W->Image();

        $T = @bab_functionality::get('Thumbnailer');
        if ($T) {
            if (!empty($contact->photo)) {
                 $T->setSourceFile($contact->photo);
            } else {
                $addon = bab_getAddonInfosInstance('LibCrm');
                 $T->setSourceFile($addon->getStylePath() . 'images/contact-default.png');
            }
            $T->setBorder(1, '#cccccc', 2, '#ffffff');
            $imageUrl = $T->getThumbnail(60, 60);
            $contactIcon->setUrl($imageUrl);
        }

        $addressFrame = self::AddressFrame($contact->getMainAddress());

        $cardLayout =
            $W->HBoxItems(
                $contactIcon,
                $W->VBoxItems(
                    $W->Label($contact->getFullName())->addClass('fn'),
                    $W->VBoxItems(
                        $addressFrame,
                        $W->Link($Crm->translate('Email: ') . $contact->getMainEmail(), $Crm->mailTo($contact->getMainEmail())),
                        $W->Label($Crm->translate('Phone: ') . $contact->getMainPhone())
                    )->setVerticalSpacing(0.5, 'em')
                )
            )->setHorizontalSpacing(1, 'em')
            ->addClass('icon-top-64 icon-top icon-64x64');

        parent::__construct($id, $cardLayout);
        $this->addClass('vcard');
    }


    protected static function AddressFrame(crm_Address $address)
    {
        require_once FUNC_CRM_UI_PATH . 'address.ui.php';
        $addressFrame = new crm_AddressFrame($address);
        return $addressFrame;
    }
}



class crm_TaskFrame extends crm_CardFrame
{
    /**
     * @var crm_Task
     */
    protected $task;

    /**
     * @var crm_TaskSet
     */
    protected $set;

    public function __construct(Func_Crm $Crm, crm_Task $task, $id, $layout)
    {
        $W = bab_Widgets();

        parent::__construct($Crm, $id, $layout);

        $this->task = $task;
        $this->set = $task->getParentSet();
    }



    protected function responsible()
    {
        if (!$this->task->responsible) {
            return null;
        }

        return $this->labeledField(
            crm_translate('Responsible'),
            $this->task,
            $this->set->responsible
        );
    }


    protected function subTasks()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $res = $this->task->selectChildTasks();

        if (0 === $res->count()) {
            return null;
        }

        $frame = $W->Frame(null,
            $W->VBoxLayout()
                ->setSpacing(.5, 'em')
        );

        foreach($res as $subTask) {
            $card = new crm_TaskCardFrame($Crm, $subTask);
            $frame->addItem($card);
        }

        return $W->VBoxItems(
            $W->Title(crm_translate('Sub-tasks')),
            $frame
        )->setVerticalSpacing(.5, 'em');
    }




    protected function getName()
    {
        $W = bab_Widgets();
        return $W->Title(implode(' / ', $this->task->getPathName()), 1);
    }

    protected function createdBy()
    {
        if (!$this->set->createdBy->isValueSet($this->task->createdBy)) {
            return null;
        }

        if ($this->task->createdBy === $this->task->responsible) {
            return null;
        }

        return $this->labelStr(crm_translate('Created by'), $this->set->createdBy->output($this->task->createdBy));
    }




    protected function description()
    {
        return $this->labeledField(
            crm_translate('Description'),
            $this->task,
            $this->set->description
        );
    }



    /**
     * duration in hours
     *
     *
     * @param string $label
     * @param ORM_Field $field
     *
     * @return Widget_Item
     */
    protected function durationField($label, ORM_Field $field)
    {
        $fieldName = $field->getName();
        $method = 'getTree'.$fieldName;
        $value = $this->task->$method();

        if (!$field->isValueSet($value)) {
            return null;
        }

        $decimal = (float) $value;
        $displayable = $field->output($value);

        if (100 === 100 * $decimal) {
            $text = crm_translate('%s hour');
        } else {
            $text = crm_translate('%s hours');
        }

        return $this->labelStr($label, sprintf($text, $displayable));
    }



    protected function work()
    {
        return $this->durationField(
            crm_translate('Planned work'),
            $this->set->work
        );
    }


    protected function actualWork()
    {
        return $this->durationField(
            crm_translate('Actual work'),
            $this->set->actualWork
        );
    }


    protected function remainingWork()
    {
        return $this->durationField(
            crm_translate('Remaining work'),
            $this->set->remainingWork
        );
    }



    protected function completion()
    {
        $W = bab_Widgets();

        $completion = $this->task->getTreeCompletion();

        if (!$completion) {
            return null;
        }

        return $W->Gauge()->setProgress($completion);
    }



    protected function startedAction()
    {
        if (!$this->set->startedAction->isValueSet($this->task->startedAction)) {
            return null;
        }

        if ($this->task->isCompleted()) {
            return null;
        }

        return $this->labelStr(
            crm_translate('Do this task:'),
            $W->Link($Crm->translate('Access the task page'), $this->task->startedAction)
        );
    }
}


class crm_TaskFullFrame extends crm_TaskFrame
{




    public function __construct(Func_Crm $Crm, crm_Task $task, $id = null)
    {
        $W = bab_Widgets();
        parent::__construct($Crm, $task, $id, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->task = $task;
        $this->set = $task->getParentSet();

        $this->addInfos();

        $this->addClass('crm-detailed-info-frame');
        $this->addClass(Func_Icons::ICON_LEFT_24);
    }


    protected function addInfos()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $this->addItem($this->getName());

        if ($this->task->isCompleted()) {
            $this->addItem($W->Title($Crm->translate('Completed task'), 3));
        }

        $this->addItem($this->responsible());

        $this->addItem($this->createdBy());


        $this->addItem(
            $W->FlowItems(
                $this->dueDate(),
                $this->scheduledStart(),
                $this->scheduledFinish()
            )->setHorizontalSpacing(4, 'em')
        );

        $this->addItem($this->description());

        $this->addItem($this->durationQuantities());

        $this->addItem($this->completion());

        $this->addItem($this->startedAction());


        // display sub-tasks as clickable card frames
        $this->addItem($this->subTasks());
    }


    protected function dueDate()
    {
        return $this->labeledField(
            crm_translate('Due date'),
            $this->task,
            $this->set->dueDate
        );
    }



    protected function scheduledDate($label, $name)
    {
        $value = $this->task->$name;
        $field = $this->set->$name;

        if (!$field->isValueSet($value)) {
            return null;
        }

        $displayable = $field->output($value);

        if ($this->task->scheduledAllDay) {
            list($displayable) = explode(' ', $displayable);
        }

        return $this->labelStr($label, $displayable);
    }



    protected function scheduledStart()
    {
        return $this->scheduledDate(
            crm_translate('Scheduled start date'),
            'scheduledStart'
        );
    }


    protected function scheduledFinish()
    {
        return $this->scheduledDate(
            crm_translate('Scheduled finish date'),
            'scheduledFinish'
        );
    }



    /**
     * @return Widget_Layout
     */
    protected function durationQuantities()
    {
        $W = bab_Widgets();

        return $W->FlowItems(
            $this->work(),
            $this->actualWork(),
            $this->remainingWork()
        )->setHorizontalSpacing(3, 'em');
    }
}



function crm_getTaskCompleteLink(Func_Crm $Crm, crm_Task $task)
{
    $W = bab_Widgets();
    return $W->Link(
        $task->isPlanned() ? $Crm->translate('I worked on this task') : $Crm->translate('I completed this task'),
        $Crm->Controller()->Task()->complete($task->id)
    );
}



class crm_TaskCardFrame extends crm_TaskFrame
{

    const SHOW_ALL = 0xFFFF;


    /**
     * @var crm_Task
     */
    protected $task;

    /**
     * @var int
     */
    protected $attributes;

    protected $section = null;
    protected $inlineMenu = null;
    protected $popupMenu = null;
    protected $toolbar = null;

    static $now = null;


    /**
     * @param Func_Crm $Crm
     * @param crm_Task $task
     * @param int $attributes
     * @param string $id
     */
    public function __construct(Func_Crm $Crm, crm_Task $task, $attributes = crm_TaskCardFrame::SHOW_ALL, $id = null)
    {
        $W = bab_Widgets();

        parent::__construct($Crm, $task, $id, $W->Layout());

        $Access = $Crm->Access();

        if (!isset(self::$now)) {
            self::$now = BAB_DateTime::now();
            self::$now = self::$now->getIsoDate();
        }


        $this->task = $task;
        $this->attributes = $attributes;

        $taskSection = $W->Section(
            $task->getNameWidget(),
            $this->taskContent = $W->VBoxItems(
                $W->RichText($task->description)
            )->setVerticalSpacing(.5,'em')->addClass('crm-small'),
            6
        );
        $taskSection->setFoldable(true, true);
        if ($task->dueDate !== '0000-00-00') {
            $dateDiff = BAB_DateTime::dateDiffIso(self::$now, $task->dueDate);
            if ($dateDiff > 1)
            {
                $days = $Crm->translate('days');
            } else {
                $days = $Crm->translate('day');
            }

            if (self::$now <  $task->dueDate) {
                $taskSection->setSubHeaderText(BAB_DateTimeUtil::relativeFutureDate($task->dueDate, false, false) . ' (' . $dateDiff . ' '.$days.')');
            } else {
                $taskSection->setSubHeaderText(BAB_DateTimeUtil::relativePastDate($task->dueDate, false, false) . ' (' . $dateDiff .' '.$days.')');
            }
        }
        $taskSection->addClass('crm-task');

        $this->section = $taskSection;
        $this->inlineMenu = $this->section->addContextMenu('inline');

        $taskSection->addClass(Func_Icons::ICON_LEFT_16);

        $this->addItem(
            $taskSection
        );

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $this->item->setCacheHeaders(Widget_Item::CacheHeaders()->setLastModified(BAB_DateTime::fromIsoDateTime($task->modifiedOn)));


        // add owner if not me

        if ($task->responsible > 0 && bab_getUserId() != $task->responsible)
        {
            $taskSection->addItem($this->responsible());
        }

        $taskSection->addItem($this->remainingWork());
        $taskSection->addItem($this->completion());



        if (!empty($task->startedAction) && $task->completion != 100) {
            $this->addMenuItem(
                $W->Link(
                    $Crm->translate('Do this task'),
                    $task->startedAction
                ),
                Func_Icons::ACTIONS_GO_NEXT
            );

        } else {
            if($Access->updateTask($task)) {
                $this->addMenuItem(
                    $W->Link(
                        $Crm->translate('Edit this task...'),
                        $Crm->Controller()->Task()->edit($task->id)
                    )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
                    Func_Icons::ACTIONS_DOCUMENT_EDIT
                );
            }
        }

        $this->addMenuItem(
            $W->Link(
                $Crm->translate('View details'),
                $Crm->Controller()->Task()->display($task->id)
            ),
            Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        );
        $this->addToolbarItem(
            $W->Link(
                $Crm->translate('View details'),
                $Crm->Controller()->Task()->display($task->id)
            ),
            Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        );




        if ($task->isDuplicable()) {
            $this->addMenuItem(
                $W->Link(
                    $Crm->translate('Duplicate this task'),
                    $Crm->Controller()->Task()->duplicate($task->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
                Func_Icons::ACTIONS_EDIT_COPY
            );
        }

        if ($Access->deleteTask($task)) {
            $this->addMenuItem(
                $W->Link(
                    $Crm->translate('Delete this task'),
                    $Crm->Controller()->Task()->confirmDelete($task->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
                Func_Icons::ACTIONS_EDIT_DELETE
            );
        }

        if ($Access->updateTask($task) && !$task->isPlanned()) {
            $this->addMenuItem(
                $W->Link(
                    $Crm->translate('Done'),
                    $Crm->Controller()->Task()->markDone($task->id)
                )
                ->setAjaxAction(null, $this->getId()),
                Func_Icons::ACTIONS_DIALOG_OK
            );
        }
    }



    /**
    * @param Widget_Displayable_Item $item
    *
    * @return self
    */
    public function addLinkedItem($item)
    {
        $this->inlineMenu->addItem($item);

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     * @param bool $important
     *
     * @return self
     */
    public function addMenuItem(Widget_Displayable_Interface $item, $classname = null, $important = true)
    {
        if (!isset($this->popupMenu)) {
            $this->popupMenu = $this->section->addContextMenu('popup');
            $this->popupMenu->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);
        }
        $this->popupMenu->addItem($item);

        $item->addClass('icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     *
     * @return crm_TaskCardFrame
     */
    public function addToolbarItem(Widget_Displayable_Interface $item, $classname = null)
    {
        $W = bab_Widgets();
        if (!isset($this->toolbar)) {
            $this->toolbar = $W->FlowLayout()
                ->setSpacing(4, 'px')
                ->addClass('icon-left-16 icon-left icon-16x16 crm-small');
            $this->section->addItem($this->toolbar);
        }
        $this->toolbar->addItem($item->addClass('widget-actionbutton'));

        $item->addClass('widget-icon icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }
        return $this;
    }

}


/**
 *
 * extends Widget_VBoxLayout
 * @see Widget_VBoxLayout
 */
class crm_UserTasks extends crm_UiObject
{
    /**
     * @var int
     */
    protected $userId = null;
    
    /**
     * @param Func_Crm $crm
     */
    public function __construct(Func_Crm $crm = null)
    {
        parent::__construct($crm);
        // We simulate inheritance from Widget_VBoxLayout.
        $this->setInheritedItem(bab_Functionality::get('Widgets')->VBoxLayout());
    }

    
    /**
     * @param crm_Task $task
     * @return crm_TaskCardFrame
     */
    public function taskPreview(crm_Task $task)
    {
        $Crm = $task->Crm();

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

        $taskPreview = $Crm->Ui()->TaskCardFrame($task);

        $linkSet = $Crm->LinkSet();
        $links = $linkSet->selectForTarget($task);

        foreach ($links as $link) {
            list(,$classname) = explode('_', $link->sourceClass);
            switch ($classname) {
                case 'Contact':
                    if ($contact = $Crm->ContactSet()->get($link->sourceId)) {
                        $taskPreview->addLinkedItem(
                            $Crm->Ui()->ContactPhoto($contact, 24, 24, false)
                        );
                    }
                    break;
                case 'Organization':
                    if ($org = $Crm->OrganizationSet()->get($link->sourceId)) {
                        $taskPreview->addLinkedItem(
                            $Crm->Ui()->OrganizationLogo($org, 24, 24, false)
                        );
                    }
                    break;
                case 'Deal':
                    if ($deal = $Crm->DealSet()->get($link->sourceId)) {
                        $taskPreview->addLinkedItem(
                            $Crm->Ui()->DealPhoto($deal, 24, 24, false, true)
                        );
                    }
                    break;
            }
        }

        return $taskPreview;
    }


    protected function section($name, $classname)
    {
        $W = bab_Widgets();
        $sectionTitleLevel = 3;
        $section = $W->Section(
            $name,
            $W->VBoxItems()->addClass($classname),
            $sectionTitleLevel
        );

        $section->addClass('compact')
            ->setPersistent(true)
            ->setFoldable(false);

        return $section;
    }

    /**
     * Defines the user
     *
     * @param int		$userId		The ovidentia user id
     * @return crm_UserTasks
     */
    public function setUser($userId)
    {
        $this->userId = $userId;
        return $this;
    }


    /**
     *
     * @param crm_TaskSet $taskSet
     * @return crm_Task[]
     */
    protected function selectTasks(crm_TaskSet $taskSet)
    {
        $tasks = $taskSet->select(
            $taskSet->responsible->is($this->userId)
            ->_AND_($taskSet->isNotCompleted())
        );

        return $tasks;
    }


    /**
     * {@inheritDoc}
     * @see crm_UiObject::display()
     */
    public function display(widget_Canvas $canvas)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $this->setVerticalSpacing(0.5, 'em');
        $this->addClass(Func_Icons::ICON_LEFT_16);

        $taskSet = $Crm->TaskSet();

        $tasks = $this->selectTasks($taskSet);

        $tasks->orderAsc($taskSet->dueDate);

        $nbUnscheduledTasks = 0;
        $nbOverdueTasks = 0;
        $nbTodayTasks = 0;
        $nbThisWeekTasks = 0;
        $nbNextWeekTasks = 0;
        $nbLaterTasks = 0;

        $unscheduledTasks = $this->section($Crm->translate('Unscheduled'), 'crm-tasks-summary-unscheduled');

        $overdueTasks = $this->section($Crm->translate('Overdue'), 'crm-tasks-summary-overdue');

        $todayTasks = $this->section($Crm->translate('Today'), 'crm-tasks-summary-today');

        $thisWeekTasks = $this->section($Crm->translate('This week'), 'crm-tasks-summary');

        $nextWeekTasks = $this->section($Crm->translate('Next week'), 'crm-tasks-summary');

        $laterTasks = $this->section($Crm->translate('Later'), 'crm-tasks-summary');

        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $today = BAB_DateTime::now();
        $endOfWeek = $today->cloneDate();
        $endOfWeek->add(1, BAB_DATETIME_DAY);
        while ($endOfWeek->getDayOfWeek() != 1) {
            $endOfWeek->add(1, BAB_DATETIME_DAY);
        }
        $endOfNextWeek = $endOfWeek->cloneDate();
        $endOfNextWeek->add(7, BAB_DATETIME_DAY);

        $todayIso = $today->getIsoDate();
        $endOfWeekIso = $endOfWeek->getIsoDate();
        $endOfNextWeekIso = $endOfNextWeek->getIsoDate();

        foreach ($tasks as $task) {
            /* @var $task crm_Task  */
            if ($task->dueDate === '0000-00-00') {
                $nbUnscheduledTasks++;
                $taskFrame = $unscheduledTasks;
            } else if ($task->dueDate < $todayIso) {
                $nbOverdueTasks++;
                $taskFrame = $overdueTasks;
            } else if ($task->dueDate === $todayIso) {
                $nbTodayTasks++;
                $taskFrame = $todayTasks;
            } else if ($task->dueDate < $endOfWeekIso) {
                $nbThisWeekTasks++;
                $taskFrame = $thisWeekTasks;
            } else if ($task->dueDate < $endOfNextWeekIso) {
                $nbNextWeekTasks++;
                $taskFrame = $nextWeekTasks;
            } else {
                $nbLaterTasks++;
                $taskFrame = $laterTasks;
            }

            $taskFrame->addItem(
                $this->taskPreview($task)
                    ->setSizePolicy('widget-list-element')
            );
        }

        if ($nbUnscheduledTasks > 0) {
            $m = $unscheduledTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbUnscheduledTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($unscheduledTasks);
        }
        if ($nbOverdueTasks > 0) {
            $m = $overdueTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbOverdueTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($overdueTasks);
        }
        if ($nbTodayTasks > 0) {
            $m = $todayTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbTodayTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($todayTasks);
        }
        if ($nbThisWeekTasks > 0) {
            $m = $thisWeekTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbThisWeekTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($thisWeekTasks);
        }
        if ($nbNextWeekTasks > 0) {
            $m = $nextWeekTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbNextWeekTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($nextWeekTasks);
        }
        if ($nbLaterTasks > 0) {
            $m = $laterTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbLaterTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($laterTasks);
        }

        return parent::display($canvas);
    }
}


/**
 *
 * @param int $userId
 *
 * @return crm_UserTasks
 */
function crm_userTasks($userId)
{
    $tasksFrame = new crm_UserTasks($userId);
    $taskFrame->setUser($userId);
    return $tasksFrame;
}



/**
 *
 * @param crm_Record $object
 *
 * @return widget_Frame
 */
function crm_userTasksForCrmObject(crm_Record $object, $linkType = null)
{
    $W = bab_Widgets();

    $Crm = $object->Crm();
    $Access = $Crm->Access();

    $tasksFrame = $W->VBoxItems();
    $tasksFrame->setVerticalSpacing(0.5, 'em');

    $taskSet = $Crm->TaskSet();

    $taskLinks = $taskSet->selectLinkedTo($object, $linkType);

    $taskLinks->setCriteria($taskLinks->getCriteria()->_AND_($taskLinks->getSet()->targetId->isNotCompleted()));

    $taskLinks->orderAsc($taskLinks->getSet()->targetId->dueDate);

    $contactSet = $Crm->ContactSet();

    $linkSet = $Crm->LinkSet();

    $myContact = $Crm->getMyContact();


    $nbUnscheduledTasks = 0;
    $nbOverdueTasks = 0;
    $nbTodayTasks = 0;
    $nbThisWeekTasks = 0;
    $nbNextWeekTasks = 0;
    $nbLaterTasks = 0;

    $overdueTasks = $W->Section(
        $Crm->translate('Overdue'), $W->VBoxItems()->addClass('crm-tasks-summary crm-tasks-summary-overdue'), 5
    )
    ->addClass('compact')
    ->setFoldable(false);
    $unscheduledTasks = $W->Section(
        $Crm->translate('Unscheduled'), $W->VBoxItems()->addClass('crm-tasks-summary crm-tasks-summary-unscheduled'), 5
    )
    ->addClass('compact')
    ->setFoldable(false);
    $todayTasks = $W->Section(
        $Crm->translate('Today'), $W->VBoxItems()->addClass('crm-tasks-summary crm-tasks-summary-today'), 5
    )
    ->addClass('compact')
    ->setFoldable(false);
    $thisWeekTasks = $W->Section(
        $Crm->translate('This week'), $W->VBoxItems()->addClass('crm-tasks-summary'), 5
    )
    ->addClass('compact')
    ->setFoldable(false);
    $nextWeekTasks = $W->Section(
        $Crm->translate('Next week'), $W->VBoxItems()->addClass('crm-tasks-summary'), 5
    )
    ->addClass('compact')
    ->setFoldable(false);
    $laterTasks = $W->Section(
        $Crm->translate('Later'), $W->VBoxItems()->addClass('crm-tasks-summary'), 5
    )
    ->addClass('compact')
    ->setFoldable(false);

    require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
    $today = BAB_DateTime::now();


    $endOfWeek = $today->cloneDate();
    $endOfWeek->add(1, BAB_DATETIME_DAY);
    while ($endOfWeek->getDayOfWeek() != 1) {
        $endOfWeek->add(1, BAB_DATETIME_DAY);
    }
    $endOfNextWeek = $endOfWeek->cloneDate();
    $endOfNextWeek->add(7, BAB_DATETIME_DAY);

    $todayIso = $today->getIsoDate();
    $endOfWeekIso = $endOfWeek->getIsoDate();
    $endOfNextWeekIso = $endOfNextWeek->getIsoDate();

    $nbTasks = 0;
    foreach ($taskLinks as $taskLink) {
        $task = $taskLink->targetId;
        if ($Access->readTask($task)) {

            if ($task && $task->selectChildTasks()->count() > 0) {
                continue;
            }

            if ($task->dueDate == '0000-00-00') {
                $nbUnscheduledTasks++;
                $taskFrame = $unscheduledTasks;
            } else if ($task->dueDate < $todayIso) {
                $nbOverdueTasks++;
                $taskFrame = $overdueTasks;
            } else if ($task->dueDate == $todayIso) {
                $nbTodayTasks++;
                $taskFrame = $todayTasks;
            } else if ($task->dueDate < $endOfWeekIso) {
                $nbThisWeekTasks++;
                $taskFrame = $thisWeekTasks;
            } else if ($task->dueDate < $endOfNextWeekIso) {
                $nbNextWeekTasks++;
                $taskFrame = $nextWeekTasks;
            } else {
                $nbLaterTasks++;
                $taskFrame = $laterTasks;
            }

            $nbTasks++;


            $taskPreview = $Crm->Ui()->TaskCardFrame($task);



            if (!empty($task->startedAction) && $task->completion != 100) {
                $taskPreview->addToolbarItem(
                    $W->Link(
                        $Crm->translate('Do this task'),
                        $task->startedAction
                    ),
                    Func_Icons::ACTIONS_GO_NEXT
                );
            } else if ($task->canUserWork() && $task->completion != 100) {
                $taskPreview->addToolbarItem(
                    crm_getTaskCompleteLink($Crm, $task),
                    Func_Icons::ACTIONS_DIALOG_OK
                );
            }

            $taskFrame->addItem(
                $taskPreview
            );


            if ($nbOverdueTasks > 0) {
                $tasksFrame->addItem($overdueTasks);
            }
            if ($nbUnscheduledTasks > 0) {
                $tasksFrame->addItem($unscheduledTasks);
            }
            if ($nbTodayTasks > 0) {
                $tasksFrame->addItem($todayTasks);
            }
            if ($nbThisWeekTasks > 0) {
                $tasksFrame->addItem($thisWeekTasks);
            }
            if ($nbNextWeekTasks > 0) {
                $tasksFrame->addItem($nextWeekTasks);
            }
            if ($nbLaterTasks > 0) {
                $tasksFrame->addItem($laterTasks);
            }
        }
    }
    if ($nbTasks === 0) {
        $tasksFrame->addItem(
            $W->Label($Crm->translate('No incoming task'))
                ->addClass('crm-empty')
        );
    }
    return $tasksFrame;
}



/**
 *
 * @param crm_Contact $contact
 *
 * @return widget_Frame
 */
function crm_userTasksForContact(crm_Contact $contact)
{
    $W = bab_Widgets();

    $Crm = $contact->Crm();

    $tasksFrame = crm_userTasksForCrmObject($contact);

    $tasksFrame = $W->Section(
        $Crm->translate('Incoming tasks for this contact'),
        $tasksFrame,
        4
    );
//	$tasksFrame->additem(
//		$W->Title(, 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM),
//		0
//	);

    return $tasksFrame;
}



/**
 * @param crm_Deal $deal
 *
 * @return widget_Frame
 */
function crm_userTasksForDeal(crm_Deal $deal)
{
    $W = bab_Widgets();

    $Crm = $deal->Crm();


    $tasksFrame = crm_userTasksForCrmObject($deal);

    $tasksFrame->additem(
        $W->Title($Crm->translate('Incoming tasks for this deal'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM),
        0
    );

    return $tasksFrame;
}


/**
 * @param crm_Organization $org
 *
 * @return widget_Frame
 */
function crm_userTasksForOrganization(crm_Organization $org)
{
    $W = bab_Widgets();

    $Crm = $org->Crm();

    $tasksFrame = crm_userTasksForCrmObject($org);

    return $tasksFrame;
}




/**
 * @return crm_Editor
 */
class crm_TaskEditor extends crm_Editor
{

    private $task;


    protected $scheduledTimerPickers = array();


    /**
     * @param Func_Crm		$Crm
     * @param string		$id
     * @param Widget_Layout	$layout
     */
    public function __construct(Func_Crm $Crm, crm_Task $task = null, $id = null, Widget_Layout $layout = null)
    {
        $this->task = $task;

        parent::__construct($Crm, $id, $layout);

        $this->setName('task');

        if (isset($task)) {

            $values = $task->getValues();

            $values['scheduledStart'] = $this->fixDateTime($values['scheduledStart'], $values['scheduledAllDay']);
            $values['scheduledFinish'] = $this->fixDateTime($values['scheduledFinish'], $values['scheduledAllDay']);

            $this->setValues($values, array('task'));
            $this->setHiddenValue('task[id]', $task->id);
        }
    }

    /**
     * @param string $dateTime
     * @return array
     */
    public function fixDateTime($dateTime, $ignoreTime)
    {
        list($date, $time) = explode(' ', $dateTime);

        if ('00:00:00' === $time && $ignoreTime) {
            $time = '';
        }

        return array(
            'date' => $date,
            'time' => $time
        );
    }


    /**
     * Makes an association between the task to create
     * and a specified crm object.
     *
     * @param crm_Record $object
     *
     * @return crm_TaskEditor
     */
    public function associateTo(crm_Record $object)
    {
        list($addon, $objectClass) = explode('_', get_class($object));
        $this->setHiddenValue('task[for]', $objectClass . ':' . $object->id);
        return $this;
    }


    protected function prependFields()
    {
        $W = bab_Widgets();

        $Crm = $this->Crm();

        $taskSet = $Crm->TaskSet();

        $taskCategorySet = $Crm->TaskCategorySet();

        $categories = $taskCategorySet->select();


        if ($categories->count() > 0) {
            $categoryFormItem = $this->labelledField(
                $Crm->translate('Category'),
                crm_OrmWidget($taskSet->category),
                'category'
            );
        } else {
            $categoryFormItem = null;
        }


        $summaryFormItem = $this->labelledField(
            $Crm->translate('Summary'),
            $W->LineEdit()->setSize(52)->setMaxSize(255)
                ->addClass('widget-fullwidth')
                ->setMandatory(true, $Crm->translate('The task summary must not be empty.')),
            'summary'
        );
        $dueDateFormItem = $this->labelledField(
            $Crm->translate('Due date'),
            $W->DatePicker(),
            'dueDate'
        );






        $descriptionFormItem = $this->labelledField(
            $Crm->translate('Description'),
            $W->TextEdit()
                ->setColumns(50)
                ->addClass('widget-fullwidth widget-autoresize crm-task-description'),
            'description'
        );
        $responsibleFormItem = $this->labelledField(
            $Crm->translate('Responsible'),
            $W->UserPicker()->setSizePolicy(Func_Icons::ICON_LEFT_16),
            'responsible'
        );

        $this->colon();

        $this->addItem($summaryFormItem);
        $this->addItem(
            $W->FlowItems(
                $categoryFormItem,
                $dueDateFormItem,
                $this->scheduledStart(),
                $this->scheduledFinish(),
                $this->scheduledAllDay()
            )->setHorizontalSpacing(4, 'em')->setVerticalAlign('bottom')
        );


        $this->addItem(
            $W->FlowItems(
                $this->work(),
                $this->actualWork(),
                $this->remainingWork()
            )->setHorizontalSpacing(4, 'em')
        );
        $this->addItem($descriptionFormItem);
        $this->addItem($responsibleFormItem);

        $this->addItem($this->invitation());

        $this->setCancelAction($Crm->Controller()->Task()->cancel());
        $this->setSaveAction($Crm->Controller()->Task()->save());
        $this->setHiddenValue('tg', bab_rp('tg'));
    }


    public function scheduledDate($label = null, $value = null)
    {
        $W = bab_Widgets();

        $datePicker = $W->DatePicker()->setName('date');
        $timePicker = $W->Select()->setName('time');

        for($t =0; $t<24*60; $t+=30) {
            $h = floor($t / 60);
            $m = $t -$h*60;

            $time = sprintf('%02d:%02d', $h, $m);

            $timePicker->addOption($time.':00', $time);
        }

        if (isset($value['date'])) {
            $datePicker->setValue($value['date']);
        }

        if (isset($value['time'])) {
            $timePicker->setValue($value['time']);
        }

        if (isset($label)) {
            $datePicker->setAssociatedLabel($label);
        }

        $this->scheduledTimerPickers[] = $timePicker;

        $frame = $W->Frame(null,
            $W->FlowItems(
                $datePicker,
                $timePicker
            )->setHorizontalSpacing(.5, 'em')->setVerticalAlign('bottom')
        );


        $frame->crm_date = $datePicker;
        $frame->crm_time = $timePicker;


        return $frame;
    }




    protected function scheduledStart()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        return $W->VBoxItems(
            $label = $W->Label($Crm->translate('Scheduled start')),
            $this->scheduledDate($label)->setName(__FUNCTION__)
        )->setVerticalSpacing(.2, 'em');

    }


    protected function scheduledFinish()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();


        return $W->VBoxItems(
            $label = $W->Label($Crm->translate('Scheduled finish')),
            $this->scheduledDate($label)->setName(__FUNCTION__)
        )->setVerticalSpacing(.2, 'em');
    }


    public function scheduledAllDayCheckBox()
    {
        $W = bab_Widgets();

        $checkbox = $W->CheckBox();
        foreach($this->scheduledTimerPickers as $timePicker) {
            $checkbox->setAssociatedDisplayable($timePicker, array('0'));
        }

        return $checkbox;
    }


    protected function scheduledAllDay()
    {

        $Crm = $this->Crm();



        return $this->labelledField(
            $Crm->translate('All day'),
            $this->scheduledAllDayCheckBox(),
            __FUNCTION__
        );
    }



    protected function work() {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $lineEdit = $W->LineEdit()->setSize(6)->setMaxSize(8);

        if (isset($this->task) && $this->task->hasStarted()) {
            $lineEdit->disable();
        }

        return $this->labelledField(
            $Crm->translate('Planned work'),
            $lineEdit,
            'work',
            null,
            crm_translate('Hours')
        );
    }


    protected function actualWork() {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        return $this->labelledField(
            $Crm->translate('Actual work'),
            $W->LineEdit()->setSize(6)->setMaxSize(8),
            'actualWork',
            null,
            crm_translate('Hours')
        );
    }


    protected function remainingWork()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        return $W->LabelledWidget(
            $Crm->translate('Remaining work'),
            $W->LineEdit()->setSize(6)->setMaxSize(8),
            'remainingWork',
            null,
            crm_translate('Hours')
        );
    }

    protected function invitation()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $checkbox = $W->CheckBox();

        return $W->LabelledWidget(
            $Crm->translate('Send email invitation'),
            $checkbox,
            'sendInvitation'
        );
    }
}



/**
 *
 * @param crm_Task $task
 *
 * @return widget_Frame
 */
function crm_taskContacts(crm_Task $task)
{
    require_once dirname(__FILE__) . '/contact.ui.php';

    $W = bab_Widgets();
    $Crm = $task->Crm();

    $contacts = $task->getLinkedContacts();
    if (count($contacts) === 0) {
        return $W->Frame();
    }

    $contactFrame = $W->VBoxLayout()
        ->addItem($W->HBoxItems(
            $W->Title($Crm->translate('Contacts linked to this task'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
        )
    );

    $contactAccordion = $W->Accordions();
    foreach ($contacts as $contact) {
        $contactCard = $Crm->Ui()->contactCardFrame($contact);
        $address = $contact->getMainAddress();
        $contactAccordion->addPanel(
            $contact->getFullName() . ' - ' . $Crm->translate($contact->getMainPosition()),
            $W->VboxItems(
                $contactCard,
                $W->FlowLayout()->addClass('crm-context-actions')->addClass('icon-left-16 icon-left icon-16x16')
                    ->addItem($W->Link($W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_USER_PROPERTIES), $Crm->Controller()->Contact()->edit($contact->id)))
                    ->addItem($W->Link($W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Contact()->display($contact->id)))
            )->setVerticalSpacing(0.5, 'em')
        );
    }

    $contactFrame->addItem($contactAccordion);
    return $contactFrame;
}





/**
 *
 * @param crm_Task $task
 *
 * @return widget_Frame
 */
function crm_taskLinkedObjects(crm_Task $task)
{
    require_once dirname(__FILE__).'/task.linked.ui.php';

    $W = bab_Widgets();
    $Crm = $task->Crm();
    $Ui = $Crm->Ui();
    $Ui->includeContact();
    $Ui->includeDeal();
    $Crm->includeOrganizationSet();

    $objectsFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em');




    $objectsFrame->addItem(crm_taskLinkedAncestor($task));
    $objectsFrame->addItem(crm_taskLinkedContacts($task));

    $orgs = array();
    if (isset($Crm->Deal)) {
        $objectsFrame->addItem(crm_taskLinkedDeals($task, $orgs));
    }

    $objectsFrame->addItem(crm_taskLinkedOrganizations($task, $orgs));


    // select record linked to the task
    foreach($task->getLinkedObjects() as $record) {
        switch(true) {
            case $record instanceof crm_Deal:
            case $record instanceof crm_Organization:
            case $record instanceof crm_Contact:
                continue 2;
        }

        if (isset($record)) {
            $objectsFrame->addItem($Ui->getCardFrame($record));
        }
    }

    return $objectsFrame;
}



/**
 *
 * @param crm_Task $task
 *
 * @return widget_Frame
 */
function crm_taskOrganizations(crm_Task $task)
{
    require_once dirname(__FILE__) . '/organization.ui.php';

    $W = bab_Widgets();
    $Crm = $task->Crm();

    $orgs = $task->getLinkedOrganizations();
    if (count($orgs) === 0) {
        return $W->Frame();
    }

    $orgFrame = $W->VBoxLayout()
        ->addItem($W->HBoxItems(
            $W->Title($Crm->translate('Organizations linked to this task'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
        )
    );

    $orgAccordion = $W->Accordions();
    foreach ($orgs as $org) {
        $orgCard = new crm_OrganizationCardFrame($org);
        $address = $org->address;
        $orgAccordion->addPanel(
            $org->name,
            $W->VboxItems(
                $orgCard,
                $W->FlowLayout()->addClass('crm-context-actions')->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
//					->addItem($W->Link($W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_USER_PROPERTIES), $Crm->Controller()->Organization()->edit($org->id)))
                    ->addItem($W->Link($W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Organization()->display($org->id)))
            )->setVerticalSpacing(0.5, 'em')
        );
    }
//		$notes = $contact->selectNotes(); // TODO Remove

    $orgFrame->addItem($orgAccordion);
    return $orgFrame;
}



/**
 *
 * @param crm_Record $object
 *
 * @return widget_Frame
 */
function crm_performedUserTasksForCrmObject(crm_Record $object)
{
    $W = bab_Widgets();

    $Crm = $object->Crm();

    $tasksFrame = $W->VBoxItems();
    $tasksFrame->setVerticalSpacing(0.5, 'em');

    $taskSet = $Crm->TaskSet();

    $taskLinks = $taskSet->selectLinkedTo($object);

    $taskLinks->setCriteria($taskLinks->getCriteria()->_AND_($taskLinks->getSet()->targetId->isCompleted()));

    $taskLinks->orderAsc($taskLinks->getSet()->targetId->dueDate);

    $tasksFrame = $W->VBoxLayout();
    $tasksFrame->addClass('crm-tasks-summary');

    $nbTasks = 0;
    foreach ($taskLinks as $taskLink) {
        $task = $taskLink->targetId;

        $nbTasks++;

        $taskPreview = new crm_TaskCardFrame($Crm, $task);

        $taskPreview->addToolbarItem(
            $W->Link(
                $Crm->translate('View page'),
                $Crm->Controller()->Task()->display($task->id)
            ),
            Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        );

        $tasksFrame->addItem(
            $taskPreview
        );
    }

    return $tasksFrame;
}


/**
 * A Ui class used to display an task as seen in history frames.
 */
class crm_HistoryTask extends crm_HistoryElement
{
    /**
     * @param crm_Task $task
     * @param string $mode One of crm_HistoryElement::MODE_* constant.
     * @param string $id Optional widget id.
     */
    public function __construct(crm_Task $task, $mode = null, $id = null)
    {
        $Crm = $task->Crm();
        parent::__construct($Crm, $mode, $id);
        $W = bab_Widgets();

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

        $Crm->Ui()->includeOrganization();
        $Crm->Ui()->includeDeal();
        $Crm->Ui()->includeContact();

        $this->setElement($task);



        $access = $Crm->Access();

        $contactSet = $Crm->ContactSet();



        if ($task->responsible) {
            $responsible = $contactSet->get($contactSet->user->is($task->responsible));
            if ($responsible && $access->viewContact($responsible)) {
                $responsibleName = $responsible->getFullName();
                $this->addIcon(
                    $W->Link(
                        $Crm->Ui()->ContactPhoto($responsible, 32, 32, 0),
                        $Crm->Controller()->Contact()->display($responsible->id)
                    )
                );
            } else {
                $responsibleName = bab_getUserName($task->responsible, true);
            }
        } else {
            $responsibleName = '';
        }


        $taskDoneBy = null;

        if ($task->completedBy) {
            $completedBy = $contactSet->get($contactSet->user->is($task->completedBy));
            if ($completedBy && $access->viewContact($completedBy)) {
                $taskDoneBy = $completedBy->getFullName();
                $this->addIcon(
                    $W->Link(
                        $Crm->Ui()->ContactPhoto($completedBy, 32, 32, 0),
                        $Crm->Controller()->Contact()->display($completedBy->id)
                    )
                );
            } else {
                $taskDoneBy = bab_getUserName($task->completedBy, true);
            }
        }


        $subtitleLayout = $W->FlowLayout();
        $subtitleLayout->setHorizontalSpacing(1, 'ex')
            ->addClass(Func_Icons::ICON_LEFT_24)
            ->addClass('crm-small');


        $subTitle = '';
        if (isset($taskDoneBy)) {
            $subTitle = sprintf($Crm->translate('Task done by %s'),  $taskDoneBy);
        }


        $this->addDefaultLinkedElements();


        $this->addTitle(
            $W->Title(
                $W->Link(
                    $task->getNameWidget(),
                    $Crm->Controller()->Task()->display($task->id)
                ),
                4
            )
        );

        $dateDone = $this->getMode() == self::MODE_SHORT ?
                        BAB_DateTimeUtil::relativePastDate($task->completedOn, true)
                        : bab_longDate(bab_mktime($task->completedOn), true);
        $taskIcon = $W->Icon($subTitle . "\n" . $dateDone, Func_Icons::ACTIONS_VIEW_PIM_TASKS);
        $subtitleLayout->addItem($taskIcon);

        $this->addTitle($subtitleLayout);



        $description = empty($task->description) ? ' ' : $task->description;

        switch ($this->getMode()) {

            case self::MODE_COMPACT :
                break;

            case self::MODE_SHORT :
                $this->addContent($W->Html(bab_toHtml(bab_abbr(strip_tags($description), BAB_ABBR_FULL_WORDS, 100))));
                break;

            case self::MODE_FULL :
            default:
                $this->addContent($W->Html(bab_toHtml($description)));
                break;
        }


    }




    /**
     * Add a contextual menu with several actions applicable
     * to the task.
     *
     * @return crm_historyTask
     */
    public function addActions()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $this->addAction(
            $W->Link(
                $W->Icon($Crm->translate('Display details'), Func_Icons::MIMETYPES_OFFICE_DOCUMENT),
                $Crm->Controller()->Task()->display($this->element->id)
            )
        );

        if ($this->Crm()->Access()->updateTask($this->element)) {
            $this->addAction(
                $W->Link(
                    $W->Icon($Crm->translate('Edit this task'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $Crm->Controller()->Task()->edit($this->element->id)
                )
            );
        }

        if ($this->Crm()->Access()->deleteTask($this->element)) {
            $this->addAction(
                $W->Link(
                    $W->Icon($Crm->translate('Delete this task'), Func_Icons::ACTIONS_EDIT_DELETE),
                    $Crm->Controller()->Task()->confirmDelete($this->element->id)
                )
            );
        }

        return $this;
    }
}
