<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * Creates a generic form fragment for the specified set.
 *
 * @param ORM_RecordSet $set
 * @return Widget_Item
 */
function crm_genericSetEditor(ORM_RecordSet $set)
{
	$O = bab_functionality::get('LibOrm');
	$O->init();
	$W = bab_Widgets();
	$layout = $W->VBoxLayout();
	$layout->setVerticalSpacing(1, 'em');


	$fields = $set->getFields();

	foreach ($fields as $field) {
		$description = $field->getDescription();
		if (empty($description)) {
			$description = $field->getName();
		}
		$fieldLabel = $W->Label($description . ':');

		if ($field instanceof ORM_DateField) {
			$widget = $W->DatePicker();
		} else if ($field instanceof ORM_TimeField) {
			$widget = $W->LineEdit()->setMaxSize(5)->setSize(5)->addClass('widget-timepicker');
		} else if ($field instanceof ORM_StringField) {
			$widget = $W->LineEdit()->setSize(min(array(80, $field->getMaxLength())));
		} else if ($field instanceof ORM_EnumField) {
			$widget = $W->Select();
			$widget->addOption('', '');
			foreach ($field->getValues() as $key => $text) {
				$widget->addOption($key, $text);
			}
		} else if ($field instanceof ORM_IntField) {
			$widget = $W->LineEdit()->setSize(9);
		} else if ($field instanceof ORM_TextField) {
			$widget = $W->TextEdit()->setColumns(80)->setLines(5);
		} else if ($field instanceof ORM_RecordSet) {
				$widget = crm_genericSetEditor($field);
				$widget->addClass('sub-form');
		} else {
			$widget = null;
		}
		if (isset($widget)) {
			$layout->addItem($W->VBoxLayout()->addItem($fieldLabel)->addItem($widget));
		}
	}

	return $layout;
}



/**
 * Creates a generic form fragment for the specified set.
 *
 * @param crm_Set $set
 * @return Widget_Item
 */
function crm_genericSetFilterForm(ORM_RecordSet $set)
{
	$O = bab_functionality::get('LibOrm');
	$O->init();
	$W = bab_Widgets();
	$layout = $W->FlowLayout();
	$layout->setVerticalSpacing(1, 'em')->setHorizontalSpacing(1, 'em');


	$fields = $set->getFields();

	foreach ($fields as $field) {
		$description = $field->getDescription();
		if (empty($description)) {
			$description = $field->getName();
		}
		$fieldLabel = $W->Label($description . ':');

		if ($field instanceof ORM_DateField) {
			$widget = $W->PeriodPicker();
		} else if ($field instanceof ORM_TimeField) {
			$widget = $W->TimePicker();
		} else if ($field instanceof ORM_StringField) {
			$widget = $W->LineEdit()->setSize(min(array(15, $field->getMaxLength())));
		} else if ($field instanceof ORM_EnumField) {
			$widget = $W->Select();
			$widget->addOption('', '');
			foreach ($field->getValues() as $key => $text) {
				$widget->addOption($key, $text);
			}
		} else if ($field instanceof ORM_IntField) {
			$widget = $W->LineEdit()->setSize(9);
		} else if ($field instanceof ORM_TextField) {
			$widget = $widget = $W->LineEdit()->setSize(15);
		} else if ($field instanceof ORM_RecordSet) {
			$widget = crm_genericSetFilterForm($field);
			$widget->addClass('sub-form');
		} else {
			$widget = null;
		}
		if (isset($widget)) {
			$layout->addItem($W->VBoxLayout()->addItem($fieldLabel)->addItem($widget));
		}
	}

	return $layout;
}




/**
 * Creates a generic form fragment for the specified set.
 *
 * @deprecated replace with widget_TableModelView::getFilterForm()
 *
 * @param crm_Set $set
 * @return Widget_Item
 */
function crm_genericTableModelViewFilterForm(widget_TableModelView $table)
{

	trigger_error('crm_genericTableModelViewFilterForm : Deprecated function, replace with widget_TableModelView::getFilterForm()');


	$O = bab_functionality::get('LibOrm');
	$O->init();
	$W = bab_Widgets();
	$layout = $W->FlowLayout();
	$layout->setVerticalSpacing(1, 'em')->setHorizontalSpacing(1, 'em');

	$fields = $table->getVisibleColumns();

	foreach ($fields as $fieldName => $field) {
		$description = $field->getDescription();
		if (empty($description)) {
			$description = $field->getName();
		}
		$fieldLabel = $W->Label($description . ':');

		if ($field instanceof ORM_DateField) {
			$widget = $W->PeriodPicker();
		} else if ($field instanceof ORM_TimeField) {
			$widget = $W->TimePicker();
		} else if ($field instanceof ORM_StringField) {
			$widget = $W->LineEdit()->setSize(min(array(15, $field->getMaxLength())));
		} else if ($field instanceof ORM_EnumField) {
			$widget = $W->Select();
			$widget->addOption('', '');
			foreach ($field->getValues() as $key => $text) {
				$widget->addOption($key, $text);
			}
		} else if ($field instanceof ORM_IntField) {
			$widget = $W->LineEdit()->setSize(9);
		} else if ($field instanceof ORM_TextField) {
			$widget = $widget = $W->LineEdit()->setSize(15);
//		} else if ($field instanceof ORM_RecordSet) {
//			$widget = crm_genericSetFilterForm($field);
//			$widget->addClass('sub-form');
		} else {
			$widget = null;
		}
		if (isset($widget)) {
			$widget->setName($fieldName);
			$layout->addItem($W->VBoxLayout()->addItem($fieldLabel)->addItem($widget));
		}
	}

	return $layout;
}




/**
 * Creates a generic filter panel containing the table and an auto-generated form.
 *
 * @deprecated replace with crm_TableModelView::FilterPanel()
 *
 *
 * @param widget_TableModelView $table
 * @param array					$filterValues
 * @param string				$name
 * @return Widget_Filter
 */
function crm_genericTableModelViewFilterPanel(widget_TableModelView $table, $filter = null, $name = null)
{

	trigger_error('crm_genericTableModelViewFilterPanel : Deprecated function, replace with crm_TableModelView::FilterPanel()');


	$W = bab_Widgets();

	$filterPanel = $W->Filter();
	$filterPanel->setLayout($W->VBoxLayout());
	if (isset($name)) {
		$filterPanel->setName($name);
	}

	$table->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : 10);

	$table->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : 0);

	if (isset($filter['sort'])) {
		$table->setSortField($filter['sort']);
//		$table->setGrouping(true);
	} else {
		$columns = $table->getVisibleColumns();
		list($sortField) = each($columns);
		$table->setSortField($sortField);
	}

	$form = $W->Form();
	$form->setReadOnly(true);
	$form->setName('filter');
	$form->setLayout(crm_genericTableModelViewFilterForm($table));

	$form->addItem($W->SubmitButton());

	if (isset($filter)) {
//		bab_debug($filter);
		$form->setValues($filter, array($name, 'filter'));
	}
	$form->setSelfPageHiddenFields();

	$filterPanel->setFilter($form);
	$filterPanel->setFiltered($table);


	return $filterPanel;
}


/**
 * Associates a label to an input widget.
 *
 * @param string $labelText
 * @param Widget_InputWidget $widget
 * @return Widget_BoxLayout
 */
function crm_LabelledWidget($labelText, Widget_Displayable_Interface $widget)
{
	$W = bab_Widgets();

	$label = $W->Label($labelText);
	if ($widget instanceof Widget_InputWidget) {
		$label->setAssociatedWidget($widget);
	}

	if ($widget instanceof Widget_CheckBox) {
		$layout = $W->HBoxItems(
			$widget->setSizePolicy(Widget_SizePolicy::MINIMUM),
			$label
		)->setVerticalAlign('middle')->setHorizontalSpacing(0.5, 'em');
	} else {
		$layout = $W->VBoxItems(
			$label,
			$widget
		)->setVerticalSpacing(0.5, 'em');
	}

	return $layout;
}



function crm_LabelledCheckbox($labelText, $checkboxName, $options = null)
{
	$W = bab_Widgets();

	$label = $W->Label($labelText)->colon(false);
	$checkbox = $W->Checkbox()->setName($checkboxName);
	$label->setAssociatedWidget($checkbox);
	if (isset($options)) {
		$label->setSizePolicy(Widget_SizePolicy::FIXED)->setCanvasOptions($options);
	}

	$layout = $W->HBoxItems(
					$checkbox->setSizePolicy(Widget_SizePolicy::MINIMUM),
					$label
				)->setVerticalAlign('middle')->setHorizontalSpacing(0.5, 'em');

	return $layout;
}




function crm_LabelledOrmSelect(ORM_MysqlIterator $iterator, $fieldName, $selectName, $label, $hidden = false, $groupFieldName = null)
{
	$W = bab_Widgets();

	if (isset($groupFieldName)) {
		$groupPathElements = explode('/', $groupFieldName);
	}

	$select = $W->Select()->setName($selectName);

	$select->addOption('', '');

	$nbOptions = 0;
	foreach ($iterator as $record) {
		if (isset($record->code)) {
			$optionText = $record->code . ' - ' . $record->$fieldName;
		} else {
			$optionText = $record->$fieldName;
		}
		if (isset($groupFieldName)) {
			$group = $record;
			foreach ($groupPathElements as $groupPathElement) {
				$group = $group->$groupPathElement;
			}
			$select->addOption($record->id, $optionText, $group);
		} else {
			$select->addOption($record->id, $optionText);
		}
		$nbOptions++;
		$lastId = $record->id;
	}

	if ($nbOptions == 1) {
		$select->setValue($lastId);
	}


	return crm_LabelledWidget($label, $select);
}




function crm_OrmWidget(ORM_Field $field)
{
	$W = bab_Widgets();


	if ($field instanceof ORM_DateField) {
		$widget = $W->DatePicker();
	} else if ($field instanceof ORM_TimeField) {
		$widget = $W->TimeEdit(); //->setMaxSize(5)->setSize(5)->addClass('widget-timepicker');
	} else if ($field instanceof ORM_EnumField) {
		$widget = $W->Select();
////		$widget = $W->MultiSelect()->setSingleSelect();
		$widget->addOption('', '');
		$values = $field->getValues();
		foreach ($values as $key => $value) {
			$widget->addOption($key, $value);
		}
	} else if ($field instanceof ORM_SetField) {
		$widget = $W->MultiSelect()->setSelectedList(2);
		$values = $field->getValues();
		foreach ($values as $key => $value) {
			$widget->addOption($key, $value);
		}
	} else if ($field instanceof ORM_CurrencyField) {
		$widget = $W->LineEdit()->setSize(6)->addClass('widget-input-currency');
	} else if ($field instanceof ORM_BoolField) {
		$widget = $W->CheckBox()->setCheckedValue('1');
	} else if ($field instanceof ORM_IntField) {
		$widget = $W->LineEdit()
						->setSize(6)->addClass('widget-input-numeric');
	} else if ($field instanceof ORM_EmailField) {
		$widget = $W->EmailLineEdit()
						->setMaxSize($field->getMaxLength());
	} else if ($field instanceof ORM_FileField) {
	    $widget = $W->FilePicker();
	} else if ($field instanceof ORM_StringField) {
		$widget = $W->LineEdit()
			->setMaxSize($field->getMaxLength());
	} else if ($field instanceof ORM_TextField) {
		$widget = $W->TextEdit();
	} else if ($field instanceof ORM_PkField) {
	    $widget = $W->Hidden();
	} else if ($field instanceof ORM_FkField) {
		$widget = $W->Select();
		$fieldName = $field->getName();
		$parentSet = clone $field->getParentSet();
		$parentSet->join($fieldName);
		$set = $parentSet->$fieldName;
		$records = $set->select();
		$records->orderAsc($set->name);
		$pkName = $set->getPrimaryKey();
		$widget->addOption('', '');
		foreach ($records as $record) {
			$widget->addOption($record->$pkName, $record->name);
		}
	} else if ($field instanceof ORM_RecordSet) {
		$widget = $W->Select();
////		$widget = $W->MultiSelect()->setSingleSelect();
//		$fieldName = $field->getName();
//		$parentSet = $field->getParentSet();
//		$parentSet->join($fieldName);
//		$set = $parentSet->$fieldName;
		$records = $field->select();
		$records->orderAsc($field->name);
		$pkName = $field->getPrimaryKey();
		$widget->addOption('', '');
		foreach ($records as $record) {
			$widget->addOption($record->$pkName, $record->name);
		}
	} else {
		$widget = $W->LineEdit();
	}

	$widget->setName($field->getName());

	return $widget;
}



function crm_LabelledOrmWidget(ORM_Field $field, $label = null, $hidden = false)
{
	$W = bab_Widgets();

	if ($hidden) {
		$widget = $W->Hidden();
		$widget->setName($field->getName());
		return $widget;
	}

	if (is_null($label)) {
		$label = $field->getDescription();
	}

	$widget = crm_OrmWidget($field);

	return crm_LabelledWidget($label, $widget);
}






/**
 *
 *
 * @param	ORM_Field 			$field
 * @param	string				$value		ISO date time
 */
function crm_dateTime(ORM_Field $field, $value) {

	$W = bab_functionality::get('Widgets');
	$name = $field->getName();
	$label = $W->Label($field->getDescription());

	$frame = crm_dateTimeField($name, $label, $value);

	return $W->VBoxItems(
		$label,
		$frame
	);

}


/**
 *
 *
 * @param	string 			$fieldName
 * @param	Widget_Label	$label
 * @param	string			$value		ISO date time
 */
function crm_dateTimeField($fieldName, Widget_Label $label, $value = null)
{
	$W = bab_functionality::get('Widgets');

	$datepart = $W->DatePicker()->setAssociatedLabel($label)->setName('date');
	$timepart = $W->TimePicker()->setName('time');

	if (isset($value)) {

		$value = explode(' ', $value);

		$datepart->setValue($value[0]);
		$timepart->setValue($value[1]);
	}

	$datetime = $W->Frame(null, $W->HBoxLayout())->setName($fieldName)
			->addItem($datepart)
			->addItem($timepart);

	return $datetime;
}





