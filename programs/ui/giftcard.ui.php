<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 *
 */
class crm_GiftCardCardFrame extends crm_CardFrame
{


	public function __construct(Func_Crm $crm, $id = null)
	{
		parent::__construct($crm, $id);
		$this->init($crm, $id);
	}

	protected function init($crm, $id)
	{
		$this->addClass('crm-giftcard-cardframe');
		$this->addItem($this->title());
		$this->addItem($this->description());
		$this->addItem($this->getBuyFrame());
	}
	
	
	/**
	 * 
	 */
	protected function title()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		return $W->Title($Crm->translate('Gift card'), 4);
	}
	
	
	
	protected function description()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$addonname = $Crm->getAddonName();
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		
		$description = $registry->getValue('giftcard_description', '');
		
		return $W->RichText(bab_abbr($description, BAB_ABBR_FULL_WORDS,200))->setRenderingOptions(BAB_HTML_ALL);
	}
	
	
	
	/**
	 * The add to cart button with default packaging
	 * @return Widget_Link
	 */
	protected function getAddToCartButton()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
	
		bab_functionality::includefile('Icons');
		
		$action = $Crm->Controller()->GiftCard()->addToShoppingCart();
	
		return $W->Link($W->Icon($this->Crm()->translate('Add to shopping cart'), Func_Icons::ACTIONS_LIST_ADD), $action);
	
	}
	
	
	
	
	
	
	/**
	 * Shopping cart link
	 * @return Widget_Link
	 */
	protected function getShoppingLink() {
	
		// TODO : add allready in shopping cart label if a gift card is allready in shopping cart
		// peut etre que dans ce cas le bouton doit etre cliquable malgre tout
		
		return $this->getAddToCartButton()->addClass('crm-shopping-link');
	}
	

	
	
	
	/**
	 * The buy frame
	 * for multiple packaging and prices or for the main packaging only
	 *
	 *
	 * @return Widget_Frame
	 */
	public function getBuyFrame()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.6,'em'))->addClass('crm-buy-item');


		$frame->addItem($W->Label($Crm->translate('Gift card amount'))->colon()->addClass('crm-giftcard-amount-label'));
		$hbox = $W->HBoxLayout()->setHorizontalSpacing(1,'em')->setVerticalAlign('middle');
		$frame->addItem($hbox);
		
		
		$select = $W->Select();
		for($i = 1; $i < 10; $i +=1)
		{
			$select->addOption($i, $i);
		}
		
		for($i = 10; $i < 30; $i +=5)
		{
			$select->addOption($i, $i);
		}
		
		for($i = 30; $i < 100; $i +=10)
		{
			$select->addOption($i, $i);
		}
		
		for($i = 100; $i <= 1000; $i += 100)
		{
			$select->addOption($i, $i);
		}
		
		$hbox->addItem($W->Items($select, $W->Label($Ui->Euro())));
		$hbox->addItem($this->getShoppingLink()->setMetadata('quantityField', $select->getId()));
		
		
		return $frame;
	}

}




/**
 * 
 *
 */
class crm_GiftCardFullFrame extends crm_GiftCardCardFrame
{

	protected function init($crm, $id)
	{
		$this->addClass('crm-giftcard-fullframe');
		$this->addItem($this->title());
		$this->addItem($this->description());
		$this->addItem($this->getBuyFrame());
		
	}
	
	
	

	
	/**
	 *
	 */
	protected function title()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
	
		return $W->Title($Crm->translate('Gift card'), 2);
	}
	
	
	protected function description()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
	
		$addonname = $Crm->getAddonName();
	
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
	
		$description = $registry->getValue('giftcard_description', '');
	
		return $W->RichText($description)->setRenderingOptions(BAB_HTML_ALL);
	}
	

}
















