<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/contact.ui.php';





class crm_MyContactOrderTableView extends crm_TableModelView
{
	public function __construct(Func_Crm $crm = null, $id = null)
	{
		parent::__construct($crm, $id);

		$this->addClass(Func_Icons::ICON_LEFT_16);
	}


	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		if ($fieldPath == 'total_ti') {
		    return $W->Label($this->computeCellTextContent($record, $fieldPath).$Crm->Ui()->Euro());
		}

		if ($fieldPath == '_download_') {
			return $W->Link($W->Icon('', Func_Icons::MIMETYPES_APPLICATION_PDF), $this->Crm()->Controller()->MyContact()->downloadPdfOrder($record->id));
		}

		if ($fieldPath == 'createdOn') {
			return $W->Label(BAB_DateTimeUtil::relativePastDate($record->createdOn));
		}
		
		if ($fieldPath == 'shoppingcart/status') {
			if (null == $record->shoppingcart->id)
			{
				return $W->Label($Crm->translate('Finished'));
			}
		}

		return parent::computeCellContent($record, $fieldPath);
	}

}


class crm_MyContactShoppingCartTableView extends crm_TableModelView
{
	public function __construct(Func_Crm $crm = null, $id = null)
	{
		parent::__construct($crm, $id);
		
		$this->addClass(Func_Icons::ICON_LEFT_16);
	}
	
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_ShoppingCart $record, $fieldPath)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		if ($fieldPath == 'name' && !empty($record->uuid)) {
			return $W->Link($record->name, $Crm->Controller()->ShoppingCart()->display($record->uuid));
		}
		

		if ($fieldPath == '_action_') {
			return $W->HBoxItems(
				$this->UseCart($record),
				$this->delete($record)
			);
		}

		if ($fieldPath == 'number_product') {
			$ShoppingCartItemset = $Crm->ShoppingCartItemSet();
			$count = count($ShoppingCartItemset->Select($ShoppingCartItemset->cart->is($record->id)));
			
			return $W->Label($count);
		}
		
		return parent::computeCellContent($record, $fieldPath);
	}
	
	
	
	protected function UseCart(crm_ShoppingCart $record)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		return $W->Link(
			$W->Icon('', Func_Icons::OBJECTS_SHOPPING_CART),
			$Crm->Controller()->MyContact()->replaceCart($record->id)
		)->setConfirmationMessage($Crm->translate('This will replace your current cart'))
		->setTitle($Crm->translate('Use this cart'));
	}
	
	
	protected function delete(crm_ShoppingCart $record)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		return $W->Link(
			$W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
			$Crm->Controller()->MyContact()->removeCart($record->id)
		)->setConfirmationMessage($Crm->translate('This will remove this recorded cart'))
		->setTitle($Crm->translate('Remove this cart'));
	}

}




/**
 * Full contact frame for current user
 *
 */
class crm_MyContactFullFrame extends crm_ContactFullFrame
{

	protected $valid = null;

	public function __construct(Func_Crm $Crm, $attributes = crm_ContactCardFrame::SHOW_ALL, $id = null)
	{
		$set = $Crm->ContactSet();
		$organizationPerson = $set->get($set->user->is($GLOBALS['BAB_SESS_USERID']));

		if (null === $organizationPerson) {
			$this->valid = false;
			return;
		}

		$this->valid = true;
		parent::__construct($Crm, $organizationPerson, $attributes, $id);
	}
	
	/**
	 * @return Widget_Item | null
	 */
	protected function Icon()
	{
	    return null;
	}


	public function isValid()
	{
		return $this->valid;
	}
	
	
	/**
	 * Data under fullname
	 * @return Widget_Item
	 */
	protected function handleContent()
	{
		
		
		$W = bab_Widgets();
		$layout = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');

		if ($address = $this->Address()) {
			$layout->addItem($address);
		}
		
		if ($address = $this->DeliveryAddress()) {
			$layout->addItem($address);
		}

		if ($web = $this->Web()) {
			$layout->addItem($web);
		}

		if ($phones = $this->Phones()) {
			$layout->addItem($phones);
		}
		
		if ($birthday = $this->birthday()) {
			$layout->addItem($birthday);
		}
		


		return $layout;
	}

}



/**
 * User contact editor
 *
 * @return crm_Editor
 */
class crm_MyContactEditor extends crm_ContactEditor
{

	protected function addFields()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
	
		$Crm->includeAddressSet();
		$Crm->Ui()->includeAddress();
	
		$this->contactSet = $Crm->ContactSet();
		$this->contactSet->join('address');
		
		
	
		$this->addItem($this->InformationsSection());
		$this->addItem($this->LoginSection());
	
		// $this->googleSuggest();
	
		$this->addresses();
	}
	
	
	protected function Email()
	{
		
		$W = bab_Widgets();
		return $this->labelledField(
				$this->Crm()->translate('Email address'),
				$W->EmailLineEdit()->setMandatory(true, $this->Crm()->translate('The email is mandatory'))
				->addClass('widget-fullwidth')
				->setSize(50),
				'email'
		);
	}
	
	
	protected function LoginSection()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		$login = $W->Section($Crm->translate('Login informations'),
				$W->VBoxItems(
						$this->nickname(),
						$this->updatePassword()
				)->setVerticalSpacing(1, 'em')
		)->setFoldable(true);
		
		
		return $login;
	}
	
	
	protected function nickname()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		if ($Crm->loginByEmail)
		{
			return null;
		}
		
		return $this->labelledField(
			$this->Crm()->translate('Login ID'),
			$W->LineEdit()->setSize(30)->setAutoComplete(false),
			__FUNCTION__
		);
	}
	
	
	/**
	 * Update the password
	 */
	protected function updatePassword()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$new_password1 = $this->labelledField(
				$this->Crm()->translate('Type your password'),
				$W->LineEdit()->setSize(30)->obfuscate(true),
				'new_password1'
		);
		
		$new_password2 = $this->labelledField(
				$this->Crm()->translate('Password verification'),
				$W->LineEdit()->setSize(30)->obfuscate(true),
				'new_password2'
		);
		
		$pframe = $W->VBoxItems($new_password1, $new_password2)->setVerticalSpacing(1, 'em');
		
		$checkbox = $this->labelledField(
				$Crm->translate('Set a new password'),
				$W->CheckBox()->setCheckedValue('1')->setAssociatedDisplayable($pframe, array('1')),
				'set_password'
		);
		
		return $W->VBoxItems($checkbox, $pframe)->setVerticalSpacing(1, 'em');
	}
	
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		$this->addButton(
			$W->SubmitButton()
				->validate()
				->setLabel($Crm->translate('Save'))
				->setAction($Crm->Controller()->MyContact()->save())
				->setSuccessAction($Crm->Controller()->MyContact()->display($this->contact->id))
				->setFailedAction($Crm->Controller()->MyContact()->edit($this->contact->id))
		);
	}

}





/**
 * Login or create a new account with an associated contact
 * 
 *
 */
class crm_MyContactLoginCreateEditor extends crm_ContactEditor
{
	/**
	 * Editor is in shopping cart context or not
	 * @var	int	 	1 : shopping cart shop process
	 * 				2 : save the shopping cart
	 *              3 : mycontact.display
	 */
	private $context = null;
	
	public function __construct(Func_Crm $crm, $context = null)
	{
		$this->context = $context;
		parent::__construct($crm);
	}
	
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		
	}
	
	
	protected function addFields()
	{
		$Crm = $this->Crm();
		
		$this->colon();
		$this->setName('loginCreate');
		$this->addClass('crm-mycontact-logincreate-editor');
		
		$Crm->includeAddressSet();
		$Crm->Ui()->includeAddress();
		
		$this->contactSet = $Crm->ContactSet();
		$this->contactSet->join('address');
		
		
		$this->addItem(
			$this->login()
		);
		
		$old = $this->oldCustomerFrame();
		$new = $this->newCustomerFrame();
		
		$method = $this->method();
		
		$method->setAssociatedDisplayable($new, array('new'));
		$method->setAssociatedDisplayable($old, array('old'));
	
		$this->addItem($method);
		
		
		$this->addItem($old);
		$this->addItem($new);
		
		$this->setHiddenValue('context', $this->context);
	}
	
	
	/**
	 * Field to enter nickname or email
	 * 
	 * 
	 */
	protected function login()
	{
		if ($this->Crm()->loginByEmail)
		{
			$label = $this->Crm()->translate('Enter your e-mail address');
		} else {
			$label = $this->Crm()->translate('Enter your login ID');
		}
		
		$W = $this->widgets;
		return $this->labelledField(
				$label,
				$W->LineEdit()->setSize(40)->setMaxSize(255)->setMandatory(),
				__FUNCTION__
		);
	}
	
	/**
	 * Choose beetween login or create an new account
	 * 
	 * @return Widget_RadioSet
	 */
	protected function method()
	{
		$W = $this->widgets;
		$set = $W->RadioSet()->setName(__FUNCTION__);
		$set->addClass('method');
		
		$set->addOption('new', $this->Crm()->translate('You are a new customer'));
		$set->addOption('old', $this->Crm()->translate('You allready are customer'));
		
		return $set;
	}
	
	
	protected function SubmitButton()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		
		
		$submit = $W->SubmitButton()
			->validate()
			->setFailedAction($Crm->Controller()->MyContact()->loginCreate());
		
		return $submit;
	}
	
	
	protected function oldCustomerFrame()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		
		switch($this->context)
		{
			case 1:
				$nextaction = $Crm->Controller()->ShoppingCart()->setDeliveryAddress();
				break;
				
			case 2:
				$nextaction = $Crm->Controller()->ShoppingCart()->recordShoppingCart();
				break;
				
			case 3:
			    $nextaction = $Crm->Controller()->MyContact()->display();
			    break;
				
			default:
				$nextaction = $W->Action()->fromUrl('?'); // homepage
		}
		
		$submit = $this->SubmitButton()
			->setAction($Crm->Controller()->MyContact()->loginCreateValidateOld())
			->setSuccessAction($nextaction)
			->setLabel($Crm->translate('Login'));
		
		$frame = $W->Frame()->addClass('crm-method-old');
		$frame->addItem($this->password());
		$frame->addItem($submit);
		
		if (isset($Crm->PasswordToken))
		{
			$frame->addItem($W->Link($Crm->translate('I forgot my password'), $Crm->Controller()->LostPassword()->edit())->addClass('crm-small'));
		}
		
		return $frame;
	}
	
	
	protected function newCustomerFrame()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		
		$frame = $W->Frame()->addClass('crm-method-new');
		
		$submit = $this->SubmitButton()
			->setAction($Crm->Controller()->MyContact()->loginCreateValidateNew())
			->setSuccessAction($Crm->Controller()->MyContact()->accountCreated($this->context))
			->setLabel($Crm->translate('Register'));
		
		$frame->addItem($this->minimal());
		$frame->addItem($this->moreinformations());
		
		
		// $frame->addItem($this->Captcha());
		$frame->addItem($submit);
		
		return $frame;
	}
	
	
	protected function minimal()
	{
		$W = $this->widgets;
		
		$informations = $W->VBoxItems(
				$this->email(),
				$this->Title(),
				$W->HBoxItems(
						$this->Firstname(),
						$this->Lastname()
				)->setHorizontalSpacing(2, 'em')
		)->setVerticalSpacing(1, 'em');
		
		$informations->addClass('crm-logincreate-informations');
		
		$informations->addItem($this->passwords());
		
		return $informations;
	}
	
	/**
	 * 
	 * @return Ambigous <Widget_Layout, Widget_Layout>
	 */
	protected function moreinformations()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$informations = $W->VBoxItems(
				
				$this->Phones(),
				$this->birthday(),
				$this->addresses()
		)->setVerticalSpacing(1, 'em');
		
		return $W->Section($Crm->translate('Fill more details now...'), $informations, 4)->setFoldable(true, true);
	}
	
	
	protected function email()
	{
		$Crm = $this->Crm();
		
		if ($Crm->loginByEmail)
		{
			return null;
		}
		
		$W = $this->widgets;
		
		return $this->labelledField(
				$this->Crm()->translate('Email'),
				$W->LineEdit()->setSize(50)->setMaxSize(255)->setMandatory(),
				__FUNCTION__
		);
	}
	
	
	protected function Firstname()
	{
		$W = $this->widgets;
		return $this->labelledField(
				$this->Crm()->translate('First name'),
				$W->LineEdit()->setSize(20)->setMaxSize(255)->setMandatory(),
				'firstname'
		);
	}
	
	protected function Lastname()
	{
		$W = $this->widgets;
		return $this->labelledField(
				$this->Crm()->translate('Last name'),
				$W->LineEdit()->setSize(20)->setMaxSize(255)->setMandatory(),
				'lastname'
		);
	}
	
	
	
	/**
	 * 
	 * @return Widget_Frame
	 */
	protected function addresses()
	{
		$W = $this->widgets;
		
		$addresses = $W->Frame();
		$addresses->addItem($this->Address('address', $this->Crm()->translate('My main address (billing address)'), false));
		
		if ($delivery = $this->add_delivery())
		{
			$addresses->addItem($delivery);
		}
		
		$addresses->addClass('crm-logincreate-addresses');
		
		return $addresses;
	}
	
	/**
	 * 
	 * @return Widget_Frame
	 */
	protected function passwords()
	{
		$W = $this->widgets;
		
		$passwords = $W->Frame();
		
		$passwords->addItem(
				$W->VBoxItems(
						$this->new_password1(),
						$this->new_password2()
				)->setVerticalSpacing(.8,'em')
		);
		
		$passwords->addClass('crm-logincreate-passwords');
		
		return $passwords;
	}
	
	
	protected function add_delivery()
	{
		$ContactSet = $this->Crm()->ContactSet();
		
		if ($this->Crm()->noDelivery || !($ContactSet->deliveryaddress instanceof ORM_FkField))
		{
			return null;
		}
		
		
		$W = $this->widgets;
		
		$condition = $this->labelledField(
			$this->Crm()->translate('I have a different delivery address'),
			$checkbox = $W->Checkbox(),
			__FUNCTION__
		);
		
		$address = $this->Address('deliveryaddress', $this->Crm()->translate('My delivery address'), true);
		
		$checkbox->setAssociatedDisplayable($address, array('1'));
		
		return $W->VBoxItems($W->Frame()->addItem($condition)->addClass('crm-mycontact-delivery-checkbox'), $address);
	}
	
	
	
	/**
	 * mandatory Titled address
	 * @param	string	$name 			prefix field name for address
	 * @param	string	$title			optional title for address
	 * @param	bool	$withRecipient	add recipient field
	 *
	 * @return Widget_VBoxLayout
	 */
	protected function Address($name, $title = null, $withRecipient = false)
	{
		$W = bab_Widgets();
	
		$layout = $W->VBoxLayout()->setVerticalSpacing(0.5,'em');
	
		if (isset($title)) {
			$layout->addItem($W->Title($title, 5));
		}
	
		$this->Crm()->Ui()->includeAddress();
	
		$layout->addItem(
			$this->Crm()->Ui()->AddressEditor(true, false)->showRecipient($withRecipient)->setName($name)
		);
		
		$layout->addClass('crm-mycontact-address');
	
		return $layout;
	
	}
	
	
	protected function Title()
	{
		$W = $this->widgets;
		$this->Crm()->includeContactSet();
		
		return $this->labelledField(
				$this->Crm()->translate('Your civility'),
				$W->RadioSet()->setOptions($this->Crm()->translateArray(crm_Contact::$titles))->setHorizontalView(),
				'title'
		);
	}
	
	
	protected function password()
	{
		$W = $this->widgets;
		return $this->labelledField(
				$this->Crm()->translate('And your password is'),
				$W->LineEdit()->setSize(40)->setMaxSize(255)->obfuscate(),
				__FUNCTION__
		);
	}
	
	
	protected function Phones()
	{
		$W = bab_Widgets();
	
		return $W->FlowItems(
				$this->BusinessPhone(),
				$this->MobilePhone()
		)->setHorizontalSpacing(2, 'em');
	
	}
	
	
	protected function new_password1()
	{
		$W = $this->widgets;
		return $this->labelledField(
				$this->Crm()->translate('Your password'),
				$W->LineEdit()->setSize(20)->setMaxSize(255)->obfuscate()->setMandatory(),
				__FUNCTION__
		);
	}
	
	
	protected function new_password2()
	{
		$W = $this->widgets;
		return $this->labelledField(
				$this->Crm()->translate('Please confirm your password'),
				$W->LineEdit()->setSize(20)->setMaxSize(255)->obfuscate()->setMandatory(),
				__FUNCTION__
		);
	}
	
}