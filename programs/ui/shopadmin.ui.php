<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Home page of online shop back office
 *
 */
class crm_ShopAdminHomeFrame extends crm_UiObject
{
	public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($crm);
		$W = bab_Widgets();
		$this->setInheritedItem($W->Frame($id, $W->HBoxLayout())->addClass('crm-shopadmin-home'));

		$main = $W->Frame(null, $W->VBoxLayout());
		$this->addItem($main);

		$main->addItem($this->orders());
		$main->addItem($this->products());

		$right = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));
		$this->addItem($right);

		// $right->addItem($this->portlets());
		$right->addItem($this->options());

		if ($approb = $this->approb())
		{
			$right->addItem($approb);
		}
	}



	/**
	 *
	 * @return Widget_Item
	 */
	protected function orders()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Crm->includeOrderSet();


		return $this->iconPanel()
		->addItem($W->Title($Crm->translate('Orders'), 3))
		->addItem(
				self::iconFrame()
				->addItem(
						$link = $W->Link(
								$W->Icon($Crm->translate('Invoices list'), Func_Icons::OBJECTS_INVOICE),
								$Crm->Controller()->Order()->displayList(array('filter' => array('type' => crm_Order::TYPE_INVOICE)))
						)
						->setTitle($Crm->translate('Display the list of all invoices, the invoicies are generated for each received payments'))
				)
				->addItem(
					$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
					->addItem($W->Label($Crm->translate($link->getTitle())))
				)

				->addItem(
						$link = $W->Link(
								$W->Icon($Crm->translate('Waiting orders'), Func_Icons::OBJECTS_WAITING_ITEM),
								$Crm->Controller()->ShoppingCartAdm()->displayWaitingList()
						)
						->setTitle($Crm->translate('Display the list of received orders in status payement made or shipment preparation'))
				)
				->addItem(
						$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
						->addItem($W->Label($Crm->translate($link->getTitle())))
				)

				->addItem(
						$link = $W->Link(
								$W->Icon($Crm->translate('Shopping carts'), Func_Icons::OBJECTS_SHOPPING_CART),
								$Crm->Controller()->ShoppingCartAdm()->displayList()
						)
						->setTitle($Crm->translate('Display the list of ongoing shopping cart in the online shop'))
				)
				->addItem(
						$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
						->addItem($W->Label($Crm->translate($link->getTitle())))
				)

				->addItem(
						$link = $W->Link(
								$W->Icon($Crm->translate('Shipped orders'), Func_Icons::OBJECTS_PRODUCT),
								$Crm->Controller()->ShoppingCartAdm()->displayHistoryList()
						)
						->setTitle($Crm->translate('Display the list of shipped orders'))
				)
				->addItem(
						$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
						->addItem($W->Label($Crm->translate($link->getTitle())))
				)
		)
		;
	}



	/**
	 *
	 *
	 * @return Widget_Item
	 */
	protected function products()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$iconframe = self::iconFrame()
		->addItem(
				$link = $W->Link(
						$W->Icon($Crm->translate('Products list'), Func_Icons::OBJECTS_PRODUCT),
						$Crm->Controller()->Article()->displayList()
				)
				->setTitle($Crm->translate('Display the list of all products, add or edit products'))
		)
		->addItem(
				$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
				->addItem($W->Label($Crm->translate($link->getTitle())))
		)
		
		
		->addItem(
				$link = $W->Link(
						$W->Icon($Crm->translate('Categories'), Func_Icons::OBJECTS_PRODUCT_CATEGORY),
						$Crm->Controller()->Catalog()->displayList()
				)
				->setTitle($Crm->translate('Display the list of all categories, add or edit categories'))
		)
		->addItem(
				$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
				->addItem($W->Label($Crm->translate($link->getTitle())))
		)
		
		
		->additem(
				$link = $W->Link(
						$W->Icon($Crm->translate('Contacts list'), Func_Icons::OBJECTS_CONTACT),
						$Crm->Controller()->Contact()->displayList()
				)
				->setTitle($Crm->translate('Display the list of customers contacts'))
		)
		->addItem(
				$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
				->addItem($W->Label($Crm->translate($link->getTitle())))
		);
		
		if (isset($Crm->Organization))
		{
			$iconframe->additem(
					$link = $W->Link(
							$W->Icon($Crm->translate('Organizations list'), Func_Icons::OBJECTS_ORGANIZATION),
							$Crm->Controller()->Organization()->displayList()
					)
			)->addItem(
					$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
					->addItem($W->Label($Crm->translate($link->getTitle())))
			);
		}
		
		$iconframe->additem(
				$link = $W->Link(
						$W->Icon($Crm->translate('Panel'), Func_Icons::APPS_STATISTICS),
						$Crm->Controller()->ShopAdmin()->panel()
				)
				->setTitle($Crm->translate('Statistics panel for online shop'))
		)->addItem(
				$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
				->addItem($W->Label($Crm->translate($link->getTitle())))
		);
		
		if (isset($Crm->ShopTrace) && $Crm->Access()->viewShopTrace())
		{
			$iconframe->additem(
					$link = $W->Link(
							$W->Icon($Crm->translate('Traces'), Func_Icons::ACTIONS_VIEW_HISTORY),
							$Crm->Controller()->ShopTrace()->displayList()
					)
					->setTitle($Crm->translate('Display the list of users sessions in online shop with a list of action for each session'))
			)->addItem(
					$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
					->addItem($W->Label($Crm->translate($link->getTitle())))
			);
		}
		
		if (isset($Crm->PrivateSell))
		{
		
			$iconframe->additem(
					$link = $W->Link(
							$W->Icon($Crm->translate('Private sell'), Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT),
							$Crm->Controller()->privateSell()->displayList()
					)
					->setTitle($Crm->translate('Add, remove, edit the private sales'))
			)->addItem(
					$W->Balloon()->setCanvasOptions($link->Options()->width(13,'em'))->displayOnMouseHover($link)
					->addItem($W->Label($Crm->translate($link->getTitle())))
			);
		}

		return $this->iconPanel()
		->addItem($W->Title($Crm->translate('Management'), 3))
		->addItem($iconframe)
		;
	}


	/**
	 *
	 *
	 * @return Widget_Item
	 */
	protected function portlets()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$ovml = sprintf('<OFPortletContainer id="%s_Home">', get_class($Crm));
		$html = bab_printOvml($ovml, array());
		return $W->Html($html);
	}



	/**
	 *
	 *
	 * @return Widget_Item
	 */
	protected function options()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		$frame = $W->Frame(null, $W->VBoxLayout()->setSpacing(.5,'em'))
			->addItem($W->Title($Crm->translate('Online shop parameters')))
			->addItem($W->Link($Crm->translate('Main options'), $Crm->Controller()->ShopAdmin()->configuration()));

		if (isset($Crm->ShippingScale)) {
			$frame->addItem($W->Link($Crm->translate('Shipping scale'), $Crm->Controller()->ShippingScale()->displayList()));
		}

		if (isset($Crm->ArticleType)) {
			$frame->addItem($W->Link($Crm->translate('Article types configuration'), $Crm->Controller()->ArticleType()->displayList()));
		}

		if (isset($Crm->Packaging) && $Access->usePackaging()) {
			$frame->addItem($W->Link($Crm->translate('Packaging configuration'), $Crm->Controller()->Packaging()->displayList()));
		}

		if (isset($Crm->CustomField)) {
			$frame->addItem($W->Link($Crm->translate('Custom fields'), $Crm->Controller()->CustomField()->displayList()));
		}

		if (isset($Crm->ArticleAvailability)) {
			$frame->addItem($W->Link($Crm->translate('Article availability'), $Crm->Controller()->ArticleAvailability()->displayList()));
		}
		
		if (isset($Crm->Discount)) {
		    $frame->addItem($W->Link($Crm->translate('Discounts management'), $Crm->Controller()->Discount()->displayList()));
		}

		if (isset($Crm->Coupon)) {
			$frame->addItem($W->Link($Crm->translate('Coupons management'), $Crm->Controller()->Coupon()->displayList()));
		}
		
		if (isset($Crm->Vat)) {
			$frame->addItem($W->Link($Crm->translate('VAT rates configuration'), $Crm->Controller()->Vat()->displayList()));
		}
		
		if (isset($Crm->CollectStore) && $Access->editCollectStore()) {
		    $frame->addItem($W->Link($Crm->translate('Collect stores'), $Crm->Controller()->CollectStore()->displayList()));
		}

		return $frame;
	}



	/**
	 *
	 * @return Widget_Frame
	 */
	protected function iconPanel()
	{
		$W = bab_Widgets();


		return $W->Frame(null, $W->VBoxLayout())->addClass('crm-shopadmin-icon-panel');
	}


	/**
	 *
	 * @return Widget_Layout
	 */
	protected function iconFrame()
	{
		$W = bab_Widgets();

		return $W->FlowLayout()
		->setHorizontalSpacing(0.5, 'em')
		->addClass(Func_Icons::ICON_TOP_48);
	}





	protected function approb()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		if (!isset($Crm->Comment))
		{
			return null;
		}

		$commentSet = $Crm->CommentSet();
		$resComment = $commentSet->select($commentSet->confirmed->is(0));


		if (0 === $resComment->count())
		{
			return null;
		}


		$frame = $W->Frame(null, $W->VBoxLayout()->setSpacing(.5,'em'))
			->addItem($W->Title($Crm->translate('Approbations')));

		if ($resComment->count() > 0)
		{
			$frame->addItem($W->Link($Crm->translate('Comments'), $Crm->Controller()->Comment()->displayList()));
		}


		return $frame;
	}


}




/**
 * Configuration form
 * selection de la passerelle de paiement
 * etc...
 *
 */
class crm_ConfigurationEditor extends crm_Editor
{
	public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($crm, $id, $layout);
		$this->setName('configuration');
		$this->colon();

		$this->addFields();
		$this->addButtons();

		$this->setHiddenValue('tg', bab_rp('tg'));

		$this->loadValues();
	}


	protected function addFields()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		$pdfFrame = $W->Section(
				$this->Crm()->translate('Variables used in PDF file'),
				$W->VBoxLayout()->setVerticalSpacing(1,'em'),
				4
		)->setFoldable(true);

		$pdfFrame->addItem($this->seller_organization_name());
		$pdfFrame->addItem($this->seller_logo());
		$pdfFrame->addItem($this->seller_organization_details());
		$pdfFrame->addItem($this->customer_pdf_type());
		$pdfFrame->addItem($this->seller_organization_footer());

		$this->addItem($pdfFrame);

		//$this->addItem($this->payboxTpe());

		$this->addItem($this->paymentSystem());

		$this->addItem($this->terms());
		$this->addItem($this->excltaxpriority());
		$this->addItem($this->excltaxcart());
		$this->addItem($this->nextInvoiceNumber());
		$this->addItem($this->shipping_cost_limit());

		$this->addItem($this->items_per_page());
		$this->addItem($this->display_empty_categories());
		
		$giftcard = $W->Section(
				$this->Crm()->translate('Gift cards'),
				$W->VBoxLayout()->setVerticalSpacing(1,'em'),
				4
		)->setFoldable(true);
		
		$giftcard->addItem($this->giftcard_disabled());
		$giftcard->addItem($this->giftcard_description());
		$giftcard->addItem($this->giftcard_validity());
		
		$this->addItem($giftcard);
	}
	
	
	
	
	protected function giftcard_disabled()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Disable gift card availability in online shop'),
				$W->Checkbox(),
				__FUNCTION__
		);
	}
	
	
	
	protected function giftcard_description()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Description of the gift card special product'),
				$W->TextEdit()->setColumns(90)->setLines(8),
				__FUNCTION__
		);
	}
	
	
	protected function giftcard_validity()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Validity duration of a gift card'),
				$W->LineEdit()->setSize(4),
				__FUNCTION__,
				null,
				$Crm->translate('Days')
		);
	}


	protected function items_per_page()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Number of items per page, products or categories'),
				$W->LineEdit()->setSize(4),
				__FUNCTION__
		);
	}
	
	
	
	protected function display_empty_categories()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		return $this->labelledField(
				$Crm->translate('Display empty categories'),
				$W->Checkbox(),
				__FUNCTION__
		);
	}


	
	protected function nextInvoiceNumber()
	{
	    $Crm = $this->Crm();
	    $W = $this->widgets;
	    
	    return $this->labelledField(
	        $Crm->translate('Next invoice number'),
	        $W->LineEdit()->setSize(4),
	        __FUNCTION__
	    );
	}


	/**
	 * Franco de port
	 */
	protected function shipping_cost_limit()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Free shipping limit'),
				$W->LineEdit()->setSize(4),
				__FUNCTION__,
				$Crm->translate('No shipping amount for shopping carts where total incl tax supperior or equal to this value, 0 disable functionality'),
				$Crm->Ui()->Euro().' '.$Crm->translate('incl tax')
		);
	}





	protected function excltaxpriority()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Prioritize the net amount when entering the price'),
				$W->Checkbox(),
				__FUNCTION__
		);
	}
	
	
	protected function excltaxcart()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Display the net amount in shopping cart'),
				$W->Checkbox(),
				__FUNCTION__
		);
	}


	/**
	 * Terms of sale
	 */
	protected function terms()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Terms of sale'),
				$W->TextEdit()->setColumns(90)->setLines(8),
				__FUNCTION__
		);
	}


// 	/**
// 	 *
// 	 */
// 	protected function payboxTpe()
// 	{
// 		$paybox = @bab_functionality::get('LibPaybox');
// 		/*@var $paybox Func_LibPaybox */

// 		$list = $paybox->getPayboxSystemList();

// 		$W = $this->widgets;
// 		return $this->labelledField(
// 				$this->Crm()->translate('Paybox system'),
// 				$W->Select()->setOptions($list),
// 				'paybox_system'
// 		);
// 	}


	/**
	 *
	 */
	protected function paymentSystem()
	{
		$W = $this->widgets;

		/*@var $payment Func_Payment */

		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
		$functionalities = new bab_Functionalities();
		$paymentFunctionalities = @$functionalities->getChildren('Payment');

		$paymentFunctionalitiesSelect = $W->Select();
		$paymentFunctionalitiesSelect->addOption('', '');



		foreach ($paymentFunctionalities as $paymentFunctionality) {

			$paymentFunctionalitiesSelect->addOption($paymentFunctionality, $paymentFunctionality);

			$paymentFuncPath = trim('Payment/' . $paymentFunctionality, '/');

			$paymentFunc = bab_Functionality::get($paymentFuncPath);

// 			$tpeList = $paymentFunc->getTpeList();
// 			foreach ($tpeList as $tpe) {
// 				$paymentFunctionalitiesSelect->addOption($tpe, $tpe);
// 			}
		}

		return $this->labelledField(
			$this->Crm()->translate('Payment system'),
			$paymentFunctionalitiesSelect,
			'payment_system'
		);
	}

	/**
	 * Informations displayed on PDF
	 */
	protected function seller_organization_name()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Seller organization name in generated PDF file'),
				$W->LineEdit(),
				__FUNCTION__
		);
	}


	/**
	 * Informations displayed on PDF
	 */
	protected function seller_logo()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		$addon = bab_getAddonInfosInstance($Crm->getAddonName());
		$path = new bab_Path($addon->getUploadPath());
		$path->push('seller');
		$path->push('logo');

		return $this->labelledField(
				$Crm->translate('Seller logo'),
				$W->ImagePicker()->setFolder($path)->oneFileMode(),
				__FUNCTION__
		);
	}



	/**
	 * Informations displayed on PDF
	 */
	protected function seller_organization_details()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Seller organization details in generated PDF file'),
				$W->TextEdit()->setColumns(50)->setLines(8),
				__FUNCTION__
		);
	}
	
	
	protected function customer_pdf_type()
	{
	    $Crm = $this->Crm();
	    $W = $this->widgets;
	    
	    return $this->labelledField(
	        $Crm->translate('PDF document type set to customer after his payment'),
	        $W->LineEdit()->setSize(50),
	        __FUNCTION__
	        );
	}
	

	/**
	 * Informations displayed on PDF
	 */
	protected function seller_organization_footer()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('PDF footer'),
				$W->TextEdit()->setColumns(90)->setLines(3),
				__FUNCTION__
		);
	}


	protected function addButtons()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();

		$this->addButton(
			$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->setAction($Crm->Controller()->ShopAdmin()->saveConfiguration())
				->setSuccessAction($Crm->Controller()->ShopAdmin()->admHome())
				->setFailedAction($Crm->Controller()->ShopAdmin()->configuration())
		);



		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction($Crm->Controller()->ShopAdmin()->cancel())
		);

	}

	protected function loadValues()
	{
		$Crm = $this->Crm();


		$addonname = $Crm->getAddonName();

		if (!$addonname)
		{
			throw new ErrorException('failed to get the addon name');
		}

		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");

		$key_list = array();
		while($key = $registry->fetchChildKey())
		{
			$key_list[] = $key;
		}

		$I = $registry->getValueEx($key_list);

		if (null === $I)
		{
			return null;
		}

		/* @var $I bab_RegistryIterator */

		foreach($I as $arr)
		{
			$this->setValue(array('configuration', $arr['key']), $arr['value']);
		}
	}
}