<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * Create a vCard V3.0
 * @author paul
 *
 */
class crm_VCard 
{
	
    private $data = array();
    
    public function __construct()
    {
    	$this->addProperty('VERSION', '3.0');
    }
    
    
    public function escape($str)
    {
    	return str_replace(";","\;",$str);
    }
    
    
    /**
     * Add generic text property
     * @param string	$property
     * @param string	$value
     * @return crm_VCard
     */
	public function addProperty($property, $value)
	{
		$this->data[$property] = $this->escape($value);
		return $this;
	}
	
	
	/**
	 * 
	 * @param string $lastname
	 * @param string $firstname
	 * @return crm_VCard
	 */
	public function setName($lastname, $firstname)
	{
		$lastname = $this->escape($lastname);
		$firstname = $this->escape($firstname);
		
		
		$this->data['N'] = "$lastname;$firstname;;;";
		$this->data['FN'] = "$firstname $lastname";
		$this->data['X-EVOLUTION-FILE-AS'] = "$lastname\, $firstname";
		
		return $this;
	}
	
	
	
	/**
	 * Set REV property, last update of vCard entry
	 * @param string $isodatetime
	 * @return crm_VCard
	 */
	public function setRev($isodatetime)
	{
		
		
		
		$arr = explode(' ', $isodatetime);
		$this->data['REV'] = $arr[0].'T'.$arr[1].'Z';
		return $this;
	}
	
	
	/**
	 * Add a voice telephon number (vCard 3.0)
	 * @param string $type		WORK | HOME | ...
	 * @param string $value
	 * @return crm_VCard
	 */
	public function addTel($type, $value)
	{
		$this->data["TEL;TYPE=$type,VOICE"] = $this->escape($value);
		return $this;
	}
	
	/**
	 * Add a fax telephon number (vCard 3.0)
	 * @param string $type		WORK | HOME | ...
	 * @param string $value
	 * @return crm_VCard
	 */
	public function addFax($type, $value)
	{
		$this->data["TEL;TYPE=$type,FAX"] = $this->escape($value);
		return $this;
	}
	
	/**
	 * Add email (vCard 3.0)
	 * @param string $type		WORK | HOME | ...
	 * @param string $value
	 * @return crm_VCard
	 */
	public function addEmail($type, $value)
	{
		$this->data["EMAIL;TYPE=$type"] = $this->escape($value);
		return $this;
	}
	
	/**
	 * Add address (vCard 3.0)
	 * 
	 * @param 	string 	$type		WORK | HOME | ...
	 * @param 	string 	$street		multiline string
	 * @param	string	$postalbox
	 * @param	string	$postalcode
	 * @param	string	$city
	 * @param	string	$state
	 * @param	string	$country
	 * @return crm_VCard
	 */
	public function addAddress($type, $street, $postalbox, $postalcode, $city, $state, $country)
	{
		$street 	= $this->escape($street);
		$postalbox	= $this->escape($postalbox);
		$postalcode	= $this->escape($postalcode);
		$city		= $this->escape($city);
		$state		= $this->escape($state);
		$country	= $this->escape($country);
		
		
		$lines = preg_split('/\n|\r\n|\r/', $street);
		
		$streetlabel = implode('\\n', $lines);
		
		$street = $lines[0];
		unset($lines[0]);
		$more = implode('\\n', $lines);
		
		$this->data["ADR;TYPE=$type"] = "$postalbox;$more;$street;$city;$state;$postalcode;$country";
		$this->data["LABEL;TYPE=$type"] = "$streetlabel;$city;$state;$postalcode;$postalbox;$country";
		
		return $this;
	}
	
	/**
	 * set organization property
	 * @param $organizationname
	 * @param $departmentname
	 * @return unknown_type
	 */
	public function setOrganization($organizationname, $departmentname = null)
	{
		$this->data['ORG'] = $this->escape($organizationname);
		
		if (isset($departmentname)) {
			$this->data['ORG'] .= ';'.$this->escape($departmentname);
		}
	}
	
	
	/**
	 * Set Photo
	 * @param 	bab_Path 	$photo		full path to photo (the best way is to give the path to a small thumbnail here)
	 * @param	string		$type		optional, default PNG
	 * @return crm_VCard
	 */
	public function setPhoto(bab_Path $photo, $type = 'PNG')
	{
		$this->data['PHOTO;TYPE=PNG;ENCODING=b'] = base64_encode(
			file_get_contents(
				$photo->toString()
			)
		);
		
		return $this;
	}
	
	
	
    
    /**
     * Get vCard as UTF-8 string
     * @return string
     */
    public function toString()
    {
    	$return = "BEGIN:VCARD\n";
    	
    	foreach($this->data as $property => $value) {
			$return .= $property.':'.$value."\n";
    	}
		
		$return .= "END:VCARD\n";
		
		
		return bab_convertStringFromDatabase($return, 'UTF-8');
    }
}