<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * The crm_Access class
 *
 * @method Func_Crm Crm()
 */
class crm_Access extends crm_Object
{
    private $currentContact;


    /**
     * Returns the current user id.
     *
     * @return string		The current user id	or '' if the user is not logged in.
     */
    public function currentUser()
    {
        return bab_getUserId();
    }


    /**
     * The contact associated to the currently connected user.
     *
     * @return crm_Contact|null
     */
    public function currentContact()
    {
        $currentUser = $this->currentUser();

        if (!$currentUser) {
            return null;
        }

        if (!isset($this->currentContact)) {
            $contactSet = $this->Crm()->ContactSet();
            $this->currentContact = $contactSet->get($contactSet->user->is($currentUser));
        }
        return $this->currentContact;
    }


    /**
     * Checks if the user can associate an ovidentia user account to a contact.
     *
     * @param int	$contact
     * @return bool
     */
    public function associateUserAccount($contact = null)
    {
        return true;
    }



    /**
     * @return crm_Contact
     */
    protected function contact($contact)
    {
        if ($contact instanceof crm_Contact) {
            return $contact;
        }
        return $this->Crm()->ContactSet()->get($contact);
    }

    /**
     * @return crm_Organization
     */
    protected function organization($organization)
    {
        if ($organization instanceof crm_Organization) {
            return $organization;
        }
        return $this->Crm()->OrganizationSet()->get($organization);
    }

    /**
     * @return crm_Deal
     */
    protected function deal($deal)
    {
        if ($deal instanceof crm_Deal) {
            return $deal;
        }
        return $this->Crm()->DealSet()->get($deal);
    }

    /**
     * @return crm_Team
     */
    protected function team($team)
    {
        if ($team instanceof crm_Team) {
            return $team;
        }
        return $this->Crm()->TeamSet()->get($team);
    }

    /**
     * @return crm_Note
     */
    protected function note($note)
    {
        if ($note instanceof crm_Note) {
            return $note;
        }
        return $this->Crm()->NoteSet()->get($note);
    }


    /**
     * @return crm_Task
     */
    protected function task($task)
    {
        if ($task instanceof crm_Task) {
            return $task;
        }
        return $this->Crm()->TaskSet()->get($task);
    }

    /**
     * @return crm_Email
     */
    protected function email($email)
    {
        if ($email instanceof crm_Email) {
            return $email;
        }
        return $this->Crm()->EmailSet()->get($email);
    }


    /**
     * @return bool
     */
    public function administer()
    {
        return bab_isUserAdministrator();
    }


    /**
     *
     * @param crm_Record	$record
     * @param string		$action
     * @return bool
     */
    protected function canPerformActionOnRecord(crm_Record $record = null, $action = '')
    {

        $Crm = $this->Crm();

        if (!isset($Crm->ContactRole)) {
            // no roles defined in this CRM

            if ($Crm->onlineShop) {
                return $this->viewShopAdmin();
            }

            return true;
        }

        $currentRoles = array();

        $currentContact = $this->currentContact();
        if (isset($currentContact)) {
            $contactRoleSet = $Crm->ContactRoleSet();
            $contactRoles = $contactRoleSet->select($contactRoleSet->contact->is($currentContact->id));

            foreach ($contactRoles as $contactRole) {
                $currentRoles[$contactRole->role] = $contactRole->role;
            }
        }

        $roleAccessSet = $Crm->RoleAccessSet();
        $currentRoleAccess = $roleAccessSet->get($roleAccessSet->role->in($currentRoles)->_AND_($roleAccessSet->action->is($action)));

        if (!isset($currentRoleAccess)) {
            return false;
        }

        switch ($currentRoleAccess->criterion) {

            case crm_RoleAccess::ACCESS_ALWAYS:
                return true;

            case crm_RoleAccess::ACCESS_NEVER:
                return false;

            case crm_RoleAccess::ACCESS_OWN:
                if (!isset($currentContact)) {
                    return false;
                }
                // We check if the current user is the owner of the record.
                $own = ($currentContact->id == $record->responsible);
                if (!$own && $record instanceof crm_Contact) {
                    // We check if the current user is the owner of the contact main organization.
                    $mainOrganization = $record->getMainOrganization();
                    $own = ($mainOrganization && $currentContact->id == $mainOrganization->responsible);
                }
                if (!$own && $record instanceof crm_Deal) {
                    // We check if the current user is the owner of the deal's lead.
                    $Crm->includeOrganizationSet();
                    $lead = $record->lead();
                    $own = ($lead && $currentContact->id == $lead->responsible);
                }
                return $own;

            case crm_RoleAccess::ACCESS_ORGANIZATION:
                if (!isset($currentContact)) {
                    return false;
                }
                $mainOrganization = $currentContact->getMainOrganization();
                if (!$mainOrganization) {
                    return false;
                }

                $contactOrganizationSet = $Crm->ContactOrganizationSet();
                $contactOrganizationSet->contact();
                $contactOrganizationSet->organization();

                $contactOrganizations = $contactOrganizationSet->select(
                    $contactOrganizationSet->organization->is($mainOrganization->id)
                    ->_AND_($contactOrganizationSet->main->is('1'))
                    ->_AND_($contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan(date('Y-m-d'))))
                    ->_AND_($contactOrganizationSet->organization->deleted->is(0))
                );

                foreach ($contactOrganizations as $contactOrganization) {
                    if ($record->createdBy == $contactOrganization->contact->user) {
                        return true;
                    }
                }
                return false;
//                 // We check if the current user's organization is the same as the record owner's organization.
//                 $contactSet = $this->Crm()->ContactSet();
//                 $ownerContact = $contactSet->get($contactSet->user->is($record->createdBy));
//                 return ($ownerContact && $ownerContact->getMainOrganization() === $currentContact->getMainOrganization());

            case crm_RoleAccess::ACCESS_SECTOR:
                if (!isset($currentContact)) {
                    return false;
                }
                // We fetch sectors affected to the current user.
                $contactSectorSet = $Crm->ContactSectorSet();
                $contactSectors = $contactSectorSet->select($contactSectorSet->contact()->id->is($currentContact->id));
//				echo $contactSectors->count();
                if ($contactSectors->count() === 0) {
                    // The current user has no affected sector, no need to go further.
                    return false;
                }

                // We select the sectors that are matched by the record...
                $sectorSet = $this->Crm()->SectorSet();
                $recordSectors = $sectorSet->selectFor($record);
//				echo count($recordSectors);
                if (count($recordSectors) === 0) {
                    // ... if record matches none of the defined sectors, no need to go further,
                    return false;
                }

                // ... if one of them is affected to the current user, the access is granted.
                foreach ($contactSectors as $contactSector) {
//					echo '(' . $contactSector->sector . ')';
                    if (isset($recordSectors[$contactSector->sector])) {
                        return true;
                    }
                }
                return false;
        }
    }



    /**
     *
     * @param crm_RecordSet	$set
     * @param string		$action
     *
     * @return ORM_Criteria
     */
    public function canPerformActionOnSet(crm_RecordSet $set, $action)
    {
        if (bab_isUserAdministrator()) {
//			return $set->all();
        }

        /* @var $Crm Func_Crm */
        $Crm = $this->Crm();


        if (!isset($Crm->ContactRole)) {
            // no roles defined in this CRM
            return $set->all();
        }

        $currentRoles = array();

        $currentContact = $this->currentContact();
        if (isset($currentContact)) {
            $contactRoleSet = $Crm->ContactRoleSet();
            $contactRoles = $contactRoleSet->select($contactRoleSet->contact->is($currentContact->id));

            foreach ($contactRoles as $contactRole) {
                $currentRoles[$contactRole->role] = $contactRole->role;
            }
        }

        $roleAccessSet = $Crm->RoleAccessSet();
        $currentRoleAccess = $roleAccessSet->get($roleAccessSet->role->in($currentRoles)->_AND_($roleAccessSet->action->is($action)));

        if (!isset($currentRoleAccess)) {
            return $set->none();
        }

        switch ($currentRoleAccess->criterion) {

            case crm_RoleAccess::ACCESS_ALWAYS:
                return $set->all();

            case crm_RoleAccess::ACCESS_NEVER:
                return $set->none();

            case crm_RoleAccess::ACCESS_OWN:
                // We check if the current user is the owner of the record.
                if (!isset($currentContact)) {
                    return $set->none();
                }

                if (!isset($set->responsible)) {
                    return $set->all();
                }

                $own = $set->responsible->is($currentContact->id);

                if ($set instanceof crm_ContactSet) {
                    $organizationSet = $Crm->OrganizationSet();
                    $responsibleOrganizations = $organizationSet->select($organizationSet->responsible->is($currentContact->id));
                    $responsibleOrganizationIds = array();
                    foreach ($responsibleOrganizations as $responsibleOrganization) {
                        $responsibleOrganizationIds[$responsibleOrganization->id] = $responsibleOrganization->id;
                    }
                    $own = $own->_OR_($set->hasMainOrganization($responsibleOrganizationIds));
                }
                if ($set instanceof crm_DealSet) {
                    $organizationSet = $Crm->OrganizationSet();
                    $responsibleOrganizations = $organizationSet->select($organizationSet->responsible->is($currentContact->id));
                    $responsibleOrganizationIds = array();
                    foreach ($responsibleOrganizations as $responsibleOrganization) {
                        $responsibleOrganizationIds[$responsibleOrganization->id] = $responsibleOrganization->id;
                    }
                    $own = $own->_OR_($set->lead->in($responsibleOrganizationIds));
                }
                return $own;

            case crm_RoleAccess::ACCESS_ORGANIZATION:
                if (!isset($currentContact)) {
                    return $set->none();
                }
                $mainOrganization = $currentContact->getMainOrganization();
                if (!$mainOrganization) {
                    return $set->none();
                }

                $contactOrganizationSet = $Crm->ContactOrganizationSet();
                $contactOrganizationSet->contact();
                $contactOrganizationSet->organization();

                $contactOrganizations = $contactOrganizationSet->select(
                    $contactOrganizationSet->organization->is($mainOrganization->id)
                    ->_AND_($contactOrganizationSet->main->is('1'))
                    ->_AND_($contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan(date('Y-m-d'))))
                    ->_AND_($contactOrganizationSet->organization->deleted->is(0))
                );

                $mainOrganizationUsers = array();
                foreach ($contactOrganizations as $contactOrganization) {
                    $mainOrganizationUsers[$contactOrganization->contact->user] = $contactOrganization->contact->user;
                }
                return $set->createdBy->in($mainOrganizationUsers);

            case crm_RoleAccess::ACCESS_SECTOR:
                if (!isset($currentContact)) {
                    return $set->none();
                }
                // We fetch sectors affected to the current user.
                $contactSectorSet = $Crm->ContactSectorSet();
                $contactSectorSet->sector();
                $contactSectors = $contactSectorSet->select($contactSectorSet->contact()->id->is($currentContact->id));

                $conditions = $set->none();
                foreach ($contactSectors as $contactSector) {
                    $sector = $contactSector->sector();
                    $conditions = $conditions->_OR_($sector->computeCriteria($set));
                }
                return $conditions;
        }
    }


    /**
     * Check if an anonymous user can login create a new account throw the CRM
     *
     *
     * @return boolean
     */
    public function loginOrCreateAccount()
    {
        if ($GLOBALS['BAB_SESS_LOGGED'])
        {
            // only for anonymous users
            return false;
        }

        // the default behaviour is to allow registration ony for CRM with implementation of the MyContact controller or online shop

        if ($this->Crm()->onlineShop)
        {
            return true;
        }

        $MyContact = $this->Crm()->Controller()->MyContact();
        if ($MyContact instanceof crm_CtrlMyContactProxy)
        {
            return false;
        }

        return true;
    }



    /**
     * Check if the user can display her own contact info.
     *
     * @return bool
     */
    public function viewMyContact()
    {
        return $this->Crm()->isContactLogged();
    }


    /**
     * Check if the user can modify her own contact info.
     *
     * @return bool
     */
    public function updateMyContact()
    {
        return $this->Crm()->isContactLogged();
    }


    /**
     * Test field allowed to be updated in the myContact edit form
     *
     * @param string $name
     * @param string | array $value
     *
     * @return bool
     */
    public function updateMyContactProperty($name, $value)
    {
        switch($name)
        {
            case 'title':
            case 'lastname':
            case 'firstname':
            case 'organization':
            case 'phone':
            case 'mobile':
            case 'fax':
            case 'email':
            case 'address':
            case 'deliveryaddress':
            case 'set_password':
            case 'new_password1':
            case 'new_password2':
            case 'birthday':
            case 'gender':
                return true;
        }

        if (!$this->Crm()->loginByEmail && 'nickname' === $name)
        {
            return true;
        }

        return false;
    }


    /**
     * Check if the user can view a the contact list
     * @return bool
     */
    public function listContact()
    {
        $Crm = $this->Crm();

        if ($Crm->onlineShop)
        {
            return $this->viewShopAdmin();
        }

        return true;
    }

    /**
     * Check if the user can view a particular contact.
     * @param crm_Contact $contact
     * @return bool
     */
    public function readContact($contact)
    {
        return $this->canPerformActionOnRecord($this->contact($contact), 'contact:read');
    }

    public function viewContact($contact) { return $this->readContact($contact); }

    public function emailContact($contact) { return $this->readContact($contact); }


    public function createContact()
    {
        return $this->listContact();
    }

    /**
     * Check if the user can update a particular contact.
     * @param crm_Contact $contact
     * @return bool
     */
    public function updateContact($contact)
    {
        return $this->canPerformActionOnRecord($this->contact($contact), 'contact:update');
    }

    /**
     * Check if the user can deletge a particular contact.
     * @param crm_Contact $contact
     * @return bool
     */
    public function deleteContact($contact)
    {
        return $this->canPerformActionOnRecord($this->contact($contact), 'contact:delete');
    }


    /**
     * Check if the user can create/delete a password token for a contact
     * @param crm_Contact $contact
     * @return bool
     */
    public function ContactPasswordToken(crm_Contact $contact)
    {
        return $this->updateContact($contact);
    }


    /**
     * Checks if the user can assign sectors to the specified contact.
     *
     * @param int|betom_Contact	$contact
     * @return bool
     */
    public function assignSectors($contact)
    {
        $contact = $this->contact($contact);
        if (!$contact->user) {
            return false;
        }
        return $this->updateContact($contact);
    }







    /**
     * Check if the user can view a particular organization.
     * @param crm_Organization $organization
     * @return bool
     */
    public function readOrganization($organization)
    {
        return $this->canPerformActionOnRecord($this->organization($organization), 'organization:read');
    }

    public function viewOrganization($organization) { return $this->readOrganization($organization); }

    /**
     * Check if the user can update a particular organization.
     * @param crm_Organization $organization
     * @return bool
     */
    public function updateOrganization($organization)
    {
        return $this->canPerformActionOnRecord($this->organization($organization), 'organization:update');
    }

    /**
     * Check if the user can delete a particular organization.
     * @param crm_Organization $organization
     * @return bool
     */
    public function deleteOrganization($organization)
    {
        return $this->canPerformActionOnRecord($this->organization($organization), 'organization:delete');
    }


    /**
     * View organization group link
     *
     * @param unknown $organization
     * @return boolean
     */
    public function viewOrganizationGroup($organization)
    {
        return true;
    }




    /**
     * Check if the user can view a particular deal.
     * @param crm_Deal|int $deal
     * @return bool
     */
    public function readDeal($deal)
    {
        return $this->canPerformActionOnRecord($this->deal($deal), 'deal:read');
    }

    public function viewDeal($deal) { return $this->readDeal($deal); }

    /**
     * Check if the user can update a particular deal.
     * @param crm_Deal|int $deal
     * @return bool
     */
    public function updateDeal($deal)
    {
        return $this->canPerformActionOnRecord($this->deal($deal), 'deal:update');
    }

    /**
     * Check if the user can update a particular deal.
     * @param crm_Deal|int $deal
     * @return bool
     */
    public function deleteDeal($deal)
    {
        return $this->canPerformActionOnRecord($this->deal($deal), 'deal:delete');
    }





    /**
     * Check if the user can view a particular team.
     * @param crm_Team $team
     * @return bool
     */
    public function readTeam($team)
    {
        $team = $this->team($team);
        return $this->readDeal($team->deal);
    }

    public function viewTeam($team)
    {
        return $this->readTeam($team);
    }


    /**
     * Checks if the user can create a team in the specified deal.
     *
     * @param int	$deal
     * @return bool
     */
    public function createTeam($deal)
    {
        return $this->updateDeal($this->deal($deal));
    }


    /**
     * Checks if the user can delete the specified team.
     *
     * @param int	$team
     * @return bool
     */
    public function deleteTeam($team)
    {
        $team = $this->team($team);
        return $this->updateDeal($team->deal);
    }


    /**
     * Checks if the user can update the specified team.
     * (Add/remove members, rename).
     *
     * @param int	$team
     * @return bool
     */
    public function updateTeam($team)
    {
        $team = $this->team($team);
        return $this->updateDeal($team->deal);
    }



    public function createEmail()
    {
        if ($this->Crm()->onlineShop)
        {
            return $this->viewShopAdmin();
        }

        return true;
    }



    /**
     * Checks if the user can read the specified email.
     *
     * @param int | crm_Email $email
     * @return bool
     */
    public function readEmail($email)
    {
        return $this->createEmail();
    }


    /**
     * @param crm_Email $email
     * @return bool
     */
    public function copyEmail(crm_Email $email)
    {
        return $this->createEmail();
    }



    /**
     * Checks if the user can read the specified task.
     *
      * @param int $task
      * @return bool
     */
    public function readTask($task)
    {
        if ($this->Crm()->onlineShop)
        {
            return $this->viewShopAdmin();
        }

        return true;
    }


    /**
     * Checks if the user can update the specified task.
     * @param int $task
     * @return bool
     */
    public function updateTask($task)
    {
        $task = $this->task($task);
        return ($task->responsible == bab_getUserId() || $task->createdBy == bab_getUserId());
    }


    /**
     * Checks if the user can delete the specified task.
     * @param int $task
     * @return bool
     */
    public function deleteTask($task)
    {
        return $this->updateTask($task);
    }


    /**
     * Checks if the user can read the specified note.
     *
     * @param int $note
     * @return bool
     */
    public function readNote($note)
    {
        if ($this->Crm()->onlineShop)
        {
            return $this->viewShopAdmin();
        }
        $note = $this->note($note);

        if ($note->private) {
            return ($note->createdBy == $this->currentUser());
        }

        return true;
    }

    /**
     * Checks if the user can update the specified note.
     *
     * @param int $note
     * @return bool
     */
    public function updateNote($note)
    {
        $note = $this->note($note);
        if ($note->private) {
            return ($note->createdBy == $this->currentUser());
        }

        return true;
    }

    /**
     * Checks if the user can delete the specified note.
     *
     * @param int $note
     * @return bool
     */
    public function deleteNote($note)
    {
        return $this->updateNote($note);
    }

    /**
     * Back office visualisation of article
     */
    public function readArticle(crm_Article	$article)
    {
        return $this->viewArticle($article);
    }


    /**
     * Back office visualisation of article
     */
    public function viewArticle(crm_Article	$article)
    {
        return $this->createArticle();
    }

    /**
     * Check if the user can create an article
     * @return bool
     */
    public function createArticle()
    {
        return $this->viewShopAdmin();
    }


    /**
     * Check if the user can put an article into catalog (create catalogItem )
     * @return bool
     */
    public function classifyArticle(crm_Article $article)
    {
        return $this->viewShopAdmin();
    }

    /**
     * upload/download of article attachments
     */
    public function useArticleAttachments()
    {
        return true;
    }


    /**
     * Check if the user can create a discount
     * @return bool
     */
    public function createDiscount()
    {
        return $this->viewShopAdmin();
    }


    /**
     * Check if the user can create a coupon
     * @return bool
     */
    public function createCoupon()
    {
        return $this->viewShopAdmin();
    }

    /**
     * Check if the user can import articles or articles images
     * @return bool
     */
    public function importArticles()
    {
        $Crm = $this->Crm();
        return isset($Crm->Import);
    }

    /**
     * Check if the user can export articles images in a zip archive
     * @return bool
     */
    public function exportArticlesPhotos()
    {
        return $this->createArticle();
    }

    /**
     * Check if the user can import articles images by upload
     * @return bool
     */
    public function importArticlesPhotos()
    {
        return $this->importArticles();
    }

    /**
     * Check if the user can update the specified article
     * @param int | crm_Article	 $article
     * @return bool
     */
    public function updateArticle($article)
    {
        return $this->createArticle();
    }


    /**
     * Check if the user can update the specified discount
     * @param int | crm_Discount	 $discount
     * @return bool
     */
    public function updateDiscount(crm_Discount $discount)
    {
        return $this->createDiscount();
    }

    /**
     * Check if the user can update the specified coupon
     * @param int | crm_Coupon	 $coupon
     * @return bool
     */
    public function updateCoupon(crm_Coupon $coupon)
    {
        return $this->createCoupon();
    }

    /**
     * Check if the user can delete the specified article
     * @param	int | crm_Article		$article
     * @return bool
     */
    public function deleteArticle(crm_Article $article)
    {
        return $this->updateArticle($article);
    }


    public function listCatalog()
    {
        if ($this->Crm()->onlineShop)
        {
            return $this->viewShopAdmin();
        }

        return true;
    }

    /**
     * Check if the user can read a catalog in online shop back office
     * @param int	$catalog
     * @return bool
     */
    public function readCatalog($catalog)
    {
        return $this->listCatalog();
    }


    /**
     * Check if the user can create a catalog
     * @return bool
     */
    public function createCatalog()
    {
        return $this->listCatalog();
    }

    /**
     * Check if the user can update a catalog
     * @param	int	$catalog
     * @return bool
     */
    public function updateCatalog($catalog)
    {
        return $this->listCatalog();
    }


    /**
     * Check if the user can delete a catalog
     * @param	int	$catalog
     * @return bool
     */
    public function deleteCatalog($catalog)
    {
        return $this->updateCatalog($catalog);
    }

    /**
     * Check if the user can add articles to a catalog
     * @param	int | crm_Article	$article
     * @param	int | crm_Catalog	$catalog
     * @return bool
     */
    public function addArticleToCatalog($article, $catalog)
    {
        return $this->updateArticle($article) && $this->viewCatalog($catalog);
    }

    /**
     * Check if the user can remove articles from a catalog
     * @param	int	| crm_Article 	$article
     * @param	int | crm_Catalog 	$catalog
     * @return bool
     */
    public function removeArticleFromCatalog($article, $catalog)
    {
        return $this->updateArticle($article) && $this->viewCatalog($catalog);
    }


    /**
     * Test if packagings can be used in back-office by the manager
     * if set to false, the system will use one predefined packaging and do not allow creation of multiple articlePackaging
     *
     * @return bool
     */
    public function usePackaging()
    {
        return true;
    }



    /**
     * Check if the user can view a catalog and the catalog items in it
     * @param	int | crm_Catalog	$catalog
     * @return bool
     */
    public function viewCatalog($catalog)
    {
        if (is_numeric($catalog))
        {
            $set = $this->Crm()->CatalogSet();
            $catalog = $set->get($catalog);
        }

        if (!$catalog)
        {
            return false;
        }

        if ($catalog->disabled)
        {
            return false;
        }

        return true;
    }

    /**
     * Check if the user can view a catalog item
     * @param	crm_CatalogItem		$catalogitem
     * @return bool
     */
    public function viewCatalogItem(crm_CatalogItem $catalogitem)
    {
        if (false === $this->viewCatalog($catalogitem->catalog))
        {
            return false;
        }

        $article = $catalogitem->article();
        /*@var $article crm_Article */
        if ($article->disabled && null === $article->getAccessiblePrivateSell())
        {
            return false;
        }



        return true;
    }


    /**
     * @return bool
     */
    public function downloadCatalogItemAttachment(crm_CatalogItem $catalogItem, $filename)
    {
       return ($this->viewCatalogItem($catalogitem) && $this->useArticleAttachments());
    }


    /**
     * Check if the user can add a catalog item to shopping cart
     * @param crm_CatalogItem 			$item
     * @param crm_ArticlePackaging		$articlePackaging
     * @return bool
     */
    public function addCatalogItemToShoppingCart(crm_CatalogItem $item, crm_ArticlePackaging $articlePackaging)
    {
        $availability = $item->article->articleavailability;

        if (!($availability instanceof crm_ArticleAvailability))
        {
            throw new crm_Exception('missing join on article availability');
        }

        if (!$availability->can_be_purchased)
        {
            return false;
        }

        return $this->viewCatalogItem($item);
    }


    /**
     * @return bool
     */
    public function addGiftCardToShoppingCart()
    {
        return $this->viewGiftCard();
    }


    /**
     *
     * @return boolean
     */
    public function viewGiftCard()
    {
        $addonname = $this->Crm()->getAddonName();

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory("/$addonname/configuration/");

        if (true === $description = $registry->getValue('giftcard_disabled', false))
        {
            return false;
        }

        return true;
    }



    /**
     * Check if the user can add a commment to a record
     * @param crm_Record $record
     * @return bool
     */
    public function createCommentOn(crm_Record $record)
    {
        if (!bab_isUserLogged())
        {
            return false;
        }



        switch(true)
        {
            case $record instanceof crm_Article:
                foreach($record->getAccessibleCatalogs() as $catalogItem)
                {
                    if ($this->viewCatalogItem($catalogItem))
                    {
                        return true;
                    }
                }
                break;
        }

        return false;
    }




    /**
     * Check if the user can add a commment to something
     * @return bool
     */
    public function viewMyComments()
    {
        if (!bab_isUserLogged())
        {
            return false;
        }
        return true;
    }


    /**
     * test if a new comment can be private or not
     * @param crm_Record $record	the object where the comment is attached
     * @return bool
     */
    public function setPrivateCommentOn(crm_Record $record)
    {
        return false;
    }


    /**
     * Test if a comment can be modified
     * @param crm_Comment $comment
     * @return boolean
     */
    public function updateComment(crm_Comment $comment)
    {
        if ($this->viewShopAdmin())
        {
            return true;
        }

        if ($comment->createdBy != $this->currentUser())
        {
            return false;
        }


        // try to get target from linked item
        $arr = $comment->getLinkedRecords();
        foreach($arr as $target)
        {
            if ($this->createCommentOn($target))
            {
                return true;
            }
        }


        return false;
    }



    /**
     * Test if user con confirm a list of comments
     * @return bool
     */
    public function confirmComments()
    {
        return $this->viewShopAdmin();
    }






    /**
     * View shopping cart details
     * @param crm_ShoppingCart $cart
     * @return bool
     */
    public function viewShoppingCart(crm_ShoppingCart $cart)
    {
        if ($this->editShoppingCart($cart)) {
            // i am shopping cart owner
            return true;
        }

        if (!empty($cart->name)) {
            // this is a wish-list
            return true;
        }

        if ($this->viewShopAdmin()) {
            // manager in back-office
            return true;
        }

        return false;
    }


    /**
     * Edit shopping cart details, do not test shopping cart status, verify if i am the shopping cart owner
     * @param crm_ShoppingCart $cart
     * @return bool
     */
    public function editShoppingCart(crm_ShoppingCart $cart)
    {
        $id_user = (int) $cart->user;

        // access by cookie
        $set = $this->Crm()->ShoppingCartSet();
        if (null !== $cart->cookie && $cart->cookie === $set->getMyCookie())
        {
            return true;
        }

        // access by user link
        if ($id_user > 0 && $GLOBALS['BAB_SESS_USERID'] == $id_user) {
            return true;
        }

        // access by session
        if (session_id() === $cart->session) {
            return true;
        }

        return false;
    }


    public function listArticles()
    {
        return $this->viewShopAdmin();
    }


    /**
     * Order is visible in back office
     * @return bool
     */
    public function listOrder()
    {
        return $this->viewShopAdmin();
    }


    /**
     * Order items, exportable list of sold products
     * @return bool
     */
    public function listOrderItem()
    {
        return $this->listOrder();
    }



    /**
     * Order is visible in back office
     * @param crm_Order $order
     */
    public function viewOrder(crm_Order $order)
    {
        return $this->viewShopAdmin();
    }

    /**
     * Order is modifiable in back office
     * @param crm_Order $order
     * @return boolean
     */
    public function editOrder(crm_Order $order)
    {
        if ($this->Crm()->onlineShop) {
            // dans une boutique
            // on ne peut pas crer de commandes par le back-office par default
            // donc on bloque aussi la modification car les commandes sont crees par les paniers
            return false;
        }

        return $this->viewOrder($order);
    }

    /**
     * Order is deletable is back office
     * @param crm_Order $order
     * @return boolean
     */
    public function deleteOrder(crm_Order $order)
    {
        return false;
    }

    /**
     * Order creation access right
     * @param crm_Order $order
     * @return boolean
     */
    public function createOrder()
    {
        return false;
    }


    /**
     * @return boolean
     */
    public function createDeal()
    {
        return false;
    }


    /**
     * Open file (download) or folder (read directory) from the file controller
     *
     *
     * @param crm_FileAttachment 	$file
     * @return bool
     */
    public function openFile(crm_FileAttachment $file)
    {
        $record = $file->getRecord();

        switch(true)
        {
            case $record instanceOf crm_Deal:
                $relativePath = $file->getRelativePath();
                if (empty($relativePath)) {
                    return false;
                }
                return true;

            case $record instanceOf crm_Article:
                return true;

            case $record instanceOf crm_Organization:
                $relativePath = $file->getRelativePath();
                if (empty($relativePath)) {
                    return false;
                }
                return true;
        }

        return false;
    }

    /**
     * Delete a file or folder from the file controller
     * Test access for the item to delete
     *
     * @param crm_FileAttachment 	$file
     * @return bool
     */
    public function deleteFile(crm_FileAttachment $file)
    {
        return $this->addFile($file);
    }

    /**
     * Add a file or folder from the file controller
     * Test access of item to create (file or folder)
     *
     * @param crm_FileAttachment 	$file
     * @return bool
     */
    public function addFile(crm_FileAttachment $file)
    {
        if ($parent = $file->getParent())
        {
            // by default, can add files and folder if parent is readable
            return $this->openFile($parent);
        }

        return $this->openFile($file); // allow creation of root directory if the root can be read
    }

    /**
     * Rename a file or folder from the file controller
     * Test access for the item to rename
     *
     * @param crm_FileAttachment 	$file
     * @return bool
     */
    public function renameFile(crm_FileAttachment $file)
    {
        return $this->addFile($file);
    }



    public function canSearchCatalogItems()
    {
        return true;
    }



    /**
     * Test if the actual user can record his shopping cart
     *
     * @return bool
     */
    public function canRecordShoppingCart()
    {
        return true;
    }


    /**
     * Can view a shopping cart as a user
     * @param crm_ShoppingCart $cart
     * @return false
     */
    public function canViewShoppingCart(crm_ShoppingCart $cart)
    {
        $Crm = $this->Crm();
        $contact = $cart->getContact();
        $mycontact = $Crm->getMyContact();

        if (null === $contact || null === $mycontact)
        {
            // no contact associated to cart, or not a logged in contact, must be the current edited cart

            $set = $cart->getParentSet();
            $mycart = $set->getMyCart();
            if ($cart->id === $mycart->id)
            {
                return true;
            }
        }

        return ($contact->id === $mycontact->id);
    }


    /**
     * @param	crm_ShoppingCart $cart
     * @return boolean
     */
    public function canEmptyShoppingCart(crm_ShoppingCart $cart)
    {
        return true;
    }



    /**
     * Test if the user is allowed to attach files to a note
     *
     * @param crm_Note 	$note			null for new note
     * @return bool
     */
    public function editNoteAttachments(crm_Note $note = null)
    {
        return true;
    }



    /**
     * Back office of online shop is visible
     * @return bool
     */
    public function viewShopAdmin()
    {
        return bab_isUserAdministrator();
    }


    public function viewShopTrace()
    {
        return false;
    }


    /**
     * Get a list of id user who have acccess to online shop administration.
     * The users will get notified of new orders, etc...
     *
     * @return array
     */
    public function getOnlineShopManagers()
    {
        return array();
    }



    public function createMyMailBox()
    {
        return $this->editMyMailBox();
    }

    public function editMyMailBox()
    {
        return true;
    }

    public function editMailBox()
    {
        return true;
    }

    public function listMailBox()
    {
        return $this->editMailBox();
    }

    public function manageMailingLists()
    {
        $ML = crm_MailingList();
        if (!$ML) {
            return false;
        }
        /*@var $func Func_Newsletter */
        return true;
    }



    /**
     * @return bool
     */
    public function editCollectStore()
    {
        return $this->viewShopAdmin();
    }

    /**
     * Can send email to all contacts from the contacts list
     * @return bool
     */
    public function canMailContacts()
    {
        return true;
    }
}
