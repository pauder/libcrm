<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



$Crm = crm_Crm();
$Crm->includeRecordController();




/**
 * This controller manages actions that can be performed on deals.
 */
class crm_CtrlDeal extends crm_RecordController
{
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	crm_DealSet		$dealSet
     * @param	array			$filter
     * @return ORM_Criteria
     */
    protected function getFilterCriteria(crm_DealSet $dealSet, $filter)
    {
        $Crm = $this->Crm();

        // Initial conditions are base on read access rights.
        $conditions = $Crm->Access()->canPerformActionOnSet($dealSet, 'deal:read');

        if (isset($filter['id']) && !empty($filter['id'])) {
            $conditions = $conditions->_AND_($dealSet->id->in(explode(',', $filter['id'])));
        }

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_($dealSet->name->contains($filter['name']));
        }

        if (isset($filter['status']) && !empty($filter['status']) && $filter['status'] > 1) {
            $status = $Crm->StatusSet()->get($filter['status']);
            if (isset($status)) {
                $conditions = $conditions->_AND_(
                    $dealSet->status->id->is($filter['status'])
                        ->_OR_($dealSet->status->isDescendantOf($status))
                );
            }
        }

        if (isset($filter['lead/name']) && !empty($filter['lead/name'])) {
            $conditions = $conditions->_AND_($dealSet->lead->name->contains($filter['lead/name']));
        }

        if (isset($filter['classification']) && !empty($filter['classification'])) {
            $classificationSet = $Crm->ClassificationSet();
            $classification = $classificationSet->get($classificationSet->name->is($filter['classification']));
            if (isset($classification)) {
                $parent = $classification->id_parent();
                $conditions = $conditions
                    ->_AND_(
                        $dealSet->hasImpliedClassification($filter['classification'])
                        ->_OR_($dealSet->hasImpliedClassification($parent->name))
                    );
            }
        }

        if (isset($filter['tag']) && !empty($filter['tag'])) {
            $conditions = $conditions->_AND_($dealSet->haveTagLabels($filter['tag']));
        }

        return $conditions;
    }





    /**
     * Displays a page with a list of filterable deals.
     *
     * @param array		deals		An array containing filtering parameters.
     *
     * @return Widget_Action
     */
    public function displayList__($deals = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Crm->Ui()->includeContact();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($deals), $Crm->translate('Deals'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-list');

        $filter = isset($deals['filter']) ? $deals['filter'] : array();

        $dealSet = $Crm->DealSet();
        $dealSet->lead();
        $dealSet->status();
        if (isset($dealSet->address)) {
            $dealSet->address();
        }

        $availableColumns = array(
            widget_TableModelViewColumn('image', ' ')
                ->addClass('widget-column-thin widget-column-center'),
            widget_TableModelViewColumn($dealSet->lead->name, $Crm->translate('Lead')),
            widget_TableModelViewColumn($dealSet->name, $Crm->translate('Deal')),
            widget_TableModelViewColumn($dealSet->status->name, $Crm->translate('Status')),
        );
        if (isset($dealSet->address)) {
            $availableColumns[] = widget_TableModelViewColumn($dealSet->address->city, $Crm->translate('City'));
            $availableColumns[] = widget_TableModelViewColumn($dealSet->address->postalCode, $Crm->translate('Postal code'));
        }


        $defaultVisibleColumns = array(
            'name' => $Crm->translate('Deal'),
            'status' => $Crm->translate('Status'),
            'lead/name' => $Crm->translate('Lead')
        );

        $conditions = $this->getFilterCriteria($dealSet, $filter);

        $dealSelection = $dealSet->select($conditions);

        $tableView = $Crm->Ui()->DealTableView();
        $tableView->setDataSource($dealSelection);

        $tableView->setAvailableColumns($availableColumns);
        $tableView->allowColumnSelection();

        $filterPanel = crm_dealFilteredTableView($Crm, $tableView, $filter, 'deals');


        $page->addItem($filterPanel);
        return $page;
    }





    /**
     * Displays the deal list filtered by the specified tag.
     *
     * @param string	$tag		The tag name to filter the list by.
     *
     * @return Widget_Action
     */
    public function displayListForTag($tag = null)
    {
        crm_redirect($this->proxy()->displayList(array('filter' => array('tag' => $tag))));
    }





    /**
     * Displays a form to add a deal to a specified organization.
     *
     * @param int	$org		The organization id
     * @return Widget_Action
     */
    public function add($org = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $organizationSet = $Crm->OrganizationSet();
        $organization = $organizationSet->get($org);

        if (!isset($organization)) {
            throw new crm_Exception($Crm->translate('Trying to access an organization with a wrong (unknown) id.'));
        }
        $Crm->Ui()->includeDeal();
        $Crm->Ui()->includeOrganization();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');


        $page->addItem($W->Title($Crm->translate('Create a new deal'), 1));

        $dealSet = $Crm->DealSet();
        $deal = $dealSet->newRecord();
        $deal->lead = $org;


        $form = $Crm->Ui()->DealEditor(null, $organization);


        if (!$form->getValue(array('deal', 'address', 'country'))) {
            $form->setValue(array('deal', 'address', 'country'), $Crm->CountrySet()->guessCountryFromBrowser('FR')->id);
        }

        $page->addItem($form);

        $lead = $deal->lead();
        if (isset($lead)) {
            $page->addContextItem(
                $Crm->Ui()->OrganizationCardFrame($lead),
                $Crm->translate('Client organization')
            );
        }

        return $page;
    }





    /**
     * @param $contact		The contact id
     * @return Widget_Action
     */
    public function addForContact($contact = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->includeOrganizationSet();

        $Crm->Ui()->includeDeal();
        $Crm->Ui()->includeOrganization();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception('Trying to access a contact with a wrong (unknown) id.');
        }

        $page->addItem($W->Title($Crm->translate('Create a new deal'), 1));

        $dealSet = $Crm->DealSet();
        $deal = $dealSet->newRecord();
        $mainOrganization = $contact->getMainOrganization();
        if ($mainOrganization) {
            $deal->lead = $mainOrganization->id;
        }
        $deal->referralContact = $contact->id;

        $form = $Crm->Ui()->DealEditor();
        $form->setName('deal');

        $form->setValues($deal);

        $page->addItem($form);

        $lead = $deal->lead();
        if (isset($lead)) {
            $page->addContextItem(
                $Crm->Ui()->OrganizationCardFrame($lead),
                $Crm->translate('Client organization')
            );
        }

        return $page;
    }





    /**
     *
     * @param int	$deal
     * @return Widget_Action
     */
    public function edit($deal = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->Ui()->includeDeal();
        $Crm->Ui()->includeOrganization();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');
        $dealSet = $Crm->DealSet();

        require_once $GLOBALS['babInstallPath'].'utilit/upgradeincl.php';
        if(bab_isTableField($dealSet->getTableName(), 'address')) {
            $dealSet->address();
            //ADRESS IS NOT LINK on deal in the default SET.
        }


        if (isset($deal)) {
            $deal = $dealSet->get($deal);
            if (!isset($deal)) {
                throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
            }
            $page->addItem($W->Title($Crm->translate('Edit deal'), 1));
            $page->addItemMenu('deal', $Crm->translate('Deal'), $this->proxy()->edit($deal->id)->url());
//			$page->addItemMenu('dealpackages', $Crm->translate('Work packages'), $this->proxy()->editPackages($deal->id)->url());
            $page->setCurrentItemMenu('deal');
        } else {
            $page->addItem($W->Title($Crm->translate('Create a new deal'), 1));
        }


        $form = $Crm->Ui()->DealEditor();
        $form->setName('deal');

        $form->setValues($deal);

        $page->addItem($form);

        if ($deal) {
            $Crm->includeOrganizationSet();
            $lead = $deal->lead();
            if (isset($lead)) {
                $page->addContextItem(
                    $Crm->Ui()->OrganizationCardFrame($lead),
                    $Crm->translate('Client organization')
                );
            }

            $photoUploadPath = $deal->getPhotoUploadPath();
            if (!is_dir($photoUploadPath->toString())) {
                $photoUploadPath->createDir();
            }
            $addon = bab_getAddonInfosInstance('LibCrm');
             $defaultImage = $addon->getStylePath() . 'images/deal-default.png';

            $imagesFormItem = $W->ImagePicker()
                ->setDimensions(256, 256)
                ->oneFileMode()
                ->setFolder($photoUploadPath)
                ->setDefaultImage(new bab_Path($defaultImage));

            $page->addContextItem(
                $imagesFormItem,
                $Crm->translate('Photo')
            );
        }

        return $page;
    }





// 	/**
// 	 * @param	string		$deal		The deal id
// 	 * @return Widget_Action
// 	 */
// 	public function editPackages($deal = null)
// 	{
// 		$W = bab_Widgets();
// 		$Crm = $this->Crm();

// 		$dealSet = $Crm->DealSet();
// 		$deal = $dealSet->get($deal);
// 		if (!isset($deal)) {
// 			throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
// 		}

// 		$Crm->Ui()->includeDeal();
// 		$Crm->Ui()->includeOrganization();

// 		$page = $Crm->Ui()->Page();
// 		$page->addClass('crm-page-editor');



// 		$page->addItem($W->Title($Crm->translate('Work packages'), 1));
// 		$page->addItemMenu('deal', $Crm->translate('Deal'), $this->proxy()->edit($deal->id)->url());
// //		$page->addItemMenu('dealpackages', $Crm->translate('Work packages'), $this->proxy()->editPackages($deal->id)->url());
// 		$page->setCurrentItemMenu('dealpackages');

// 		$toolbar = new crm_Toolbar();
// 		$toolbar->addButton($Crm->translate('Add work package'), Func_Icons::ACTIONS_DOCUMENT_NEW, $this->proxy()->addWorkPackage($deal->id));

// 		$page->addToolbar($toolbar);


// 		$page->addContextItem(
// 			$Crm->Ui()->DealCardFrame($deal),
// 			$Crm->translate('Deal')
// 		);

// 		$dealPackagesEditor = new crm_Editor($Crm);


// 		$dealPackagesEditor->setCancelAction($Crm->Controller()->Deal()->cancel());
// 		$dealPackagesEditor->setSaveAction($Crm->Controller()->Deal()->savePackages());

// 		$dealPackagesEditor->setHiddenValue('tg', bab_rp('tg'));

// 		$page->addItem($dealPackagesEditor);
// 		return $page;
// 	}





// 	/**
// 	 * @param	string		$deal		The deal id
// 	 * @return Widget_Action
// 	 */
// 	public function addWorkPackage($deal = null)
// 	{

// 	}





// 	public function savePackages()
// 	{

// 	}



    /**
     * @param $deal
     *
     * @ajax
     */
    public function filesSection($deal)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if (!($deal instanceof crm_Deal)) {
            $dealSet = $Crm->DealSet();
            /* @var $deal crm_Deal */
            $deal = $dealSet->get($deal);
            if (!isset($deal)) {
                throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
            }

            if (!$Access->readDeal($deal)) {
                throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
            }
        }

        $Crm->Ui()->includeFile();

        if (!$deal->getAttachmentsUploadPath()->isDir()) {
            $deal->getAttachmentsUploadPath()->createDir();
        }
        $folderView = crm_folderView($Crm, $deal->getRef(), 'attachments');
        $folderView->setIconThumbnailSize(30);
        $folderView->setPreviewThumbnailSize(200, 200);
        $folderView->setIconPosition('left');
        $folderView->setIconWidth(160);

        $uploadAction = $this->proxy()->filesSection($deal->id);

        $folderView->setUploadAction($uploadAction);

        $filesSection = $W->Section(
            $Crm->translate('Attached files'),
            $folderView,
            4
        )
        ->setId('crm_deal_attached_files_' . $deal->id)
        ->setPersistent(true)
        ->setFoldable(true, true)
        ->addClass('compact');

        $menu = $filesSection->addContextMenu('inline');
        $menu->addItem($folderView->filePicker());

        return $filesSection;
    }





    /**
     *
     * @param int		$deal
     * @param string	$type
     * @throws crm_AccessException
     *
     * @return Widget_Displayable_Interface
     */
    public function planning($deal = null, $type = null, $id = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $Access = $Crm->Access();

        $Ui->includeDeal();
        $dealSet = $Crm->DealSet();

        $deal = $dealSet->request($deal);

        if (!$Access->readDeal($deal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

        if (!isset($type)) {
            $type = $W->getUserConfiguration('dealPlanningViewType', $Crm->addonPrefix, true);
            if (!isset($type)) {
                $type = 'timeline';
            }
        }

        $planningFrame = $W->Frame();
        if (isset($id)) {
            $planningFrame->setId($id);
        }

        if ($type === 'timeline') {
            $planningFrame->addItem(
                $Ui->DealTimeline($deal, $Crm->Controller()->Task()->display(0)->url(), 'task')
            );
        } else {
            $planningFrame->addItem(
                $Ui->DealPlanningList($deal, $Crm->Controller()->Task()->display(0)->url(), 'task')
            );
        }



        $planningFrame->setReloadAction($Crm->Controller()->Deal()->planning($deal->id), null, $planningFrame->getId());

        return $planningFrame;
    }



    /**
     * Sets the
     * @ajax
     *
     * @param unknown $type
     */
    public function setPlanningViewType($type = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $W->setUserConfiguration('dealPlanningViewType', $type, $Crm->addonPrefix, true);

        return true;
    }



    protected function createActionFrame($deal)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $Access = $Crm->Access();

        $Ui->includePage();
        $page = new crm_Page();
        $actionsFrame = $page->ActionsFrame();

        if ($Access->updateDeal($deal)) {
            $actionsFrame->addItem(
                $W->Link(
                    $W->Icon($Crm->translate('Edit this deal'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $Crm->Controller()->Deal()->edit($deal->id)
                )
            );
            if (isset($Crm->DealClassification)) {
                $actionsFrame->addItem(
                    $W->Link(
                        $W->Icon($Crm->translate('Edit codification'), Func_Icons::ACTIONS_VIEW_LIST_TREE),
                        $Crm->Controller()->Deal()->classify($deal->id)
                    )
                );
            }
        }

        if ($Access->deleteDeal($deal)) {
            $actionsFrame->addItem(
                $W->Link(
                    $W->Icon($Crm->translate('Delete this deal'), Func_Icons::ACTIONS_EDIT_DELETE),
                    $Crm->Controller()->Deal()->confirmDelete($deal->id)
                )
            );
        }

        return $actionsFrame;
    }


    /**
     *
     * @param Widget_Page $page
     * @param crm_Record $deal
     * @param string $current
     */
    protected function menu(Widget_Page $page, crm_Record $deal, $current = null)
    {
        $Crm = $this->Crm();
        $page->addItemMenu('deal', $Crm->translate('Deal'), $this->proxy()->display($deal->id)->url());
        $page->addItemMenu('dealfiles', $Crm->translate('Attached files'), $Crm->Controller()->File()->display($deal->getRef(), crm_Deal::ATTACHMENTSSUBFOLDER)->url());
        if (isset($current)) {
            $page->setCurrentItemMenu($current);
        }
    }


    /**
     * Displays a page with relevant information about the
     * specified deal.
     *
     * @param int	$deal			The deal id.
     *
     * @return Widget_Action
     */
    public function display($deal)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $Access = $Crm->Access();

        $dealSet = $Crm->DealSet();
        /* @var $deal crm_Deal */
        $deal = $dealSet->get($deal);
        if (!isset($deal)) {
            throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
        }

        if (!$Access->readDeal($deal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

        $Ui->includeDeal();
        $Ui->includeContact();
        $Ui->includeOrganization();
        $Ui->includeTask();


        crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($deal->id), $deal->name);


        $page = $Ui->Page();

        $this->menu($page, $deal, 'deal');

        $mainFrame = $W->Frame()->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
        $mainFrame->addClass('crm-detailed-info-frame');
        $page->addItem($mainFrame);


        $mainFrame->addItem($Ui->DealFullFrame($deal));



        // Timeline


        $planningFrame = $this->planning($deal->id);

//        $planningFrame->setReloadAction($Crm->Controller()->Deal()->planning($deal->id));
        $timelineSection = $W->Section(
            $Crm->translate('Planning'),
            $W->VBoxItems($planningFrame),
            //$timelineFrame = crm_DealTimeline($deal, $Crm->Controller()->Task()->display(0)->url(), 'task'),
            2
        )->addClass('compact')
        ->setFoldable(true, false);

        $menu = $timelineSection->addContextMenu();
        $menu->addItem(
            $W->Link($Crm->translate('Timeline view'), $Crm->Controller()->Deal()->setPlanningViewType('timeline'))
                ->addClass('icon', Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE)
                //->setAjaxAction(null, $planningFrame)
        );
        $menu->addItem(
            $W->Link($Crm->translate('List view'), $Crm->Controller()->Deal()->setPlanningViewType('list'))
                ->addClass('icon', Func_Icons::ACTIONS_VIEW_LIST_TEXT)
                ->setAjaxAction(null, $planningFrame)
        );
        $menu->addClass(Func_Icons::ICON_LEFT_16);

        $mainFrame->addItem($timelineSection);


        // Recent history

        $maxHistory = 8;
        $historyFrame = $W->VBoxItems(
            $Crm->Controller()->Search(false)->history($deal->getRef(), $maxHistory, 0, true)
        );
        $mainFrame->addItem(
            $W->Section(
                $Crm->translate('History'),
                $historyFrame,
                2
            )->setFoldable(true)
            ->addClass('compact')
        );


        // Tell clipboard what actions are possible on this page.

        $this->addAcceptedClipboardObject(
            'Email',
            $Crm->Controller()->Deal()->CopyFromClipboard('__1__', $deal->id),
            $Crm->translate('Copy email in this deal history'),
            Func_Icons::ACTIONS_EDIT_PASTE
        );


        // Actions
        $actionFrame = $this->createActionFrame($deal);
        $page->addContextItem($actionFrame);

        if ($deal->lead) {
            $clientSection = $W->Section(
                $Crm->translate('Client organization'),
                $W->VBoxLayout()->setVerticalSpacing(0.5, 'em'),
                4
            );

            $Crm->includeOrganizationSet();
            $client = $deal->lead();
            if ($client) {
                $clientSection->addItem(
                    $Ui->organizationCardFrame($client, crm_OrganizationCardFrame::SHOW_ALL - crm_OrganizationCardFrame::SHOW_ADDRESS)
                );

                $page->addContextItem($clientSection);

                if ($referralContact = $deal->referralContact()) {

                    $contactCard = $Ui->ContactCardFrame($referralContact, crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_NAME | crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_ADDRESS ));


                    $fullName = $referralContact->getFullName();

                    $position = $referralContact->getMainPosition();
                    if (empty($position)) {
                        $position = '';
                    }
                    $contactSection = $W->Section(
                        $fullName,
                        $contactCard,
                        6
                    )
                    ->setFoldable(true, true);
                    $contactSection->setSubHeaderText($position);

                    $contactInfo = $contactSection->addContextMenu('inline');
                    $contactInfo->addItem(
                        $Ui->ContactPhoto($referralContact, 24, 24)
                            ->addClass('widget-hide-unfolded')
                    );

                    $contactMenu = $contactSection->addContextMenu('popup');

                    $contactMenu->addItem($W->Link($W->Icon($Crm->translate('View contact page'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $this->Crm()->Controller()->Contact()->display($referralContact->id)));
                    if ($email = $referralContact->getMainEmail()) {
                        $contactMenu->addItem(
                            $W->Link(
                                $W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
                                $Crm->Controller()->Email()->edit($referralContact->getMainEmail(), null, null, $referralContact->getRef() . ',' . $deal->getRef())
                            )
                        );
                    } else {
                        $contactMenu->addItem(
                            $W->Link(
                                $W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_USER_PROPERTIES),
                                $Crm->Controller()->Contact()->edit($referralContact->id)
                            )
                        );
                    }



// 					$clientSection->addItem(
// 						$W->VBoxItems(
// 							$W->Title($Crm->translate('Referral contact'), 4),
// 							$contactSection
// 						)
// 					);

                    $page->addContextItem($contactSection, $Crm->translate('Referral contact'));
                }
            }
        }


        // Tasks

        $tasksSection = $W->Section(
            $Crm->translate('Incoming tasks for this deal'),
            $Ui->userTasksForCrmObject($deal),
            4
        );

        $tasksSectionMenu = $tasksSection->addContextMenu('inline');

        $tasksSectionMenu->addItem(
            $W->VboxItems(
                $W->Link($Crm->translate('Add'), $Crm->Controller()->Task()->edit(null, $deal->getRef()))
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setTitle($Crm->translate('Add a task'))
                    ->addClass('widget-instant-button widget-actionbutton')
            )->addClass('widget-instant-container')
        );

        $page->addContextItem($tasksSection);


// 		// Files

// 		$filesSection = $this->filesSection($deal);

// 		$page->addContextItem($filesSection);


        // Teams

        $Ui->includeTeam();

        $teamsSection = $W->Section(
            $Crm->translate('Teams'),
            $applicationsPanel = crm_teamsPreview($deal),
            4
        );

        if ($Access->addTeam($deal->id)) {
            $teamsSection->addContextMenu('popup')
                ->addItem(
                    $W->Link(
                        $W->Icon($Crm->translate('Build teams'), Func_Icons::ACTIONS_USER_GROUP_NEW),
                        $Crm->Controller()->Team()->build($deal->id)
                    )
                );
        }

        $page->addContextItem($teamsSection);


        return $page;
    }




    /**
     * Link the specified object to this deal.
     *
     * @param string $ref 		A reference to the object to copy.
     * @param int    $deal		The destination deal id.
     */
    public function CopyFromClipboard($ref = null, $deal = null)
    {
        $Crm = $this->Crm();

        $dealSet = $Crm->DealSet();
        /* @var $deal crm_Deal */
        $deal = $dealSet->get($deal);
        if (!isset($deal)) {
            throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
        }

        $object = $Crm->getRecordByRef($ref);

        if ($object instanceof crm_Email) {
            $object->linkTo($deal);
        }

        return true;
    }

    /**
     * Display the form to select the deal classification.
     *
     * @param int   $id
     * @return crm_Page
     */
    public function classify($id= null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $record->requireUpdatable($Crm->translate('You are not allowed to access this page.'));

        $Ui->includeClassification();
        $Ui->includeOrganization();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Edit deal codification'));

        $dealClassificationSet = $Crm->DealClassificationSet();
        $dealClassifications = $dealClassificationSet->selectForDeal($record->id);

        $classifications = array();
        foreach ($dealClassifications as $dealClassification) {
            $classifications[$dealClassification->classification] = $dealClassification->classification;
        }

        $classificationSelector = crm_classificationSelector($Crm, $classifications);

        $classificationSelector->setHiddenValue('deal', $record->id);

        $classificationSelector->setCancelAction($Crm->Controller()->Deal()->cancel());
		$classificationSelector->setSaveAction($Crm->Controller()->Deal()->saveClassification(), $Crm->translate('Save the codification'));

        $page->addItem($classificationSelector);

        return $page;
    }





    /**
     * Saves the deal classification
     *
     * @param int	$deal				the deal to classify
     * @param array	$classifications	array where keys correspond to selected classification ids.
     * @return Widget_Action
     */
    public function saveClassification($deal = null, $classifications = array())
    {
        $Crm = $this->Crm();

        $Access = $Crm->Access();
        if (!$Access->updateDeal($deal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

        $dealClassificationSet = $Crm->DealClassificationSet();
        $dealClassificationSet->delete($dealClassificationSet->deal->is($deal));

        foreach (array_keys($classifications) as $classification) {
            $dealClassification = $dealClassificationSet->newRecord();
            $dealClassification->deal = $deal;
            $dealClassification->classification = $classification;
            $dealClassification->save();
        }

        $this->addMessage($Crm->translate('The deal codification has been saved.'));
        return true;
    }




    public function editStatusHistory($dealStatusHistory = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $dealStatusHistorySet = $Crm->DealStatusHistorySet();
        $dealStatusHistorySet->deal();

        /* @var $deal crm_Deal */
        $dealStatusHistory = $dealStatusHistorySet->get($dealStatusHistory);
        if (!isset($dealStatusHistory)) {
            throw new crm_Exception($Crm->translate('Trying to access a dealStatusHistory with a wrong (unknown) id.'));
        }
        $Access = $Crm->Access();
        if (!$Access->updateDeal($dealStatusHistory->deal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

//		var_dump($dealStatusHistory->getValues());
        $Crm->Ui()->includeDeal();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $editor = new crm_Editor($Crm, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();

        $editor->setName('dealStatusHistory');

        $editor->addItem(
            $W->FlowItems(
                $editor->labelledField(
                    $Crm->translate('Status'),
                    $statusSelector = crm_DealStatusSelector($Crm, false),
                    'status'
                ),
                $editor->labelledField(
                    $Crm->translate('Date'),
                    $datePicker = $W->DatePicker()
                        ->setValue(BAB_DateTime::now())
                        ->setMandatory(true, $Crm->translate('You must specified the date.')),
                    'date'
                )
            )->setHorizontalSpacing(2, 'em')
        );
        $editor->addItem(
            $editor->labelledField(
                $Crm->translate('Comment'),
                $comment = $W->TextEdit()->setColumns(70),
                'comment'
            )
        );

        $statusSelector->setValue($dealStatusHistory->status);
        $datePicker->setValue($dealStatusHistory->date);
        $comment->setValue($dealStatusHistory->comment);

        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setHiddenValue('dealStatusHistory[id]', $dealStatusHistory->id);
        $editor->setSaveAction($this->proxy()->saveStatusHistory());
        $editor->setCancelAction($this->proxy()->cancel());

        $page->addItem($W->Title($Crm->translate('Edit deal status'), 1));
        $page->addItem($editor);

        $page->addContextItem(
            $Crm->Ui()->DealCardFrame($dealStatusHistory->deal)
        );

        return $page;
    }


    public function saveStatusHistory($dealStatusHistory = null)
    {
        $Crm = $this->Crm();

        $data = $dealStatusHistory;
        $dealStatusHistorySet = $Crm->DealStatusHistorySet();
        $dealStatusHistorySet->deal();

        $dealStatusHistory = $dealStatusHistorySet->get($data['id']);
        if (!isset($dealStatusHistory)) {
            throw new crm_Exception($Crm->translate('Trying to access a dealStatusHistory with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();
        if (!$Access->updateDeal($dealStatusHistory->deal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

        $dealStatusHistory->setFormInputValues($data);
        $dealStatusHistory->save();

        $dealStatusHistory->deal->updateLatestStatus();

        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The deal status has been saved.'));
    }



    /**
     * Deletes the specified deal status history.
     * @param int $dealStatusHistory
     */
    public function deleteStatusHistory($dealStatusHistory)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        $dealStatusHistorySet = $Crm->DealStatusHistorySet();
        $dealStatusHistorySet->deal();

        $dealStatusHistory = $dealStatusHistorySet->get($dealStatusHistory);
        if (!isset($dealStatusHistory)) {
            throw new crm_Exception($Crm->translate('Trying to access a dealStatusHistory with a wrong (unknown) id.'));
        }

        if (!$Access->updateDeal($dealStatusHistory->deal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

        $deal = $dealStatusHistory->deal;

        $dealStatusHistory->delete();

        $deal->updateLatestStatus();

        return true;
    }



    /**
     * Displays a form to edit the deal status.
     *
     * @param int	$deal 	The deal id.
     *
     * @return Widget_Action
     */
    public function editStatus($deal = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $dealSet = $Crm->DealSet();
        /* @var $deal crm_Deal */
        $deal = $dealSet->get($deal);
        if (!isset($deal)) {
            throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
        }
        $Access = $Crm->Access();
        if (!$Access->updateDeal($deal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $Crm->Ui()->includeDeal();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $editor = new crm_Editor($Crm, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();

        $editor->setName('deal');

        $editor->addItem(
            $W->FlowItems(
                $editor->labelledField(
                    $Crm->translate('Status'),
                    crm_DealStatusSelector($Crm, false),
                    'status'
                ),
                $editor->labelledField(
                    $Crm->translate('Date'),
                    $W->DatePicker()
                        ->setValue(BAB_DateTime::now())
                        ->setMandatory(true, $Crm->translate('You must specified the date.')),
                    'date'
                )
            )->setHorizontalSpacing(2, 'em')
        );
        $editor->addItem(
            $editor->labelledField(
                $Crm->translate('Comment'),
                $W->TextEdit()->setColumns(70),
                'comment'
            )
        );

        $editor->setValue('deal/status', $deal->status);
        $editor->setValue('deal/date', BAB_DateTime::now()->getIsoDateTime());
        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setHiddenValue('deal[id]', $deal->id);
        $editor->setSaveAction($this->proxy()->saveStatus());
        $editor->setCancelAction($this->proxy()->cancel());

        $page->addItem($W->Title($Crm->translate('Edit deal status'), 1));
        $page->addItem($editor);

        $page->addContextItem(
            $Crm->Ui()->DealCardFrame($deal)
        );

        return $page;
    }





    /**
     * Saves the deal status.
     *
     * @param array	$deal		['id', 'status', 'comment', 'date']
     * @return Widget_Action
     */
    public function saveStatus($deal = null)
    {
        if (!isset($deal)) {
            throw new Exception('Missing form data');
        }

        $Crm = $this->Crm();
        /* @var $currentDeal crm_Deal */
        $currentDeal = $Crm->DealSet()->get($deal['id']);

        if (!isset($currentDeal)) {
            throw new Exception('Wrong deal');
        }
        $Access = $Crm->Access();
        if (!$Access->updateDeal($currentDeal)) {
            throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
        }

        $status = $deal['status'];
        $comment = $deal['comment'];

        $date = $Crm->DealStatusHistorySet()->date->convert($deal['date']);

        $currentDeal->updateStatus($status, $comment, $date);

        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The deal status has been saved.'));
    }






    protected function setValues($record, $data)
    {
        $Crm = $this->Crm();

        /*@var $record crm_Deal */

        $record->setFormInputValues($data);

        if (isset($data['lead_name']) && $data['lead_name'] !== '') {
            $leadSet = $Crm->OrganizationSet();
            $lead = $leadSet->get($leadSet->name->is($data['lead_name']));
            if ($lead) {
                $record->lead = $lead->id;
            }
        }

        if (isset($data['referralContact_name'])) {

            if (trim($data['referralContact_name']) === '') {
                $record->referralContact = 0;
            } else {
                $contactSet = $Crm->ContactSet();
                list($namestart) = explode(' ', $data['referralContact_name']);
                $referralContacts = $contactSet->select($contactSet->lastname->startsWith($namestart)->_OR_($contactSet->firstname->startsWith($namestart)));
                foreach ($referralContacts as $referralContact) {
                    if (bab_composeUserName($referralContact->firstname, $referralContact->lastname) == $data['referralContact_name']) {
                        $record->referralContact = $referralContact->id;
                        break;
                    }
                }
            }
        }


        if (isset($data['responsible_name'])) {
            if (trim($data['responsible_name']) === '') {
                $record->responsible = 0;
            } else {
                $contactSet = $Crm->ContactSet();
                list($namestart) = explode(' ', $data['responsible_name']);
                $responsibleContacts = $contactSet->select($contactSet->lastname->startsWith($namestart)->_OR_($contactSet->firstname->startsWith($namestart)));
                foreach ($responsibleContacts as $responsibleContact) {
                    if (bab_composeUserName($responsibleContact->firstname, $responsibleContact->lastname) == $data['responsible_name']) {
                        $record->responsible = $responsibleContact->id;
                        break;
                    }
                }
            }
        }

        return;
    }





    /**
     * Saves/updates a deal.
     *
     * @param array		$deal		An indexed array containing the data to store in the deal.
     *
     * @return Widget_Action
     */
    public function save($deal = null)
    {
        $Crm = $this->Crm();

        $set = $Crm->DealSet();

        $set->address();

        if (isset($deal['id']) && !empty($deal['id'])) {
            $newDeal = false;
            $record = $set->get($deal['id']);
            if (!isset($record)) {
                throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
            }
        } else {
            $newDeal = true;
            $record = $set->newRecord();
        }
        /* @var $record crm_Deal */


        $this->setValues($record, $deal);

        $newDeal = !isset($record->id);

        $record->save();

        $record->getPhotoUploadPath()->createDir();

        if (isset($deal['origin'])) {

            $entrySet = $Crm->EntrySet();
            $entries = $entrySet->select($entrySet->category->is(crm_Entry::DEAL_ORIGINS)->_AND_($entrySet->text->is($deal['origin'])));
            if ($entries->count() === 0) {
                $entry = $entrySet->newRecord();
                $entry->text = $deal['origin'];
                $entry->category = crm_Entry::DEAL_ORIGINS;
                $entry->save();
            }
        }

        $attachmentsPath = $record->getAttachmentsUploadPath();
        $attachmentsPath->createDir();

        if ($newDeal) {
            crm_redirect($Crm->Controller()->Deal()->classify($record->id, true), $Crm->translate('The deal has been created.'));
        }

        crm_redirect($Crm->Controller()->Deal()->display($record->id), $Crm->translate('The deal has been saved.'));
    }





    /**
     * Deletes the specified deal.
     *
     * @see crm_CtrlDeal::confirmDelete()
     *
     * @param int	$deal		The deal id.
     *
     * @return Widget_Action
     */
    public function delete($deal = null)
    {
        $Crm = $this->Crm();

        $set = $Crm->DealSet();
        /* @var $deal crm_Deal */
        $deal = $set->get($deal);
        if (!isset($deal)) {
            throw new crm_Exception($Crm->translate('Trying to access an deal with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();

        if (!$Access->deleteDeal($deal)) {
            throw new crm_Exception($Crm->translate('You do not have access to this action.'));
        }

        $deal->delete();

        crm_redirect($this->proxy()->displayList(), $Crm->translate('The deal has been deleted.'));
    }





    /**
     * Displays an information page about elements linked to a deal
     * before deleting it, and propose the user to confirm or cancel
     * the deal deletion.
     *
     * @param int $deal
     *
     * @return Widget_Action
     */
    public function confirmDelete($deal = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $set = $Crm->DealSet();
        $deal = $set->get($deal);
        if (!isset($deal)) {
            throw new crm_Exception($Crm->translate('Trying to delete a deal with a wrong (unknown) id.'));
        }

        $page = $Crm->Ui()->Page();
        $page->setMainPanelLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Delete deal'));

        $page->addContextItem(
            $Crm->Ui()->DealCardFrame($deal),
            $Crm->translate('Deal')
        );

        $toolbar = new crm_Toolbar();
        $toolbar->local = true;

        $relatedRecords = $deal->getRelatedRecords();

        $tasks = $deal->selectTasks();
        $notes = $deal->selectNotes();
        $emails = $deal->selectEmails();

        $nbTasks = $tasks->count();
        $nbNotes = $notes->count();
        $nbEmails = $emails->count();

        if ($nbTasks > 0 || $nbNotes > 0 || $nbEmails > 0 || count($relatedRecords) > 0) {

            $relatedRecordSection = $W->Section(
                $Crm->translate('Several CRM elements are related to this deal'),
                $W->VBoxItems()->setVerticalSpacing(1, 'em')
            );
            $page->addItem($relatedRecordSection);

            if ($nbTasks > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Tasks:') . ' ' . $nbTasks));
            }
            if ($nbNotes > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Notes:') . ' ' . $nbNotes));
            }
            if ($nbEmails > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Emails:') . ' ' . $nbEmails));
            }

            foreach ($relatedRecords as $setClassName => $records) {

                list($prefix, $crmSetClassName) = explode('_', $setClassName);
                $set = $Crm->$crmSetClassName();
                $relatedRecordSection->addItem(
                    $section = $W->Section(
                        $Crm->translate($set->getDescription()),
                        $W->VBoxItems()->setVerticalSpacing(1, 'em'),
                        4
                    )->setFoldable(true)
                );

                $section->addContextMenu('inline')->addItem($W->Label($records->count())->addClass('crm-counter-label'));

                foreach ($records as $record) {
                    switch (true) {
                        case $record instanceof crm_Deal:
                            $relatedRecordSection->addItem($Crm->Ui()->DealCardFrame($record));
                            break;
                        case $record instanceof crm_Contact:
                            $section->addItem(
                                $Crm->Ui()->ContactCardFrame(
                                    $record,
                                    crm_ContactCardFrame::SHOW_ALL
                                        & ~(crm_ContactCardFrame::SHOW_ADDRESS | crm_ContactCardFrame::SHOW_FAX | crm_ContactCardFrame::SHOW_PHONE | crm_ContactCardFrame::SHOW_MOBILE)
                                )
                            );
                            break;
                        case $record instanceof crm_Organization:
                            $relatedRecordSection->addItem($Crm->Ui()->OrganizationCardFrame($record));
                            break;
                        case $record instanceof crm_Team:
                            $relatedRecordSection->addItem($Crm->Ui()->TeamCardFrame($record));
                            break;

                        default:
                            $relatedRecordSection->addItem($W->Label($record->getRecordTitle()));
                            break;
                    }
                }
            }

            $toolbar->addItem(
                $W->Link(
                    $Crm->translate('Replace with an existing deal...'),
                    $Crm->Controller()->Deal()->searchReplacement($deal->id)
                )->addClass('icon', Func_Icons::ACTIONS_VIEW_REFRESH, 'widget-actionbutton')
            );

            $toolbar->addItem(
                $W->Link(
                    $Crm->translate('Delete the deal and all associated elements'),
                    $Crm->Controller()->Deal()->delete($deal->id)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE, 'widget-actionbutton')
            );
        } else {

            $page->addItem($W->Label($Crm->translate('This deal is not associated with any CRM element.')));
            $toolbar->addItem(
                $W->Link(
                    $Crm->translate('Delete deal'),
                    $Crm->Controller()->Deal()->delete($deal->id)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE, 'widget-actionbutton')
            );

        }

        $toolbar->addItem(
            $W->Link(
                $Crm->translate('Cancel'),
                $Crm->Controller()->Deal()->cancel()
            )->addClass('icon', Func_Icons::ACTIONS_GO_PREVIOUS, 'widget-actionbutton')
        );

        $page->addItem($toolbar);

        return $page;
    }





    /**
     *
     * @param int $deal
     *
     * @return Widget_Action
     */
    public function searchReplacement($deal = null, $keyword = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $dealSet = $Crm->DealSet();
        $deal = $dealSet->get($deal);
        if (!isset($deal)) {
            throw new crm_Exception($Crm->translate('Trying to access a deal with a wrong (unknown) id.'));
        }
        if (!isset($keyword)) {
            $keyword = $deal->name();
        }

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Search replacement for deal'), 1));

        $page->addContextItem($Crm->Ui()->DealCardFrame($deal), $Crm->translate('Deal to replace'));

        $alreadyMatchedDealIds = array($deal->id => $deal->id);

        $keywords = array();
        foreach (explode(' ', $keyword) as $k) {
            if (mb_strlen($k) > 3) {
                $keywords[] = $k;
            }
        }

        $editor = new crm_Editor($Crm, null, $W->VBoxLayout());
        $editor->colon();

        $editor->addItem($editor->labelledField($Crm->translate('Keywords'), $W->LineEdit()->setSize(80), 'keyword'));
        $editor->setSaveAction($Crm->Controller()->Deal()->searchReplacement(), $Crm->translate('Search'));
        $editor->setReadOnly();
        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setHiddenValue('deal', $deal->id);
        $editor->setValue('keyword', implode(' ', $keywords));
        $page->addItem($editor);


        if (count($keywords) > 0) {

            $similarDealsSection = $W->Section(
                $Crm->translate('Similar deals'),
                $W->VBoxLayout()
                    ->setVerticalSpacing(1, 'em'),
                3
            );
            $page->addItem($similarDealsSection);

            $criteria = new ORM_FalseCriterion();
            foreach ($keywords as $k) {
                $criteria = $criteria->_OR_($dealSet->name->contains($k));
            }

            $deals = $dealSet->select(
                $dealSet->id->notIn($alreadyMatchedDealIds)
                ->_AND_($criteria)
            );


            foreach ($deals as $deal) {
                $similarDealsSection->addItem($Crm->Ui()->DealCardFrame($deal));
                $alreadyMatchedDealIds[$deal->id] = $deal->id;
            }
        }

        return $page;
    }











    /**
     * Does nothing and return to the previous page.
     *
     * @param string	$message
     *
     * @return Widget_Action
     */
    public function cancel($message = null)
    {
        crm_redirect(crm_BreadCrumbs::last(), $message);
    }





    /**
     * Displays help on this page.
     *
     * @return Widget_Action
     */
    public function help()
    {

    }
}
