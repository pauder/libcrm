<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



$Crm = crm_Crm();
$Crm->includeRecordController();


class crm_ContactOrOrganizationSet extends crm_RecordSet
{
	function __construct(Func_Crm $crm)
	{
		parent::__construct($crm);

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name'),
			ORM_StringField('activity')
					->setDescription('Activity'),
			ORM_StringField('postalCode')
					->setDescription('Postal code'),
			ORM_StringField('city')
					->setDescription('City'),
			ORM_IntField('relevance')
					->setDescription('Relevance')
		);

	}
}

class crm_ContactOrOrganization extends crm_Record
{
}


/**
 * This controller manages actions that can be performed on teams.
 */
class crm_CtrlTeam extends crm_RecordController
{

	/**
	 * Select contacts by relevance
	 * @return array
	 */
	protected function selectContacts($filter)
	{
		$Crm = $this->Crm();
		$Crm->includeAddressSet();
		$contactSet = $Crm->ContactSet();
		$contactSet->join('address');
		$contactSet->join('organization');

		$criteria1 = null;
		$criteria2 = null;
		$criteria3 = null;
		$criteria4 = null;

		$conditions = new ORM_TrueCriterion();
		$exactConditions = new ORM_TrueCriterion();

		if (isset($filter)) {

			if (isset($filter['name']) && !empty($filter['name'])) {
				$conditions = $conditions->_AND_($contactSet->lastname->contains($filter['name']));
				$exactConditions = $exactConditions->_AND_($contactSet->lastname->contains($filter['name']));
				$criteria2 = $contactSet->lastname->contains($filter['name']);
			}
			if (isset($filter['position']) && !empty($filter['position'])) {
				$conditions = $conditions->_AND_($contactSet->position->contains($filter['position']));
				$exactConditions = $exactConditions->_AND_($contactSet->position->contains($filter['position']));
				$criteria3 = $contactSet->position->contains($filter['position']);
			}
			if (isset($filter['address/postalCode']) && !empty($filter['address/postalCode'])) {
				$conditions = $conditions->_AND_($contactSet->address->postalCode->startsWith($filter['address/postalCode']));
				$exactConditions = $exactConditions->_AND_($contactSet->address->postalCode->startsWith($filter['address/postalCode']));
				$criteria4 = $contactSet->address->postalCode->startsWith($filter['address/postalCode']);
			}
			if (isset($filter['address/city']) && !empty($filter['address/city'])) {
				$conditions = $conditions->_AND_($contactSet->address->city->contains($filter['address/city']));
				$exactConditions = $exactConditions->_AND_($contactSet->address->city->contains($filter['address/city']));
			}


			if (isset($filter['classifications']) && is_array($filter['classifications'])) {

				$hasOneClassification = new ORM_FalseCriterion();
				$hasAllClassifications = new ORM_TrueCriterion();
				$criteria1 = new ORM_TrueCriterion();
				$nbClassifications = 0;
				foreach ($filter['classifications'] as $classificationId => $classificationName) {
					if ($classificationName !== '') {
						$nbClassifications++;
						$hasOneClassification = $hasOneClassification->_OR_($contactSet->hasClassification($classificationName, 'implied'));
						$hasAllClassifications = $hasAllClassifications->_AND_($contactSet->hasClassification($classificationName, 'implied'));
						$criteria1 = $criteria1->_AND_($contactSet->hasClassification($classificationName, 'implied'));
					}
				}
				if ($nbClassifications > 0) {
					$conditions = $conditions->_AND_($hasOneClassification);
					$exactConditions = $exactConditions->_AND_($hasAllClassifications);
				}
			}
		}



		$selectedContacts = array();

		$contacts = $contactSet->select($exactConditions);

		foreach ($contacts as $contact) {

			$selectedContacts['Contact:' . $contact->id] = array(
				'id' => 'Contact:' . $contact->id,
				'relevance' => 1000,
				'name' => $contact->getFullName() . "\n" . ($contact->getMainOrganization() ? $contact->getMainOrganization()->name : ''),
				'activity' => $contact->position,
				'postalCode' => $contact->address->postalCode,
				'city' => $contact->address->city,
				'displayUrl' => $Crm->Controller()->Contact()->display($contact->id)
			);
		}

		$contacts = $contactSet->select($conditions);

		foreach ($contacts as $contact) {

			if (isset($selectedContacts['Contact:' . $contact->id])) {
				continue;
			}
			$selectedContacts['Contact:' . $contact->id] = array(
				'id' => 'Contact:' . $contact->id,
				'relevance' => 100,
				'name' => $contact->getFullName() . "\n" . ($contact->getMainOrganization() ? $contact->getMainOrganization()->name : ''),
				'activity' => $contact->position,
				'postalCode' => $contact->address->postalCode,
				'city' => $contact->address->city,
				'displayUrl' => $Crm->Controller()->Contact()->display($contact->id)
			);
		}

		if (!isset($filter['sort'])) {
			$sortField = 'relevance';
			$sortOrder = 'up';
		} else {
			list($sortField, $sortOrder) = explode(':', $filter['sort']);
		}
		bab_Sort::asort($selectedContacts, $sortField);
		if ($sortOrder === 'down' || ($sortOrder === 'up' && $sortField === 'relevance')) {
			$selectedContacts = array_reverse($selectedContacts);
		}
		return $selectedContacts;
	}


	/**
	 * Select organizations by relevance
	 * @return array
	 */
	protected function selectOrganizations($filter)
	{
		$Crm = $this->Crm();
		$Crm->includeOrganizationTypeSet();

		$organizationSet = $Crm->OrganizationSet();
		$organizationSet->join('address');
		//$organizationSet->join('type');

		$criteria1 = null;
		$criteria2 = null;
		$criteria3 = null;
		$criteria4 = null;

		$conditions = new ORM_TrueCriterion();
		$exactConditions = new ORM_TrueCriterion();

		if (isset($filter)) {
			if (isset($filter['name'])) {
				$conditions = $conditions->_AND_($organizationSet->name->contains($filter['name']));
				$exactConditions = $exactConditions->_AND_($organizationSet->name->contains($filter['name']));
				$criteria2 = $organizationSet->name->contains($filter['name']);
			}
			if (isset($filter['address/postalCode'])) {
				$conditions = $conditions->_AND_($organizationSet->address->postalCode->startsWith($filter['address/postalCode']));
				$exactConditions = $exactConditions->_AND_($organizationSet->address->postalCode->startsWith($filter['address/postalCode']));
				$criteria4 = $organizationSet->address->postalCode->startsWith($filter['address/postalCode']);
			}
			if (isset($filter['address/city'])) {
				$conditions = $conditions->_AND_($organizationSet->address->city->contains($filter['address/city']));
				$exactConditions = $exactConditions->_AND_($organizationSet->address->city->contains($filter['address/city']));
			}


			if (isset($filter['classifications']) && is_array($filter['classifications'])) {

				$hasOneClassification = new ORM_FalseCriterion();
				$hasAllClassifications = new ORM_TrueCriterion();
				$criteria1 = new ORM_TrueCriterion();
				$nbClassifications = 0;
				foreach ($filter['classifications'] as $classificationId => $classificationName) {
					if ($classificationName !== '') {
						$nbClassifications++;
						$hasOneClassification = $hasOneClassification->_OR_($organizationSet->hasClassification($classificationName, 'implied'));
						$hasAllClassifications = $hasAllClassifications->_AND_($organizationSet->hasClassification($classificationName, 'implied'));
						$criteria1 = $criteria1->_AND_($organizationSet->hasClassification($classificationName, 'implied'));
					}
				}
				if ($nbClassifications > 0) {
					$conditions = $conditions->_AND_($hasOneClassification);
					$exactConditions = $exactConditions->_AND_($hasAllClassifications);
				}
			}

		}

		$selectedOrganizations = array();

		$organizations = $organizationSet->select($exactConditions);

		foreach ($organizations as $organization) {
			$selectedOrganizations['Organization:' . $organization->id] = array(
				'id' => 'Organization:' . $organization->id,
				'relevance' => 1000,
				'name' => $organization->name,
				'activity' => $organization->activity,
				'postalCode' => $organization->address->postalCode,
				'city' => $organization->address->city,
				'displayUrl' => $Crm->Controller()->Organization()->display($organization->id)
			);
		}

		$organizations = $organizationSet->select($conditions);

		foreach ($organizations as $organization) {
			if (isset($selectedOrganizations['Organization:' . $organization->id])) {
				continue;
			}
			$selectedOrganizations['Organization:' . $organization->id] = array(
				'id' => 'Organization:' . $organization->id,
				'relevance' => 100,
				'name' => $organization->name,
				'activity' => $organization->activity,
				'postalCode' => $organization->address->postalCode,
				'city' => $organization->address->city,
				'displayUrl' => $Crm->Controller()->Organization()->display($organization->id)
			);
		}

		if (!isset($filter['sort'])) {
			$sortField = 'relevance';
			$sortOrder = 'up';
		} else {
			list($sortField, $sortOrder) = explode(':', $filter['sort']);
		}
		bab_Sort::asort($selectedOrganizations, $sortField);
		if ($sortOrder === 'down' || ($sortOrder === 'up' && $sortField === 'relevance')) {
			$selectedOrganizations = array_reverse($selectedOrganizations);
		}

		return $selectedOrganizations;
	}


	protected function selectContactsAndOrganizations($filter)
	{
		$Crm = $this->Crm();
		$contacts = $this->selectContacts($filter);
		$organizations = $this->selectOrganizations($filter);

		$all = $contacts + $organizations;

		if (!isset($filter['sort'])) {
			$sortField = 'relevance';
			$sortOrder = 'up';
		} else {
			list($sortField, $sortOrder) = explode(':', $filter['sort']);
		}
		bab_Sort::asort($all, $sortField);
		if ($sortOrder === 'down' || ($sortOrder === 'up' && $sortField === 'relevance')) {
			$all = array_reverse($all);
		}

		return $all;
	}


	/**
	 * Displays the page dedicated to team building.
	 *
	 * @param	int		$deal
	 * @param	int		$pageSize
	 * @param	int		$pageNumber
	 * @param	array	$contacts
	 *
	 * @return Widget_Action
	 */
	public function build($deal = null, $pageSize = 15, $pageNumber = 0, $contacts = null)
	{
		require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Crm->Ui()->includeContact();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->build($deal, $pageSize, $pageNumber, $contacts), $Crm->translate('Build teams'));

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');


		$mainPanel = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
		$page->addItem($mainPanel);


		$page->addItemMenu('build', $Crm->translate('Teams'), $this->proxy()->build($deal, $pageSize, $pageNumber, $contacts));
		$page->setCurrentItemMenu('build');


		// Filtered contact list

		$contactsFilter = $contacts['filter'];

		if (!isset($contactsFilter['classifications'])) {
// 			$d = $Crm->DealSet()->get($deal);

// 			$contactsFilter['classifications'] = $d->getClassifications();
// 			ksort($contactsFilter['classifications']);
		} else if (isset($contactsFilter['classifications']['name'])) {
			$classificationSet = $Crm->ClassificationSet();
			$newClassification = $classificationSet->get($classificationSet->id->isNot(1)->_AND_($classificationSet->name->is($contactsFilter['classifications']['name'])));
			if (isset($newClassification)) {
				$contactsFilter['classifications'][$newClassification->id] = $newClassification->name;
			}
		}


		if (!isset($contactsFilter['type'])) {
			$contactsFilter['type'] = 'organizations';
		}
		if ($contactsFilter['type'] === 'organizations') {
			$dataSource = $this->selectOrganizations($contactsFilter);
		} else if ($contactsFilter['type'] === 'contacts') {
			$dataSource = $this->selectContacts($contactsFilter);
		}

		$Crm->Ui()->includeTeam();

		$contactTable = new crm_TeamContactAndOrganizationTableView();
		$contactTable->setDeal($Crm->DealSet()->get($deal));

		$contactOrOrganizationSet = new crm_ContactOrOrganizationSet($Crm);
		$contactTable->setDataSource($dataSource, $contactOrOrganizationSet);
		$contactTable->setVisibleColumns(
			array(
				'relevance' => $Crm->translate('Relevance'),
				'name' => $Crm->translate('Name or reason'),
				'activity' => $Crm->translate('Function or activity'),
				'postalCode' => $Crm->translate('Postal code'),
				'city' => $Crm->translate('City'),
				'__actions__' => ''
			),
			$contactOrOrganizationSet
		);
		$contactTable->setPageLength($pageSize);
		$contactTable->setCurrentPage($pageNumber);

		$contactTable->addClass('icon-16x16 icon-left icon-left-16');


		$filteredContactTable = crm_teamBuildingTableModelViewFilterPanel($Crm, $contactTable, $contactsFilter, 'contacts');


		// Deal panel

		$Crm->Ui()->includeDeal();
		$dealSection = $W->Section(
			$Crm->translate('Deal'),
			$Crm->Ui()->DealCardFrame($Crm->DealSet()->get($deal)),
			4
		);
		$page->addContextItem($dealSection);


		// Teams

		$page->addContextItem(
			crm_teamsPreview($Crm->DealSet()->get($deal)),
			$Crm->translate('Teams')
		);

		$page->addItem($filteredContactTable);

		$proposedSearchFrame = $W->Frame()
			->setLayout($W->FlowLayout()->addClass('crm-actions')->addClass('icon-left-16 icon-16x16 icon-left'));

		$page->addItem($proposedSearchFrame);

		return $page;
	}





	/**
	 * Adds a new team to the specified deal.
	 *
	 * @param int $deal
	 * @return Widget_Action
	 */
	public function add($deal = null)
	{

	}




    /**
     * Adds the specified contact to the members of the team.
     *
     * @param int $team
     * @param int $contact
     * @return Widget_Action
     */
    public function addMember($team = null, $object = null)
    {
        $Crm = $this->Crm();

        $teamSet = $Crm->TeamSet();

        $team = $teamSet->request($team);

        $object = $Crm->getRecordByRef($object);
        if (!isset($object)) {
            throw new crm_Exception('Trying to access an object with a wrong (unknown) reference.');
        }

        $team->addMember($object);

        return true;
    }






    /**
     * Adds the specified contact/organization to the members of a new team.
     * The new team is named according to the member name.
     *
     * @param int $team
     * @param string	$object		A Crm reference to a crm object.
     * @return Widget_Action
     */
    public function addMemberToNewTeam($deal, $object = null)
    {
        $Crm = $this->Crm();

        $object = $Crm->getRecordByRef($object);
        if (!isset($object)) {
            throw new crm_Exception('Trying to access an object with a wrong (unknown) reference.');
        }

        $dealSet = $Crm->DealSet();
        $deal = $dealSet->request($deal);

        $teamName = sprintf($Crm->translate('Team %s'), $object->getName());
        $teamSet = $Crm->TeamSet();
        $team = $teamSet->get(
            $teamSet->name->is($teamName)->_AND_($teamSet->deal->is($deal->id))
        );
        if (!isset($team)) {
            $team = $teamSet->newRecord();
            $team->name = $teamName;
            $team->deal = $deal->id;
            $team->save();
        }

        $team->addMember($object);

        return true;
    }







    /**
     * Removes the specified member.
     *
     * @param int $id       The team id
     * @param int $member	The member link id
     * @return Widget_Action
     */
    public function removeMember($id = null, $member = null)
    {
        $Crm = $this->Crm();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $linkSet = $Crm->LinkSet();

        $linkSet->delete(
            $linkSet->id->is($member)
            ->_AND_($linkSet->sourceIsA(get_class($record)))
            ->_AND_($linkSet->sourceId->is($record->id))
        );

        return true;
    }





    /**
     *
     * @param int	$team
     * @return Widget_Action
     */
    public function rename($team = null)
    {
        $Crm = $this->Crm();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $teamSet = $Crm->TeamSet();
        $teamSet->deal();
        $team = $teamSet->request($team);

        $page->setTitle($Crm->translate('Rename team'));

        $Crm->Ui()->includeTeam();
        $teamEditor = crm_teamEditor($Crm, $team);

        $teamEditor->setValues(array('team' => $team->getValues()));

        $page->addItem($teamEditor);

        return $page;
    }


    /**
     * Save a record
     *
     * @requireSaveMethod
     *
     * @param array $data
     */
    public function save($team = null)
    {
    	return parent::save($team);
	}






	/**
	 * Displays a page with information about a team and its members.
	 *
	 * Let the user check the team and accept or reject it for application if the team is waiting for verification.
	 *
	 * @param string	$team
	 * @return Widget_Action
	 */
	public function display($team)
	{
		$Crm = $this->Crm();

		$Crm->Ui()->includeContact();
		$Crm->Ui()->includeOrganization();

		$teamSet = $Crm->TeamSet();
		$team = $teamSet->get($team);

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($team->id), $team->name);

		$W = bab_Widgets();

		$page = $Crm->Ui()->Page()
			->addClass('crm-detailed-info-frame');

		$members = $team->getMembers();

		$page->addItem($W->Title($team->name, 1));

		$membersBox = $W->Section(
			$Crm->translate('Team members'),
			$W->FlowLayout()
				->setVerticalAlign('top')
				->setSpacing(1, 'em'),
			4
		);

		$foldersBox = $W->Section(
			$Crm->translate('Folders'),
			$W->VBoxLayout()->setVerticalSpacing(2, 'em'),
			4
		);

		$page->addItem(
			$contentBox = $W->HBoxItems(
				$membersBox
			)->setHorizontalSpacing(2, 'em')
		);


		foreach ($members as $memberLink) {
			$member = $memberLink['member'];

			if ($member instanceof crm_Contact) {
				$memberCard = $Crm->Ui()->ContactCardFrame($member);
				$displayUrl = $Crm->Controller()->Contact()->display($member->id);
			} else {
				$memberCard = $Crm->Ui()->OrganizationCardFrame($member);
				$displayUrl = $Crm->Controller()->Organization()->display($member->id);
			}
			$address = $member->getMainAddress();
			$membersBox->addItem(
				$memberCard
					->setCanvasOptions(Widget_Item::Options()->width(20, 'em'))
					->addClass('crm-card')
			);
		}



		// Actions panel

		$actionsFrame = $page->ActionsFrame();

		$page->addContextItem($actionsFrame);



		$actionsFrame->addItem(
			$W->Link(
				$W->Icon($Crm->translate('Rename team...'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
				$Crm->Controller()->Team()->rename($team->id)
			)
		);


		$Crm->Ui()->includeDeal();

		// Deal panel

		$page->addContextItem(
			$Crm->Ui()->DealCardFrame($team->deal())
				->addClass('crm-card')
		);

		return $page;
	}






// 	/**
// 	 *
// 	 * @param array $team
// 	 * @return Widget_Action
// 	 */
// 	public function save($team = null)
// 	{
// 		$Crm = $this->Crm();
// 		$teamSet = $Crm->TeamSet();
// 		$t = $teamSet->get($team['id']);
// 		if (!$t) {
// 			return;
// 		}
// 		$t->name = $team['name'];
// 		$t->save();

// 		crm_redirect(crm_BreadCrumbs::last());
// 	}




	/**
	 * Deletes the specified team
	 *
	 * @param int $team
	 * @return Widget_Action
	 */
	public function delete($team)
	{
		$Crm = $this->Crm();
		$teamSet = $Crm->TeamSet();

		$linkSet = $Crm->LinkSet();

		$team = $teamSet->get($team);
		$linkSet->deleteForSource($team);

		$teamSet->delete($teamSet->id->is($team->id));
		crm_redirect(crm_BreadCrumbs::last());
	}




	/**
	 * Does nothing and return to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}

}
