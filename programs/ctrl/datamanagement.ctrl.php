<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on articles.
 */
class crm_CtrlDataManagement extends crm_Controller
{
	/**
	 * Display Menu
	 * @return Widget_Action
	 */
	public function displayMenu()
	{

	}

	/**
	 * Export Organizations CSV file
	 * @return Widget_Action
	 */
	public function exportOrganizations()
	{
		$this->Crm()->includeOrganizationTypeSet();
		$this->Crm()->includeAddressSet();
		$this->Crm()->includeCountrySet();

		$set = $this->Crm()->OrganizationSet();
		$set->join('type');
		$set->join('address');
		$set->address->join('country');

		$name = $this->Crm()->CountrySet()->getNameColumn();

		$export = array(

			'name' 						=> $set->name->getDescription(),
			'description'				=> $set->description->getDescription(),
			'activity'					=> $set->activity->getDescription(),
			'type/name'					=> $this->Crm()->translate('Type'),
			'address/street'			=> $set->address->street->getDescription(),
			'address/postalCode'		=> $set->address->postalCode->getDescription(),
			'address/city'				=> $set->address->city->getDescription(),
			'address/state'				=> $set->address->state->getDescription(),
			"address/country/$name"		=> $this->Crm()->translate('Country'),
			'modifiedOn'				=> $set->modifiedOn->getDescription(),
			'uuid'						=> $set->uuid->getDescription()
		);

		$this->Crm()->exportSet($set, $export, $this->Crm()->translate('Organizations'));
	}




	/**
	 * Export contacts CSV file
	 * @return Widget_Action
	 */
	public function exportContacts()
	{

		$set = $this->Crm()->ContactSet();
		$this->Crm()->includeOrganizationSet();
		$set->join('organization');


		$export = array(

			'title'					=> $set->title->getDescription(),
			'lastname'				=> $set->lastname->getDescription(),
			'firstname' 			=> $set->firstname->getDescription(),
			'gender'				=> $set->gender->getDescription(),
			'organization/name'		=> $this->Crm()->translate('Organization'),
			'modifiedOn'			=> $set->modifiedOn->getDescription(),
			'uuid'					=> $set->uuid->getDescription()
		);

		$this->Crm()->exportSet($set, $export, $this->Crm()->translate('Contacts'));
	}





	/**
	 * Export contact database with vCard format
	 * @return Widget_Action
	 */
	public function exportVCardContacts()
	{
		$set = $this->Crm()->ContactSet();
		$this->Crm()->includeOrganizationSet();
		$set->join('organization');
		$contacts = $set->select();

		$filename = 'Contacts.vcf';

		header("Content-Disposition: attachment; filename=$filename");
		header("Connection: close");
		header("Content-Type: text/x-vCard; name=$filename");

		foreach($contacts as $contact) {
			echo $contact->vCard()->toString();
		}

		die();
	}




	/**
	 * Delete import record
	 * @return Widget_Action
	 */
	public function delete($import = null)
	{
		$set = $this->Crm()->ImportSet();
		$set->delete($set->id->is($import));

		crm_redirect($this->proxy()->displayMenu());
	}



	/**
	 * Delete import record
	 * @return Widget_Action
	 */
	public function deleteImported($import = null)
	{
	    $Crm = $this->Crm();
	    $Access = $Crm->Access();
	    	    
	    $importSet = $Crm->ImportSet();
	    $import = $importSet->get($import);
	    
	     
	    if (!isset($import)) {
	        throw new crm_Exception('Invalid import id');
	    }
	    
        $linkSet = $Crm->LinkSet();
        switch ($import->objecttype) {
            case crm_Import::TYPE_ORGANIZATION:
                $set = $Crm->OrganizationSet();
                $recordClassName = $Crm->OrganizationClassName();
                break;
            case crm_Import::TYPE_CONTACT:
                $set = $Crm->ContactSet();
                $recordClassName = $Crm->ContactClassName();
                break;
            case crm_Import::TYPE_ARTICLE:
                $set = $Crm->ArticleSet();
                $recordClassName = $Crm->ArticleClassName();
                break;
            case crm_Import::TYPE_DEAL:
                $set = $Crm->DealSet();
                $recordClassName = $Crm->DealClassName();
                break;
            default:
                return;
        }
        $linkSet->joinTarget($recordClassName);
         
        $conditions = $set->id->in(
            $linkSet->sourceId->is($import->id)->_AND_(
                $linkSet->sourceClass->is(get_class($import))->_AND_(
                    $linkSet->targetClass->is($recordClassName)
                )
            ),
            'targetId'
        );
        
        $set->delete($conditions);
	    
	    $importSet->delete($importSet->id->is($import->id));
	
	    crm_redirect($this->proxy()->displayMenu());
	}


	/**
	 * Does nothing and return to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}
}
