<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * This controller manages actions that can be performed on orders.
 */
class crm_CtrlOrder extends crm_Controller implements crm_ShopAdminCtrl
{

	protected function orderTableModelView($filter)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		
		$table = $Ui->OrderTableView();
		
		$orderSet = $Crm->OrderSet();
		$orderSet->deliveryaddress();
		
		if (isset($Crm->ShoppingCart))
		{
			$this->Crm()->includeShoppingCartSet();
			$orderSet->join('shoppingcart');
		}
		
		$table->addDefaultColumns($orderSet);
		
		$criteria = $table->getFilterCriteria($filter);
		
		if (!empty($filter['item']))
		{
			// filtre par element vendu
			$oiSet = $Crm->OrderItemSet();
			$items = $oiSet->reference->like($filter['item'])->_OR_($oiSet->name->matchAll($filter['item']));
			$criteria = $criteria->_AND_($orderSet->id->in($items));
		}
		
		$orderSelection = $orderSet->select($criteria);
		
		bab_debug($orderSelection->getSelectQuery());
		
		$table->setDataSource($orderSelection);
		
		
		
		return $table;
	}
	
	
	
	
	
	/**
	 * Display the list of orders
	 * @return Widget_Action
	 */
	public function displayList($orders = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();
		$Crm->includeOrderSet();
		
		if (!empty($orders['filter']['type']))
		{
			$types = crm_Order::getListTitle();
			$label = $types[$orders['filter']['type']];
		} else {
			$label = $Crm->translate('Accounting documents');
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $label);
		
		
		if (!$Access->listOrder() || !isset($Crm->Order))
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}

		$page = $Ui->Page();
		$page->addClass('crm-page-list');

		$filter = isset($orders['filter']) ? $orders['filter'] : array('sort' => 'name:down');
		
		if (!isset($filter['sort']))
		{
			$filter['sort'] = 'name:down';
		}

		$table = $this->orderTableModelView($filter);
		
		
		
		
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Export in CSV'), Func_Icons::ACTIONS_ARROW_DOWN, $this->proxy()->exportList($orders));
		$page->addToolbar($toolbar);
		
		$page->addItem($table->filterPanel($filter, 'orders'));

		return $page;
	}
	
	
	
	
	/**
	 * @param array	 $orders		List parameters
	 *
	 * @return Widget_Action
	 */
	public function exportList($orders = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();
	
		if (!$Access->listOrder())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
	
		$filter = isset($orders['filter']) ? $orders['filter'] : array();
	
		$tableView = $this->orderTableModelView($filter);
		$tableView->downloadCsv('export.csv');
	}
	
	

	/**
	 * Display one order
	 * @return Widget_Action
	 */
	public function display($order = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$ui = $Crm->Ui();

		

		$set = $Crm->OrderSet();
		if (isset($Crm->Currency))
		{
			$Crm->includeCurrencySet();
			$set->currency();
		}

		if (isset($Crm->ShoppingCart))
		{
			$Crm->includeShoppingCartSet();
			$set->shoppingcart();
		}

		$orderRecord = $set->request($order);


		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($order), $set->type->output($orderRecord->type));
		
		$Access = $Crm->Access();
		if (!$Access->viewOrder($orderRecord)) {
		    throw new crm_AccessException(crm_translate('This order is not accessible'));
		}
		

		$page = $Crm->Ui()->Page();

		$mainFrame = $ui->OrderFullFrame($orderRecord);
		$mainFrame->addClass('crm-detailed-info-frame');
		$page->addItem($mainFrame);


		// add note

		$mainFrame->addItem(
				$W->VboxItems(
						$W->Link($Crm->translate('Add a note'), $Crm->Controller()->Note()->edit(null, $orderRecord->getRef()))
						->addClass('widget-instant-button widget-actionbutton'),
						$Crm->Ui()->AddNoteEditor($orderRecord, 'Order')
						->setTitle($Crm->translate('Add a note'))
						->addClass('widget-instant-form')
				)->addClass('widget-instant-container')
		);


		// Recent history
		$historyFrame = $Crm->Ui()->History($orderRecord);
		$mainFrame->addItem(
			$W->Section(
				$Crm->translate('History'),
				$historyFrame,
				2
			)->setFoldable(true)
			->addClass('compact')
		);




		// Actions

		$page->addContextItem($this->getActionsFrame($page, $orderRecord));
		
		
		return $page;
	}
	
	
	protected function getActionsFrame(crm_Page $page, crm_Order $orderRecord)
	{
	    $W = bab_Widgets();
	    $Crm = $this->Crm();
	    $actionsFrame = $page->ActionsFrame();
	    
	    if ($Crm->Access()->deleteOrder($orderRecord))
	    {
	        $actionsFrame->addItem(
	            $W->Link(
	                $W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
	                $Crm->Controller()->Order()->delete($orderRecord->id)
	                )->setConfirmationMessage($Crm->translate('Do you really whant to delete this order?'))
	            );
	    }
	    
	    $actionsFrame->addItem(
	        $W->Link(
	            $W->Icon($Crm->translate('Download PDF'), Func_Icons::MIMETYPES_APPLICATION_PDF),
	            $Crm->Controller()->Order()->pdf($orderRecord->id)
	            )
	        ->setOpenMode(Widget_Link::OPEN_POPUP)
	    );
	    
	    $actionsFrame->addItem(
	        $W->Link(
	            $W->Icon($Crm->translate('Download delivery slip PDF'), Func_Icons::MIMETYPES_APPLICATION_PDF),
	            $Crm->Controller()->Order()->deliverySlipPdf($orderRecord->id)
	            )
	        ->setOpenMode(Widget_Link::OPEN_POPUP)
	    );
	    
	    if (isset($Crm->ShoppingCart))
	    {
	        $actionsFrame->addItem(
	            $W->Link(
	                $W->Icon($Crm->translate('Following order'), Func_Icons::ACTIONS_GO_NEXT),
	                $Crm->Controller()->ShoppingCartAdm()->display($orderRecord->shoppingcart->id)
	                )
	            );
	    }
	    
	    
	    return $actionsFrame;
	}



	/**
	 * Edit order
	 * @param	int	$order
	 * @return Widget_Action
	 */
	public function edit($order = null)
	{

	}
	
	
	protected function getPdfOrderSet()
	{
	    $set = $this->Crm()->OrderSet();
	    $set->join('billingaddress');
	    $set->join('deliveryaddress');
	    $set->join('shippingaddress');
	     
	    return $set;
	}


	/**
	 * Download PDF file with one order
	 * @param int $order
	 * @return Widget_Action
	 */
	public function pdf($order = null)
	{
	    $Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Crm->includeAddressSet();
		$set = $this->getPdfOrderSet();
		$order = $set->request($order);

		if (!$order) {
			return;
		}
		

		$Access = $Crm->Access();
		if (!$Access->viewOrder($order)) {
		    throw new crm_AccessException(crm_translate('This order is not accessible'));
		}

		$pdf = $Ui->OrderPdf($order);
		$pdf->setOrderTitle($order->name);
		
		$addonname = $Crm->getAddonName();
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		$type = $registry->getValue('customer_pdf_type');
		if (empty($type)) {
		    $type = $set->type->output($order->type);
		}
		
		$pdf->setOrderType($type);
		$pdf->Output(sprintf('%s.pdf', $order->name));
		exit;
	}
	
	/**
	 * Download a delivery slip
	 * @param int $order
	 * @return Widget_Action
	 */
	public function deliverySlipPdf($order = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Crm->includeAddressSet();
		$set = $Crm->OrderSet();
		$set->join('billingaddress');
		$set->join('deliveryaddress');
		$set->join('shippingaddress');
		
		$order = $set->get($order);
		
		if (!$order) {
			return;
		}
		
		$Access = $Crm->Access();
		if (!$Access->viewOrder($order)) {
		    throw new crm_AccessException(crm_translate('This order is not accessible'));
		}
		
		$pdf = $Ui->DeliverySlipPdf($order);
		$pdf->setOrderTitle('BL'.$order->name);
		$pdf->setOrderType($Crm->translate('Delivery slip'));
		$pdf->Output(sprintf('%s.pdf', 'BL'.$order->name));
		exit;
	}



	/**
	 * Organization form data (ajax)
	 *
	 * @param	string	$type
	 * @param 	string 	$organizationname
	 *
	 * @return Widget_Action
	 */
	public function organizationFormData($type = null, $organizationname = null)
	{
		$set = $this->Crm()->OrganizationSet();
		$res = $set->select($set->name->like($organizationname));

		if (1 === $res->count())
		{
			foreach($res as $organization) {

				$recipient = $organization->getBillingAddressRecipient();
				$address = $organization->getBillingAddress();

				if (!isset($address)) {
					$address = $organization->getMainAddress();
				}

				if (!isset($address)) {
					break;
				}

				echo $this->organizationFormDataOuput($recipient, $address);

			}
		}
		die();
	}



	protected function organizationFormDataOuput($recipient, crm_Address $address)
	{
		return "

		{
			recipient 	: '".bab_toHtml($recipient, BAB_HTML_JS)."',
			address		: {
				street			: '".bab_toHtml($address->street, BAB_HTML_JS)."',
				city 			: '".bab_toHtml($address->city, BAB_HTML_JS)."',
				cityComplement 	: '".bab_toHtml($address->cityComplement, BAB_HTML_JS)."',
				postalCode		: '".bab_toHtml($address->postalCode, BAB_HTML_JS)."',
				country			: '".bab_toHtml($address->country, BAB_HTML_JS)."'
			}
		}

		";
	}



	/**
	 * Catalog item form data (ajax) used to initalize an order item from a catalog item
	 *
	 * @see crm_OrderItemEditor::catalogitem()
	 *
	 * @param	int		$order
	 * @param 	string 	$articlereference
	 *
	 * @return Widget_Action
	 */
	public function catalogItemFormData($order = null, $articlereference = null)
	{
		$Crm = $this->Crm();
		$Crm->includeArticleSet();
		$set = $Crm->CatalogItemSet()->join('article');
		$res = $set->select($set->article->reference->like($articlereference));

		$order = $Crm->OrderSet()->get($order);
		
		$Access = $Crm->Access();
		if (!$Access->listArticles()) {
		    throw new crm_AccessException(crm_translate('Access denied'));
		}

		if (1 === $res->count())
		{
			foreach($res as $catalogitem) {

				echo "

				{
					name 		: '".bab_toHtml($catalogitem->article->getName(), BAB_HTML_JS)."',
					description : '".bab_toHtml($catalogitem->article->getDescription(), BAB_HTML_JS)."',
					unit_cost	: '".bab_toHtml($catalogitem->getOrderDefaultUnitCost($order), BAB_HTML_JS)."',
					quantity	: '".bab_toHtml($catalogitem->getOrderDefaultQuantity($order), BAB_HTML_JS)."',
					vat			: '".bab_toHtml($catalogitem->getOrderDefaultVat($order), BAB_HTML_JS)."'
				}

				";
			}
		}
		die();
	}





	/**
	 * Get organization ID from suggestion organization value
	 * @param array 	$order
	 * @param string 	$prefix
	 * @return array	order
	 */
	protected function getOrganization($order, $prefix)
	{
		if (empty($order[$prefix.'organization']) && !empty($order[$prefix.'organizationname']))
		{
			$set = $this->Crm()->OrganizationSet();
			$res = $set->select($set->name->like($order[$prefix.'organizationname']));
			$res->orderAsc($set->name);

			if (0 !== $res->count())
			{
				foreach($res as $organization) {
					$order[$prefix.'organization'] = (int) $organization->id;
					break;
				}
			}
		}


		if (isset($order[$prefix.'organization']))
		{
			$order[$prefix.'organization'] = (int) $order[$prefix.'organization'];
		}

		return $order;
	}



	/**
	 * Save items rows from order edit form
	 * @param crm_Order $order
	 * @param array $postedItems
	 * @return bool
	 */
	protected function saveItems(crm_Order $order, $postedItems)
	{
		if (empty($postedItems)) {
			return false;
		}

		foreach($order->getItems() as $item) {
			$arr = $postedItems[$item->id];
			$item->setValues($arr);
			$item->save();
		}

		return true;
	}



	/**
	 * Save taxes rows from order edit form
	 * @param crm_Order $order
	 * @param array $postedTaxes
	 * @return bool
	 */
	protected function saveTaxes(crm_Order $order, $postedTaxes)
	{
		if (empty($postedTaxes)) {
			return false;
		}

		foreach($order->getTaxes() as $item) {
			$arr = $postedTaxes[$item->id];

			$item->name = $arr['name'];
			if (isset($arr['amount']) && !$item->readonly) {
				$item->amount = $arr['amount'];
			}

			$item->save();
		}

		return true;
	}






	/**
	 * Save order modifications
	 * @param	array	$order
	 * @return Widget_Action
	 */
	public function save($order = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();



		$Crm->includeAddressSet();
		$set = $Crm->OrderSet();

		$set->join('billingaddress');
		$set->join('shippingaddress');
		$set->join('deliveryaddress');

		if (!empty($order['id']))
		{
			$orderRecord = $set->get($order['id']);

			if (!$access->editOrder($orderRecord))
			{
				throw new crm_AccessException($Crm->translate('Access denied'));
			}

			if (isset($order['items'])) {
				$this->saveItems($orderRecord, $order['items']);
			}

		} else {
			$orderRecord = $set->newRecord();

			if (!$access->createOrder())
			{
				throw new crm_AccessException($Crm->translate('Access denied'));
			}
		}

		$order = $this->getOrganization($order, 'billing');
		$order = $this->getOrganization($order, 'shipping');
		$order = $this->getOrganization($order, 'delivery');

		$orderRecord->setValues($order);

		if (isset($order['items']['tax'])) {
			// update taxes
			$this->saveTaxes($orderRecord, $order['items']['tax']);
		}
		$orderRecord->refreshTotal();

		try {
			$orderRecord->save();
		} catch(crm_OrderDuplicateException $e)
		{
			crm_redirect(crm_BreadCrumbs::last(), $e->getMessage());
		}


		return true;
	}





	/**
	 * Delete order
	 * @param int	$order
	 * @return Widget_Action
	 */
	public function delete($order = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();

		$set = $this->Crm()->OrderSet();
		$orderRecord = $set->request($order);

		if (!$access->deleteOrder($orderRecord)) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}

		$set->delete($set->id->is($order));
		crm_redirect($this->proxy()->displayList());
	}







	/**
	 * Edit one order item
	 * @param 	int $orderitem
	 * @param	int	$order
	 * @return Widget_Action
	 */
	public function editItem($orderitem = null, $order = null)
	{
		$Crm = $this->Crm();

		$page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

		$page->setTitle($Crm->translate('Edit order item'));

		$set = $Crm->OrderItemSet();

		if ($orderitem) {
			$orderItemRecord = $set->get($orderitem);
		}


		$form = $Crm->Ui()->OrderItemEditor();

		if (isset($orderItemRecord)) {
			$form->setOrderItem($orderItemRecord);
			$order = $orderItemRecord->parentorder;
		} else {
			$form->setOrder($order);
		}
		
		$orderSet = $Crm->OrderSet();
		$orderRecord = $orderSet->get($order);
		
		$Access = $Crm->Access();
		if (!$Access->editOrder($orderRecord)) {
		    throw new crm_AccessException(crm_translate('This order is not modifiable'));
		}

		$form->addFields();

		$page->addItem($form);

		return $page;
	}



	/**
	 * Save order item
	 * @param array $orderItem
	 * @return Widget_Action
	 */
	public function saveItem($orderItem = null, $order = null)
	{
		$orderSet = $this->Crm()->OrderSet();
		$orderItemSet = $this->Crm()->OrderItemSet();

		if (isset($orderItem['id'])) {
			$orderItemRecord = $orderItemSet->get($orderItem['id']);
			$order = $orderItemRecord->parentorder;
		} else {
			$orderItemRecord = $orderItemSet->newRecord();
			$orderItemRecord->parentorder = $order;
		}

		if (!$order) {
			trigger_error('missing order');
			return;
		}

		if (isset($orderItem['catalogitemreference'])) {
			if (!$orderItem['catalogitem'] && $orderItem['catalogitemreference']) {
				$this->Crm()->includeArticleSet();
				$ciSet = $this->Crm()->CatalogItemSet();
				$ciSet->join('article');

				$catalogitem = $ciSet->get($ciSet->article->reference->like($orderItem['catalogitemreference']));

				if ($catalogitem) {
					$orderItem['catalogitem'] = $catalogitem->id;
				}
			}


			$orderItem['reference'] = $orderItem['catalogitemreference'];
		}
		
		$orderRecord = $orderSet->get($order);
		
		$Access = $Crm->Access();
		if (!$Access->editOrder($orderRecord)) {
		    throw new crm_AccessException(crm_translate('This order is not modifiable'));
		}


		$orderItemRecord->setValues($orderItem);
		$orderItemRecord->save();

		$orderRecord->refreshTotal();
		$orderRecord->save();

		crm_redirect(crm_BreadCrumbs::last());
	}





	/**
	 * Delete order item and refresh total
	 *
	 * @param	int		$orderItem
	 * @return 	Widget_Action
	 */
	public function deleteItem($orderItem = null)
	{
	    $Crm = $this->Crm();
		$orderSet = $Crm->OrderSet();


		$orderItemSet = $this->Crm()->OrderItemSet();
		$orderItemRecord = $orderItemSet->get($orderItem);
		$orderRecord = $orderSet->get($orderItemRecord->parentorder);
		
		$Access = $Crm->Access();
		if (!$Access->editOrder($orderRecord)) {
		    throw new crm_AccessException(crm_translate('This order is not modifiable'));
		}
		
		$orderItemSet->delete($orderItemSet->id->is($orderItem));

		$orderRecord->refreshTotal();
		$orderRecord->save();

		crm_redirect(crm_BreadCrumbs::last());
	}







	/**
	 * Edit or create one order tax
	 * @param 	int $ordertax
	 * @param	int	$order
	 * @return Widget_Action
	 */
	public function editTax($ordertax = null, $order = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$page = $Crm->Ui()->Page();


		$page->setTitle($Crm->translate('Edit order tax'));

		$set = $Crm->OrderTaxSet();

		if ($ordertax) {
			$orderTaxRecord = $set->get($ordertax);
			
		}


		$form = $Crm->Ui()->OrderTaxEditor();

		if (isset($orderTaxRecord)) {
			$form->setOrderTax($orderTaxRecord);
			$order = $orderTaxRecord->parentorder;
		} else {
			$form->setOrder($order);
		}

		$orderSet = $Crm->OrderSet();
		$orderRecord = $orderSet->get($order);
		

		$Access = $Crm->Access();
		if (!$Access->editOrder($orderRecord)) {
		    throw new crm_AccessException(crm_translate('This order is not modifiable'));
		}

		$form->addFields();

		$page->addItem($form);

		if (isset($orderTaxRecord)) {

			$actions = new crm_Actions();
			$actions->additem(
				$W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->deleteTax($orderTaxRecord->id))
			);

			$page->addContextItem($actions);
		}


		return $page;
	}






	/**
	 * Save order item
	 * @param array $orderItem
	 * @return Widget_Action
	 */
	public function saveTax($orderTax = null, $order = null)
	{
	    $Crm = $this->Crm();
		$orderSet = $this->Crm()->OrderSet();
		$orderTaxSet = $this->Crm()->OrderTaxSet();

		if (isset($orderTax['id'])) {
			$orderTaxRecord = $orderTaxSet->get($orderTax['id']);
			$order = $orderTaxRecord->parentorder;
		} else {
			$orderTaxRecord = $orderTaxSet->newRecord();
			$orderTaxRecord->parentorder = $order;
		}

		if (!$order) {
			trigger_error('missing order');
			return;
		}


		$orderRecord = $orderSet->get($order);

		$Access = $Crm->Access();
		if (!$Access->editOrder($orderRecord)) {
		    throw new crm_AccessException(crm_translate('This order is not modifiable'));
		}
		
		$orderTaxRecord->setValues($orderTax);
		$orderTaxRecord->save();


		

		$orderRecord->refreshTotal();
		$orderRecord->save();

		crm_redirect(crm_BreadCrumbs::last());
	}





	/**
	 * Delete order tax and refresh total
	 *
	 * @param	int		$orderTax
	 * @return 	Widget_Action
	 */
	public function deleteTax($orderTax = null)
	{
	    $Crm = $this->Crm();
		$orderSet = $Crm->OrderSet();


		$orderTaxSet = $this->Crm()->OrderTaxSet();
		$orderTaxRecord = $orderTaxSet->get($orderTax);

		if (!$orderTaxRecord)
		{
			return;
		}

		$orderRecord = $orderSet->get($orderTaxRecord->parentorder);

		$Access = $Crm->Access();
		if (!$Access->editOrder($orderRecord)) {
		    throw new crm_AccessException(crm_translate('This order is not modifiable'));
		}
		
		
		$orderTaxSet->delete($orderTaxSet->id->is($orderTax));

		$orderRecord->refreshTotal();
		$orderRecord->save();

		crm_redirect(crm_BreadCrumbs::last());
	}















	/**
	 * Display error message
	 *
	 * @param string $message
	 * @return Widget_Action
	 */
	protected function error($message)
	{
		$this->Crm()->Ui()->Page()->addError($message)->displayHtml();
	}



	/**
	 * Does nothing and returns to the previous page.
	 *
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}



	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
