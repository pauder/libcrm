<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


$Crm = crm_Crm();
$Crm->includeRecordController();

/**
 * crm_CtrlCustomSection
 */
class crm_CtrlCustomSection extends crm_RecordController
{
    /**
     * Returns an array of field names and descriptions that can be used in Full/CardFrames.
     *
     * @return string[]
     */
    public function getAvailableDisplayFields()
    {
        $recordSet = $this->getRecordSet();

        $availableFields = array();

        foreach ($recordSet->getFields() as $name => $field) {
            $availableFields[$name] = array(
                'name' => $field->getName(),
                'description' => $field->getDescription()
            );
        }

        return $availableFields;
    }


    /**
     *
     */
    public function addDisplayField($id = null, $filter = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $customSectionSet = $Crm->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($id));

        $page = $Crm->Ui()->Page();
        $page->setTitle($Crm->translate('Add field'));

        $availableFields = $this->getAvailableDisplayFields();

        $box = $W->VBoxItems();

        foreach ($availableFields as $field) {
            $fieldName =  $field['name'];
            $fieldDescription = $field['description'];
            if (substr($fieldName, 0, 1) !== '_') {
                $fieldDescription = $Crm->translate($fieldDescription);
            }
            $box->addItem(
                $W->VBoxItems(
                    $W->Link(
                        $fieldDescription,
                        $this->proxy()->saveDisplayField($customSection->id, $fieldName)
                    )->addClass('widget-strong', 'widget-close-dialog')
                    ->setAjaxAction(),
                    $W->Label($fieldName)->addClass('widget-small'),
                    $W->Hidden()->setName(array('sections', $field['name']))
                )->setSizePolicy('widget-list-element')
            );
        }

        $page->addItem($box);
        return $page;
    }


    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return boolean
     */
    public function saveDisplayField($section, $fieldName)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));
        $customSection->addField($fieldName);
        $customSection->save();

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }


    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return boolean
     */
    public function removeDisplayField($section, $fieldName)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));

        $customSection->removeField($fieldName);
        $customSection->save();

        return true;
    }




    /**
     * @param string    $view
     * @param int       $id
     * @param string    $object
     * @return crm_Page
     */
    public function edit($view, $id = null, $object = null)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $customSectionSet = $Crm->CustomSectionSet();
        if (isset($id)) {
            $record = $customSectionSet->request($id);
        } else {
            $record = $customSectionSet->newRecord();
            $record->object = $object;
        }

        $page = $Crm->Ui()->Page();

        $page->setTitle($Crm->translate('Section'));
        $page->addClass('crm-page-editor');

        $editor = new crm_Editor($Crm);
        $editor->setHiddenValue('tg', $Crm->controllerTg);
        $editor->setHiddenValue('data[view]', $view);
        if ($record->id) {
            $editor->setHiddenValue('data[id]', $record->id);
        }
        $editor->setName('data');
        $editor->addItem(
            $W->Hidden()->setName('object')
        );

        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('Name'),
                $W->LineEdit()->setName('name')
            )
        );

        $sizePolicyClassnames = array(
            'col-md-1' => '1',
            'col-md-2' => '2',
            'col-md-3' => '3',
            'col-md-4' => '4',
            'col-md-5' => '5',
            'col-md-6' => '6',
            'col-md-7' => '7',
            'col-md-8' => '8',
            'col-md-9' => '9',
            'col-md-10' => '10',
            'col-md-11' => '11',
            'col-md-12' => '12',
        );


        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('Size policy'),
                $W->Select()
                    ->addOptions($sizePolicyClassnames)
                    ->setName('sizePolicy')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('Fields layout'),
                $W->Select()
                    ->addOptions(crm_CustomSection::getFieldsLayouts())
                    ->setName('fieldsLayout')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('Foldable'),
                $foldableCheckBox = $W->CheckBox()->setName('foldable')
            )
        );
        $editor->addItem(
            $foldedWidget = $W->LabelledWidget(
                $Crm->translate('Folded'),
                $W->CheckBox()->setName('folded')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('Editable'),
                $W->CheckBox()->setName('editable')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('Class'),
                $W->LineEdit()->setName('classname')
            )
        );

        $foldableCheckBox->setAssociatedDisplayable($foldedWidget, array(true));

        $editor->setValues($record->getFormOutputValues(), array('data'));

        $editor->addButton(
            $W->SubmitButton(/*'save'*/)
                ->validate(true)
                ->setAction($this->proxy()->save())
                ->setAjaxAction()
                ->setLabel($Crm->translate('Save'))
        );

        $editor->addButton(
            $W->SubmitButton(/*'cancel'*/)
                ->addClass('widget-close-dialog')
                ->setLabel($Crm->translate('Cancel'))
        );

        $page->addItem($editor);

        return $page;
    }


    /**
     *
     * @param int $id
     * @return crm_Page
     */
    public function editVisibility($id = null)
    {
        $Crm = $this->Crm();

        $page = $Crm->Ui()->Page();

        $page->setTitle($Crm->translate('Section visibility'));
        $page->addClass('crm-page-editor');

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $object = $record->object . 'Set';

        $objectRecordSet = $Crm->$object();


        $editor = $Crm->Ui()->CriteriaEditor($objectRecordSet->all());
        $editor->setHiddenValue('data[id]', $record->id);
        $editor->setName('data');

        if (!empty($record->visibilityCriteria)) {
            $arrCriteria = json_decode($record->visibilityCriteria, true);

            $data = array();
            foreach ($arrCriteria as $fieldName => $operation) {
                $data['field'] = $fieldName;
                foreach ($operation as $condition => $value) {
                    $data['condition']['default'] = $condition;
                    $data['condition']['bool'] = $condition;
                    $data['value'] = $value;
                }
            }

            $editor->setValues($data, array('data'));
        }


        $editor->setSaveAction($this->proxy()->saveVisibility());
        $editor->isAjax = bab_isAjaxRequest();

        $page->addItem($editor);

        return $page;
    }



    /**
     * {@inheritDoc}
     * @see crm_RecordController::save()
     */
    public function save($data = null)
    {
        parent::save($data);

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }




    /**
     * @param array $data
     */
    public function saveVisibility($data = null)
    {
        $Crm = $this->Crm();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($data['id']);

        $object = $record->object . 'Set';

        $objectRecordSet = $Crm->$object();

        $field = $data['field'];

        $field = $objectRecordSet->$field;

        $condition = $data['condition'];

        if ($field instanceof ORM_BoolInterface) {
            $condition = $condition['bool'];
        } else {
            $condition = $condition['default'];
        }

        $criteria = $field->{$condition}($data['value']);

        $record->visibilityCriteria = $criteria->toJson();

        $record->save();

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }
}
