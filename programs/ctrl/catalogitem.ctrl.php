<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on catalog items
 * a catalog item is an article linked to a catalog (public view of articles)
 *
 */
class crm_CtrlCatalogItem extends crm_Controller
{
	
	/**
	 * Change display mode in list view, search results
	 * @var bool
	 */
	protected $change_display_mode = true;
	
	
	/**
	 * Change order in list view, search results
	 * @var bool
	 */
	protected $change_order = true;
	
	
	/**
	 * Number of items per page in search result or in catalog view
	 * @return int
	 */
	protected function getItemsPerPage()
	{
		static $items_per_page = null;
		
		if (null === $items_per_page)
		{
		
			$Crm = $this->Crm();
			$addonname = $Crm->getAddonName();
			
			$registry = bab_getRegistryInstance();
			$registry->changeDirectory("/$addonname/configuration/");
			
			$items_per_page = $registry->getValue('items_per_page', 12);
			
			if (empty($items_per_page))
			{
				$items_per_page = 12;
			}
		}
		
		return $items_per_page;
	}
	
	
	

	
	/**
	 * Manage the default search parmeters
	 * if no search parameters, try to set the previously used serach parameters from the previous query
	 * 
	 * @param array|null 	$search
	 * @param int			$reset_memory		remove last search memorized in session
	 * @return array|null
	 */
	protected function searchMemory($search, $reset_memory)
	{
		$Crm = $this->Crm();
		$addon = $Crm->getAddonName();
		
		if ($reset_memory && isset($_SESSION[$addon]['lastSearch']))
		{
			unset($_SESSION[$addon]['lastSearch']);
		}
		
		if (null === $search)
		{
			if (isset($_SESSION[$addon]['lastSearch']))
			{
				$search = $_SESSION[$addon]['lastSearch'];
			}
			
			
		} else {
			
			$_SESSION[$addon]['lastSearch'] = $search;
		}
		
		return $search;
	}
	
	
	/**
	 * 
	 * @param string $sort
	 */
	protected function getOrderParam($sort)
	{
		$sortcol = 'unit_cost';
		$sortmethod = 'orderAsc';
		
		if (null !== $sort)
		{
			$arr = explode('.', $sort);
			switch($arr[0])
			{
				case 'unit_cost':
				case 'name':
					$sortcol = $arr[0];
					break;
			}
			
			if (isset($arr[1]))
			{
				switch($arr[1])
				{
					case 'desc':
						$sortmethod = 'orderDesc';
						break;
				}
			}
		}
		
		return array($sortcol, $sortmethod);
	}


	/**
	 * Search results
	 * Display the list of article of one or multiple catalogs for customer purpose
	 * 
	 * 
	 * 
	 * @return Widget_Action
	 */
	public function displayList($search = null, $displayform = 1, $reset_memory = 0)
	{
		$search = $this->searchMemory($search, $reset_memory);
		
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($search), $Crm->translate('Search product'));
		bab_sitemap::setPosition($Crm->classPrefix.'Search');
		
		$page = $Ui->Page();
		
		if ($displayform)
		{
			$searchForm = $Ui->CatalogItemSearchEditor();
			$searchForm->setValues(array('search' => $search));
			
			$page->addItem($searchForm);
		}
		
		$pageNumber = isset($search['pageNumber']) ? (int) $search['pageNumber'] : 0;
		$mode = isset($search['mode']) ? $search['mode'] : 1;
		$displayCount = (!isset($search['displayCount']) || (bool) $search['displayCount']);
		
		// display results
		
		if (null !== $search)
		{
			$resultFrame = $W->Frame()->addClass('crm-catalog-result');
			
			
			$Crm->includeCatalogSet();
			$Crm->includeArticleSet();
			
			$set = $Crm->CatalogItemSet();
			$set->catalog();
			$set->article();
			$artset = $set->article;
			$artset->joinHasOneRelations();
			$artset->addUnitCostField();
			
			
			$criteria = $set->getSearchCriteria($search);
			$res = $set->select($criteria);
			$res->groupBy($set->article->id);
			
			if (!isset($search['order']))
			{
				$search['order'] = $this->getDefaultSort();
			}
			

			list($sortcol, $sortmethod) = $this->getOrderParam($search['order']);
			
			$res->$sortmethod($set->article->$sortcol);
			
			
			$searchForm->setMetadata('resultcount', $res->count());
			
			if (isset($Crm->SearchHistory)){
				if (isset($search['keywords']))
				{
					$searchHistory = $Crm->SearchHistorySet();
					$searchHistory->log($search['keywords'], $search, $res->count());
				}
			}
			
			if (0 === $res->count())
			{
				$page->addError($Crm->translate('No results found'));
				$addon = $Crm->getAddonName();
				if (isset($_SESSION[$addon]['lastSearch']))
				{
					unset($_SESSION[$addon]['lastSearch']);
				}
				return $page;
			}
			
			$resultcount = $W->HBoxItems()->setSizePolicy(Widget_SizePolicy::MAXIMUM)->addClass('crm-catalogitem-search-results');
			$page->addItem($resultcount);
			
			if ($displayCount)
			{
				if ($res->count() > 1)
				{
					$label = sprintf($Crm->translate('%d results found'), $res->count());
				} else {
					$label = sprintf($Crm->translate('%d result found'), $res->count());
				}
				$resultcount->addItem($W->Label($label)->setSizePolicy(Widget_SizePolicy::MAXIMUM));
			}
			$resultcount->addItem($W->Label($Crm->translate('Edit my search parameters'))->addClass('button'));
			
			$start = $pageNumber * $this->getItemsPerPage();
			$end = $start + $this->getItemsPerPage();
			$res->seek($start);
			
			$page->addItem($resultFrame);
			
			$resultFrame->addItem(
					$W->PageSelector()
					->setIterator($res)
					->setPageLength($this->getItemsPerPage())
					->setCurrentPage($pageNumber)
					->setPageNamePath(array('search','pageNumber'))
			);
			
			while($res->valid() && $res->key() < $end)
			{
				$catalogItem = $res->current();
				$display = $Ui->CatalogItemDisplay($catalogItem);
				$resultFrame->addItem($display->getShopResultFrame($mode));
				$res->next();
				
			}
			
				
			$resultFrame->addItem(
				$W->PageSelector()
					->setIterator($res)
					->setPageLength($this->getItemsPerPage())
					->setCurrentPage($pageNumber)
					->setPageNamePath(array('search','pageNumber'))
			);
			
			
			if ($button = $this->getDisplayListChangeModeButton($mode))
			{
				$frame = $W->Frame()->addClass(Func_Icons::ICON_LEFT_16)->addClass('crm-catalog-displaymode');
				$frame->addItem($button);
				$page->addItem($frame);
			}
			
			if ($button = $this->getDisplayListOrderButton($search))
			{
				$frame = $W->Frame()->addClass(Func_Icons::ICON_LEFT_16)->addClass('crm-catalog-order');
				$frame->addItem($button);
				$page->addItem($frame);
			}
			
		}
		
		return $page;
	}
	
	
	
	protected function getDefaultSort()
	{
	    return 'unit_cost.asc';
	}
	
	
	
	/**
	 * Display items in a category: sub-categories and articles
	 * @param	int		$catalog
	 * @param	int		$pageNumber
	 * @param	int		$catalog_displaymode				1:list | 0:card
	 * @param  	int		$catalogitem_displaymode			1:list | 0:card
	 * @param	string	$catalogitem_order					unit_cost.asc | unit_cost.desc
	 * @return Widget_Action
	 */
	public function catalog($catalog, $pageNumber = null, $catalog_displaymode = null, $catalogitem_displaymode = null, $catalogitem_order = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		
		$catalogSet = $Crm->CatalogSet();
		
		$catalogRecord = $catalogSet->get($catalog);
		/*@var $catalogRecord crm_Catalog */
		

		
		
		if (isset($catalogRecord))
		{
			if (null === $catalog_displaymode)
			{
				$catalog_displaymode = (int) $catalogRecord->catalog_displaymode;
			}
			
			if (null === $catalogitem_displaymode)
			{
				$catalogitem_displaymode = (int) $catalogRecord->catalogitem_displaymode;
			}
			
			
			crm_BreadCrumbs::setCurrentPosition($this->proxy()->catalog($catalog, $pageNumber), $catalogRecord->name);
			bab_siteMap::setPosition($Crm->classPrefix, 'Catalog'.$catalog);
			
				
			$head = bab_getInstance('babHead');
			/*@var $head babHead */
			$head->setTitle($catalogRecord->getPageTitle());
			$head->setDescription($catalogRecord->getPageDescription());
			$head->setKeywords($catalogRecord->getPageKeywords());
			
			$iterrator = $catalogSet->selectFromParent($catalog);
			
		} else {
			
			if (null === $catalog_displaymode)
			{
				$catalog_displaymode = 1;
			}
			
			if (null === $catalogitem_displaymode)
			{
				$catalogitem_displaymode = 0;
			}

			crm_BreadCrumbs::setCurrentPosition($this->proxy()->catalog(0, $pageNumber), $Crm->translate('Online shop'));
			bab_siteMap::setPosition($Crm->classPrefix, 'OnlineShop');
			
			// firstlevel catalog
			
			$iterrator = $catalogSet->selectFromParent(0);
		}
		
		
		$page = $Ui->Page();
		
		
		
		
		$flow = $W->FlowLayout()->addClass('crm-catalog-result');
		$page->addItem($flow);
		
		if (isset($catalogRecord))
		{
			$display = $Ui->CatalogDisplay($catalogRecord);
			$flow->addItem($display->getShopHeader());
		}
		
		foreach($iterrator as $subCatalogRecord)
		{
			$display = $Ui->CatalogDisplay($subCatalogRecord);
			$flow->addItem($display->getShopResultFrame($catalog_displaymode));
		}
		
		
		$flow = $W->FlowLayout()->addClass('crm-catalog-result');
		$page->addItem($flow);
		
		if (!isset($catalogitem_order))
		{
		    $catalogitem_order = $this->getDefaultSort();
		}
		
		
		list($sortcol, $sortmethod) = $this->getOrderParam($catalogitem_order);
		if (isset($catalogRecord)) {
    		$res = $catalogRecord->getVisibleArticles($sortcol, $sortmethod);
    
    		$start = (int) $pageNumber * $this->getItemsPerPage();
    		$end = $start + $this->getItemsPerPage();
    		$res->seek($start);
    
    		while($res->valid() && $res->key() < $end)
    		{
    			$catalogItem = $res->current();
    			$display = $Ui->CatalogItemDisplay($catalogItem);
    			$flow->addItem($display->getShopResultFrame($catalogitem_displaymode));
    			$res->next();
    		}
    
    		
    		$resultFrame = $W->Frame()->addClass('crm-catalog-result');
    		
    		if (isset($res))
    		{
    			$resultFrame->addItem(
    					$W->PageSelector()
    					->setIterator($res)
    					->setPageLength($this->getItemsPerPage())
    					->setCurrentPage($pageNumber)
    					->setPageNamePath(array('pageNumber'))
    					->setSizePolicy(Widget_SizePolicy::MAXIMUM)
    			);
    			
    			$page->addItem($resultFrame);
    		}
		
		
		
    		
    		if (isset($res) && $res->count() > 1)
    		{
    			// products order, display if more than one
    			$order = $this->getOrderParam($catalogitem_order);
    			
    			if ($button = $this->getCatalogOrderButton($catalog, $pageNumber, $catalog_displaymode, $catalogitem_displaymode, $order))
    			{
    				$frame = $W->Frame()->addClass(Func_Icons::ICON_LEFT_16)->addClass('crm-catalog-order');
    				$frame->addItem($button);
    				$page->addItem($frame);
    			}
    		}
		
		}
		
		return $page;
	}
	
	
	
	
	
	
	
	
	/**
	 * Display items in a category: sub-categories and articles
	 * @param	int		$searchcatalog
	 * @param	int		$pageNumber
	 * @param  	int		$catalogitem_displaymode			1:list | 0:card
	 * @param	string	$catalogitem_order					unit_cost.asc | unit_cost.desc
	 * @return Widget_Action
	 */
	public function searchcatalog($searchcatalog, $pageNumber = null, $catalogitem_displaymode = null, $catalogitem_order = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
	
		$searchcatalogSet = $Crm->SearchCatalogSet();
	
		$searchCatalogRecord = $searchcatalogSet->get($searchcatalog);
		/*@var $searchCatalogRecord crm_SearchCatalog */
	
	
	
	
		if (!isset($searchCatalogRecord))
		{
			throw new crm_AccessException($Crm->translate('This virtual folder does not exists'));
		}


		if (null === $catalogitem_displaymode)
		{
			$catalogitem_displaymode = (int) $searchCatalogRecord->catalogitem_displaymode;
		}


		crm_BreadCrumbs::setCurrentPosition($this->proxy()->searchcatalog($searchcatalog, $pageNumber), $searchCatalogRecord->name);
		bab_siteMap::setPosition($Crm->classPrefix, 'SearchCatalog'.$searchcatalog);


		$head = bab_getInstance('babHead');
		/*@var $head babHead */
		$head->setTitle($searchCatalogRecord->getPageTitle());
		$head->setDescription($searchCatalogRecord->getPageDescription());
		$head->setKeywords($searchCatalogRecord->getPageKeywords());

		
	
	
		$page = $Ui->Page();

	
		$flow = $W->FlowLayout()->addClass('crm-catalog-result');
		$page->addItem($flow);
	
		$display = $Ui->CatalogDisplay($searchCatalogRecord);
		$flow->addItem($display->getShopHeader());
		
		$flow = $W->FlowLayout()->addClass('crm-catalog-result');
		$page->addItem($flow);
	
		
		list($sortcol, $sortmethod) = $this->getOrderParam($catalogitem_order);
		$res = $searchCatalogRecord->getVisibleArticles($sortcol, $sortmethod);

		$start = (int) $pageNumber * $this->getItemsPerPage();
		$end = $start + $this->getItemsPerPage();
		$res->seek($start);

		while($res->valid() && $res->key() < $end)
		{
			$catalogItem = $res->current();
			$display = $Ui->CatalogItemDisplay($catalogItem);
			$flow->addItem($display->getShopResultFrame($catalogitem_displaymode));
			$res->next();
		}
	
	
		
	
	
		$resultFrame = $W->Frame()->addClass('crm-catalog-result');
	
		$resultFrame->addItem(
				$W->PageSelector()
				->setIterator($res)
				->setPageLength($this->getItemsPerPage())
				->setCurrentPage($pageNumber)
				->setPageNamePath(array('pageNumber'))
				->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);
	
		$page->addItem($resultFrame);
	
	
		if (!isset($catalogitem_order))
		{
			$catalogitem_order = $this->getDefaultSort();
		}
	
		$order = $this->getOrderParam($catalogitem_order);
	
		if ($button = $this->getSearchCatalogOrderButton($searchcatalog, $pageNumber, $catalogitem_displaymode, $order))
		{
			$frame = $W->Frame()->addClass(Func_Icons::ICON_LEFT_16)->addClass('crm-catalog-order');
			$frame->addItem($button);
			$page->addItem($frame);
		}
	
	
		return $page;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @param int $mode
	 * @return Widget_Item
	 */
	protected function getDisplayListChangeModeButton($mode)
	{
		if (false === $this->change_display_mode)
		{
			return null;
		}
		
		
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		switch($mode)
		{
			default:
			case 1:
				$search['mode'] = 0;
				return $W->Link($W->Icon($Crm->translate('Icon view'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $this->proxy()->displayList($search));
		
			case 0:
				$search['mode'] = 1;
				return $W->Link($W->Icon($Crm->translate('List view'), Func_Icons::ACTIONS_VIEW_LIST_TEXT), $this->proxy()->displayList($search));
		}
	}
	
	
	/**
	 * 
	 * @param string $order
	 * @return NULL|Widget_Link
	 */
	protected function getDisplayListOrderButton($search)
	{
		if (false === $this->change_order)
		{
			return null;
		}
		
		if (isset($search['order']))
		{
			$order = $search['order'];
		} else {
			$order = 'unit_cost.asc';
		}
		
		list($sortcol, $mode) = explode('.', $order);
		
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$mode = 'desc' === $mode ? 'desc' : 'asc';
		
		switch($mode)
		{
			default:
			case 'desc':
				$search['order'] = 'unit_cost.asc';
				return $W->Link($W->Icon($Crm->translate('Sort by rising prices'), Func_Icons::ACTIONS_ARROW_UP), $this->proxy()->displayList($search));
		
			case 'asc':
				$search['order'] = 'unit_cost.desc';
				return $W->Link($W->Icon($Crm->translate('Sort by decreasing prices'), Func_Icons::ACTIONS_ARROW_DOWN), $this->proxy()->displayList($search));
		}
	}
	
	
	
	/**
	 *
	 * @param array $catalogitem_order
	 * @return NULL|Widget_Link
	 */
	protected function getCatalogOrderButton($catalog, $pageNumber, $catalog_displaymode, $catalogitem_displaymode, Array $catalogitem_order)
	{
		if (false === $this->change_order)
		{
			return null;
		}
	
	
		$W = bab_Widgets();
		$Crm = $this->Crm();
	
		$catalogitem_order = 'orderDesc' === $catalogitem_order[1] ? 'desc' : 'asc';
	
		switch($catalogitem_order)
		{
			default:
			case 'desc':
				$catalogitem_order = 'unit_cost.asc';
				return $W->Link($W->Icon($Crm->translate('Sort by rising prices'), Func_Icons::ACTIONS_ARROW_UP), $this->proxy()->catalog($catalog, $pageNumber, $catalog_displaymode, $catalogitem_displaymode, $catalogitem_order));
	
			case 'asc':
				$catalogitem_order = 'unit_cost.desc';
				return $W->Link($W->Icon($Crm->translate('Sort by decreasing prices'), Func_Icons::ACTIONS_ARROW_DOWN), $this->proxy()->catalog($catalog, $pageNumber, $catalog_displaymode, $catalogitem_displaymode, $catalogitem_order));
				
		}
	}
	
	
	
	/**
	 *
	 * @param array $catalogitem_order
	 * @return NULL|Widget_Link
	 */
	protected function getSearchCatalogOrderButton($searchcatalog, $pageNumber, $catalogitem_displaymode, Array $catalogitem_order)
	{
		if (false === $this->change_order)
		{
			return null;
		}
	
	
		$W = bab_Widgets();
		$Crm = $this->Crm();
	
		$catalogitem_order = 'orderDesc' === $catalogitem_order[1] ? 'desc' : 'asc';
	
		switch($catalogitem_order)
		{
			default:
			case 'desc':
				$catalogitem_order = 'unit_cost.asc';
			return $W->Link($W->Icon($Crm->translate('Sort by rising prices'), Func_Icons::ACTIONS_ARROW_UP), $this->proxy()->searchcatalog($searchcatalog, $pageNumber, $catalogitem_displaymode, $catalogitem_order));
	
			case 'asc':
				$catalogitem_order = 'unit_cost.desc';
				return $W->Link($W->Icon($Crm->translate('Sort by decreasing prices'), Func_Icons::ACTIONS_ARROW_DOWN), $this->proxy()->searchcatalog($searchcatalog, $pageNumber, $catalogitem_displaymode, $catalogitem_order));
	
		}
	}
	
	
	
	/**
	 * get catalog item set used for display
	 * from catalog item
	 * 
	 * @return crm_CatalogItemSet
	 */
	protected function getCatalogItemSet($item)
	{
		$Crm = $this->Crm();
		
		$Crm->includeArticleSet();
		$itemSet = $Crm->CatalogItemSet();
		$itemSet->article();
		
		if (isset($itemSet->article->articletype))
		{
			$itemSet->article->articletype();
		}
		
		if (isset($itemSet->article->articleavailability))
		{
			$itemSet->article->articleavailability();
		}
		
		return $itemSet;
	}
	

	
	

	/**
	 * Display the article (catalog item)
	 * @param int $item
	 * @return Widget_Action
	 */
	public function display($item = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		$Access = $Crm->Access();
		
		$itemSet = $this->getCatalogItemSet($item);
		$catalogItemRecord = $itemSet->get($item);
		/*@var $catalogItemRecord crm_CatalogItem */

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($item), $catalogItemRecord->article->getName());
		
		if ($catalogItemRecord->article->disabled)
		{
			// si le produit est descative, on y arrive probablement depuis une vente privee
			if ($privateSell = $catalogItemRecord->article->getAccessiblePrivateSell())
			{
				bab_siteMap::setPosition($Crm->classPrefix, 'PrivateSell'.$privateSell->id);
			}
		} else {
			bab_siteMap::setPosition($Crm->classPrefix, 'Catalog'.$catalogItemRecord->catalog);
		}
		
		
		if (!$Access->viewCatalogItem($catalogItemRecord))
		{
		    $exception = new crm_AccessException($Crm->translate('This product is not available'));
		    $exception->requireCredential = false;
			throw $exception;
		}
		
		$page = $Ui->Page();
		
		$head = bab_getInstance('babHead');
		/*@var $head babHead */
		$head->setTitle($catalogItemRecord->getPageTitle());
		$head->setDescription($catalogItemRecord->getPageDescription());
		$head->setKeywords($catalogItemRecord->getPageKeywords());
		$head->setCanonicalUrl($catalogItemRecord->getRewritenUrl());
		if ($photoUrl = $catalogItemRecord->article->getPhotoUrl())
		{
			$head->setImageUrl($photoUrl);
		}
		
		

		$page->additem($Crm->Ui()->CatalogItemFullFrame($catalogItemRecord));
		
		return $page;
	}


	
	
	
	public function listPrivateSell()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		$Access = $Crm->Access();
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->listPrivateSell(), $Crm->translate('Private sell'));
		bab_siteMap::setPosition($Crm->classPrefix, 'PrivateSales');

		$page = $Ui->Page();
		
		$head = bab_getInstance('babHead');
		/*@var $head babHead */
		$head->setTitle($Crm->translate('Private sales'));
		
		
		
		$Crm->includeArticleSet();
		$itemSet = $Crm->CatalogItemSet();
		$itemSet->article();
		$itemSet->catalog();
		
		$PrivateSellSet = $Crm->PrivateSellSet();
		// $PrivateSells = $PrivateSellSet->select($PrivateSellSet->startedOn->lessThanOrEqual(date('Y-m-d'))->_AND_($PrivateSellSet->expireOn->greaterThanOrEqual(date('Y-m-d'))));
		$PrivateSells = $PrivateSellSet->select();
		
		$frame = $W->FlowLayout()->addClass('crm-catalog-result');
		
		foreach($PrivateSells as $PrivateSell){
			$frame->addItem($Ui->PrivateSellDisplay($PrivateSell)->getShopListFrame());
		}
		
		$page->addItem($frame);
		
		return $page;
	}
	
	
	public function privateSellDisplay($privatesell)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		$Access = $Crm->Access();
	
	
		$PrivateSellSet = $Crm->PrivateSellSet();
		$PrivateSell = $PrivateSellSet->get($PrivateSellSet->id->is($privatesell));
	
		if (!$PrivateSell)
		{
			throw new crm_Exception('This private sell does not exists');
		}
	
	
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->listPrivateSell(), $PrivateSell->name);
		bab_siteMap::setPosition($Crm->classPrefix, 'PrivateSell'.$privatesell);
	
		$page = $Ui->Page();
	
		$head = bab_getInstance('babHead');
		/*@var $head babHead */
		$head->setTitle($Crm->translate('Private sell'));
	
	
		$Crm->includeArticleSet();
		$itemSet = $Crm->CatalogItemSet();
		$itemSet->article();
		$itemSet->article->joinHasOneRelations();
		$itemSet->catalog();
	
		$ArticlePrivateSellSet = $Crm->ArticlePrivateSellSet();
		$articles = $ArticlePrivateSellSet->privatesell->is($privatesell);
	
		$catalogItems = $itemSet->select($itemSet->article->id->in($articles));
		$catalogItems->groupBy($itemSet->article->id);
	
		$frame = $W->FlowLayout()->addClass('crm-catalog-result');
	
		foreach($catalogItems as $catalogItem){
			$frame->addItem($Ui->CatalogItemDisplay($catalogItem)->getShopListFrame());
		}
	
	
		$page->addItem(
				$W->VBoxItems(
						$Ui->PrivateSellDisplay($PrivateSell)->getShopHeaderFrame(),
						$frame
				)
		);
	
		return $page;
	
	}
	
	
	
	
	public function saveComment($comment = null)
	{
		return true;
	}
	
	
	
	
	
	/**
	 * Display product from rewrited url
	 * @param string $title
	 */
	public function rewritearticle($title = null)
	{
		$Crm = $this->Crm();
		$set = $Crm->CatalogItemSet();
		$Crm->includeArticleSet();
		$set->article();
	
		$catalogItem = $set->get($set->article->rewritename->is($title));
	
		if (null === $catalogItem)
		{
			header("HTTP/1.0 404 Not Found");
			die('404 Not Found');
		}
	
		return $this->display($catalogItem->id);
	}
	
	
	
	
	/**
	 * List of articles in RSS format compatible with google shopping
	 * 
	 */
	public function rss()
	{
		$Crm = $this->Crm();
		$head = bab_getInstance('babHead');
		
		$pageTitle = $head->getTitle();
		if (empty($pageTitle))
		{
			$pageTitle = $GLOBALS['babSiteName'];
		}
		
		
		$set = $Crm->ArticleSet();
		$Iterator = $set->select();
		
		if (0 == $Iterator->count())
		{
			header("HTTP/1.0 404 Not Found");
			die('404 Not Found');
		}
		
		header('Content-Type: application/xml; charset=UTF-8');
		
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		echo '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">'."\n";
		echo '<channel>'."\n";
		echo '<title>'.$this->xmlOutput($pageTitle).'</title>'."\n";
		echo '<link>'.$this->xmlOutput($GLOBALS['babUrl']).'</link>'."\n";
		echo '<description>'.$this->xmlOutput($head->getDescription()).'</description>'."\n";
		
		foreach($Iterator as $article)
		{
			bab_setTimeLimit(10);
			
			/*@var $article crm_Article */
			$properties = $article->getRss();
			
			if (null === $properties)
			{
				continue;
			}
			
			echo "\t<item>\n";
			
			foreach($properties as $tag => $value)
			{
				echo "\t\t<$tag>".$this->xmlOutput($value)."</$tag>\n";
			}
				
			echo "\t</item>\n";
		}
		
		
		echo '</channel>'."\n";
		echo '</rss>'."\n";
		
		exit;
	}
	
	
	protected function xmlOutput($str)
	{
		return htmlspecialchars(bab_convertStringFromDatabase($str, 'UTF-8'), ENT_QUOTES, 'UTF-8');
	}

	
	public function getJson($keyword = null)
	{
		$Crm = $this->Crm();
		$set = $Crm->CatalogItemSet();
		$set->article();
		$set->article->vat();
		$set->catalog();
		
		$criteria = $set->getAccessibleItems()->_AND_(
			$set->article->name->like($keyword)->_OR_($set->article->reference->like($keyword))
		);


		$catalogItems = $set->select($criteria);
		
		
//		echo $catalogItems->getSelectQuery();
		
		if ($catalogItems->count() >= 1) {
			foreach ($catalogItems as $catalogItem) {
				
				$json = crm_json_encode($catalogItem->getValues());
				echo $json;
				die;
			}
		}
		
		die;
	}
	
	
	
	
	
	/**
	 * Download article attachment, restricted to the attachements subfolder
	 * @param int $catalogItem
	 * @param string $filename
	 * @return Widget_Action
	 */
	public function downloadAttachment($catalogItem = null, $filename = null)
	{
	    $Crm = $this->Crm();
	    $access = $Crm->Access();
	
	    $Crm->includeArticleSet();
	    $set = $Crm->CatalogItemSet();
	    $set->article();
	    $catalogItem = $set->get($catalogItem);
	    if (!isset($catalogItem)) {
	        return $this->Crm()->translate('The specified catalog item does not seem to exist.');
	    }
	

	    if (!$access->downloadCatalogItemAttachment($catalogItem, $filename))
	    {
	        $message = 'Access denied';
	        throw new crm_AccessException($message);
	    }
	    
	    
	    $article = $catalogItem->article;
	    
	    $attachements = $article->uploadPath();
	    $attachements->push('attachments');
	
	    
	
	    $file = bab_Widgets()->FilePicker()->getByName($attachements, $filename);
	    if (!isset($file)) {
	        return $this->Crm()->translate('The file you requested does not seem to exist.');
	    }
	    
	    $file->download();
	}

	
	

	/**
	 * Does nothing and returns to the previous page.
	 *
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::getPosition(-2));
	}

}
