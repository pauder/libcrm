<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on calendars.
 */
class crm_CtrlCalendar extends crm_Controller
{
    const TYPE_MONTH = 'month';
    const TYPE_WEEK = 'week';
    const TYPE_WORKWEEK = 'workweek';
    const TYPE_DAY = 'day';
    const TYPE_TIMELINE = 'timeline';


    /**
     * Returns a json encoded array of events
     *
     * @param string	$inputDateFormat	The format of start and end parameters.
     * @param string	$start				Start date/time
     * @param string	$end				End date/time
     *
     * @return Widget_Action
     */
    public function events($inputDateFormat = 'timestamp', $start = null, $end = null)
    {
        $Crm = $this->Crm();
        $Crm->Ui()->includeCalendar();

        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        if ($inputDateFormat === 'timestamp') {
            $startDate = BAB_DateTime::fromTimeStamp($start);
            $endDate = BAB_DateTime::fromTimeStamp($end);
        } else {
            $startDate = BAB_DateTime::fromIsoDateTime($start);
            $endDate = BAB_DateTime::fromIsoDateTime($end);
        }

        $calendarEvents = crm_getCalendarEvents($startDate, $endDate);

        $jsonEvents = array();
        foreach ($calendarEvents as $calendarEvent) {
            $jsonEvents[] = crm_Event::fromCalendarPeriod($calendarEvent);
        }

        echo crm_json_encode($jsonEvents);

        die;
    }

    /**
     * Displays the calendar.
     *
     * @param string	$type			The type of view.
     * @param string	$dateToShow		An ISO-formatted date that should be visible on this calendar.
     * @param int		$workspace		If specified, will change the current workspace.
     * @param int		$ajax			If set, the html for the calendar widget is returned as standalone.
     *
     * @return widget_Action
     */
    public function display($type = crm_CtrlCalendar::TYPE_WORKWEEK, $dateToShow = null, $ajax = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Crm->Ui()->includeBase();

        if (!isset($ajax)) {
            crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($type, $dateToShow), $Crm->translate('Calendar'));
        }

        if (!(isset($dateToShow) && is_string($dateToShow))) {
            if (!isset($_SESSION['crm']['dateToShow'])) {
                $dateToShow = BAB_DateTime::now()->getIsoDate();
            } else {
                $dateToShow = $_SESSION['crm']['dateToShow'];
            }
        }
        $_SESSION['crm']['dateToShow'] = $dateToShow;

        $Crm->Ui()->includeCalendar();
        $explorer = crm_calendarView($Crm, $type, $dateToShow);
        if (isset($ajax)) {
            $calendarWidget = Widget_Item::getById('calendar');
            $htmlCanvas = $W->HtmlCanvas();
            die($calendarWidget->display($htmlCanvas));
        }

        $page = $Crm->Ui()->Page()
            ->addItemMenu('calendar', $Crm->translate('Calendar'), $this->proxy()->display()->url())
            ->addItemMenu('timeline', $Crm->translate('Timeline'), $this->proxy()->timeline()->url())
            ->addItemMenu('dealtimelines', $Crm->translate('Deals timeline'), $this->proxy()->dealTimelines()->url())
            ->setCurrentItemMenu('calendar');

        $page->addToolbar(crm_calendarToolbar($Crm));
        $page->addItem($explorer);

        return $page;
    }



    /**
     * Sets the date to be displayed next time a calendar view is displayed.
     *
     * @param string $date		ISO formatted date.
     *
     */
    public function setDate($date = null)
    {
        if (isset($date) && is_string($date)) {
            $_SESSION['crm']['dateToShow'] = $date;
        }
        die;
    }



    /**
     * Displays all actions on a timeline.
     *
     * @return widget_Action
     */
    public function timeline($select = 'all')
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->timeline($select), $Crm->translate('Timeline'));

        $W = bab_Widgets();
        $Ui = $Crm->Ui();
        $Ui->includeContact();

        $timeline = $W->Timeline();
        $timeline->setAutoWidth(true);

        $timeline->addBand(new Widget_TimelineBand('5%', Widget_TimelineBand::INTERVAL_DAY, 45, Widget_TimelineBand::TYPE_OVERVIEW));
        $timeline->addBand(new Widget_TimelineBand('80%', Widget_TimelineBand::INTERVAL_DAY, 45, Widget_TimelineBand::TYPE_ORIGINAL));
        $timeline->addBand(new Widget_TimelineBand('15%', Widget_TimelineBand::INTERVAL_MONTH, 150, Widget_TimelineBand::TYPE_OVERVIEW));


        $now = BAB_DateTime::now();
        $nowIso = $now->getIsoDate();

        $taskStart = $now->cloneDate();
        $taskStart->add(-1, BAB_DATETIME_YEAR);

        $taskSet = $Crm->TaskSet();


        $taskSelection = $taskSet->dueDate->greaterThan($taskStart->getIsoDate());

        switch ($select) {
            case 'user':
                $taskSelection = $taskSelection->_AND_($taskSet->responsible->is($GLOBALS['BAB_SESS_USERID']));
                break;

            case 'all':
            default:
                break;
        }

        $tasks = $taskSet->select($taskSelection);

        $widgetAddonInfo = bab_getAddonInfosInstance('widgets');
        $baseIconPath = $GLOBALS['babUrl'] . $widgetAddonInfo->getTemplatePath() . 'timeline/timeline_js/images/';
        $myNormalIcon = $baseIconPath . 'dark-blue-circle.png';
        $otherNormalIcon = $baseIconPath . 'dull-blue-circle.png';
        $myOverdueIcon = $baseIconPath . 'dark-red-circle.png';
        $otherOverdueIcon = $baseIconPath . 'dull-red-circle.png';
        $myCompleteIcon = $baseIconPath . 'dark-green-circle.png';
        $otherCompleteIcon = $baseIconPath . 'dull-green-circle.png';



        foreach ($tasks as $task) {
            /* @var $task crm_Task */
            $start = BAB_DateTime::fromIsoDateTime($task->dueDate);
            $period = $timeline->createMilestone($start);
            $period->start = $task->dueDate . ' 12:00';
            $period->setTitle($task->summary);
            $period->setBubbleContent(
                $W->VBoxItems(
                    $W->Link($W->Title($task->summary, 2), $GLOBALS['babUrl'] . $Crm->Controller()->Task()->display($task->id)->url()),
                    $W->Html($Crm->translate('Responsible:') . ' <b>' . bab_toHtml(bab_getUserName($task->responsible, true)) . '</b>'),
                    $W->Html($task->description),
                    $taskActions = $W->FlowItems(
                        $W->Link($Crm->translate('Details...'), $Crm->Controller()->Task()->display($task->id))
                            ->addClass('widget-actionbutton')
                    )
                    ->setHorizontalSpacing(1, 'em')
                )->setVerticalSpacing(0.5, 'em')
            );


            if ($Access->updateTask($task)) {
                $taskActions->addItem(
                    $W->Link($Crm->translate('Edit...'), $Crm->Controller()->Task()->edit($task->id))
                        ->addClass('widget-actionbutton')
                );
            }


        	if ($task->responsible == $GLOBALS['BAB_SESS_USERID']) {
        		$period->classname = 'crm-personal-task';
        		if ($task->completion >= 100) {
        			$period->icon = $myCompleteIcon;
        		} else  {
        			if ($nowIso > $task->dueDate) {
        				$period->icon = $myOverdueIcon;
        			} else {
        				$period->icon = $myNormalIcon;
        			}
        			$period->setTitle($task->summary);
        		}
        	} else {
        		$period->classname = 'crm-other-task';
        		if ($task->completion >= 100) {
        			$period->icon = $otherCompleteIcon;
        		} else  {
        			if ($nowIso > $task->dueDate) {
        				$period->icon = $otherOverdueIcon;
        			} else {
        				$period->icon = $otherNormalIcon;
        			}
        			$period->setTitle($task->summary);
        		}
        	}
        	$timeline->addPeriod($period);
        }

        $page = $Crm->Ui()->Page()
        	->addItemMenu('calendar', $Crm->translate('Calendar'), $this->proxy()->display()->url())
        			->addItemMenu('timeline', $Crm->translate('Timeline'), $this->proxy()->timeline()->url())
        			->addItemMenu('dealtimelines', $Crm->translate('Deals timeline'), $this->proxy()->dealTimelines()->url())
        			->setCurrentItemMenu('timeline');

        $page->addItem($W->Title($Crm->translate('Timeline'), 1));
        $page->addItem(
            $W->FlowItems(
                $W->Link($Crm->translate('My tasks'), $Crm->Controller()->Calendar()->timeline('user'))
                    ->addClass('widget-actionbutton'),
                $W->Link($Crm->translate('All tasks'), $Crm->Controller()->Calendar()->timeline('all'))
                    ->addClass('widget-actionbutton')
            )->setSpacing(4, 'px')
        );
        $page->addItem($timeline);



		$page->addClass('crm-detailed-info-frame');

		return $page;
	}




	/**
	 * Displays a form to edit an existing calendar event.
	 *
	 * @param int|array $event
	 * @return Widget_Action
	 */
	public function editEvent($event = null)
	{
		$Crm = $this->Crm();
		$Crm->Ui()->includeCalendar();

		$W = bab_Widgets();

		$page = $Crm->Ui()->Page();
		if(isset($event['event']['id']) || !is_array($event)){
			$editor = crm_eventEditor($Crm, false);
		}else{
			$editor = crm_eventEditor($Crm, true);
		}

		if (!is_array($event)) {
			require_once  $GLOBALS['babInstallPath'] . '/utilit/cal.periodcollection.class.php';

			list($calendarId, $eventId) = explode(':', $event);

			$calendar = bab_getICalendars()->getEventCalendar($calendarId);

			if (!isset($calendar)) {
				die('Cannot instantiate the event calendar (' . $calendarId . ')');
			}

			$collection = new bab_CalendarEventCollection;
			$collection->setCalendar($calendar);

			$backend = $calendar->getBackend();
			$period = $backend->getPeriod($collection, $eventId);
			if (!isset($period)) {
				die('Cannot instantiate the event (' . $eventId . ')');
			}

			$start = bab_DateTime::fromICal($period->getProperty('DTSTART'));
			$end = bab_DateTime::fromICal($period->getProperty('DTEND'));

			$category = bab_getCalendarCategory($period->getProperty('CATEGORIES'));
			if (isset($category)) {
				$category = $category['id'];
			}
			$editor->setValue(array('event', 'title'), $period->getProperty('SUMMARY'));
			$editor->setValue(array('event', 'description'), $period->getProperty('DESCRIPTION'));
			$editor->setValue(array('event', 'location'), $period->getProperty('LOCATION'));
			$editor->setValue(array('event', 'startdate'), $start->getIsoDate());
			$editor->setValue(array('event', 'starttime'), $start->getIsoTime());
			$editor->setValue(array('event', 'endtime'), $end->getIsoTime());
			$editor->setValue(array('event', 'enddate'), $end->getIsoDate());
			$editor->setValue(array('event', 'category'), $category);
			$editor->setHiddenValue('event[id]', $event);
		}else{
			if(isset($event['event']['id'])){
				$editor->setHiddenValue('event[id]', $event['event']['id']);
			}
			$editor->setValues($event);
		}
		if (isset($error)) {
			$page->addError($error);
		}

		$page->addItemMenu('calendar', $Crm->translate('Calendar'), $this->proxy()->display()->url())
			  ->addItemMenu('timeline', $Crm->translate('Timeline'), $this->proxy()->timeline()->url())
			  ->addItem($W->Title($Crm->translate('Edit event'), 1))
			  ->addItem($editor);

		$page->addClass('crm-page-editor');

		$page->displayHtml();
	}



	/**
	 * Displays a form to edit a new calendar event.
	 *
	 * @param string	$date	ISO formatted date 'YYYY-MM-DD'
	 * @param string	$time	ISO formatted time 'HH:MM'
	 * @param string	$dateEnd	ISO formatted date 'YYYY-MM-DD'
	 * @param string	$timeEnd	ISO formatted time 'HH:MM'
	 * @param string	$title
	 * @param string	$location
	 * @param string	$description
	 *
	 * @return Widget_Action
	 */
	public function addEvent($date = null, $time = null, $dateEnd = null, $timeEnd = null, $title = null, $location = null, $description = null, $error = null)
	{
		$Crm = $this->Crm();
		$Crm->Ui()->includeCalendar();
		$W = bab_Widgets();

		$page = $Crm->Ui()->Page();
		$editor = crm_eventEditor($Crm, true);
		if (isset($date)) {
			$editor->setValue(array('event', 'startdate'), $date);
		}
		if (isset($dateEnd)) {
			$editor->setValue(array('event', 'enddate'), $dateEnd);
		}
		if (isset($time)) {
			$editor->setValue(array('event', 'starttime'), $time);
			list($hours, $minutes) = explode(':', $time);
			if (!isset($timeEnd)) {
				$hours += 2;
				if ($hours >= 24) {
					$hours = 23;
					$minutes = 59;
				}
				$timeEnd = sprintf('%02d:%02d', $hours, $minutes);
			}

			$editor->setValue(array('event', 'endtime'), $timeEnd);
		}
		if (isset($title)) {
			$editor->setValue(array('event', 'title'), $title);
		}
		if (isset($location)) {
			$editor->setValue(array('event', 'location'), $location);
		}
		if (isset($description)) {
			$editor->setValue(array('event', 'description'), $description);
		}
		if (isset($error)) {
			$page->addError($error);
		}

		$page->addItemMenu('calendar', $Crm->translate('Calendar'), $this->proxy()->display()->url())
			 ->addItemMenu('timeline', $Crm->translate('Timeline'), $this->proxy()->timeline()->url())
			 ->addItem($W->Title($Crm->translate('Create a new event'), 1))
			 ->addItem($editor);


		$page->addClass('crm-page-editor');




		$page->displayHtml();
	}

	/**
	 * Moves the event by the specified offset in days and/or minutes relatively to its current start.
	 *
	 * @param int	$event				The event id
	 * @param int	$offsetDays
	 * @param int	$offsetMinutes
	 * @return Widget_Action
	 */
	public function moveEvent($event = null, $offsetDays = null, $offsetMinutes = null, $offsetDuration = null, $ajax = null)
	{
		$Crm = $this->Crm();
		$Crm->Ui()->includeCalendar();

		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/calincl.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

		list($calendarId, $eventId) = explode(':', $event);

		$calendar = bab_getICalendars()->getEventCalendar($calendarId);

		if (!isset($calendar)) {
			die('Cannot instantiate the event calendar (' . $calendarId . ')');
		}

		$backend = $calendar->getBackend();

		$collection = $backend->CalendarEventCollection($calendar);

		$period = $backend->getPeriod($collection, $eventId);

		if (!isset($period)) {
			die('Cannot instantiate the event (' . $eventId . ')');
		}

		$start = bab_DateTime::fromICal($period->getProperty('DTSTART'));
		$end = bab_DateTime::fromICal($period->getProperty('DTEND'));

		if (isset($offsetDays)) {
			$start->add($offsetDays, BAB_DATETIME_DAY);
			$end->add($offsetDays, BAB_DATETIME_DAY);
		}
		if (isset($offsetMinutes)) {
			$start->add($offsetMinutes, BAB_DATETIME_MINUTE);
			$end->add($offsetMinutes, BAB_DATETIME_MINUTE);
		}
		if (isset($offsetDuration)) {
			$end->add($offsetDuration, BAB_DATETIME_MINUTE);
		}

		$period->setProperty('DTSTART', $start->getICal());
		$period->setProperty('DTEND', $end->getICal());

		$eventArgs = array(
			'calid' => $calendarId,
			'evtid' => $eventId,
			'selected_calendars' => array($calendarId),
			'title' => $period->getProperty('SUMMARY'),
			'description' => $period->getProperty('DESCRIPTION'),
			'descriptionformat' => 'html',
			'location' => $period->getProperty('LOCATION'),
			'yearbegin' => $start->getYear(),
			'monthbegin' => $start->getMonth(),
			'daybegin' => $start->getDayOfMonth(),
			'timebegin' => $start->getHour() . ':' . $start->getMinute(),
			'yearend' => $end->getYear(),
			'monthend' => $end->getMonth(),
			'dayend' => $end->getDayOfMonth(),
			'timeend' => $end->getHour() . ':' . $end->getMinute(),
			'owner' => $GLOBALS['BAB_SESS_USERID'],
			'bfree' => true
		);
		$calendarIds = array($calendarId);
		$errorMessages = null;
		$result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages, BAB_CAL_EVT_CURRENT);




		if (isset($ajax)) {
			die;
		}
		crm_redirect(crm_BreadCrumbs::last());
	}


	/**
	 * Saves the event and returns to the previous page.
	 *
	 * @param array $event
	 * @return Widget_Action
	 */
	public function saveEvent($event = null)
	{
		if (!is_array($event)) {
			crm_redirect(crm_BreadCrumbs::last());
			return;
		}
		$Crm = $this->Crm();
		$Crm->Ui()->includeCalendar();

		require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
		$Crm = $this->Crm();

		$event['startDatetime'] = $event['startdate'] . ' ' .  $event['starttime'];
		if ($event['enddate'] == '') {
			$event['enddate'] = $event['startdate'];
		}

		$event['endDatetime'] = $event['enddate'] . ' ' .  $event['endtime'];

		$dateStart = BAB_DateTime::fromIsoDateTime($event['startDatetime'].':00');
		$dateEnd = BAB_DateTime::fromIsoDateTime($event['endDatetime'].':00');

		if (BAB_DateTime::compare($dateStart, $dateEnd) == 1) {

			$event['startDatetime'] = $event['enddate'] . ' ' .  $event['endtime'];
			$event['endDatetime'] = $event['startdate'] . ' ' .  $event['starttime'];
		} else if (BAB_DateTime::compare($dateStart, $dateEnd) == 0){
			$this->editEvent($_POST, $Crm->translate('The event must include at last one minute.'));
			return;
		}
		$event['creatorId'] = $GLOBALS['BAB_SESS_USERID'];



		$calendarId = crm_getPersonalCalendarId();
		$eventArgs = array(
			'selected_calendars' => array($calendarId)
		);

		$eventArgs += array(
			'title' => $event['title'],
			'description' => $event['description'],
			'descriptionformat' => 'html',
			'location' => $event['location'],
			'yearbegin' => $dateStart->getYear(),
			'monthbegin' => $dateStart->getMonth(),
			'daybegin' => $dateStart->getDayOfMonth(),
			'timebegin' => $dateStart->getHour() . ':' . $dateStart->getMinute(),
			'yearend' => $dateEnd->getYear(),
			'monthend' => $dateEnd->getMonth(),
			'dayend' => $dateEnd->getDayOfMonth(),
			'timeend' => $dateEnd->getHour() . ':' . $dateEnd->getMinute(),
			'owner' => $GLOBALS['BAB_SESS_USERID'],
			'color' => /*$event['calendar'] === 'public' ? 'DCC7F2' : */'C7DCF2',
			'bfree' => true,
			'category' => $event['category']
		);

		$calendarIds = array($calendarId);
		$errorMessages = null;

		if (isset($event['id'])) {
			$result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages, BAB_CAL_EVT_CURRENT);
		} else {
			$result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages);
		}


		if (!$result) {
			$this->editEvent($_POST, 'Erreur : ' . $errorMessages);
			return;
		}
		crm_redirect(crm_BreadCrumbs::last());
	}



	/**
	 * Deletes the specified event.
	 *
	 * @param int	$event
	 * @return Widget_Action
	 */
	public function deleteEvent($event = null)
	{
		if (!isset($event['id'])) {
			workspace_redirect(crm_BreadCrumbs::last());
			return;
		}

		list($calendarId, $eventId) = explode(':', $event['id']);

		$calendar = bab_getICalendars()->getEventCalendar($calendarId);

		if (!isset($calendar)) {
			die('Cannot instantiate the event calendar (' . $calendarId . ')');
		}

		$backend = $calendar->getBackend();

		$collection = $backend->CalendarEventCollection($calendar);

		$period = $backend->getPeriod($collection, $eventId);

		$backend->deletePeriod($period);

		crm_redirect(crm_BreadCrumbs::last());
	}





	/**
	 * Does nothing and return to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}

}


