<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on import
 */
class crm_CtrlImport extends crm_Controller
{
	/**
	 * Display catalogs list
	 * @return Widget_Action
	 */
	public function displayList()
	{
		$Crm = $this->Crm();
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $Crm->translate('Imports'));

		return $page;
	}



	/**
	 * Display an import
	 * @return Widget_Action
	 */
	public function display($import = null)
	{
		$Crm = $this->Crm();
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($import), $Crm->translate('Catalog'));

		$page = $this->Crm()->Ui()->Page();

		return $page;
	}


	/**
	 * Add edit import
	 * @return unknown_type
	 */
	public function edit()
	{

	}


	/**
	 * Delete import
	 * @param int	$import
	 * @return unknown_type
	 */
	public function delete($import = null)
	{

		$set = $this->Crm()->ImportSet();
		$set->delete($set->id->is($import));

		crm_redirect($this->proxy()->displayList());
	}


	/**
	 * save import.
	 *
	 * @param array	$import
	 * @return Widget_Action
	 */
	public function save($import = null)
	{
		$set = $this->Crm()->ImportSet();

		$importRecord = $set->get($import['id']);
		if (!$importRecord)
		{
			$importRecord = $set->newRecord();
		}

		$importRecord->setValues($import);
		$importRecord->save();

		$filePicker = bab_Widgets()->FilePicker();

		if ($iterator = $filePicker->getTemporaryFiles('attachment')) {

			foreach($iterator as $file) {
				$importRecord->attachFile($file);
			}
			$filePicker->cleanup();
		}

		crm_redirect(crm_BreadCrumbs::last());
	}



	/**
	 * Process import
	 * @param int $import
	 * @param 1|0 $save		0 = simulate import | 1 = import data
	 * @return unknown_type
	 */
	public function process($import = null, $save = null)
	{


	}




	/**
	 * Downloads the attachement.
	 *
	 * @param int		$import		The import id
	 * @param string	$filename
	 * @return Widget_Action
	 */
	public function downloadAttachment($import = null, $inline = 1)
	{
		$Crm = $this->Crm();
		$import = $Crm->ImportSet()->get($import);
		if (!isset($import)) {
			return;
		}

		$import->getAttachment()->download($inline == 1);
	}




	/**
	 * Does nothing and return to the previous page.
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}

}