<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on user contact.
 */
class crm_CtrlMyContact extends crm_Controller
{


	/**
	 * Display user contact information
	 *
	 *
	 * @param int $contact
	 * @return Widget_Action
	 */
	public function display()
	{
	    bab_requireCredential();
	    
		$Crm = $this->Crm();
		$access = $Crm->Access();
		if (!$access->viewMyContact()) {
			throw new crm_AccessException(crm_translate('You are not logged as a customer'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display(), $Crm->translate('Personnal informations'));
		

		$W = bab_Widgets();
		$Ui = $Crm->Ui();
		$page = $Ui->Page();

		$fullFrame = $Ui->MyContactFullFrame();
		$fullFrame->addClass('crm-detailed-info-frame')->addClass('crm-shoppingprocess-frame');

		if ($fullFrame->isValid()) {
			$page->addItem($fullFrame);
		
			
			if ($access->updateMyContact()) {
			    $page->addContextItem($W->Link(crm_translate('Edit my profile'), $this->proxy()->edit()));
			}
			
			$page->addContextItem($W->Link(crm_translate('My orders'), $this->proxy()->orderList()));
			
		} else {
			$page->addError($Crm->translate('This page is not accessible'));
		}

		return $page;
	}



/**
	 * Editor contact form associated to user
	 *
	 * @param int | null $contact
	 * @return Widget_Action
	 */
	public function edit()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();

		$access = $this->Crm()->Access();
		if (!$access->updateMyContact()) {
			throw new crm_AccessException($Crm->translate('Access denied, you are not logged in as a customer'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit(), $Crm->translate('My account'));
		bab_sitemap::setPosition($Crm->classPrefix.'MyAccount');
		

		/* @var $Ui crm_Ui */
		$frame = $Crm->Ui();
		$page = $Ui->Page();

		$contact = $Crm->getMyContact();

		if (null === $contact)
		{
			throw new crm_AccessException($Crm->translate('You are not logged in as a customer'));
		}


		$frame = $W->Frame();
		$frame->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
		$frame->addClass('crm-shoppingprocess-frame');
		$frame->addItem(
			$W->Section(
				$Crm->translate('Your information'),
				$Ui->MyContactEditor($contact),
				2
			)
		);


        // gestion multi adresse desactive par defaut
        /*
		$section = $W->Section(
			$Crm->translate('Manage your addresses'),
			$W->VBoxItems(
				$W->DelayedItem($Crm->Controller()->MyContact()->addresses())
					->setPlaceHolderItem($Crm->Controller()->MyContact(false)->addresses())
					->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE)
			),
			2
		);


		$frame->addItem($section);
		*/

		$page->addItem($frame);
		
	    $page->addContextItem($W->Link(crm_translate('View my profile'), $this->proxy()->display()));
	    $page->addContextItem($W->Link(crm_translate('My orders'), $this->proxy()->orderList()));
	
		
		return $page;
	}





	/**
	 * Save contact form
	 *
	 * @param	array	$contact
	 *
	 * @return Widget_Action
	 */
	public function save($contact = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		if (!$access->updateMyContact()) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}

		$set = $Crm->ContactSet();
		$record = $Crm->getMyContact();

		if (!$record) {
			throw new crm_SaveException($Crm->translate('You are not logged in as a customer'));
		}

		if (empty($contact['lastname']))
		{
			throw new crm_SaveException($Crm->translate('The lastname is mandatory'));
		}

		if (empty($contact['firstname']))
		{
			throw new crm_SaveException($Crm->translate('The firstname is mandatory'));
		}

		if (empty($contact['email']))
		{
			throw new crm_SaveException($Crm->translate('The email is mandatory'));
		}

		if (!$Crm->loginByEmail && empty($contact['nickname']))
		{
			throw new crm_SaveException($Crm->translate('The login ID is mandatory'));
		}

		// do not user $contact in $record->setValues() for security reasons

		$filtered_contact = array();
		foreach($contact as $key => $value)
		{
			if (true === $access->updateMyContactProperty($key, $value))
			{
				$filtered_contact[$key] = $value;
			}
		}

		$record->setValues($filtered_contact);
		$record->updateUser();
		$record->save();

		if (!empty($filtered_contact['set_password']))
		{
			$record->updatePassword($filtered_contact['new_password1'], $filtered_contact['new_password2']);
		}

		if (!$this->Crm()->loginByEmail)
		{
			$record->updateNickname($filtered_contact['nickname']);
		}

		return true;
	}
	
	
	
	
	/**
	 *
	 * @param crm_Address $address
	 * @param string $label
	 * @param Widget_RadioSet $radioSet
	 */
	protected function listItemAddress($address, $label, $isDeliveryAddress = false, $isBillingAddress = false)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();
	
		$contact = $cartRecord->getContact();
	
		$country = $address->getCountry();
		$shippingamount = $cartRecord->computeShippingCost(
				$address->postalCode,
				$country ? $address->getCountry()->id : 0
		);
		$shippable = isset($shippingamount);
	
		$actionsBox = $W->VBoxItems()->setVerticalSpacing(0.5, 'em');
	
	
		if ($isBillingAddress) {
			$actionsBox->addItem(
					$W->Label($Crm->translate('Billing address'))
					->addClass('crm-strong')
			);
		}
	
		if (!$shippable) {
			$actionsBox->addItem(
					$W->Label($Crm->translate('We do not make delivery to this address'))
			);
		} else {
			if (!$isDeliveryAddress) {
				$selectLink = $W->Link(
						$Crm->translate('Set as default delivery address'),
						$Crm->Controller()->MyContact()->selectDeliveryAddress($address->id)
				);
				$selectLink->setAjaxAction($Crm->Controller()->MyContact()->selectDeliveryAddress($address->id), $selectLink);
				$actionsBox->addItem($selectLink);
			} else {
				$actionsBox->addItem(
						$W->Label($Crm->translate('Default delivery address'))
						->addClass('crm-strong')
				);
			}
		}
	
		if (!$isDeliveryAddress && !$isBillingAddress) {
			$deleteLink = $W->Link(
					$Crm->translate('Delete this address'),
					$this->proxy()->deleteDeliveryAddress($address->id, 'shipping')
			);
			$deleteLink->setAjaxAction($this->proxy()->deleteDeliveryAddress($address->id, 'shipping'), $deleteLink);
			$deleteLink->setConfirmationMessage($Crm->translate('Are you sure you want to delete this address?'));
			$actionsBox->addItem($deleteLink);
		}
	
	
		$editAddressForm = $W->Form();
		$editAddressForm->setName('address');
		$editAddressForm->setLayout(
				$W->VBoxLayout()->setVerticalSpacing(2, 'em')
		);
		$editAddressForm->addItem(
				$Ui->MandatoryAddressEditor(true, false, !$isBillingAddress)
		);
		$editAddressForm->setValues($address->getValues(), array('address'));
		$editAddressForm->setHiddenValue('address[id]', $address->id);
	
		$editAddressButton = $W->VboxItems(
				$W->Link($Crm->translate('Edit this address'))
				->addClass('widget-instant-button'),
				$editAddressForm
				->addClass('widget-instant-form')
	
		)->addClass('widget-instant-container');
	
		$editAddressForm->addItem(
				$W->SubmitButton()
				->setAjaxAction($Crm->Controller()->MyContact()->saveDeliveryAddress(), $editAddressButton)
				->setLabel($Crm->translate('Save address'))
		);
	
		$organizationLabel = null;
		if ($isBillingAddress) {
			$organizationLabel = $W->Label($contact->organizationname)->addClass('crm-strong');
		}
	
		$addressBox = $W->VBoxItems(
				$organizationLabel,
				empty($address->recipient) ? ($W->Label($label)->addClass('crm-strong')) : null,
				$Ui->AddressFrame($address),
				$editAddressButton
		)->setSizePolicy('widget-30em');
	
		$listItemAddress = $W->FlowItems(
				$addressBox,
				$actionsBox
		)
		->addClass('crm-listitem')
		->setHorizontalSpacing(4, 'px')
		->setVerticalAlign('top');
	
		if ($isBillingAddress) {
			$listItemAddress->addClass('crm-selected');
		}
	
		return $listItemAddress;
	}
	
	
	
	/**
	 * @return Widget_Layout
	 */
	public function addresses()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$access = $Crm->Access();
		$W = bab_Widgets();
		$Icon = bab_functionality::get('Icons');
		$Ui->includeAddress();
	
		$shoppingCartSet = $Crm->ShoppingCartSet();
	
		$cartRecord = $shoppingCartSet->getMyCart();
	
		$contact = $cartRecord->getContact();
		if (!$contact) {
			throw new crm_AccessException($Crm->translate('You are not logged in as a regular customer'));
		}
	
	
		$deliveryAddress = $contact->getDeliveryAddress();
	
	
		$allContactAddressesBox = $W->VBoxItems();
		$allContactAddressesBox->setVerticalSpacing(1, 'em');
	
		$typedAddresses = $contact->getAddresses();
	
		$invoiceAddress = $contact->address();
		if (!$invoiceAddress->isEmpty()) {
			$typedAddresses =  array('billingaddress' => array($invoiceAddress)) + $typedAddresses;
		}
	
		$displayedAddresses = array();
	
		foreach ($typedAddresses as $type => $addresses) {
	
			foreach ($addresses as $address) {
	
				if (isset($displayedAddresses[$address->id])) {
					continue;
				}
				$displayedAddresses[$address->id] = $address->id;
	
				if ($deliveryAddress->id == $address->id) {
					$type = 'delivery';
				} elseif ($invoiceAddress->id == $address->id) {
					$type = 'billing';
				} else {
					$type = null;
				}
	
				$isDeliveryAddress = ($deliveryAddress->id == $address->id);
				$isBillingAddress = ($invoiceAddress->id == $address->id);
	
				$addressLabel = $contact->getParentSet()->title->output($contact->title) . ' ' . $contact->getFullName();
				$allContactAddressesBox->addItem(
						$this->listItemAddress($address, $addressLabel, $isDeliveryAddress, $isBillingAddress)
				);
			}
		}
	
	
		$newDeliveryAddressForm = $W->Form();
		$newDeliveryAddressForm->setName('address');
		$newDeliveryAddressForm->setLayout(
				$W->VBoxLayout()->setVerticalSpacing(2, 'em')
		);
	
	
		$newDeliveryAddressForm->addItem(
				$Ui->MandatoryAddressEditor(true, false, true)
		);
	
		$addAddressButton = $W->VBoxItems(
				$W->Link($Crm->translate('Add an address'))
				->setSizePolicy(Func_Icons::ICON_LEFT_16)
				->addClass(Func_Icons::ACTIONS_LIST_ADD . ' icon widget-icon widget-instant-button crm-dialog-button widget-actionbutton'),
				$newDeliveryAddressForm
				->addClass('widget-instant-form')
	
		)->addClass('widget-instant-container');
	
		$newDeliveryAddressForm->addItem(
				$W->SubmitButton()
				->setAjaxAction($Crm->Controller()->MyContact()->saveDeliveryAddress(), $addAddressButton)
				->setLabel($Crm->translate('Add this address'))
		);
		$allContactAddressesBox->addItem($addAddressButton);
	
		return $allContactAddressesBox;
	}




	/**
	 * Login or create a new contact/account
	 * accessible for anonymous users
	 * 
	 * @param	int	$context	1: shoppingcart 
	 *                          2: save shopping cart
	 *                          3: my contact page
	 *
	 * @return Widget_Action
	 */
	public function loginCreate($context = null, $loginCreate = null)
	{

		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		if (null === $context && isset($_POST['context']))
		{
		    // en cas de formulaire non valide (ex captcha, mot de passe non identiques)
		    $context = $_POST['context'];
		}

		if (1 === (int) $context && $Crm->isContactLogged())
		{
			crm_redirect($Crm->Controller()->ShoppingCart()->setDeliveryAddress());
		}
		
		$Crm->ShopLog('Login / create account page', true);

		$access = $this->Crm()->Access();
		$Ui = $this->Crm()->Ui();
		
		if (1 === (int) $context)
		{
			// i am in shopping cart
			bab_siteMap::setPosition($Crm->classPrefix, 'ShoppingCart');
		} else {
			// i am in login create page
			bab_siteMap::setPosition($Crm->classPrefix, 'LoginCreate');
		}
		
		
		$page = $Ui->Page();

		$page->addClass('crm-page-editor');

		if (!$access->loginOrCreateAccount()) {
			throw new crm_AccessException($this->Crm()->translate('This page is not accessible'));
		}

		$page->addItem($frame = $W->Frame());
		
		

		
		

		$form = $Ui->MyContactLoginCreateEditor((int) $context);
		
		
		if (null === $loginCreate && isset($_POST['loginCreate']))
		{
			// en cas de formulaire non valide (ex captcha)
			$loginCreate = $_POST['loginCreate'];
		}
		
		
		$form->setValues(array('loginCreate' => (array) $loginCreate));


		// if previous page is shopping cart...
		if (1 === (int) $context)
		{
			$cart = $Crm->ShoppingCartSet()->getMyCart(false);
			if (isset($cart))
			{
				$steps = $Ui->ShoppingCartSteps($cart, 1);
				$frame->addItem($steps);
				$frame->addClass('crm-shoppingprocess-frame');
			}
		}


		$frame->addItem($form);

		return $page;
	}

	/**
	 * Login or create contact/account (login an old account)
	 * accessible for anonymous users
	 * 
	 * @param	array	$loginCreate		Login Create array informations
	 * 
	 *
	 * @return Widget_Action
	 */
	public function loginCreateValidateOld($loginCreate = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();

		if (!$access->loginOrCreateAccount() || $loginCreate['method'] !== 'old') { 
			throw new crm_AccessException($Crm->translate('This page is not accessible'));
		}

		return $this->login($loginCreate['login'], $loginCreate['password']);
	}
	
	
	/**
	 * Login or create contact/account (create a new account)
	 * accessible for anonymous users
	 * 
	 * @param	array	$loginCreate		Login Create array informations
	 * 
	 *
	 * @return Widget_Action
	 */
	public function loginCreateValidateNew($loginCreate = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();

		if (!$access->loginOrCreateAccount() || $loginCreate['method'] !== 'new') {
			throw new crm_AccessException($Crm->translate('This page is not accessible'));
		}

		return $this->createAccount($loginCreate);
	}


	/**
	 * Authenticate
	 * @param string $login
	 * @param string $password
	 */
	protected function login($login, $password)
	{
		$Crm = $this->Crm();


		if ($Crm->loginByEmail)
		{
			$Auth = bab_functionality::get('PortalAuthentication/AuthEmail');

			if (!$Auth)
			{
				$error = new crm_SaveException('Authentication configuration error, the AuthEmail addon is not installed');
				$error->redirect = false;
				throw $error;
			}

		} else {
			$Auth = bab_functionality::get('PortalAuthentication/AuthOvidentia');
		}


		if (!$Auth)
		{
			$error = new crm_SaveException($this->Crm()->translate('Authentication configuration error'));
			$error->redirect = false;
			throw $error;
		}


		/*@var $Auth Func_PortalAuthentication */

		$id_user = $Auth->authenticateUser($login, $password);


		if ($Auth->errorMessages)
		{
			foreach($Auth->errorMessages as $error)
			{
				$error = new crm_SaveException($error);
				$error->redirect = false;
				throw $error;
			}
		}


		if ($Auth->userCanLogin($id_user))
		{
			$Auth->setUserSession($id_user, 86400);
			$Crm->ShopLog(sprintf('User logged in with %s', $login));
		}

		return true;
	}



	/**
	 *
	 * @param 	array 		$loginCreate
	 * 
	 */
	protected function createAccount($loginCreate)
	{
		$Crm = $this->Crm();
		$Crm->ShopLog('Create account');

		
// 		$captcha = bab_functionality::get('Captcha');
// 		/*@var $captcha Func_Captcha */
// 		if (!$captcha->securityCodeValid($loginCreate['captcha']))
// 		{
// 			$e = new crm_SaveException($Crm->translate('The security code is invalid'));
// 			$e->redirect = false;
// 			throw $e;
// 		}
		

		if ($Crm->loginByEmail)
		{
			$email = $loginCreate['login'];
		} else {
			$email = $loginCreate['email'];
		}
		
		
		if (!preg_match("/^[a-zA-Z0-9+_\.-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$/", $email))
		{
			$e = new crm_SaveException($Crm->translate('This is a wrong email format'));
			$e->redirect = false;
			throw $e;
		}
		
			
		// verify email unicity because the bab_addUser function will warn about a duplicate on login

		$user = bab_getUserIdByEmail($email);
		if ($user > 0)
		{
			$e = new crm_SaveException($Crm->translate('A user with this email allready exists'));
			$e->redirect = false;
			throw $e;
		}
		

		// first, creation of the user

		$error = '';
		$id_user = bab_registerUser(
			$loginCreate['firstname'],
			$loginCreate['lastname'],
			$loginCreate['login'],	// login as middlename to prevent duplicate entry in ovidentia users
			$loginCreate['login'],
			$loginCreate['login'],
			$loginCreate['new_password1'],
			$loginCreate['new_password2'],
			true,
			$error
		);

		if (false === $id_user)
		{
			$e = new crm_SaveException($error);
			$e->redirect = false;
			throw $e;
		}

		// create contact

		$set = $Crm->ContactSet();
		$set->address();
		if (isset($loginCreate['add_delivery']) && $loginCreate['add_delivery'])
		{
			$set->deliveryaddress();
		} else {
			unset($loginCreate['deliveryaddress']);
		}

		$contact = $set->newRecord();
		/* @var $contact crm_Contact */
		$contact->setValues($loginCreate);
		$contact->email = $email;
		$contact->user = $id_user;

		$contact->save();
		
		$contact->addAddress($contact->address, 'billingaddress');
		
		// if a specific delivery address has been set, link in list of addresses
		if (isset($loginCreate['add_delivery']) && $loginCreate['add_delivery'])
		{
			$contact->addAddress($contact->deliveryaddress, 'deliveryaddress');
		}
		
		$Crm->ShopLog('Contact created');

		// notify

		$notify = $Crm->Notify()->myContact_createAccount($contact, $loginCreate['new_password1']);
		$notify->send();

		return $this->login($loginCreate['login'], $loginCreate['new_password1']);
	}
	
	
	

	
	
	
	/**
	 * Account creation confirmation page
	 * @param int $context		1 : if account creation is in shopping cart buying context
	 * 							2 : if account creation is in shopping cart record context
	 */
	public function accountCreated($context = 0)
	{
		$Crm = $this->Crm();
		$Ui = $this->Crm()->Ui();
		$W = bab_Widgets();
		$Access = $Crm->Access();
		
		
		$Crm->ShopLog('Account creation confirmation page');
		

		$page = $Ui->Page();
		
		$head = bab_getInstance('babHead');
		$head->setTitle($Crm->translate('Your account has been created'));
		
		$frame = $W->Frame(null, $W->VBoxItems()->setVerticalSpacing(2,'em'))
		->addClass('crm-account-created')
		->addClass('crm-dialog');


		if (1 === (int) $context)
		{
			$cart = $Crm->ShoppingCartSet()->getMyCart(false);
			if (isset($cart))
			{
				// the shopping cart exists, the user should be logged in since the account has been created
				$contact = $Crm->getMyContact();
				
				if ($contact && !$cart->contact)
				{
					// link current shopping cart to my contact
					$cart->contact = $contact->id;
					$cart->save();
				}
				
				
				$steps = $Ui->ShoppingCartSteps($cart, 1);
				$frame->addItem($steps);
				$frame->addClass('crm-shoppingprocess-frame');
				$frame->addItem($this->accountCreatedMessage($context));
				$frame->addItem($this->accountCreatedInShoppingCart($steps));
			}
			
		} else if (2 === (int) $context) {
			
			$frame->addItem($this->accountCreatedMessage($context));
			$frame->addItem($W->Link($Crm->translate('Continue'), $Crm->Controller()->ShoppingCart()->recordShoppingCart())->addClass('crm-dialog-button'));
			
		} else {
			
			$frame->addItem($this->accountCreatedMessage($context));
			$frame->addItem($W->Link($Crm->translate('Get back to homepage'), '?')->addClass('crm-dialog-button'));
		}
		
		$page->addItem($frame);
		
		return $page;
	}
	
	
	/**
	 * Get content to display if the account has been created in shopping cart context
	 * @param	crm_ShoppingCartSteps	$steps
	 * @return Widget_Displayable_Interface
	 */
	protected function accountCreatedInShoppingCart(crm_ShoppingCartSteps $steps)
	{
		return $steps->getButtonsContainer()->addItem($steps->button_setDeliveryAddress());
	}
	
	
	
	/**
	 * Message displayed after account creation, the contact is logged in
	 * Set position in sitemap here because inherited CRM can display different messages on different position in sitemap based on contact informations
	 * 
	  * @param int $context		1 : if account creation is in shopping cart buying context
	 * 							2 : if account creation is in shopping cart record context
	 * @return Widget_Displayable_Interface
	 */
	protected function accountCreatedMessage($context)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		bab_sitemap::setPosition($Crm->classPrefix.'AccountCreated');
		
		return $W->Label($Crm->translate('Thank you for your registration on our site.'));
	}



	/**
	 * List of my orders
	 * 
	 */
	public function orderList($orders = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();
		$access = $Crm->Access();
		
		$Crm->ShopLog('My orders page');

		$page = $Ui->Page();
		$page->addClass('crm-page-list');

		bab_siteMap::setPosition(''); // TODO : positionner cette page dans le plan du site
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->orderList($orders), $Crm->translate('Orders'));

		$contact = $Crm->getMyContact();

		if (null === $contact)
		{
			throw new crm_AccessException($Crm->translate('Access denied, you are not logged in as a customer'));
		}

		$table = $Ui->MyContactOrderTableView($contact);

		$filter = isset($orders['filter']) ? $orders['filter'] : array();

		$orderSet = $this->Crm()->OrderSet();
		/*@var $orderSet crm_OrderSet */
		$this->Crm()->includeShoppingCartSet();
		$orderSet->shoppingcart();

		$table->addColumn(widget_TableModelViewColumn($orderSet->name, $Crm->translate('Number')));
		$table->addColumn(widget_TableModelViewColumn($orderSet->shoppingcart->status, $Crm->translate('Status')));
		$table->addColumn(widget_TableModelViewColumn($orderSet->request_date, $Crm->translate('Date'))->setSearchable(false));
		$table->addColumn(widget_TableModelViewColumn($orderSet->total_ti, $Crm->translate('Total incl. tax'))->setSearchable(false));
		$table->addColumn(widget_TableModelViewColumn('_download_', $Crm->translate('Download'))->setSearchable(false));

		$orderSelection = $orderSet->select($table->getFilterCriteria()->_AND_($orderSet->contact->is($contact->id)));
		$orderSelection->orderDesc($orderSet->createdOn);
		$table->setDataSource($orderSelection);

		// $page->addItem($table->filterPanel($filter, 'orders'));
		
		$page->addItem($W->Frame()
		        ->addItem($table)
		        ->addClass('crm-shoppingprocess-frame')
		);
		
		$page->addContextItem($W->Link(crm_translate('View my profile'), $this->proxy()->display()));
		
		if ($access->updateMyContact()) {
		    $page->addContextItem($W->Link(crm_translate('Edit my profile'), $this->proxy()->edit()));
		}

		return $page;
	}




	/**
	 *
	 * @return crm_OrderSet
	 */
	protected function getPdfOrderSet()
	{
		$this->Crm()->includeContactSet();
		$this->Crm()->includeAddressSet();
		$set = $this->Crm()->OrderSet();
		$set->join('billingaddress');
		$set->join('deliveryaddress');
		$set->join('shippingaddress');
		$set->contact();

		return $set;
	}


	/**
	 * Download order as pdf file
	 * @param	int	$order
	 */
	public function downloadPdfOrder($order = null)
	{
		$Crm = $this->Crm();

		if (!$Crm->isContactLogged())
		{
			throw new crm_AccessException($Crm->translate('Access denied, you are not logged in as a customer'));
			return;
		}

		$Ui = $this->Crm()->Ui();
		$set = $this->getPdfOrderSet();

		$order = $set->get($set->id->is($order)->_AND_($set->contact->user->is($GLOBALS['BAB_SESS_USERID'])));

		if (!$order) {
			throw new crm_AccessException('Order not found or not associated to current contact');
			return;
		}
		
		$Crm->ShopLog(sprintf('Download PDF of order %s', $order->name));

		$pdf = $Ui->OrderPdf($order);
		$pdf->Output(sprintf('%s.pdf', $order->name));
	}


	/**
	 * Unsubscribe to the news letter
	 * You should redifined this method when you want to implement newsletter in your CRM
	 * It should be accessed when a contact click on a link in a newsletter
	 */
	public function unsubscribeNewsletter()
	{
		return $this;
	}


	/**
	 * Does nothing and returns to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}



	public function displayCartList()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();
		$Access = $Crm->Access();

		bab_siteMap::setPosition($Crm->classPrefix.'MyShoppingCarts'); 
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayCartList(), $Crm->translate('Saved shopping cart list'));
		
		bab_requireCredential();
		

		$page = $Ui->Page();
		$page->addClass('crm-page-list');
		$ShoppingCartset = $Crm->ShoppingCartSet();
		//$ShoppingCartItemset = $Crm->ShoppingCartItemSet();
		$carts = $ShoppingCartset->select(
				$ShoppingCartset->cookie->is('')
				->_AND_($ShoppingCartset->session->is('')
				->_AND_($ShoppingCartset->name->is('')->_NOT())
		)->_AND_($ShoppingCartset->user->is($GLOBALS['BAB_SESS_USERID'])));

		$access = $Crm->Access();
		if(!$access->canRecordShoppingCart())
		{
			throw new crm_AccessException($Crm->translate("You can't record your shopping cart"));
		}

		$table = $Ui->MyContactShoppingCartTableView();

		$filter = isset($orders['filter']) ? $orders['filter'] : array();

		$table->addColumn(widget_TableModelViewColumn($ShoppingCartset->name, $Crm->translate('Name')));
		$table->addColumn(widget_TableModelViewColumn($ShoppingCartset->createdOn, $Crm->translate('Date')));
		$table->addColumn(widget_TableModelViewColumn('number_product', $Crm->translate('Nombre de produit')));
		$table->addColumn(widget_TableModelViewColumn('_action_', '')
				->addClass('widget-column-thin widget-column-center'));

		$carts->orderDesc($ShoppingCartset->createdOn);
		$table->setDataSource($carts);

		// $page->addItem($table->filterPanel($filter, 'orders'));
		$page->addItem($table);

		return $page;
	}

	public function replaceCart($cart)
	{
		$Crm = $this->Crm();
		
		$Crm->ShopLog('Replace shopping cart', true);
		
		$contact = $Crm->getMyContact();
		
		if (!$contact)
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$ShoppingCartset = $Crm->ShoppingCartSet();
		$ShoppingCartItemset = $Crm->ShoppingCartItemSet();
		$cartRecord = $ShoppingCartset->get($ShoppingCartset->id->is($cart)->_AND_($ShoppingCartset->contact->is($contact->id)));

		if (!$cartRecord)
		{
			throw new crm_AccessException($Crm->translate('The shopping cart does not exists'));
		}


		$access = $Crm->Access();
		if(!$access->canRecordShoppingCart())
		{
			throw new crm_AccessException($Crm->translate("You can't record your shopping cart"));
		}

		$currentCartRecord = $ShoppingCartset->getMyCart();
		
		if (crm_ShoppingCart::EDIT !== (int) $currentCartRecord->status)
		{
			throw new crm_AccessException($Crm->translate("You can't replace your shopping cart because the is an unfinished order"));
		}
		
		
		
		$currentCartRecord->emptyCart();


		$items = $ShoppingCartItemset->select($ShoppingCartItemset->cart->is($cart));
		foreach($items as $item){
			$item->id = null;
			$item->cart = $currentCartRecord->id;
			$item->save();
		}

		crm_redirect($Crm->Controller()->ShoppingCart()->edit(), $Crm->translate('Your shopping cart has been successfuly restored.'));
	}

	public function removeCart($cart)
	{
		$Crm = $this->Crm();
		$ShoppingCartset = $Crm->ShoppingCartSet();
		$ShoppingCartItemset = $Crm->ShoppingCartItemSet();


		$access = $Crm->Access();
		if(!$access->canRecordShoppingCart())
		{
			throw new crm_AccessException($Crm->translate("You can't record your shopping cart"));
		}

		$cartRecord = $ShoppingCartset->get($ShoppingCartset->id->is($cart));
		if (!$cartRecord)
		{
			throw new crm_AccessException($Crm->translate('The shopping cart does not exists'));
		}

		$ShoppingCartset->delete($ShoppingCartset->user->is($GLOBALS['BAB_SESS_USERID'])->_AND_($ShoppingCartset->id->is($cart)));

		crm_redirect($Crm->Controller()->MyContact()->displayCartList(), $Crm->translate('Your shopping cart has been successfuly removed.'));

	}



	/**
	 * Number of items per page in search result or in catalog view
	 * @return int
	 */
	protected function getItemsPerPage()
	{
		static $items_per_page = null;

		if (null === $items_per_page)
		{

			$Crm = $this->Crm();
			$addonname = $Crm->getAddonName();

			$registry = bab_getRegistryInstance();
			$registry->changeDirectory("/$addonname/configuration/");

			$items_per_page = $registry->getValue('items_per_page', 12);

			if (empty($items_per_page))
			{
				$items_per_page = 12;
			}
		}

		return $items_per_page;
	}

	public function comments($pageNumber = 0)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();

		bab_siteMap::setPosition(''); // TODO : positionner cette page dans le plan du site
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayCartList(), $Crm->translate('Your comments'));

		$page = $Ui->Page();
		$page->addClass('crm-page-list');
		$page->setTitle($Crm->translate('Your comments'));

		$access = $Crm->Access();

		if (!$access->viewMyComments()) {
			throw new crm_AccessException($Crm->translate('You are not allowed to have comments'));
		}

		$commentSet = $Crm->CommentSet();
		$commentSet->article();
		$commentSet->article->joinHasOneRelations();
		
		$comments = $commentSet->select($commentSet->confirmed->is(true)->_AND_($commentSet->createdBy->is($GLOBALS['BAB_SESS_USERID'])));
		$comments->orderAsc($commentSet->article->id);
		$comments->orderDesc($commentSet->createdOn);

		$listComment = $W->VBoxItems();

		$start = $pageNumber * $this->getItemsPerPage();
		$end = $start + $this->getItemsPerPage();
		$comments->seek($start);
		

		$articleID = 0;
		while($comments->valid() && $comments->key() < $end)
		{
			$comment = $comments->current();
			/*@var $comment crm_Comment */

			if($comment->article->id != $articleID)
			{
				
				
				
				$catalogItem = $comment->article->getAccessibleCatalogItem(true);
				if (isset($catalogItem))
				{
					$display = $this->Crm()->Ui()->CatalogItemDisplay($catalogItem);
					$listComment->addItem($display->getShopListFrame());
				}
			}
			$articleID = $comment->article->id;

			$listComment->addItem($this->Crm()->Ui()->HistoryElement($comment, crm_HistoryElement::MODE_FULL));

			$comments->next();

		}

		if (!$articleID) {
			$page->addItem($W->Label($Crm->translate('No comments post')));
		}

		$listComment->addItem(
			$W->PageSelector()
				->setIterator($comments)
				->setPageLength($this->getItemsPerPage())
				->setCurrentPage($pageNumber)
				->setPageNamePath(array('pageNumber'))
		);

		$page->addItem($listComment);

		return $page;
	}








	/**
	 * Add new address to the contact
	 *
	 * @var array $address
	 */
	public function saveDeliveryAddress($address = null)
	{
		$Crm = $this->Crm();
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();

		$contact = $cartRecord->getContact();

		$addressSet = $Crm->AddressSet();
		if (isset($address['id'])) {
			$addressRecord = $addressSet->get($address['id']);
		} else {
			$addressRecord = $addressSet->newRecord();
		}

		if (!isset($addressRecord)) {
			die;
		}
		
		$Crm->ShopLog('Save delivery address');

		foreach ($address as &$addressField) {
			$addressField = bab_convertToDatabaseEncoding($addressField);
		}

		$addressRecord->recipient = isset($address['recipient']) ? $address['recipient'] : '';
		$addressRecord->street = $address['street'];
		$addressRecord->postalCode = $address['postalCode'];
		$addressRecord->city = $address['city'];
		$addressRecord->cityComplement = $address['cityComplement'];
		$addressRecord->country = $address['country'];
		$addressRecord->instructions = isset($address['instructions']) ? $address['instructions'] : '';
		$addressRecord->save();

		$contact->addAddress($addressRecord, 'shipping');
		
		$isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
		
		if ($isAjaxRequest)
		{
			die();
		}
		
		return true;
	}





	/**
	 * Saves the selected delivery address for the current shopping cart.
	 *
	 * @param int $address The address identifier.
	 */
	public function selectDeliveryAddress($address = null)
	{
		$Crm = $this->Crm();
		
		$Crm->ShopLog('Select a delivery address');
		
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();

		$contact = $cartRecord->getContact();
		if (!$contact) {
			die;
		}

		$addressSet = $Crm->addressSet();
		$address = $addressSet->get($address);

		if (!$address) {
			die;
		}
		
		if (isset($Crm->CollectStore)) {
		    $cartRecord->collectstore = 0;
		    $cartRecord->save();
		}

		$contactSet = $Crm->ContactSet();
		$contact = $contactSet->get($contact->id);
		

		$found = false;
		
		if ($address->id == $contact->getFkPk('address') || $address->id == $contact->getFkPk('deliveryaddress')) {
		    $found = true;
		} else {
    		foreach ($contact->getAddresses() as $type => $addresses) {
    		    // select the billing address for delivery is allowed
        		foreach ($addresses as $linkedAddress) {
        		    if ($linkedAddress->id === $address->id) {
        		        $found = true;
        		    }
        		}
        	}
		}
		
		if (!$found) {
		    throw new crm_AccessException(crm_translate('Access denied to selected address'));
		}
        
		$contact->deliveryaddress = $address->id;
		$contact->save();


		die;
	}




	/**
	 * Deletes the specified address.
	 *
	 * @param int $address The address identifier.
	 */
	public function deleteDeliveryAddress($address = null, $type = null)
	{
		$Crm = $this->Crm();
		
		$Crm->ShopLog('Delete a delivery address');
		
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();

		$contact = $cartRecord->getContact();
		if (!$contact) {
			die;
		}

		$addressSet = $Crm->addressSet();
		$address = $addressSet->get($address);

		if (!$address) {
			die;
		}

		$contactSet = $Crm->ContactSet();
		$contact = $contactSet->get($contact->id);

		$contact->removeAddress($address, $type);

		die;
	}
}
