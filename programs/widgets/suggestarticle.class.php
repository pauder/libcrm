<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');






/**
 * Constructs a crm_SuggestArticle.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestArticle
 */
function crm_SuggestArticle($id = null)
{
	return new crm_SuggestArticle($id);
}


/**
 * A crm_SuggestArticle
 */
class crm_SuggestArticle extends Widget_SuggestLineEdit implements Widget_Displayable_Interface, crm_Object_Interface
{

	private $crm = null;

	private $recordSet = null;

	/**
	 * Get Crm object
	 * @return Func_Crm
	 */
	public function Crm()
	{
		return $this->crm;
	}

	/**
	 * Forces the Func_Crm object to which this object is 'linked'.
	 *
	 * @param Func_Crm	$crm
	 * @return crm_SuggestArticle
	 */
	public function setCrm(Func_Crm $crm = null)
	{
		$this->crm = $crm;
		return $this;
	}



	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-suggestarticle';
		return $classes;
	}


	/**
	 *
	 * @return crm_OrganizationSet
	 */
	public function getRecordSet()
	{
	    if (!isset($this->recordSet)) {
	        $this->recordSet = $this->Crm()->ArticleSet();
	    }
	    return $this->recordSet;
	}


	/**
	 * Send suggestions
	 */
	public function suggest()
	{
		$set = $this->getRecordSet();

		if (false !== $keyword = $this->getSearchKeyword()) {

			$criteria = $set->name->contains($keyword)->_OR_($set->reference->startsWith($keyword));

			$articles = $set->select($criteria);
			$articles->orderAsc($set->reference);

			$i = 0;
			foreach ($articles as $article) {
				/* @var $article crm_Article */

				$i++;
				if ($i > Widget_SuggestLineEdit::MAX) {
					break;
				}


				parent::addSuggestion(
					$article->id,
					$article->getReference(),
					$article->getName()
				);
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas) {

		$this->suggest();
		return parent::display($canvas);
	}


	/**
	 * Set the value of the record name from the id of this record
	 * If the record id does not exist it does nothing.
	 *
	 * @param 	int 	$id		record id
	 * @return 	$this
	 */
	public function setIdValue($id)
	{
	    $recordSet = $this->getRecordSet();
	    $record = $recordSet->get($id);
	    if ($record) {
	        $this->setValue($record);
	    }

	    return parent::setIdValue($id);
	}



	/**
	 * Sets the value.
	 *
	 * @param mixed $value
	 * @return 	$this
	 */
	public function setValue($value)
	{
	    if ($value instanceof crm_Article) {
	        parent::setValue($value->name);
	        parent::setIdValue($value->id);
	    } else {
	        if($value == 0){
	            parent::setValue('');
	            return $this;
	        }
	        parent::setValue($value);
	    }
	    return $this;
	}
}

