<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');






/**
 * Constructs a crm_SuggestContact.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestContact
 */
function crm_SuggestContact($id = null)
{
	return new crm_SuggestContact($id);
}


/**
 * A crm_SuggestContact
 */
class crm_SuggestContact extends Widget_SuggestLineEdit implements Widget_Displayable_Interface, crm_Object_Interface
{

    private $crm = null;

    private $organizationId = null;

    protected $criteria = null;
    protected $contactOrganizationCriteria = null;

    protected $contactSet = null;
    protected $contactOrganizationSet = null;

    protected $relatedOrganizations = null;


    /**
     * Get Crm object
     * @return Func_Crm
     */
    public function Crm()
    {
        return $this->crm;
    }

    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return crm_SuggestContact
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }



    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'crm-suggestcontact';
        return $classes;
    }




    /**
     *
     * @return crm_ContactSet
     */
    public function getContactSet()
    {
        if (!isset($this->contactSet)) {
            $this->contactSet = $this->Crm()->ContactSet();
        }
        return $this->contactSet;
    }



    /**
     *
     * @return crm_ContactOrganizationSet
     */
    public function getContactOrganizationSet()
    {
        if (!isset($this->contactOrganizationSet)) {
            $this->contactOrganizationSet = $this->Crm()->ContactOrganizationSet();
            $this->contactOrganizationSet->contact();
            $this->contactOrganizationSet->organization();
        }
        return $this->contactOrganizationSet;
    }

    /**
     * Specifies criteria on organzation contact set that will be applied to suggested contacts.
     *
     * @return crm_SuggestContact
     */
    public function setContactOrganzationCriteria(ORM_Criteria $criteria = null)
    {
        $this->contactOrganizationCriteria = $criteria;
        return $this;
    }


    /**
     * Specifies criteria that will be applied to suggested contacts.
     *
     * @return crm_SuggestContact
     */
    public function setCriteria(ORM_Criteria $criteria = null)
    {
        $this->criteria = $criteria;
        return $this;
    }



    public function setOrganization($organization)
    {
        $this->organizationId = $organization;
    }


    /**
     * Set the widget used for specifying the organization
     *
     * @return 	crm_SuggestContact
     */
    public function setRelatedOrganization($orgs)
    {
        if ($orgs instanceof Widget_Displayable_Interface) {
            $fields = $this->getMetadata('fields');

            if (null === $fields) {
                $fields = array();
            }

            $fields[$this->getId().'_organization'] = $orgs->getId();
            $fields['relatedOrganizationNames'] = $orgs->getId();

            $this->setMetadata('fields', $fields);
        } elseif ($orgs instanceof crm_Organization) {
            $orgIds = array($orgs->id);
            $suggestAction = $this->getSuggestAction();
            $suggestAction->setParameter('relatedOrganizations', $orgs->id);
            $this->setSuggestAction($suggestAction);
//             $suggestUrl =  $this->getMetadata('suggesturl');
//             $suggestUrl['relatedOrganizations'] = implode(',', $orgIds);
//             $this->setMetadata('suggesturl', $suggestUrl);
            $this->relatedOrganizations = array($orgs->id, $orgs->name);
        } elseif (!empty($orgs)) {
            $orgIds = array_keys($orgs);
            $suggestUrl =  $this->getMetadata('suggesturl');
            $suggestUrl['relatedOrganizations'] = implode(',', $orgIds);
            $this->setMetadata('suggesturl', $suggestUrl);
            $this->relatedOrganizations = $orgs;
        }

        return $this;
    }


    /**
     * @return false | array
     */
    private function getRelatedOrganization()
    {
        if (isset($this->relatedOrganizations)) {
            return $this->relatedOrganizations;
        }
        $organizationName = bab_rp($this->getId().'_organization', false);

        if (false !== $organizationName) {
            // $organizationName is always received as UTF-8
            if ((bab_charset::getIso() !== bab_Charset::UTF_8)) {
                if (function_exists('iconv')) {
                    $organizationName = iconv(bab_Charset::UTF_8, bab_charset::getIso().'//TRANSLIT', $organizationName);
                } else {
                    $organizationName = mb_convert_encoding($organizationName, bab_charset::getIso(), bab_Charset::UTF_8);
                }
            }
        }

        return $organizationName;
    }



    /**
     * Send suggestions
     */
    public function suggest()
    {
        if (false !== $keyword = $this->getSearchKeyword()) {
            if (null !== $this->organizationId) {
                $set = $this->getContactOrganizationSet();

                $criteria = $set->contact->lastname->startsWith($keyword)->_OR_($set->contact->firstname->startsWith($keyword));
                $criteria = $criteria->_AND_($set->organization->id->is($this->organizationId));
                $criteria = $criteria->_AND_($set->history_to->is('0000-00-00')->_OR_($set->history_to->greaterThan(date('Y-m-d'))));
				$criteria = $criteria->_AND_($set->contact->deleted->is(false));
                $contacts = $set->select($criteria)->orderAsc($set->contact->lastname);
            } else if (false !== $organizationName = $this->getRelatedOrganization()) {
                $set = $this->getContactOrganizationSet();

                $criteria = $set->contact->lastname->startsWith($keyword)->_OR_($set->contact->firstname->startsWith($keyword));

                if (is_array($organizationName)) {
                    $criteria = $criteria->_AND_($set->organization->name->in($organizationName));
                } else {
                    $criteria = $criteria->_AND_($set->organization->name->is($organizationName));
                }
                $criteria = $criteria->_AND_($set->history_to->is('0000-00-00')->_OR_($set->history_to->greaterThan(date('Y-m-d'))));

                if (isset($this->criteria)) {
                    $criteria = $criteria->_AND_($this->criteria);
                }
				$criteria = $criteria->_AND_($set->contact->deleted->is(false));
				
                $contacts = $set->select($criteria)->orderAsc($set->contact->lastname);
            } elseif (isset($this->contactOrganizationCriteria)) {
                $set = $this->getContactOrganizationSet();
                $criteria = $set->contact->lastname->startsWith($keyword)->_OR_($set->contact->firstname->startsWith($keyword));
                $criteria = $criteria->_AND_($set->history_to->is('0000-00-00')->_OR_($set->history_to->greaterThan(date('Y-m-d'))));
                $criteria = $criteria->_AND_($this->contactOrganizationCriteria);
				$criteria = $criteria->_AND_($set->contact->deleted->is(false));
                $contacts = $set->select($criteria)->orderAsc($set->contact->lastname);
            } else {
                $set = $this->getContactSet();
                $criteria = $set->lastname->startsWith($keyword)->_OR_($set->firstname->startsWith($keyword));

                if (isset($this->criteria)) {
                    $criteria = $criteria->_AND_($this->criteria);
                }
				$criteria = $criteria->_AND_($set->deleted->is(false));
                $contacts = $set->select($criteria)->orderAsc($set->lastname);
            }


            $this->sendSuggestionsFromContacts($contacts);
        }
    }
    
    
    /**
     * 
     * @param crm_OrganizationPerson[] $contacts
     */
    public function sendSuggestionsFromContacts($contacts)
    {
        $i = 0;
        foreach ($contacts as $person) {
            /* @var $person crm_OrganizationPerson */
        
            $contact = $person->getContact();
            /* @var $contact crm_Contact */
        
            $i++;
            if ($i > Widget_SuggestLineEdit::MAX) {
                break;
            }
        
            $organization = $contact->getMainOrganization();
        
            if ($organization) {
                $orgname = $organization->name;
            } else {
                $orgname = '';
            }
        
            parent::addSuggestion(
                $contact->id,
                $contact->getFullName(),
                $orgname,
                $orgname
                );
        }
        
        parent::sendSuggestions();
    }



    /**
     * {@inheritDoc}
     * @see Widget_SuggestLineEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->suggest();
        return parent::display($canvas);
    }


    /**
     * Set the value of the contact name from the id of this contact
     * If the contact id does not exist it does nothing
     * @param 	int 	$id		contact id
     * @return 	crm_SuggestContact
     */
    public function setIdValue($id)
    {
        $contactSet = $this->getContactSet();
        $contact = $contactSet->get($id);
        if ($contact) {
            $this->setValue($contact);
        }

        return parent::setIdValue($id);
    }


    /**
     * Sets the value.
     *
     * @param mixed $value
     */
    public function setValue($value)
    {
        if ($value instanceof crm_Contact) {
            parent::setValue($value->getFullName());
            parent::setIdValue($value->id);
        } else {
            parent::setValue($value);
        }
        return $this;
    }
}




/**
 * A crm_SuggestContact limited to an organization
 */
class crm_ContactSelector extends Widget_LineEdit
{

	private $Crm = null;

	protected $criteria = null;
	protected $contactSet = null;



	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-suggestorganizationcontact';
		return $classes;
	}




	/**
	 *
	 * @return crm_ContactSet
	 */
	public function getContactSet()
	{
		if (!isset($this->contactSet)) {
			$this->contactSet = $this->Crm()->ContactSet();
			$this->contactSet->join('organization');
		}
		return $this->contactSet;
	}




	/**
	 * Send suggestions
	 */
	private function suggest() {

		if (false !== $keyword = $this->getSearchKeyword()) {

			$contactSet = $this->getContactSet();
//			$contactSet->join('organization');

			$criteria = $contactSet->lastname->startsWith($keyword);
			$contacts = $contactSet->select($criteria);

			$i = 0;
			foreach ($contacts as $contact) {
				/* @var $contact crm_Contact */

				$i++;
				if ($i > Widget_SuggestLineEdit::MAX) {
					break;
				}

				parent::addSuggestion(
					$contact->id,
					$contact->getFullName(),
					$contact->organization->name,
					$contact->organization->name
				);
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas) {

		$this->suggest();
		return parent::display($canvas);
	}

}