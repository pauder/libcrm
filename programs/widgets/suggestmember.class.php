<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');






/**
 * Constructs a crm_SuggestMember.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestMember
 */
function crm_SuggestMember($id = null)
{
    return new crm_SuggestMember($id);
}


/**
 * A crm_SuggestMember
 */
class crm_SuggestMember extends Widget_SuggestLineEdit implements Widget_Displayable_Interface, crm_Object_Interface
{

    private $crm = null;

    protected $criteria = null;
    protected $contactSet = null;
    protected $contactOrganizationSet = null;
    protected $organizationSet = null;

    protected $relatedOrganizations = null;

    /**
     * Get Crm object
     * @return Func_Crm
     */
    public function Crm()
    {
        return $this->crm;
    }

    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return self
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'crm-suggestmember';
        return $classes;
    }



    /**
     * @return crm_ContactSet
     */
    public function getContactSet()
    {
        if (!isset($this->contactSet)) {
            $this->contactSet = $this->Crm()->ContactSet();
        }
        return $this->contactSet;
    }

    /**
     * @return crm_OrganizationSet
     */
    public function getOrganizationSet()
    {
        if (!isset($this->organizationSet)) {
            $this->organizationSet = $this->Crm()->OrganizationSet();
        }
        return $this->organizationSet;
    }


    /**
     * @return crm_ContactOrganizationSet
     */
    public function getContactOrganizationSet()
    {
        if (!isset($this->organizationOrganizationSet)) {
            $this->contactOrganizationSet = $this->Crm()->ContactOrganizationSet();
            $this->contactOrganizationSet->contact();
            $this->contactOrganizationSet->organization();
        }
        return $this->contactOrganizationSet;
    }



    /**
     * Specifies criteria that will be applied to suggested contacts.
     *
     * @return crm_SuggestMember
     */
    public function setCriteria(ORM_Criteria $criteria = null)
    {
        $this->criteria = $criteria;
        return $this;
    }



    /**
     * Set the widget used for specifying the organization
     *
     * @return 	crm_SuggestMember
     */
    public function setRelatedOrganizations($orgs)
    {
        $this->relatedOrganizations = $orgs;

        return $this;
    }



    /**
     * Send suggestions
     */
    public function suggest()
    {
        $Crm = $this->Crm();
        if (false !== $keyword = $this->getSearchKeyword()) {
            $contactSet = $Crm->ContactSet();
            $contactSet->organization();

            $members = array();

            $criteria = $contactSet->any(
                $contactSet->lastname->startsWith($keyword),
                $contactSet->firstname->startsWith($keyword),
                $contactSet->firstname->concat(' ', $contactSet->lastname)->startsWith($keyword)
            );

            if (isset($this->criteria)) {
                $criteria = $criteria->_AND_($this->criteria);
            }

            $contacts = $contactSet->select($criteria);
            $contacts->orderAsc($contactSet->lastname);

            $i = 0;
            foreach ($contacts as $contact) {
                $i++;
                if ($i > Widget_SuggestLineEdit::MAX) {
                    break;
                }
                $members[$contact->getRef()] = array(
                    'value' => $contactSet->title->output($contact->title) . ' ' . $contact->getFullName(),
                    'info' => $contact->organization->name,
                    'css' => Func_Icons::OBJECTS_CONTACT
                );
            }

            $organizationSet = $Crm->OrganizationSet();
            $organizationSet->address();

            $criteria = $organizationSet->any(
                $organizationSet->name->startsWith($keyword)
            );

            if (isset($this->criteria)) {
                $criteria = $criteria->_AND_($this->criteria);
            }

            $organizations = $organizationSet->select($criteria);
            $organizations->orderAsc($organizationSet->name);

            $i = 0;
            foreach ($organizations as $organization) {
                $i++;
                if ($i > Widget_SuggestLineEdit::MAX) {
                    break;
                }
                $members[$organization->getRef()] = array(
                    'value' => $organization->name,
                    'info' => $organization->getSuggestInfos(),
                    'css' => Func_Icons::OBJECTS_ORGANIZATION
                );
            }

            bab_Sort::asort($members, 'value');


            $i = 0;
            foreach ($members as $memberRef => $member) {
                $i++;
                if ($i > Widget_SuggestLineEdit::MAX) {
                    break;
                }

                $this->addSuggestion(
                    $memberRef,
                    $member['value'],
                    $member['info'],
                    '',
                    $member['css']
                );
            }

            $this->sendSuggestions();
        }
    }


    /**
     * {@inheritDoc}
     * @see Widget_SuggestLineEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->suggest();
        return parent::display($canvas);
    }
}

