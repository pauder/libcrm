<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

//require_once dirname(__FILE__) . '/suggestclassification.class.php';

$W = bab_Widgets();
$W->includePhpClass('Widget_ContainerWidget');




/**
 * Constructs a crm_ClassificationsSelector.
 *
 * @param string		$id			The item unique id.
 * @return crm_ClassificationsSelector
 */
function crm_ClassificationsSelector($id = null)
{
	return new crm_ClassificationsSelector($id);
}


/**
 * A crm_ClassificationsSelector
 */
class crm_ClassificationsSelector extends Widget_ContainerWidget implements Widget_Displayable_Interface, crm_Object_Interface
{

	protected $suggestClassification = null;

	protected $classificationLabelsBox = null;

	protected $updateAction = null;


	/**
	 * @var Func_Widgets
	 */
	protected $widgets = null;

	private $crm = null;


	/**
	 * @param string	$id
	 * @param Func_Crm	$crm;
	 */
	public function __construct($id, Func_Crm $crm)
	{
		$this->widgets = bab_functionality::get('Widgets');
		parent::__construct($id, $this->widgets->HBoxLayout()->setVerticalAlign('middle'));

		$this->setCrm($crm);
		$this->suggestClassification = $this->Crm()->Ui()->SuggestClassification()->setMinChars(0)->setSize(20)->setName('name');

		$this->classificationLabelsBox = $this->widgets->FlowLayout()->setHorizontalSpacing(4, 'px')->setVerticalAlign('middle');

		$this->addItem($this->classificationLabelsBox);
		$this->addItem($this->suggestClassification);
	}


	/**
	 *
	 * @param Widget_Action $action
	 * @return crm_ClassificationsSelector
	 */
	public function setUpdateAction(Widget_Action $action)
	{
		$this->updateAction = $action;
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see programs/crm_Object_Interface::Crm()
	 */
	public function Crm()
	{
		return $this->crm;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/crm_Object_Interface::setCrm()
	 */
	public function setCrm(Func_Crm $crm = null)
	{
		$this->crm = $crm;
		return $this;
	}



	private function classificationLabel($classification)
	{
		$tagLabel = $this->widgets->Label($classification->name);

		$tagDeleteLink = $this->widgets->Link(
			$this->widgets->Icon('', Func_Icons::ACTIONS_EDIT_DELETE)
//			$crm->Controller()->Tag()->unlink($tag->id, $associatedObjectClass . ':' . $associatedObject->id)
		)->addClass('crm-codification-remove');

		$tagLayout = $this->widgets->FlowItems(
			$tagLabel,
			$tagDeleteLink,
			$this->widgets->Hidden()->setName($classification->id)->setValue($classification->name)
		)
		->setVerticalAlign('middle')
		->addClass('crm-codification')->addClass('icon-left-16 icon-16x16 icon-left-16');

		return $tagLayout;
	}

	/**
	 *
	 *
	 */
	public function addClassification($classification)
	{
		if (!($classification instanceof crm_Classification)) {
			$classificationSet = $this->Crm()->ClassificationSet();
			$classification = $classificationSet->get($classification);
		}

		if (isset($classification)) {
			$this->classificationLabelsBox->addItem(
				$this->classificationLabel($classification)
			);
		}
		return $this;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Item::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-codifications-selector';
		return $classes;
	}


	public function display(Widget_Canvas $canvas)
	{
		if (isset($this->updateAction)) {
			$this->setMetadata('updateUrl', $this->updateAction->url());
		}

		$html = $this->getLayout()->display($canvas);
		$html .= $canvas->metadata($this->getId(), $this->getMetadata());

		return $html;
	}

}
