<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');



/**
 * Constructs a crm_SuggestEntry.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestEntry
 */
function crm_SuggestEntry($id = null)
{
	return new crm_SuggestEntry($id);
}


/**
 * A crm_SuggestEntry
 */
class crm_SuggestEntry extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{

	private $category = null;


	/**
	 * @param string $category		The category to select from.
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($category, $id = null)
	{
		$this->category = $category;
		parent::__construct($id);
	}



	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-suggestentry';
		return $classes;
	}



	/**
	 * Get Crm object
	 * @return Func_Crm
	 */
	public function Crm()
	{
		return $this->crm;
	}

	/**
	 * Forces the Func_Crm object to which this object is 'linked'.
	 *
	 * @param Func_Crm	$crm
	 * @return crm_SuggestContact
	 */
	public function setCrm(Func_Crm $crm = null)
	{
		$this->crm = $crm;
		return $this;
	}


	/**
	 * Send suggestions
	 */
	public function suggest()
	{
		$Crm = $this->Crm();

		$notChecked = $Crm->translate('Not checked');
		$checked = '';

		if (false !== $keyword = $this->getSearchKeyword()) {

			if (isset($Crm->Entry))
			{
				$entrySet = $Crm->EntrySet();

				$entries = $entrySet->select(
					$entrySet->category->is($this->category)
					->_AND_(
						$entrySet->text->contains($keyword)
						->_OR_($entrySet->description->contains($keyword))
					)
				);
				$entries->orderAsc($entrySet->text);

				$i = 0;
				foreach ($entries as $entry) {
					/* @var $label crm_Entry */

					$i++;
					if ($i > Widget_SuggestLineEdit::MAX) {
						break;
					}


					parent::addSuggestion(
						$entry->id,
						$entry->text,
						$entry->checked ? $checked : $notChecked,
						''
					);
				}
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas) {

		$this->suggest();
		return parent::display($canvas);
	}

}