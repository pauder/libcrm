<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');




/**
 * A crm_SuggestAddressType
 */
class crm_SuggestAddressType extends crm_UiObject implements Widget_Displayable_Interface /* , Widget_SuggestLineEdit */
{
	/**
	 * @param Func_Crm $crm
	 */
	public function __construct($Crm, $id = null)
	{
		parent::__construct($Crm);
		// We simulate inheritance from Widget_VBoxLayout.
		$this->setInheritedItem(bab_Functionality::get('Widgets')->SuggestLineEdit($id));
		$this->addClass('crm-suggestposition');
	}



	/**
	 * Send suggestions
	 */
	public function suggest()
	{
		if (false !== $keyword = $this->getSearchKeyword()) {

			$entrySet = $this->Crm()->EntrySet();

			$positions = $entrySet->select(
				$entrySet->category->is(crm_Entry::ADDRESS_TYPES)
				->_AND_($entrySet->text->contains($keyword))
			);


			$i = 0;
			foreach ($positions as $positionId => $position) {
				/* @var $position crm_Position */

				if ($i++ > 100) {
					break;
				}

				$this->addSuggestion(
					$positionId,
					$position->text,
					'',
					''
				);
			}


			$this->sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas)
	{
		$this->suggest();
		return $this->item->display($canvas);
	}

}

