<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * crm_Status represents hierachical status used for deals (project).
 *
 *
 * @property ORM_StringField	$name
 * @property ORM_StringField	$reference
 * @property self				$lf
 * @property self				$lr
 * @property self				$parent
 */
class crm_StatusSet extends crm_RecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Status');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name'),
			ORM_StringField('reference')
					->setDescription('Reference to a default status : don\'t delete the status if a reference exist !)'),
			ORM_StringField('color')
					->setDescription('Color'),
			ORM_StringField('icon')
					->setDescription('Associated icon')
		);

		$this->hasOne('lf', get_class($this));
		$this->hasOne('lr',  get_class($this));
		$this->hasOne('id_parent',  get_class($this));
	}


	/**
	 *
	 * @return ORM_Criterion
	 */
	public function isChildOf($node)
	{
		if (!is_int($node)) {
			$node = $node->id;
		}
		return $this->id_parent->is($node);
	}


	/**
	 *
	 * @return ORM_Criterion
	 */
	public function isDescendantOf($node)
	{
		if (is_numeric($node)) {
			$set = new self();
			$node = $set->get($node);
		}
		return $this->lf->greaterThan($node->lf)->_AND_($this->lr->lessThan($node->lr));
	}


	/**
	 * Updates lf and lr on tree nodes.
	 *
	 * @internal
	 */
	public function update($lr, $offset = 1)
	{
		global $babDB;

		$offset *= 2;
		$operation = ($offset > 0 ? '+' : '-');
		$offset = abs($offset);
		$babDB->db_query('UPDATE ' . $this->getTableName() . ' SET lr = lr' . $operation . $offset . ' WHERE lr > ' . $lr);
		$babDB->db_query('UPDATE ' . $this->getTableName() . ' SET lf = lf' . $operation . $offset . ' WHERE lf > ' . $lr);
	}


	/**
	 * Delete records, the number of records depends on criteria
	 *
	 * @param ORM_Criteria	$oCriteria	Criteria for selecting records to delete
	 *
	 * @return boolean					True on success, False otherwise
	 */
	public function delete(ORM_Criteria $criteria = null)
	{
		$nodes = $this->select($criteria);
		foreach ($nodes as $node) {

			$lf = $node->lf;
			$lr = $node->lr;

			if ($lr - $lf > 1) {
				$subTreeCriteria = $this->lf->greaterThanOrEqual($lf)->_AND_($this->lf->lessThanOrEqual($lr));
				$subTreeNodes = $this->select($subTreeCriteria);
				parent::delete($subTreeCriteria);
				$this->update($lr, ($lr - $lf + 1) / 2);
			} else {
				parent::delete($this->id->is($node->id));
			}
		}
		return true;
	}



	/**
	 * @return ORM_Criteria
	 */
	public function isReadable()
	{
	    return $this->all();
	}

	/**
	 * @return ORM_Criteria
	 */
	public function isUpdatable()
	{
	    if ($this->Crm()->Access()->administer()) {
	        return $this->all();
	    }
	    return $this->none();
	}

	/**
	 * @return ORM_Criteria
	 */
	public function isDeletable()
	{
	    return $this->isUpdatable();
	}
}


/**
 * crm_Status represents hierachical status used for deals (project) abilities.
 *
 * @property int							$id
 * @property string							$name
 * @property self			$lf
 * @property self			$lr
 * @property self			$id_parent
 */
class crm_Status extends crm_Record
{
	/**
	 * Returns an iterator on the status's descendants.
	 *
	 * @return self[]
	 */
	public function getDescendants()
	{
		$set = $this->getParentSet();
		return $set->select($set->isDescendantOf($this))->orderAsc($set->lf);
	}


	/**
	 * Returns an array of the status's ascendants.
	 *
	 * @return self[]
	 */
	public function getAscendants()
	{
		$set = $this->getParentSet();

		$ascendants = array();
		$parentId = $this->id_parent;
		while ($parentId != 1 && $parent = $set->get($parentId)) {
			$ascendants[$parentId] = $parent;
			$parentId = $parent->id_parent;
		}
		return $ascendants;
	}


	/**
	 * Returns an iterator on the status's children.
	 *
	 * @return self[]
	 */
	public function getChildren()
	{
		$set = $this->getParentSet();
		return $set->select($set->isChildOf($this))->orderAsc($set->lf);
	}

	/**
	 *
	 *
	 * @return string[]
	 */
	public function getPathname()
	{
		$pathname = array();
		$ascendants = $this->getAscendants();
		foreach ($ascendants as $status) {
			$pathname[] = $status->name;
		}
		$pathname[] = $this->name;
		return $pathname;
	}


	/**
	 * Returns the first child of the node or null.
	 *
	 * @return self|null
	 */
	public function getFirstChild()
	{
		$set = $this->getParentSet();
		$children = $set->select($set->id_parent->is($this->id));
		$children->orderAsc($set->lf);
		foreach ($children as $child) {
			return $child;
		}
		return null;
	}

	/**
	 * Returns the last child of the node or null.
	 *
	 * @return self|null
	 */
	public function getLastChild()
	{
		$set = $this->getParentSet();
		$children = $set->select($set->id_parent->is($this->id));
		$children->orderDesc($set->lf);
		foreach ($children as $child) {
			return $child;
		}
		return null;
	}


	/**
	 * Saves the specified status as the last child.
	 *
	 * @return bool
	 */
	public function appendChild(ORM_Record $status)
	{
		$status->id_parent = $this->id;
		if ($lastChild = $this->getLastChild()) {
			$lr = $lastChild->lr;
		} else {
			$lr = $this->lf;
		}
		$this->getParentSet()->update($lr);
		$status->lf = $lr + 1;
		$status->lr = $lr + 2;

		return $status->save();
	}

}


/**
 * @return crm_Status
 */
function crm_Status($status = null)
{
	$statusSet = new crm_StatusSet();

	if (isset($status)) {
		return $statusSet->get($status);
	} else {
		return $statusSet->newRecord();
	}
	return $status;
}
