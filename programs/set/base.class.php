<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


// Initialize ORM backend.
if (!class_exists('ORM_RecordSet')) {
    bab_functionality::get('LibOrm')->initMySql();
}
$ormBackend = ORM_RecordSet::getBackend();
if (!isset($ormBackend)) {
    $ormBackend = new ORM_MySqlBackend($GLOBALS['babDB']);
    ORM_RecordSet::setBackend($ormBackend);
}







/**
 * Objects implementing crm_Object_Interface are object related to a
 * specific instance of a Func_Crm functionality.
 *
 * These objects are generally created through one of this functionality's
 * factory methods. It is possible to retrieve this instance of Func_Crm by
 * using the Crm() method.
 */
interface crm_Object_Interface
{
    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return crm_Object
     */
    public function setCrm(Func_Crm $crm = null);

    /**
     * Returns the Func_Crm object 'linked' to object.
     *
     * @return Func_Crm
     */
    public function Crm();
}



class crm_Object implements crm_Object_Interface
{

    public function __construct(Func_Crm $crm = null)
    {
        $this->setCrm($crm);
    }

    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return self
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }

    /**
     * Get CRM object to use with this SET
     *
     * @return Func_Crm
     */
    public function Crm()
    {
        if (!isset($this->crm)) {
            // If the crm object was not specified (through the setCrm() method)
            // we try to select one according to the classname prefix.
            list($prefix) = explode('_', get_class($this));
            $functionalityName = ucwords($prefix);
            $this->crm = @bab_functionality::get('Crm/' . $functionalityName);
            if (!$this->crm) {
                $this->crm = @bab_functionality::get('Crm');
            }
        }
        return $this->crm;
    }

}


bab_Widgets()->includePhpClass('Widget_Item');


/**
 * The crm_UiObject class is used to simulate multiple inheritance from
 * both a crm_Object and a Widget_Item
 *
 * Typically to simulate inheritance from a Widget_VBoxLayout for example,
 * the constructor of the class will be defined as follow:
 * <pre>
 * 	public function __construct($crm, $id = null)
 *	{
 *		parent::__construct($crm);
 *		// We simulate inheritance from Widget_VBoxLayout.
 *		$this->setInheritedItem(bab_Functionality::get('Widgets')->VBoxLayout($id));
 *	}
 * </pre>
 */
class crm_UiObject extends crm_Object implements Widget_Displayable_Interface
{
    /**
     * @var Widget_Item $item
     */
    protected $item = null;


    /**
     * @param Func_Crm $crm
     */
    public function __construct(Func_Crm $crm)
    {
        parent::__construct($crm);
    }

    /**
     * Sets the item from which we will 'inherit'.
     *
     * @param Widget_Item	$item
     * @return crm_UiObject
     */
    public function setInheritedItem($item)
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @ignore
     */
    public function __call($name, $arguments)
    {
        // We delegate all undefined methods to the $item object.
        if (isset($this->item)) {
            $returnedValue = call_user_func_array(array($this->item, $name), $arguments);
            if ($returnedValue === $this->item) {
                $returnedValue = $this;
            }
            return $returnedValue;
        } else {
            trigger_error('the method '.$name.' does not exists on '.get_class($this).' and there is no widget defined with the setInheritedItem method');
        }
    }

    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        return $this->item->display($canvas);
    }
}



/**
 * @method crm_Record   get()
 * @method crm_Record   request()
 * @method crm_Record   select()
 */
class crm_RecordSet extends ORM_RecordSet implements crm_Object_Interface
{
    /* @var $crm Func_Crm */
    protected $crm = null;

    protected $accessRights = null;

    /**
     * @var array
     */
    private $customFields = null;

    /**
     * @param Func_Crm $crm
     */
    public function __construct(Func_Crm $crm = null)
    {
        parent::__construct();
        $this->setCrm($crm);
        $this->accessRights = array();
    }

    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return crm_RecordSet
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }

    /**
     * Get CRM object to use with this SET
     *
     * @return Func_Crm
     */
    public function Crm()
    {
        if (!isset($this->crm)) {
            // If the crm object was not specified (through the setCrm() method)
            // we try to select one according to the classname prefix.
            list($prefix) = explode('_', get_class($this));
            $functionalityName = ucwords($prefix);
            $this->crm = bab_functionality::get('Crm/' . $functionalityName);
            if (!$this->crm) {
                $this->crm = bab_functionality::get('Crm');
            }
        }
        return $this->crm;
    }

    /**
     * @param string $setName
     */
    protected function extractSetPrefixAndName($setName)
    {
        return explode('_', $setName);
    }

    /**
     * Similar to the ORM_RecordSet::join() method but instanciates the
     * joined RecordSet using Crm methods.
     *
     * @see ORM_RecordSet::join()
     */
    public function join($fkFieldName)
    {
        $fkField = $this->getField($fkFieldName);
        if (!($fkField instanceof ORM_FkField)) {
            return $this;
        }
        $setName = $fkField->getForeignSetName();

        if (!$setName || 'Set' === $setName) {
            throw new Exception('The set name is missing on foreign key field '.$fkFieldName);
        }

        list($prefix, $crmSetName) = $this->extractSetPrefixAndName($setName);

        $set = $this->Crm()->$crmSetName();
        $set->setName($fkField->getName());
        $set->setDescription($fkField->getDescription());

        $this->aField[$fkFieldName] = $set;
        $set->setParentSet($this);
        return $this;
    }


    /**
     *
     * @param string $accessName
     * @param string $type
     */
    public function addAccessRight($accessName, $type = 'acl')
    {
        $this->accessRights[$accessName] = $type;
    }


    /**
     * @return array
     */
    public function getAccessRights()
    {
        return $this->accessRights;
    }



    /**
     * @return crm_CustomField[]
     */
    public function getCustomFields()
    {
        $Crm = $this->Crm();

        if (null === $this->customFields)
        {
            $this->customFields = array();

            if (isset($Crm->CustomField))
            {
                $set = $Crm->CustomFieldSet();
                $object = mb_substr(get_class($this), mb_strlen($Crm->classPrefix), -mb_strlen('Set'));
                try {
                    $res = $set->select($set->object->is($object));

                    foreach($res as $customfield)
                    {
                        $this->customFields[] = $customfield;

                        /*@var $customfield crm_CustomField */

                    }
                } catch(ORM_BackEndSelectException $e)
                {
                    // table does not exist, this error is thrown by the install program while creating the sets
                }
            }
        }

        return $this->customFields;
    }




    /**
     * @return crm_CustomField[]
     */
    public function selectCustomFields()
    {
        $Crm = $this->Crm();

        $set = $Crm->CustomFieldSet();
        $object = mb_substr(get_class($this), mb_strlen($Crm->classPrefix), -mb_strlen('Set'));

        $customFields = $set->select($set->object->is($object));

        return $customFields;
    }


    /**
     * @return self
     */
    public function addCustomFields()
    {
        $customFields = $this->selectCustomFields();
        foreach ($customFields as $customField) {
            /*@var $customField crm_CustomField */
            $description = $customField->name;
            $ormField = $customField->getORMField()
                ->setDescription($description);
            if ($ormField instanceof ORM_FkField) {
                $this->hasOne($customField->fieldname, $ormField->getForeignSetName())
                    ->setDescription($description);
            } else {
                $this->addFields($ormField);
            }
        }

        return $this;
    }


    /**
     * Similar to ORM_RecordSet::get() method  but throws a crm_NotFoundException if the
     * record is not found.
     *
     * @since 1.0.18
     *
     * @throws ORM_Exception
     * @throws crm_NotFoundException
     *
     * @param ORM_Criteria|string $mixedParam    Criteria for selecting records
     *                                           or the value for selecting record
     * @param string              $sPropertyName The name of the property on which
     *                                           the value applies. If not
     *                                           specified or null, the set's
     *                                           primary key will be used.
     *
     * @return crm_Record
     */
    public function request($mixedParam = null, $sPropertyName = null)
    {
        $record = $this->get($mixedParam, $sPropertyName);
        if (!isset($record)) {
            // This will remove the default criteria for TraceableRecords and
            // fetch even 'deleted' ones.
            $this->setDefaultCriteria(null);
            $record = $this->get($mixedParam, $sPropertyName);
            if (isset($record)) {
                throw new crm_DeletedRecordException($record, $mixedParam);
            }
            throw new crm_NotFoundException($this, $mixedParam);
        }
        return $record;
    }


    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return false;
    }


    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return ORM_Criterion
     */
    public function isReadable()
    {
        bab_debug('The query in '.get_class($this).' use the default isReadable() method: none()');
        return $this->none();
    }


    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->none();
    }

    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->none();
    }
}



/**
 *
 *
 * @method crm_RecordSet getParentSet()
 */
class crm_Record extends ORM_Record implements crm_Object_Interface
{
    /* @var $crm Func_Crm */
    protected $crm = null;




    /**
     * Returns the value of the specified field or an iterator if $sFieldName represents a 'Many relation'.
     *
     * @param string $sFieldName The name of the field or relation for which the value must be returned.
     * @param array  $args       Optional arguments.
     *
     * @return mixed The value of the field or null if the field is not a part of the record.
     */
    public function __call($sFieldName, $args)
    {
        $value = $this->oParentSet->getBackend()->getRecordValue($this, $sFieldName);
        $field = $this->oParentSet->$sFieldName;
        if (!is_null($value) && $field instanceof ORM_FkField) {

            $sClassName = $field->getForeignSetName();

            list( , $methodName) = explode('_', $sClassName);

            $Crm = $this->Crm();
            $set = $Crm->$methodName();

            $set->setName($field->getName());
            $set->setDescription($field->getDescription());

            $record = $set->get($value);
            return $record;
        }
        return $value;
    }

    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return crm_RecordSet
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }

    /**
     * Get CRM object to use with this record
     *
     * @return Func_Crm
     */
    public function Crm()
    {
        if (!isset($this->crm)) {
            // If the crm object was not specified (through the setCrm() method),
            // we set it as parent set's Crm.
            $this->setCrm($this->getParentSet()->Crm());
        }
        return $this->crm;
    }


    /**
     * Returns the base class name of a record.
     * For example xxx_Contact will return 'Contact'.
     *
     * @since 1.0.40
     * @return string
     */
    public function getClassName()
    {
        list(, $classname) = explode('_', get_class($this));
        return $classname;
    }


    /**
     * Returns the string reference corresponding to the record.
     *
     * @return string 	A reference string (e.g. Contact:12)
     */
    public function getRef()
    {
        if (!isset($this->id)) {
            throw new crm_Exception('Trying to get the reference string of a record without an id.');
        }
        $classname = $this->getClassName();
        return $classname . ':' . $this->id;
    }


    public function getController()
    {
        $Crm = $this->Crm();

        $ctrlName = $this->getClassName();
        return $Crm->Controller()->$ctrlName();
    }

    /**
     * Select associated notes.
     *
     * @return ORM_Iterator
     */
    public function selectNotes()
    {
        $noteSet = $this->Crm()->NoteSet();
        return $noteSet->selectLinkedTo($this);
    }


    /**
     * Select associated tasks.
     *
     * @return ORM_Iterator
     */
    public function selectTasks()
    {
        $taskSet = $this->Crm()->TaskSet();
        return $taskSet->selectLinkedTo($this);
    }


    /**
     * get array with associated unfinished tasks.
     *
     * @return acebtpopc_Task[]
     */
    public function getUnfinishedTasks()
    {
        $res = $this->selectTasks();
        $tasks = array();

        foreach ($res as $link) {
            $task = $link->targetId;
            /*@var $task acebtpopc_Task */
            if ($task->completion < 100) {
                $tasks[] = $task;
            }
        }

        return $tasks;
    }


    /**
     * Select associated emails.
     *
     * @return ORM_Iterator
     */
    public function selectEmails()
    {
        $emailSet = $this->Crm()->EmailSet();
        return $emailSet->selectLinkedTo($this);
    }


    /**
     * Select associated orders
     *
     * @return ORM_Iterator
     */
    public function selectOrders()
    {
        return null;
    }



    /**
     * Deletes the record with respect to referential integrity.
     *
     * Uses referential integrity as defined by hasManyRelation to delete/update
     * referenced elements.
     *
     * @see crm_RecordSet::hasMany()
     *
     * @return self
     */
    public function delete()
    {
        $Crm = $this->Crm();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Uses referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();


        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();
            $method = mb_substr($foreignSetClassName, mb_strlen($Crm->classPrefix));
            $foreignSet = $Crm->$method();

            switch ($manyRelation->getOnDeleteMethod()) {

                case ORM_ManyRelation::ON_DELETE_SET_NULL:

                    $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

                    foreach ($foreignRecords as $foreignRecord) {
                        $foreignRecord->$foreignSetFieldName = 0;
                        $foreignRecord->save();
                    }

                    break;

                case ORM_ManyRelation::ON_DELETE_CASCADE:

                    $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

                    foreach ($foreignRecords as $foreignRecord) {
                        $foreignRecord->delete();
                    }

                    break;

                case ORM_ManyRelation::ON_DELETE_NO_ACTION:
                default:
                    break;

            }
        }


        // We remove all links to and from this record.
        $linkSet = $Crm->LinkSet();

        $linkSet->delete(
            $linkSet->sourceClass->is(get_class($this))->_AND_($linkSet->sourceId->is($recordId))
            ->_OR_(
                $linkSet->targetClass->is(get_class($this))->_AND_($linkSet->targetId->is($recordId))
            )
        );


        $set->delete($set->$recordIdName->is($recordId));

        return $this;
    }



    /**
     * Reassociates all data asociated to the record to another
     * specified one.
     *
     * @param	int		$id
     *
     * @return self
     */
    public function replaceWith($id)
    {
        $Crm = $this->Crm();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Use referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();


        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();
            $method = mb_substr($foreignSetClassName, mb_strlen($Crm->classPrefix));
            $foreignSet = $Crm->$method();
            // $foreignSet = new $foreignSetClassName($Crm);
            $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

            foreach ($foreignRecords as $foreignRecord) {
                $foreignRecord->$foreignSetFieldName = $id;
                $foreignRecord->save();
            }
        }


        // We replace all links to and from this record.
        $linkSet = $Crm->LinkSet();

        $links = $linkSet->select(
            $linkSet->sourceClass->is(get_class($this))->_AND_($linkSet->sourceId->is($recordId))
        );

        foreach ($links as $link) {
            $link->sourceId = $id;
            $link->save();
        }

        $links = $linkSet->select(
            $linkSet->targetClass->is(get_class($this))->_AND_($linkSet->targetId->is($recordId))
        );

        foreach ($links as $link) {
            $link->targetId = $id;
            $link->save();
        }

        return $this;
    }


    /**
     *
     *
     * @return array
     */
    public function getRelatedRecords()
    {
        $Crm = $this->Crm();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Use referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();

        $relatedRecords = array();

        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();

            $method = mb_substr($foreignSetClassName, mb_strlen($Crm->classPrefix));
            $foreignSet = $Crm->$method();
            // $foreignSet = new $foreignSetClassName($Crm);
            $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));


            if ($foreignRecords->count() > 0) {
                $relatedRecords[$foreignSetClassName] = $foreignRecords;
            }
        }

        return $relatedRecords;
    }






    /**
     * Upload path for record attachments
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        $path = $this->Crm()->uploadPath();

        if (null === $path)
        {
            throw new Exception('Missing upload path information');
            return null;
        }

        $path->push(get_class($this));
        $path->push($this->id);

        return $path;
    }






    /**
     * import a value into a tracable record property if the value is not equal
     *
     * @param	string	$name		property name
     * @param	mixed	$value		value to set
     *
     * @return int		1 : the value has been modified | 0 : no change
     */
    protected function importProperty($name, $value)
    {
        if (((string) $this->$name) !== ((string) $value)) {
            $this->$name = $value;
            return 1;
        }

        return 0;
    }



    /**
     * import a value into a tracable record property if the value is not equal, try with multiple date format
     * this method work for date field 0000-00-00
     *
     * @param	string	$name		property name
     * @param	mixed	$value		value to set
     *
     * @return int		1 : the value has been modified | 0 : no change
     */
    protected function importDate($name, $value)
    {
        if (preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/',$value)) {
            return $this->importProperty($name, $value);
        }

        // try in DD/MM/YYYY format

        if (preg_match('/(?P<day>[0-9]+)\/(?P<month>[0-9]+)\/(?P<year>[0-9]{2,4})/',$value, $matches)) {

            $value = sprintf('%04d-%02d-%02d', (int) $matches['year'], (int) $matches['month'], (int) $matches['day']);

            return $this->importProperty($name, $value);
        }

    }




    /**
     *
     * @param crm_Organization $organization
     * @return string[]
     */
    public function getTemplateDocuments(crm_Organization $organization = null)
    {
        $Crm = $this->Crm();
        $organizationSet = $Crm->OrganizationSet();
        if (!isset($organization)) {
            $organization = $organizationSet->request(lcrm_getMainOrganization());
        }

        $templates = array();
        $path = $organization->uploadPath()
            ->push('templates')
            ->push(strtolower($this->getClassName()));

        foreach ($path as $file) {
            $templates[] = $file;
        }

        return $templates;
    }


    /**
     *
     * @return string[]
     */
    public function getViews()
    {
        $Crm = $this->Crm();

        $customSectionSet = $Crm->CustomSectionSet();
        $customSections = $customSectionSet->select($customSectionSet->object->is($this->getClassName()));
        $customSections->groupBy($customSectionSet->view);

        $views = array();
        foreach ($customSections as $customSection) {
            $views[] = $customSection->view;
        }

        return $views;
    }



    /**
     * Checks if the record is readable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isReadable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Checks if the record is updatable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isUpdatable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isUpdatable()->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * Checks if the record is deletable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isDeletable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isDeletable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Ensures that the record is readable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws crm_AccessException
     */
    public function requireReadable($message = null)
    {
        if (!$this->isReadable()) {
            $Crm = $this->Crm();
            if (!isset($message)) {
                $message = $Crm->translate('Access denied');
            }
            throw new crm_AccessException($message);
        }
    }

    /**
     * Ensures that the record is updatable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws crm_AccessException
     */
    public function requireUpdatable($message = null)
    {
        if (!$this->isUpdatable()) {
            $Crm = $this->Crm();
            if (!isset($message)) {
                $message = $Crm->translate('Access denied');
            }
            throw new crm_AccessException($message);
        }
    }

    /**
     * Ensures that the record is deletable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws crm_AccessException
     */
    public function requireDeletable($message = null)
    {
        if (!$this->isDeletable()) {
            $Crm = $this->Crm();
            if (!isset($message)) {
                $message = $Crm->translate('Access denied');
            }
            throw new crm_AccessException($message);
        }
    }
}






/**
 *
 * @property ORM_PkField		$id
 * @property ORM_IntField		$createdBy
 * @property ORM_DateTimeField	$createdOn
 * @property ORM_IntField		$modifiedBy
 * @property ORM_DateTimeField	$modifiedOn
 * @property ORM_IntField		$deletedBy
 * @property ORM_DateTimeField	$deletedOn
 * @property ORM_BoolField		$deleted
 */
class crm_TraceableRecordSet extends crm_RecordSet
{
    /**
     * @var bool
     */
    private $loggable = false;

    /**
     * @var bool
     */
    private $traceable = true;

    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_UserField('createdBy')
                    ->setDescription('Created by'),
            ORM_DateTimeField('createdOn')
                    ->setDescription('Created on'),
            ORM_UserField('modifiedBy')
                    ->setDescription('Modified by'),
            ORM_DateTimeField('modifiedOn')
                    ->setDescription('Modified on'),
            ORM_IntField('deletedBy')
                    ->setDescription('Deleted by'),
            ORM_DateTimeField('deletedOn')
                    ->setDescription('Deleted on'),
            ORM_BoolField('deleted')
                    ->setDescription('Deleted'),
            ORM_StringField('uuid')
                    ->setDescription('Universally Unique IDentifier')

        );

        // This condition will be applied whenever we select or join Records from this RecordSet.
        $this->setDefaultCriteria($this->deleted->is(false));
    }


    /**
     * Defines if the insertions/updates/deletions on the recordSet will be logged.
     *
     * @param bool $loggable
     * @return self
     */
    protected function setLoggable($loggable)
    {
        $this->loggable = $loggable;
        return $this;
    }


    /**
     * Checks if the insertions/updates/deletions on the recordSet will be logged.
     *
     * @return bool
     */
    protected function isLoggable()
    {
        return $this->loggable;
    }




    /**
     * Defines if the insertions/updates/deletions on the recordSet will be traced.
     *
     * @param bool $traceable
     * @return self
     */
    public function setTraceable($traceable)
    {
        $this->traceable = $traceable;
        return $this;
    }


    /**
     * Checks if the insertions/updates/deletions on the recordSet will be traced.
     *
     * @return bool
     */
    public function isTraceable()
    {
        return $this->traceable;
    }


    /**
     * @param crm_TraceableRecord   $record
     * @param bool                  $noTrace
     */
    protected function logSave(crm_TraceableRecord $record, $noTrace)
    {
        if (!$this->isLoggable()) {
            return;
        }
        $Crm = $this->Crm();
        $userId = bab_getUserId();
        $logSet = $Crm->LogSet();
        $log = $logSet->newRecord();
        $log->noTrace = $noTrace;
        $log->objectClass = get_class($record);
        $log->objectId = $record->id;
        $now = date('Y-m-d H:i:s');
        $log->modifiedOn = $now;
        $log->modifiedBy = $userId;
        $log->data = $logSet->serialize($record);
        $log->save();
    }


    /**
     * @param ORM_Criteria          $criteria
     * @param bool                  $noTrace
     */
    protected function logDelete(ORM_Criteria $criteria, $noTrace)
    {
        if (!$this->isLoggable()) {
            return;
        }
        $Crm = $this->Crm();
        $userId = bab_getUserId();
        $logSet = $Crm->LogSet();
        $deletedRecords = $this->select($criteria);
        foreach ($deletedRecords as $record) {
            $log = $logSet->newRecord();
            $log->noTrace = $noTrace;
            $log->objectClass = get_class($record);
            $log->objectId = $record->id;
            $now = date('Y-m-d H:i:s');
            $log->modifiedOn = $now;
            $log->modifiedBy = $userId;
            $log->data = '';
            $log->save();
        }
    }

    /**
     * Returns an iterator on records matching the specified criteria.
     * The iterator will not include records flagged as deleted unless
     * the $includeDeleted parameter is set to true.
     *
     * @param ORM_Criteria	$criteria			Criteria for selecting records.
     * @param bool			$includeDeleted		True to include delete-flagged records.
     *
     * @return ORM_Iterator				Iterator on success, null if the backend has not been set
     */
    public function select(ORM_Criteria $criteria = null, $includeDeleted = false)
    {
        if ($includeDeleted) {
            $this->setDefaultCriteria(null);
        }
        return parent::select($criteria);
    }


    /**
     * Returns the first item matching the specified criteria.
     * The item will not include records flagged as deleted unless
     * the $includeDeleted parameter is set to true.
     *
     * @param ORM_Criteria	$criteria			Criteria for selecting records.
     * @param string		$sPropertyName		The name of the property on which the value applies. If not specified or null, the set's primary key will be used.
     * @param bool			$includeDeleted		True to include delete-flagged records.
     *
     * @return ORM_Item				Iterator on success, null if the backend has not been set
     */
    /*public function get(ORM_Criteria $criteria = null, $sPropertyName = null, $includeDeleted = false)
    {
        if ($includeDeleted) {
            $this->setDefaultCriteria(null);
        }
        return parent::get($criteria, $sPropertyName);
    }*/


    /**
     * Deleted records matching the specified criteria.
     * If $definitive is false, records are not actually deleted
     * from the database but only flagged as so.
     *
     * @param ORM_Criteria	$criteria		The criteria for selecting the records to delete.
     * @param bool			$definitive		True to delete permanently the record.
     *
     * @return boolean				True on success, false otherwise
     */
    public function delete(ORM_Criteria $criteria = null, $definitive = false)
    {
        $definitive = $definitive || !$this->isTraceable();
        $this->logDelete($criteria, $definitive);
        if ($definitive) {
            return parent::delete($criteria);
        }

        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        $now = BAB_DateTime::now()->getIsoDateTime();

        $records = $this->select($criteria);


        foreach ($records as $record) {
            /* @var $record crm_TraceableRecord */
            // Could be optimized at ORM level
            $record->deleted = true;
            $record->deletedOn = $now;
            $record->deletedBy = $GLOBALS['BAB_SESS_USERID'];
            if (!parent::save($record)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Saves a record and keeps traces of the user doing it.
     *
     * @param crm_TraceableRecord	$record			The record to save.
     * @param bool					$noTrace		True to bypass the tracing of modifications.
     *
     * @return boolean				True on success, false otherwise
     */
    public function save(ORM_Record $record, $noTrace = false)
    {
        $noTrace = $noTrace || !$this->isTraceable();
        $this->logSave($record, $noTrace);
        if ($noTrace) {
            return parent::save($record);
        }

        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $now = BAB_DateTime::now()->getIsoDateTime();

        // We first check if the record already has a createdBy.
        $set = $record->getParentSet();
        $primaryKey = $set->getPrimaryKey();

        if (empty($record->{$primaryKey})) {
            $record->initValue('createdBy', bab_getUserId());
            $record->initValue('createdOn', $now);
            $record->initValue('uuid', $this->uuid());
        }

        $record->initValue('modifiedBy', bab_getUserId());
        $record->initValue('modifiedOn', $now);

        return parent::save($record);
    }




    /**
     * Generates a Universally Unique IDentifier, version 4.
     * RFC 4122 (http://www.ietf.org/rfc/rfc4122.txt)
     * @return string
     */
    private function uuid()
    {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) );
    }

    /**
     * Get record by UUID or null if the record does not exists or is deleted or if the uuid is empty
     * @param	string	$uuid
     * @return crm_TraceableRecord
     */
    public function getRecordByUuid($uuid)
    {
        if ('' === (string) $uuid) {
            return null;
        }


        $record = $this->get($this->uuid->is($uuid));

        if (!isset($record)) {
            return null;
        }

        if (!($record instanceOf crm_TraceableRecord)) {
            return null;
        }

        if ($record->deleted) {
            return null;
        }

        return $record;
    }







    /**
     * Returns an iterator of traceableRecord linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @param	crm_Record | array		$source			source can be an array of record
     * @param	string					$linkType
     *
     * @return ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();
        if (is_array($source) || ($source instanceof Iterator)) {
            return $linkSet->selectForSources($source, $this->getRecordClassName(), $linkType);
        } else {
            return $linkSet->selectForSource($source, $this->getRecordClassName(), $linkType);
        }
    }

    /**
     * Returns an iterator of traceableRecord linked to the specified target,
     * optionally filtered on the specified link type.
     *
     * @param	crm_Record | array		$target			target can be an array of record
     * @param	string					$linkType
     *
     * @return ORM_Iterator
     */
    public function selectLinkedFrom($target, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();
        if (is_array($target) || ($target instanceof Iterator)) {
            return $linkSet->selectForTargets($target, $this->getRecordClassName(), $linkType);
        } else {
            return $linkSet->selectForTarget($target, $this->getRecordClassName(), $linkType);
        }
    }


    /**
     * Returns a criteria usable to select records of this set which are source of crm_Links to the specified crm_Record.
     *
     * @param crm_Record $target
     * @param string     $linkType
     * @return ORM_Criteria
     */
    public function isSourceOf(crm_Record $target, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();

        $linkSet->hasOne('sourceId', get_class($this));

        $criteria =	$linkSet->targetClass->is(get_class($target))
            ->_AND_($linkSet->targetId->is($target->id))
            ->_AND_($linkSet->sourceClass->is($this->getRecordClassName()));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }



    /**
     * Returns a criteria usable to select records of this set which are target of crm_Links to the specified crm_Record.
     *
     * @param crm_Record $source
     * @param string     $linkType
     * @return ORM_Criteria
     */
    public function isTargetOf(crm_Record $source, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();

        $linkSet->hasOne('targetId', get_class($this));

        $criteria =	$linkSet->sourceClass->is(get_class($source))
            ->_AND_($linkSet->sourceId->is($source->id))
            ->_AND_($linkSet->targetClass->is($this->getRecordClassName()));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * Returns a criteria usable to select records of this set which are target of crm_Links from the specified crm_Records.
     *
     * @since 1.0.23
     *
     * @param crm_Record[]     $sources
     * @param string|null      $linkType
     * @return ORM_Criteria
     */
    public function isTargetOfAny($sources, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();
        $linkSet->hasOne('targetId', get_class($this));

        $sourceIdsByClasses = array();
        foreach ($sources as $source) {
            $sourceClass = get_class($source);
            if (!isset($sourceIdsByClasses[$sourceClass])) {
                $sourceIdsByClasses[$sourceClass] = array();
            }
            $sourceIdsByClasses[$sourceClass][] = $source->id;
        }

        $sourcesCriteria = array();
        foreach ($sourceIdsByClasses as $sourceClass => $sourceIds) {
            $sourcesCriteria[] = $linkSet->sourceClass->is($sourceClass)->_AND_($linkSet->sourceId->in($sourceIds));
        }

        $criteria =	$linkSet->all(
            $linkSet->targetClass->is($this->getRecordClassName()),
            $linkSet->any($sourcesCriteria)
        );
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * Returns a criteria usable to select records of this set which are source of crm_Links to the specified crm_Records.
     *
     * @since 1.0.23
     *
     * @param crm_Record[]     $targets
     * @param string|null      $linkType
     * @return ORM_Criteria
     */
    public function isSourceOfAny($targets, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();
        $linkSet->hasOne('sourceId', get_class($this));

        $targetIdsByClasses = array();
        foreach ($targets as $target) {
            $targetClass = get_class($target);
            if (!isset($targetIdsByClasses[$targetClass])) {
                $targetIdsByClasses[$targetClass] = array();
            }
            $targetIdsByClasses[$targetClass][] = $target->id;
        }

        $targetsCriteria = array();
        foreach ($targetIdsByClasses as $targetClass => $targetIds) {
            $targetsCriteria[] = $linkSet->targetClass->is($targetClass)->_AND_($linkSet->targetId->in($targetIds));
        }

        $criteria =	$linkSet->all(
            $linkSet->sourceClass->is($this->getRecordClassName()),
            $linkSet->any($targetsCriteria)
        );
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * Returns a criteria usable to select records of this set associated to the specified tags.
     *
     * @array	$tags		An array of tag ids
     * @string	$type		The link type [optional]
     * @return ORM_Criteria
     */
    public function haveTags($tags, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();

        $criteria = $linkSet->sourceClass->is($this->getRecordClassName())
            ->_AND_($linkSet->targetClass->is($this->Crm()->TagClassName()));
        if (is_array($tags)) {
            $criteria = $criteria->_AND_($linkSet->targetId->in($tags));
        } else {
            $criteria = $criteria->_AND_($linkSet->targetId->is($tags));
        }
        if (isset($linkType)) {
            $criteria = $criteria->_AND_($linkSet->type->is($linkType));
        }
        $links = $linkSet->select($criteria);

        $ids = array();
        foreach ($links as $link) {
            $ids[$link->sourceId] = $link->sourceId;
        }

        return $this->id->in($ids);
    }


    /**
     * Returns a criteria usable to select records of this set associated to the specified tags.
     *
     * @array	$tags		An array of tag ids
     * @string	$type		The link type [optional]
     * @return ORM_Criteria
     */
    public function haveTagLabels($tagLabels, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();

        $this->Crm()->includeTagSet();
        $tagClassName = $this->Crm()->TagClassName();

        $linkSet->joinTarget($tagClassName);
        $criteria = $linkSet->sourceClass->is($this->getRecordClassName())
            ->_AND_($linkSet->targetClass->is($tagClassName));
        if (is_array($tagLabels)) {
            $criteria = $criteria->_AND_($linkSet->targetId->label->in($tagLabels));
        } else {
            $criteria = $criteria->_AND_($linkSet->targetId->label->is($tagLabels));
        }
        if (isset($linkType)) {
            $criteria = $criteria->_AND_($linkSet->type->is($linkType));
        }
        $links = $linkSet->select($criteria);

        $ids = array();
        foreach ($links as $link) {
            $ids[$link->sourceId] = $link->sourceId;
        }

        return $this->id->in($ids);
    }


    /**
     * Returns records of this set associated to the specified tags.
     *
     * @array	$tags		An array of tag ids
     * @string	$type		The link type [optional]
     * @return ORM_Iterator
     */
    public function selectHavingTags($tags, $linkType = nul)
    {
        return $this->select($this->haveTags($tags, $linkType));
    }

}


/**
 * A traceable record automatically stores by whom and when it was created,
 * modified and even deleted.
 *
 * By default "deleted" records (through the standard delete() methods)
 * actually stay in the database and are only flagged as deleted.
 *
 * @property int        $id
 * @property int		$createdBy
 * @property string		$createdOn
 * @property int		$modifiedBy
 * @property string		$modifiedOn
 * @property int		$deletedBy
 * @property string		$deletedOn
 * @property string		$uuid
 * @property bool		$deleted
 */
class crm_TraceableRecord extends crm_Record
{
    /**
     * Saves the record.
     *
     * @param bool		$noTrace		True to bypass the tracing of modifications.
     *
     * @return boolean True on success, false otherwise
     */
    public function save($noTrace = false)
    {
        return $this->getParentSet()->save($this, $noTrace);
    }



    /**
     * @return bool
     */
    public function isLinkedTo(crm_Record $source, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();

        $criteria =	$linkSet->sourceClass->is(get_class($source))
            ->_AND_($linkSet->sourceId->is($source->id))
            ->_AND_($linkSet->targetClass->is(get_class($this)))
            ->_AND_($linkSet->targetId->is($this->id));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);

        $isLinked = ($links->count() > 0);

        $linkSet->__destruct();
        unset($linkSet);

        return $isLinked;
    }



    /**
     * @return bool
     */
    public function isSourceOf(crm_Record $target, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();

        $criteria =	$linkSet->targetClass->is(get_class($target))
            ->_AND_($linkSet->targetId->is($target->id))
            ->_AND_($linkSet->sourceClass->is(get_class($this)))
            ->_AND_($linkSet->sourceId->is($this->id));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);

        $isLinked = ($links->count() > 0);

        $linkSet->__destruct();
        unset($linkSet);

        return $isLinked;
    }



    /**
     * Link record to $source
     *
     * @return crm_TraceableRecord
     */
    public function linkTo(crm_Record $source, $linkType = '')
    {
        $linkSet = $this->Crm()->LinkSet();
        /* @var $link crm_Link */
        $link = $linkSet->newRecord();
        $link->sourceClass = get_class($source);
        $link->sourceId = $source->id;
        $link->targetClass = get_class($this);
        $link->targetId = $this->id;
        $link->type = $linkType;
        $link->save();

        $link->__destruct();
        unset($link);

        $linkSet->__destruct();
        unset($linkSet);

        return $this;
    }



    /**
     * Unlink record from $source
     *
     * @return crm_TraceableRecord
     */
    public function unlinkFrom(crm_Record $source, $linkType = null)
    {
        $linkSet = $this->Crm()->LinkSet();

        $linkSet->deleteLink($source, $this, $linkType);

        return $this;
    }



}






/**
 * Organization person
 * an object to represent a person with a position in organization
 * a contact can have multiple organizations, the selectContact method of organization will return an iterator of crm_OrganizationPerson
 *
 * @see	crm_Organization::selectContacts()
 */
interface crm_OrganizationPerson
{
    /**
     * Position in organization
     * @return string
     */
    public function getMainPosition();

    /**
     * Organization
     * @return crm_Organization
     */
    public function getMainOrganization();

    /**
     * The contact of person, if the organization person is the contact, the method return $this
     * @return crm_Contact
     */
    public function getContact();
}