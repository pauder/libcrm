<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_Contact is a person
 *
 * @property ORM_StringField    $firstname      First name
 * @property ORM_StringField    $lastname       Last name
 * @property ORM_EnumField      $title          The civil title
 * @property ORM_Date           $birthday
 * @property ORM_UserField      $user           The (optional) ovidentia user associated to this contact.
 * @property ORM_StringField    $phone
 * @property ORM_StringField    $mobile
 * @property ORM_StringField    $fax
 * @property ORM_StringField    $email
 * @property crm_AddressSet     $address
 * @property crm_ContactSet     $responsible	The collaborator responsible for this contact.
 *
 * @method crm_Contact  get
 */
class crm_ContactSet extends crm_TraceableRecordSet
{
    /**
     *
     * @var crm_ContactOrganizationSet
     */
    protected $contactOrganizationSet = null;



    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setDescription('Contact');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('firstname')
                ->setDescription('First name'),
            ORM_StringField('lastname')
                ->setDescription('Last name'),
            ORM_EnumField('title', $Crm->translateArray(crm_Contact::$titles))
                ->setDescription('Title'),
            ORM_DateField('birthday')
                ->setDescription('Birthday'),
            ORM_UserField('user')
                ->setDescription('Associated portal user id'),
            ORM_StringField('phone')
                ->setDescription('Phone'),
            ORM_StringField('mobile')
                ->setDescription('Mobile'),
            ORM_StringField('fax')
                ->setDescription('Fax'),
            ORM_StringField('email')
                ->setDescription('Email')
        );

        $this->hasOne('responsible', $Crm->ContactSetClassName())
            ->setDescription('The collaborator responsible for this contact');

        $this->hasOne('address', $this->Crm()->AddressSetClassName())
            ->setDescription('Address');

        if ($Crm->onlineShop && !$Crm->noDelivery)
        {
            // myContact.loginCreate use this deliveryaddress field if exists
            $this->hasOne('deliveryaddress', $this->Crm()->AddressSetClassName());
        }


        if (isset($Crm->Order))
        {
            $relation = $this->hasMany('orders', $Crm->OrderSetClassName(), 'contact');
            $relation->setOnDeleteMethod(ORM_ManyRelation::ON_DELETE_SET_NULL);
        }

        if (isset($Crm->ShoppingCart))
        {
            $relation = $this->hasMany('shoppingcarts', $Crm->ShoppingCartSetClassName(), 'contact');
            $relation->setOnDeleteMethod(ORM_ManyRelation::ON_DELETE_SET_NULL);
        }

    }



    /**
     * Creates the specified number of random contacts.
     *
     * @param int	$nbContacts
     * @return array	The ids of contacts actually created
     */
    public function populateRandomly($nbContacts)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $Pop = bab_functionality::get('Populator');
        if (!($Pop instanceof bab_functionality)) {
            return 0;
        }

        // if joined with address we also fill addresses
        $withAddress = ($this->address instanceof ORM_RecordSet);

        $CountrySet = $this->Crm()->CountrySet();
        $country = $CountrySet->get($CountrySet->code->is('FR'));


        $recordIds = array();
        for ($nbCreatedContacts = 0; $nbCreatedContacts < $nbContacts; $nbCreatedContacts++) {
            $userinfo = $Pop->getRandomUserInfo();
            $contact = $this->newRecord();
            $contact->firstname = $userinfo['givenname'];
            $contact->lastname = $userinfo['sn'];
            $contact->title = $userinfo['gender'] == 'M' ? crm_Contact::TITLE_MR : crm_Contact::TITLE_MRS;
            $contact->gender = $userinfo['gender'] == 'M' ? crm_Contact::GENDER_MALE : crm_Contact::GENDER_FEMALE;
            $birthdate = BAB_DateTime::now();
            $birthdate->add(-rand(18 * 365, 65 * 365), BAB_DATETIME_DAY);
            $contact->birthday = $birthdate->getIsoDate();

            $filepath = $Pop->getRandomPhoto($userinfo['gender']);

            $contact->photo = basename($filepath);

            if ($withAddress) {
                $contact->address->street = $userinfo['bstreetaddress'];
                $contact->address->postalCode = $userinfo['bpostalcode'];
                $contact->address->city = $userinfo['bcity'];
                $contact->address->country = $country->id;
            }
            $contact->save();
            $contact->importPhoto(new bab_Path($filepath), false);

            $recordIds[] = $contact->id;
        }
        return $recordIds;
    }



    /**
     *
     */
    public function renameRandomly()
    {
        $Crm = $this->Crm();

        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $Pop = bab_functionality::get('Populator');
        if (!($Pop instanceof bab_functionality)) {
            return 0;
        }

        $contactSet = $Crm->ContactSet();
        $contacts = $contactSet->select();

        foreach ($contacts as $contact) {
            $userinfo = $Pop->getRandomUserInfo();
            $contact->firstname = $userinfo['givenname'];
            $contact->lastname = $userinfo['sn'];
            $contact->title = $userinfo['gender'] == 'M' ? crm_Contact::TITLE_MR : crm_Contact::TITLE_MRS;
            $contact->gender = $userinfo['gender'] == 'M' ? crm_Contact::GENDER_MALE : crm_Contact::GENDER_FEMALE;
            $contact->phone = $userinfo['btel'];
            $contact->email = $userinfo['email'];
            $contact->save();
//			$contact->importPhoto(new bab_Path($filepath), false);
        }

    }




    /**
     * Contact organization set with cache in contactSet
     *
     * @see crm_Contact::getContactOrganizationSet()
     *
     * @return crm_ContactOrganizationSet
     */
    public function getContactOrganizationSet()
    {
        if (null === $this->contactOrganizationSet)
        {
            $this->Crm()->includeOrganizationSet();

            $this->contactOrganizationSet = $this->Crm()->ContactOrganizationSet();
            $this->contactOrganizationSet->organization();
        }

        return $this->contactOrganizationSet;
    }





    /**
     * Returns a criteria for matching contacts having the specified organization
     * as their main organization.
     *
     * @param int|array	$organization		The organization id
     *
     * @return ORM_Criteria
     */
    public function hasMainOrganization($organization)
    {
        $Crm = $this->Crm();
        if (!isset($Crm->Organization)) {
            return new ORM_FalseCriterion();
        }

        $contactOrganizationSet = $this->getContactOrganizationSet();

        return $this->id->in(
            $contactOrganizationSet->organization->id->in($organization)
                ->_AND_($contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan(date('Y-m-d'))))
                ->_AND_($contactOrganizationSet->organization->deleted->is(0))
                ->_AND_($contactOrganizationSet->main->is('1'))
        );
    }





    /**
     * Returns a criteria for matching contacts being members of the specified
     * organization.
     *
     * @param int|array	$organization		The organization id
     *
     * @return ORM_Criteria
     */
    public function hasOrganization($organization)
    {
        $Crm = $this->Crm();
        if (!isset($Crm->Organization)) {
            return new ORM_FalseCriterion();
        }

        $contactOrganizationSet = $this->getContactOrganizationSet();

        return $this->id->in(
            $contactOrganizationSet->organization->id->in($organization)
                ->_AND_($contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan(date('Y-m-d'))))
                ->_AND_($contactOrganizationSet->organization->deleted->is(0))
        );
    }


    /**
     * @param int $organization
     */
    public function hasOnlyOrganization($organization)
    {
        $contactOrganizationSet = $this->Crm()->ContactOrganizationSet();

        return $this->all(
            $this->id->in(
                $contactOrganizationSet->organization->is($organization),
                'contact'
            ),
            $this->id->notIn(
                $contactOrganizationSet->organization->isNot($organization),
                'contact'
            )
        );
    }



    /**
     * Returns a criteria for matching contacts being known by a contact (at
     * specified levels).
     *
     * @param int|array	$contact		The contact id
     * @param int|array	$level			The acquaintance level
     *
     * @return ORM_Criteria
     */
    public function isKnownBy($contact, $level = null)
    {
        $Crm = $this->Crm();

        $contactAcquaintanceSet = $Crm->ContactAcquaintanceSet();
        $contactAcquaintanceSet->contact();

        return $this->id->in(
            $contactAcquaintanceSet->contact->id->in($contact)
                ->_AND_($contactAcquaintanceSet->level->in($level))
                ->_AND_($contactAcquaintanceSet->contact->deleted->is(0))
        );
    }


    /**
     * Returns an iterator of contact linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @return ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = 'hasContact')
    {
        return parent::selectLinkedTo($source, $linkType);
    }




    private function importTag($contact, $rowTag)
    {
        $Crm = $this->Crm();
        $tagSet = $Crm->TagSet();

        $tag = trim($rowTag);

        /* Verify if the tag exist */
        $tagRecord = $tagSet->get($tagSet->label->is($tag));
        if (!isset($tagRecord)) {
            $tagRecord = $tagSet->newRecord();
            $tagRecord->label = $tag;
            $tagRecord->save();

            $hasTag = false;
            $tagRecord->linkTo($contact); /* see tag.class in LibCrm */

        } else {

            $tagRecord->linkTo($contact); /* see tag.class in LibCrm */
            //$linkSet = $Crm->LinkSet();

            //$links = $linkSet->select(
            //      $linkSet->sourceClass->is('Contact')
            //      ->_AND_($linkSet->destClass->is('Tag'))
            //      ->_AND_($linkSet->sourceId->is($contact->id))
            //      ->_AND_($linkSet->destId->is($tagRecord->id))
            //);

            //$hasTag = ($links->count() > 0);
        }

        if (!$hasTag) {
            /* Link the tag with the contact */
        }

        $tagRecord->__destruct();
        unset($tagRecord);
    }


    /**
     * Import a CSV row
     *
     * @throw Widget_ImportException
     *
     * @param	crm_Import			$import
     * @param	Widget_CsvRow		$row
     *
     * @return bool | int
     */
    public function import(crm_Import $import, Widget_CsvRow $row, $notrace = false)
    {


        if (isset($row->uuid) && '' !== (string) $row->uuid) {

            // update row if uuid is present


            $contact = $this->get($this->uuid->is($row->uuid));

            if (!$contact) {

                $message = sprintf($this->Crm()->translate('Error on line %d, there is a value in the unique identifier column but the contact does not exists in the database.
To create the contact, you must remove the value in the universally unique identifier column'), $row->line());

                $exception = new Widget_ImportException($message);
                $exception->setCsvRow($row);

                throw $exception;

                return false;
            }

        } else {

            // contact creation

            $contact = $this->newRecord();
        }


        $nb_prop = $contact->import($row); /* see import() in class crm_Contact */
        if (0 < $nb_prop) {
            $contact->save($notrace);
            $import->addContact($contact);
        }

        $id = $contact->id;

        /* Create a tag in TagSet if value exist in CSV row : tag for contact */
        if (isset($row->tag1) && $row->tag1 != '') {
            $this->importTag($contact, $row->tag1);
        }
        if (isset($row->tag2) && $row->tag2 != '') {
            $this->importTag($contact, $row->tag2);
        }
        if (isset($row->tag3) && $row->tag3 != '') {
            $this->importTag($contact, $row->tag3);
        }
        if (isset($row->tag4) && $row->tag4 != '') {
            $this->importTag($contact, $row->tag4);
        }
        if (isset($row->tag5) && $row->tag5 != '') {
            $this->importTag($contact, $row->tag5);
        }
        if (isset($row->tag6) && $row->tag6 != '') {
            $this->importTag($contact, $row->tag6);
        }
        if (isset($row->tag7) && $row->tag7 != '') {
            $this->importTag($contact, $row->tag7);
        }

        $contact->__destruct();
        unset($contact);

        return $id;
    }


    /**
     * If this method returnd true, the user's sitemap profile will be cleared on user field change.
     * Set the returned value to true if the user gains or loses access to sitemap items when associated to a contact.
     *
     * @return	bool
     */
    public function updateSiteMapOnUserChange()
    {
        return false;
    }

    /**
     * If this method returns true, there will be one contact per user.
     * if a contact is saved with a user already associated to another contact, the user field will be set to 0 on the other contact.
     *
     * @return bool
     */
    public function oneContactPerUser()
    {
        return $this->Crm()->onlineShop;
    }



    /**
     * Deletes records matching the specified criteria.
     * If $definitive is false, records are not actually deleted
     * from the database but only flagged as so.
     *
     * @param ORM_Criteria	$criteria		The criteria for selecting the records to delete.
     * @param bool			$definitive		True to delete permanently the record.
     *
     * @return boolean				True on success, false otherwise
     */
    public function delete(ORM_Criteria $criteria = null, $definitive = false)
    {
        if ($this->updateSiteMapOnUserChange()) {

            $records = $this->select($criteria);

            foreach ($records as $record) {
                /* @var $record crm_Contact */

                if ($record->user) {
                    bab_siteMap::clear($record->user);
                }
            }
        }


        return parent::delete($criteria, $definitive);
    }


    /**
     * Saves a record and keeps traces of the user doing it.
     * add specific action for contact->user
     *
     *
     * @param crm_Contact			$record			The record to save.
     * @param bool					$noTrace		True to bypass the tracing of modifications.
     *
     * @return boolean				True on success, false otherwise
     */
    public function save(ORM_Record $record, $noTrace = false)
    {
        if ($this->oneContactPerUser() && $record->user > 0) {

            $res = $this->select($this->user->is($record->user)->_AND_($this->id->is($record->id)->_NOT()));
            foreach($res as $duplicateuser)
            {
                $duplicateuser->user = 0;
                parent::save($duplicateuser, true);
            }
        }



        if ($this->updateSiteMapOnUserChange()) {

            $old_user = 0;
            $new_user = (int) $record->user;

            if ($record->id) {
                $oldrecord = $this->get($record->id);
                if ($oldrecord && $oldrecord->user) {
                    $old_user = (int) $oldrecord->user;
                }
            }

            if ($old_user !== $new_user) {
                if ($new_user) {
                    // clear sitemap profile if new user different than before and set
                    bab_siteMap::clear($new_user);
                }

                if ($old_user) {
                    // clear sitemap profile if old user different than after and set
                    bab_siteMap::clear($old_user);
                }
            }
        }


        return parent::save($record, $noTrace);
    }



    /**
     * Checks if the user is a collaborator.
     *
     * @return bool
     */
    public function isCollaborator()
    {
        $Crm = $this->Crm();
        $crmMainOrganizationId = $Crm->getMainOrganization();

        if (!$crmMainOrganizationId) {
            return new ORM_FalseCriterion();
        }

        $contactOrganizationSet = $Crm->ContactOrganizationSet();
        $contactOrganizationSet->contact();

        $today = date('Y-m-d');
        $contactOrganizations = $contactOrganizationSet->select(
            $contactOrganizationSet->organization->is($crmMainOrganizationId)
            ->_AND_(
                $contactOrganizationSet->contact->user->isNot(0)
            )
            ->_AND_(
                $contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan($today))
            )
            ->_AND_(
                $contactOrganizationSet->history_from->is('0000-00-00')->_OR_($contactOrganizationSet->history_from->lessThanOrEqual($today))
            )
            ->_AND_(
                $contactOrganizationSet->history_from->is('0000-00-00')->_OR_($contactOrganizationSet->history_from->lessThanOrEqual($today))
            )
        );

        $contacts = array();
        foreach($contactOrganizations as $contactOrganization) {
            $contacts[$contactOrganization->contact->id] = $contactOrganization->contact->id;
        }

        return $this->id->in($contacts);
    }

}





/**
 * A crm_Contact is a person
 *
 * @property string             $firstname
 * @property string             $lastname
 * @property string             $title
 * @property BAB_DateTime       $birthday
 * @property crm_Address        $address
 * @property int                $user		The (optional) ovidentia user associated to this contact.
 * @property string             $phone
 * @property string             $mobile
 * @property string             $fax
 * @property string             $email
 */
class crm_Contact extends crm_TraceableRecord implements crm_OrganizationPerson
{
    const TITLE_MR = 1;
    const TITLE_MRS = 2;
    const TITLE_MS = 3;
    const TITLE_DR = 4;

    public static $titles =	array(
        crm_Contact::TITLE_MR => 'Mr.',
        crm_Contact::TITLE_MRS => 'Mrs',
        crm_Contact::TITLE_MS => 'Ms',
        crm_Contact::TITLE_DR => 'Dr',
    );


    public static $fulltitles =	array(
        crm_Contact::TITLE_MR => 'Mister',
        crm_Contact::TITLE_MRS => 'Madam',
        crm_Contact::TITLE_MS => 'Miss',
        crm_Contact::TITLE_DR => 'Doctor',
    );



    const SUBFOLDER = 'contacts';
    const PHOTOSUBFOLDER = 'photo';
    const ATTACHMENTSSUBFOLDER = 'attachments';

    /**
     * if true, a contact has been saved into an ovidentia directory entry
     * @var bool
     */
    private static $directory_update = false;


    /**
     * Returns the age of the contact in years at the specified date (or now if no date specified).
     *
     * @param BAB_DateTime	$date		The date at which the age will be calculated.
     * @return int
     */
    public function getAge($date = null)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        if (!isset($date)) {
            $date = BAB_DateTime::now();
        }
        $birthday = BAB_DateTime::fromIsoDateTime($this->birthday);
        $nbYears = $date->getYear() - $birthday->getYear();
        if ($birthday->getMonth() . $birthday->getDayOfMonth() > $date->getMonth() . $date->getDayOfMonth()) {
            $nbYears--;
        }
        return $nbYears;
    }


    /**
     * @see crm_OrganizationPerson
     * @return string
     */
    public function getFullName()
    {
        return bab_composeUserName($this->firstname, $this->lastname);
    }


    /**
     * @see crm_OrganizationPerson
     * @return string
     */
    public function getFullNameWithTitle()
    {
        return $this->getParentSet()->title->output($this->title) . ' ' . bab_composeUserName($this->firstname, $this->lastname);
    }

    /**
     *
     * @return string
     */
    public function getFullTitle()
    {
        $Crm = $this->Crm();

        $t = self::$fulltitles;
        if (isset($t[$this->title]))
        {
            return $Crm->translate($t[$this->title]);
        }

        return '';
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->getFullName();
    }


    /**
     * overload default record title
     * @return string
     */
    public function getRecordTitle()
    {
        return $this->getFullName();
    }


    /**
     * Contact organization set with cache in Contact set
     * @return crm_ContactOrganizationSet
     */
    protected function getContactOrganizationSet()
    {
        return $this->getParentSet()->getContactOrganizationSet();
    }

    /**
     * Get the link beetween organization and contact record
     * default beahavior : link is set manually as the main link
     *
     * @return crm_ContactOrganization | null
     */
    public function getMainContactOrganization()
    {
        $Crm = $this->Crm();

        if (!isset($Crm->Organization))
        {
            return null;
        }


        $set = $this->getContactOrganizationSet();

        return $set->get(
            $set->contact->is($this->id)
            ->_AND_($set->main->is('1'))
            ->_AND_($set->history_to->is('0000-00-00')->_OR_($set->history_to->greaterThan(date('Y-m-d'))))
            ->_AND_($set->organization->deleted->is(0))
        );
    }

    /**
     * Get one organization associated to contact
     * default beahavior : link is set manually as the main link
     *
     * @return crm_Organization | null
     */
    public function getMainOrganization()
    {
        if (!isset($this->Crm()->Organization))
        {
            return null;
        }

        $this->Crm()->includeOrganizationSet();
        $contactOrganization = $this->getMainContactOrganization();
        if (null === $contactOrganization) {
            return null;
        }
        return $contactOrganization->organization;
    }

    /**
     * Get organization associated to contact without history
     * ordered with main organization first and by organization name
     *
     * @param bool	$includeHistory		True to select organizations where the contact position (history_to) is finished.
     * @return ORM_Iterator	<crm_ContactOrganization>
     */
    public function selectOrganizations($includeHistory = false)
    {
        if (!isset($this->Crm()->Organization))
        {
            return null;
        }


        $set = $this->getContactOrganizationSet();
        $criteria = $set->contact->is($this->id)->_AND_($set->organization->deleted->is(0));
        if (!$includeHistory) {
            $criteria = $criteria->_AND_($set->history_to->is('0000-00-00')->_OR_($set->history_to->greaterThan(date('Y-m-d'))));
        }
        $res = $set->select($criteria);

        if ($includeHistory) {
            $res->orderAsc($set->history_to);
        }
        $res->orderDesc($set->main)->orderAsc($set->organization->name);

        return $res;
    }

    /**
     *
     * @return string
     */
    public function getMainPosition()
    {
        $record = $this->getMainContactOrganization();
        if (null === $record) {
            return '';
        }
        return $record->getMainPosition();
    }





    /**
     * Get main organization and descendants
     * @return array
     */
    public function getRelatedOrganizations()
    {
        $Crm = $this->Crm();

        $orgs = array();

        if ($mainOrganization = $this->getMainOrganization()) {
            $orgs[$mainOrganization->id] = $mainOrganization->name;

            $descendantIds = array();
            $mainOrganization->getDescendants($descendantIds);
            $organizationSet = $Crm->OrganizationSet();
            $descendants = $organizationSet->select($organizationSet->id->in($descendantIds));

            foreach ($descendants as $descendant) {
                $orgs[$descendant->id] = $descendant->name;
            }
        }

        return $orgs;
    }




    /**
     * return contact to respect the organization person interface
     * @see crm_OrganizationPerson
     * @return crm_Contact
     */
    public function getContact()
    {
        return $this;
    }

    /**
     *
     * @return crm_Address
     */
    public function getMainAddress()
    {
        $this->Crm()->includeAddressSet();
        return $this->address();
    }




    /**
     * Get billing address
     * Used in online shop to get the billing address in a new order (order, invoice, quotation, ...)
     * @return crm_Address
     */
    public function getBillingAddress()
    {
        return $this->getMainAddress();
    }



    /**
     * Default delivery address of contact for online shop
     * @return crm_Address
     */
    public function getDeliveryAddress()
    {
        $this->Crm()->includeAddressSet();
        $set = $this->getParentSet();

        if (isset($set->deliveryaddress) && ($set->deliveryaddress instanceof ORM_FkField || $set->deliveryaddress instanceof ORM_RecordSet))
        {
            $delivery = $this->deliveryaddress();
            if (null !== $delivery && !$delivery->isEmpty())
            {
                return $delivery;
            }
        }
        return $this->getMainAddress();
    }

    /**
     * @return string
     */
    public function getMainEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMainPhone()
    {
        return $this->phone;
    }


    public function getMainFax()
    {
        return $this->fax;
    }

    /**
     * @return string
     */
    public function getMainMobilePhone()
    {
        return $this->mobile;
    }


    /**
     * @return string
     */
    public function getAlternatePhone()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getAlternateMobilePhone()
    {
        return null;
    }





    public function updateMainAddress()
    {
        $Crm = $this->Crm();
        $addressSet = $Crm->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this);

        if ($this->address instanceof crm_Address) {
            $thisAddressId = $this->address->id;
        } else {
            $thisAddressId = $this->address;
        }

        $firstAddressId = null;
        $firstAddressRecord = null;
        $thisAddressOk = false;
        foreach ($addressLinks as $addressLink) {
            if ($addressLink->targetId->id == $thisAddressId) {
                // We found the current main address in the linked addresses.
                $thisAddressOk = true;
                break;
            }
            if (!isset($firstAddressId)) {
                $firstAddressId = $addressLink->targetId->id;
                $firstAddressRecord = $addressLink->targetId;
            }
        }

        if (!$thisAddressOk) {
            // If the current main address is not part of the linked addresses,
            // we replace it with the first linked address.
            if ($this->address instanceof crm_Address) {
                $this->address = $firstAddressRecord;
            } else {
                $this->address = $firstAddressId;
            }

            $this->save();
        }
    }



    /**
     * Adds a typed address to the contact.
     *
     * @param crm_Address $address
     * @param string $type
     */
    public function addAddress(crm_Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }

        if (!$address->isLinkedTo($this, $type)) {
            $address->linkTo($this, $type);
            $this->updateMainAddress();
        }
        return $this;
    }


    /**
     * Removes a typed address from the contact.
     *
     * @param crm_Address $address
     * @param string $type
     *
     * @return crm_Contact
     */
    public function removeAddress(crm_Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }
        $address->unlinkFrom($this, $type);

        $this->updateMainAddress();
        return $this;
    }



    /**
     * Update the type of a linked address for the contact.
     *
     * @param crm_Address $address
     * @param string $previousAddressType
     * @param string $newAddressType
     *
     * @return crm_Contact
     */
    public function updateAddress(crm_Address $address, $previousAddressType, $newAddressType)
    {
        if (!isset($this->id)) {
            return;
        }

        $Crm = $this->Crm();
        $addressSet = $Crm->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this, $previousAddressType);
        foreach ($addressLinks as $addressLink) {
            $addressLink->type = $newAddressType;
            $addressLink->save();
        }

        return $this;
    }



    /**
     *
     * @param string $type
     *
     * @return array  Eg. array('Type 1' => array(address1), 'Type 2' => array(address2, address3 ), ...)
     */
    public function getAddresses($type = null)
    {
        $Crm = $this->Crm();

        $addressSet = $Crm->AddressSet();

        $addresses = $addressSet->selectLinkedTo($this, $type);

        $typedAddresses = array();

        foreach ($addresses as $address) {
            $type = $address->type;
            if (!isset($typedAddresses[$type])) {
                $typedAddresses['' . $type] = array();
            }
            $typedAddresses['' . $type][] = $address->targetId;
        }


        return $typedAddresses;
    }



    /**
     * Get the upload path for files related to this contact.
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

        $path = $this->Crm()->uploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }


    /**
     * Get the upload path for attached files.
     *
     * @return bab_Path
     */
    public function getAttachmentsUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::ATTACHMENTSSUBFOLDER);
        return $path;
    }

    /**
     * Get the upload path for the photo file
     * this method can be used with the image picker widget
     *
     * @return bab_Path
     */
    public function getPhotoUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::PHOTOSUBFOLDER);
        return $path;
    }




    /**
     * Return the full path of the photo file
     * The photo field contain a file name or a description of the photo
     * this method return null if there is no photo to display
     *
     * @return bab_Path | null
     */
    public function getPhotoPath()
    {


        $uploadpath = $this->getPhotoUploadPath();

        if (!isset($uploadpath)) {
            return null;
        }

        $W = bab_Widgets();

        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

        if (!isset($uploaded)) {
            return null;
        }

        foreach($uploaded as $file) {
            return $file->getFilePath();
        }

        return null;
    }




    /**
     * Move photo to photo upload path
     * this method is used to import a photo from a temporary directory of the filePicker widget or another file
     * warning, the default behavior remove the source file and existing contact photo file
     *
     * @param	bab_Path 	$sourcefile
     * @param	bool		$temporary			default is true, if the sourcefile is a filePicker temporary file
     * @return 	bool
     */
    public function importPhoto(bab_Path $sourcefile, $temporary = true)
    {
        $uploadpath = $this->getPhotoUploadPath();

        if (!isset($uploadpath)) {
            return false;
        }

        $uploadpath->createDir();


        if (null !== $photo = $this->getPhotoPath()) {
            unlink($photo->toString());
        }

        if (!$temporary) {
            $W = bab_Widgets();
            return $W->imagePicker()->setFolder($uploadpath)->importFile($sourcefile);
        }


        $original = $sourcefile->toString();
        $uploadpath->push(basename($original));

        return rename($original, $uploadpath->toString());
    }



    public function addNote(crm_Note $note)
    {
        $note->linkTo($this);
    }

    /**
     * Returns an iterator of notes linked to this contact.
     *
     * @return ORM_Iterator
     */
    public function selectNotes()
    {
        $noteSet = $this->Crm()->NoteSet();
        return $noteSet->selectLinkedTo($this);
    }


    /**
     *
     */
    public function linkTo(crm_Record $source, $linkType = 'hasContact')
    {
        parent::linkTo($source, $linkType);
    }


    /**
     * Add organization to contact
     *
     * @param	int			$organization		The organization id
     * @param	string		$position
     *
     * @return crm_Contact
     */
    public function addOrganization($organization, $position = '')
    {
        if (!$this->id) {
            throw new Exception('Error, the contact is not inserted, cannot add organization');
            return $this;
        }

        // if no main organization, add this link as the main organization

        if (null === $this->getMainOrganization()) {
            $main = '1';
        } else {
            $main = '0';
        }

        // set without join
        static $set = null;

        if (null === $set) {
            $set = $this->Crm()->ContactOrganizationSet();
        }

        $record = $set->get(
            $set->organization->is($organization)
            ->_AND_($set->contact->is($this->id))
            ->_AND_($set->position->is($position))
            ->_AND_($set->main->is($main))
        );

        if(!$record){
            $record = $set->newRecord();
            $record->organization = $organization;
            $record->contact = $this->id;
            $record->position = $position;
            $record->main = $main;
            $record->save();
        }

        $this->updateMainOrganization();

        return $this;
    }




    /**
     * Update the organization field in the contact record according
     * to the main organization.
     *
     * @return bool True if contact was changed.
     */
    public function updateMainOrganization()
    {
        $Crm = $this->Crm();

        $contact = $Crm->ContactSet()->get($this->id);

        $this->Crm()->includeOrganizationSet();
        $contactOrganization = $this->getMainContactOrganization();

        if (!isset($contactOrganization)) {
            $contact->organization = 0;
            $contact->position = 0;
            $contact->save();
            return true;
        } elseif ($contactOrganization->organization->id != $contact->organization || $contactOrganization->position != $contact->position) {
            $contact->organization = $contactOrganization->organization->id;
            $contact->position = $contactOrganization->position;
            $contact->save();
            return true;
        }
        return false;
    }




    /**
     * user account creation and password modification
     *
     * @throw crm_ContactUserException
     * @see crm_ContactEditor::User()
     *
     * @param	Array	$arr	posted values from crm_ContactEditor
     */
    public function manageUserAccount($arr)
    {
        $Crm = $this->Crm();

        if (isset($arr['user_extra'])) {

            self::$directory_update = true;

            if ($this->user && isset($arr['user_extra']['disabled'])) {

                // change password or disable account

                $new_password = trim($arr['user_extra']['new_password']);
                $infos = array(
                    'disabled' => $arr['user_extra']['disabled']
                );

                if (isset($arr['user_extra']['nickname'])) {
                    $infos['nickname'] = trim($arr['user_extra']['nickname']);
                }

                if ($new_password) {
                    $infos['password'] = $new_password;
                }

                bab_updateUserById($this->user, $infos, $error);
                if ($error) {
                    throw new crm_ContactUserException($error);
                }

                $this->updateUser();

                // Notify the user about the new password
                if ($new_password) {
                    $notification = $Crm->Notify()->myContact_newPassword($this, $new_password);
                    $notification->send();
                }


            } else if (3 === (int) $arr['usertype']) {

                $nickname 	= $arr['user_extra']['nickname'];

                if (!empty($nickname)) {

                    // create user account
                    $user = $this->createUser($nickname, $arr['user_extra']['password'], $arr['user_extra']['notify'], true);

                    if ($user) {
                        $this->user = $user;
                    }
                }
            }
        }



    }


    /**
     * Update user password
     * @param	string	$new_password
     * @throws  crm_ContactUserException
     * @return bool
     */
    public function updatePassword($new_password1, $new_password2)
    {
        $Crm = $this->Crm();

        if (!$this->user)
        {
            throw new crm_ContactUserException($Crm->translate('Missing user account'));
        }

        if ($new_password1 !== $new_password2)
        {
            throw new crm_ContactUserException($Crm->translate('Passwords not match'));
        }


        $infos = array();
        $infos['password'] = $new_password1;

        bab_updateUserById($this->user, $infos, $error);

        if ($error) {
            throw new crm_ContactUserException($error);
        }

        return true;
    }

    /**
     * Update user nickname
     * @param string $nickname
     * @throws  crm_ContactUserException
     * @return bool
     */
    public function updateNickname($nickname)
    {
        if (!$this->user)
        {
            throw new crm_ContactUserException($Crm->translate('Missing user account'));
        }

        $infos = array();
        $infos['nickname'] = $nickname;

        bab_updateUserById($this->user, $infos, $error);

        if ($error) {
            throw new crm_ContactUserException($error);
        }

        return true;
    }



    /**
     * Create ovidentia account and return user id
     *
     * @throw crm_ContactUserException
     *
     * @param	string	$nickname
     * @param	string	$password
     * @param	bool	$notify				1|0 as string or bool
     * @param	bool	$notify_password	1|0 as string or bool
     *
     * @return int
     */
    public function createUser($nickname, $password, $notify = false, $notify_password = false)
    {
        $error = '';

        // add a middlename to allow firstname/lastname duplicates
        $middlename = $this->getMainEmail();

        $id_user = bab_registerUser(
            empty($this->firstname) ? ' ' : $this->firstname,
            $this->lastname,
            $middlename,
            $this->getMainEmail(),
            $nickname,
            $password,
            $password,
            1,
            $error
        );


        if (!empty($error)) {

            $e = new crm_ContactUserException($error);
            $e->redirect = false;
            throw $e;
            return false;
        }

        if ($id_user) {

            $infos = $this->getUserInfos();
            if ($infos) {
                bab_updateUserById($id_user, $infos, $error);

                if ($error) {
                    $oError = new crm_ContactUserException($error);
                    $oError->redirect = false;
                    throw $oError;
                    return $id_user;
                }
            }

            if ($notify) {
                if ($notify_password) {
                    $this->createUserNotify($nickname, $password);
                } else {
                    $this->createUserNotify($nickname, null);
                }
            }




            return $id_user;
        }

        return false;
    }




    /**
     * Update ovidentia user account from record if there is a user id in the user field
     *
     * @throw crm_ContactUserException
     *
     * @return bool
     */
    public function updateUser()
    {
        self::$directory_update = true;


        if ($this->user) {

            $infos = $this->getUserInfos();
            $error = null;
            $return = bab_updateUserById($this->user, $infos, $error);

            if ($error) {
                throw new crm_ContactUserException($error);
            }

            return $return;
        }

        return false;
    }



    /**
     * The ovidentia user or directory entry linked to this contact has been modified
     * @return crm_Contact
     */
    public function onUserModified()
    {
        $Crm = $this->Crm();

        if (self::$directory_update) {
            // when the contact is saved by crm, the user account is updated and next the bab_eventUserModified is triggered
            // the crm end up trying to update the contact. in this case the exit is here
            return;
        }

        if (isset($Crm->ContactDirectoryMap)) // if directory mapping exists in this CRM
        {
            $infos = bab_admGetDirEntry($this->user, BAB_DIR_ENTRY_ID_USER);
            $oldEmail = $this->email;
            $this->updateFromUserInfos($infos);
            $this->save();

            if($oldEmail && $oldEmail != $this->email) {
                $ML = crm_MailingList();
                if (false !== $ML)
                {
                    $oldContact = $ML->newContact();
                    $oldContact->setEmail($oldEmail);
                    $newContact = $ML->newContact();
                    $newContact->setEmail($this->email);
                    try {
                        $ML->updateContact($oldContact, $newContact);
                    } catch (LibNewsletterException $e) {
                        // if newsletter installed but not configured
                        bab_debug($e->getMessage());
                    }
                }
            }

            if ($contactOrganization = $this->getMainContactOrganization())
            {
                $contactOrganization->updateFromUserInfos($infos);
                $contactOrganization->save();
            }
        }

        // copy direntry photo to contact

        if (isset($infos['jpegphoto']['photo']) && $infos['jpegphoto']['photo'] instanceOf bab_dirEntryPhoto) {
            $entryphoto = $infos['jpegphoto']['photo'];
            /*@var $entryphoto bab_dirEntryPhoto */
            $data = $entryphoto->getData();

            $photo = $this->getPhotoPath();
            if (!$photo)
            {
                switch($entryphoto->getType())
                {
                    case 'image/jpeg':
                        $ext = 'jpg';
                        break;
                    case 'image/gif':
                        $ext = 'gif';
                        break;
                    case 'image/png':
                        $ext = 'png';
                        break;
                }

                $W = bab_Widgets();
                $imagePicker = $W->ImagePicker();


                $photo = $this->getPhotoUploadPath();
                $photo->createDir();
                $imagePicker->setFolder($photo);
                if ($h = $imagePicker->openFile('image.'.$ext, bab_charset::getIso(), 'w'))
                {
                    fwrite($h, $data);
                    fclose($h);
                }

            } else {

                file_put_contents($photo->toString(), $data);
            }

        } elseif ($photo = $this->getPhotoPath()) {
            unlink($photo->toString());
        }

        return $this;
    }



    /**
     * Update contact from user infos
     * @see self::onUserModified()
     * @param	Array	$infos
     */
    protected function updateFromUserInfos(array $infos)
    {
        $Crm = $this->Crm();
        $mapSet = $Crm->ContactDirectoryMapSet();
        $res = $mapSet->select($mapSet->type->is('contact')->_AND_($mapSet->contactfield->startsWith('Contact.')));

        foreach($res as $record)
        {


            $contactfield = explode('.',$record->contactfield);
            bab_debug($contactfield);

            $recordName = array_shift($contactfield);
            $value = $infos[$record->userfield]['value'];

            if (!isset($value) || '' === $value)
            {
                continue;
            }

            $fieldname = array_shift($contactfield);

            switch($fieldname)
            {
                case 'id':
                case 'user':
                    // never update these fields
                    break;

                case 'address':
                    $fieldname = array_shift($contactfield);

                    if ('country' === $fieldname)
                    {
                        $cSet = $Crm->CountrySet();
                        $res = $cSet->select($cSet->name_en->like($value)->_OR_($cSet->name_fr->like($value)));
                        $value = null;
                        foreach($res as $country)
                        {
                            $value = $country->id;
                        }
                    }

                    $this->address->$fieldname = $value;
                    break;


                default:
                    $this->$fieldname = $value;

                    break;
            }
        }
    }



    /**
     * Method used to get user info for the bab_updateUserById function
     *
     * @return array
     */
    protected function getUserInfos()
    {
        $output = array();

        $output['email'] 		= $this->getMainEmail();
        $output['givenname'] 	= empty($this->firstname) ? ' ' : $this->firstname;
        $output['sn'] 			= $this->lastname;

        if ($path = $this->getPhotoPath()) {
            $output['jpegphoto'] = bab_fileHandler::copy($path->toString());
        } else {
            $output['jpegphoto'] = '';
        }

        $output['mobile'] 		= $this->getMainMobilePhone();
        $output['htel']			= $this->getMainPhone();


        if ($organization = $this->getMainOrganization()) {
            /*@var $organization crm_Organization */

            $output += $organization->getUserOrganizationInfos();

        } else {

            $output['btel']				= '';
            $output['bfax']				= '';
            $output['organisationname']	= '';
            $output['bstreetaddress']	= '';
            $output['bcity']			= '';
            $output['bpostalcode']		= '';
            $output['bstate']			= '';
            $output['bcountry']			= '';
        }




        if ($address = $this->getMainAddress()) {
            /* @var $address crm_Address */

            $output['hstreetaddress']	= $address->street;
            $output['hcity']			= $address->city;

            if ($address->cityComplement) {
                $output['hcity'] .= ' '.$address->cityComplement;
            }

            $output['hpostalcode']	= $address->postalCode;
            $output['hstate']		= $address->state;

            if ($country = $address->getCountry()) {
                /* @var $orgcountry crm_Country */
                $output['hcountry']	= $country->getName();
            } else {
                $output['hcountry']	= '';
            }

        } else {

            $output['hstreetaddress']	= '';
            $output['hcity']			= '';
            $output['hpostalcode']		= '';
            $output['hstate']			= '';
            $output['hcountry']			= '';
        }

        return $output;
    }



    /**
     * Notify the user about account creation
     *
     * @param	string			$nickname
     * @param	string | null	$password		set password to null to notify without password
     *
     *
     * @return bool
     */
    protected function createUserNotify($nickname, $password)
    {
        $email = $this->getMainEmail();
        if (null === $email) {
            return;
        }

        return bab_registerUserNotify($this->getFullName(), $email, $nickname, $password);
    }




    /**
     * Test if the user associated to contact is currently connected to portal
     * return the last hit timestamp if user is logged in
     *
     * @return false | int	timestamp
     */
    public function isOnline()
    {
        if (!$this->user) {
            return false;
        }


        static $sessions = null;
        if (null === $sessions) {
            $sessions = bab_getActiveSessions($this->user);
        }

        if (0 === count($sessions)) {
            return false;
        }

        // additional verification because the id_user parameter need a version 7.3.0 and may be not taken
        foreach($sessions as $arr) {
            if ($this->user === $arr['id_user']) {
                return (int) $arr['last_hit_date'];
            }
        }

        return false;
    }



    /**
     * Checks if the user is a collaborator.
     *
     * @return bool
     */
    public function isCollaborator()
    {
        $set = $this->Crm()->ContactSet();
        $collaborators = $set->select($set->isCollaborator());

        foreach ($collaborators as $collaborator) {
            if($collaborator->id == $this->id) {
                return true;
            }
        }

        return false;
    }





    /**
     * Update properties with CSV row
     * @param	Widget_CsvRow	$row	A CSV row
     *
     *
     * @throw Widget_ImportException
     *
     * @return 	int						number of updated properties
     */
    public function import(Widget_CsvRow $row)
    {
        $up_prop = 0;


        if (isset($row->organization)) {

            if (null == $this->id) {
                $this->save();
            }

            $organizationId = null;
            $position = isset($row->position) ? $row->position : null;

            if (is_numeric($row->organization)) {
                $organizationId = $row->organization;
            } else {
                $OSet = $this->Crm()->OrganizationSet();
                $organization = $OSet->get($OSet->name->like($row->organization));
                if ($organization instanceOf crm_Organization) {
                    $organizationId = $organization->id;
                }
            }

            if ($organizationId) {
                $contactOrganization = $this->getMainContactOrganization();
                if (isset($contactOrganization)) {
                    $contactOrganization->organization =$organizationId;
                    if (isset($position)) {
                        $contactOrganization->position = $position;
                    }
                    $contactOrganization->save();
                } else  {
                    $this->addOrganization($organizationId, $position);
                }
            }
        }


        if (isset($row->title)) {
            $up_prop += $this->importProperty('title', $row->title);
        }

        if (isset($row->gender)) {
            $up_prop += $this->importProperty('gender', $row->gender);
        }

        if (isset($row->lastname)) {
            $up_prop += $this->importProperty('lastname', $row->lastname);
        }

        if (isset($row->firstname)) {
            $up_prop += $this->importProperty('firstname', $row->firstname);
        }

        if (isset($row->gender)) {
            $up_prop += $this->importProperty('gender', $row->gender);
        }

        if (isset($row->birthday)) {
            $up_prop += $this->importProperty('birthday', $row->birthday);
        }

        if (isset($row->photo)) {
            $up_prop += $this->importProperty('photo', $row->photo);
        }

        if (isset($row->user)) {
            $up_prop += $this->importProperty('user', $row->user);
        }

        // update the id address if possible
        if (!($this->address instanceOf crm_Address) && isset($row->address) && is_numeric($row->address) && $row->address > 0) {
            $up_prop += $this->importProperty('address', $row->address);
        }

        // or update the joined address if possible
        if ($this->address instanceOf crm_Address) {
            $up_prop += $this->address->import($row);

        } else if (isset($row->street) && isset($row->city)) {

            $ASet = $this->Crm()->AddressSet();
            $address = $this->getMainAddress();

            if (!isset($address)) {
                /* @var $address crm_Address */
                $address = $ASet->newRecord();
            }

            $up_prop_address = $address->import($row);
            if (0 < $up_prop_address) {

                $address->save();
                $up_prop += $up_prop_address;
            }

            $up_prop += $this->importProperty('address', $address->id);
        }

        return $up_prop;
    }


    /**
     * Get vCard as object
     *
     * @return crm_VCard
     */
    public function vCard()
    {
        require_once dirname(__FILE__).'/../vcard.class.php';
        $this->Crm()->Ui()->includeContact();

        $vCard = new crm_VCard();
        if ('0000-00-00 00:00:00' === $this->modifiedOn) {
            $vCard->setRev($this->createdOn);
        } else {
            $vCard->setRev($this->modifiedOn);
        }

        $vCard->addProperty('UID', $this->uuid);
        $vCard->setName($this->lastname, $this->firstname);

        if (isset($this->birthday) && '0000-00-00' !== $this->birthday) {
            $vCard->addProperty('BDAY', $this->birthday);
        }

        if ($title = $this->getMainPosition()) {
            $vCard->addProperty('TITLE', $title);
        }

        $vCard->addTel('WORK', $this->getMainPhone());
        $vCard->addEmail('WORK', $this->getMainEmail());


        if ($organization = $this->getMainOrganization()) {
            $vCard->setOrganization($organization->name);

            if ($workaddress = $organization->getMainAddress()) {
                $vCard->addAddress('WORK', $workaddress->street, null, $workaddress->postalCode, $workaddress->city, $workaddress->state, $workaddress->getCountry()->getName());
            }
        }

        if ($homeaddress = $this->getMainAddress()) {
            $vCard->addAddress('HOME', $homeaddress->street, null, $homeaddress->postalCode, $homeaddress->city, $homeaddress->state, $homeaddress->getCountry()->getName());
        }

// 		/* @var $T Func_Thumbnailer */
// 		$T = @bab_functionality::get('Thumbnailer');

// 		if ($T) {
// 			if ($photoPath = $this->getPhotoPath()) {
// 		 		$T->setSourceFile($photoPath->toString());
// 		 		$imagePath = $T->getThumbnailPath(64, 64);

// 		 		if ($imagePath instanceOf bab_Path) {
// 		 			$vCard->setPhoto($imagePath);
// 		 		}
// 			}
// 		}

        return $vCard;
    }



    /**
     * Select associated orders
     *
     * @return ORM_Iterator
     */
    public function selectOrders()
    {
        $Crm = $this->Crm();
        $set = $Crm->OrderSet();
        if (isset($Crm->Currency)) {
            $set->currency();
        }
        return $set->select($set->contact->is($this->id));
    }




    /**
     * Returns the contacts knowing this contact.
     *
     * @return ORM_Iterator	<crm_Contact>
     */
    public function selectContactsKnowing()
    {
        $acquaintanceSet = $this->Crm()->ContactAcquaintanceSet();
        return $acquaintanceSet->selectKnowing($this);
    }


    /**
     * @return ORM_Iterator <crm_Deal>
     */
    public function getDeals()
    {
        $dealSet = $this->Crm()->DealSet();
        $deals = $dealSet->select($dealSet->referralContact->is($this->id));

        return $deals;
    }




    /**
     * create an temporary url for password initialisation
     *
     * @throws ErrorException
     *
     * @return string
     */
    public function getPasswordTokenUrl($init = false)
    {
        $Crm = $this->Crm();

        // check if the password tocken allready exists

        $set = $Crm->PasswordTokenSet();
        $passwordToken = $set->get($set->contact->is($this->id));
        /*@var $passwordToken crm_PasswordToken */

        if (null === $passwordToken)
        {
            $passwordToken = $set->newRecord();
            $passwordToken->contact = $this->id;
            $passwordToken->createdOn = date('Y-m-d H:i:s');
        }
        else if (!$passwordToken->isValid())
        {
            $passwordToken->createdOn = date('Y-m-d H:i:s');
        }

        if ($init)
        {
            $passwordToken->createdOn = date('Y-m-d H:i:s');
        }

        $passwordToken->save();

        return $passwordToken->getUrl();
    }




    public function delete()
    {
        parent::delete();
        $Crm = $this->Crm();

        if (isset($Crm->ContactOrganization))
        {
            // Delete links with organizations.
            $contactOrganizationSet = $Crm->ContactOrganizationSet();
            $contactOrganizationSet->delete($contactOrganizationSet->contact->is($this->id));
        }

        return $this;
    }


    /**
     * Saves the fact that contact $isKnownByContact knows this contact.
     *
     * @return betom_Contact
     */
    public function setKnownBy($isKnownByContact, $level)
    {
        $Crm = $this->Crm();

        if ($isKnownByContact instanceof crm_Contact) {
            $isKnownByContactId =  $isKnownByContact->id;
        } else {
            $isKnownByContactId = $isKnownByContact;
        }
        $acquaintanceSet = $Crm->ContactAcquaintanceSet();
        $acquaintance = $acquaintanceSet->get($acquaintanceSet->contact->is($isKnownByContactId)->_AND_($acquaintanceSet->acquaintance->is($this->id)));

        if ($level == 0) {
            // if level is 0 we remove the acquaintance.
            if (isset($acquaintance)) {
                $acquaintance->delete();
            }
        } else {
            if (!isset($acquaintance)) {
                $acquaintance = $acquaintanceSet->newRecord();
            }
            $acquaintance->level = $level;
            $acquaintance->contact = $isKnownByContactId;
            $acquaintance->acquaintance = $this->id;
            $acquaintance->save();
        }
        return $this;
    }


    /**
     * Test if the associated user is valid (can log-in)
     * @return bool
     */
    public function isUserValid()
    {
        if (!$this->user) {
            return false;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';
        return bab_userInfos::isValid($this->user);
    }
}






/**
 * Exception on errors with the user creation or modification
 */
class crm_ContactUserException extends crm_SaveException {}
