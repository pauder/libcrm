<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * @property	int				$id				The team unique id
 * @property	string			$name			The team name
 * @property	crm_DealSet				$deal			The deal to which the team is associated
 * @method		crm_DealSet		deal()			The deal to which the team is associated
 *
 * @property	ORM_MysqlIterator	$members
 */
class crm_TeamSet extends crm_TraceableRecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Team');

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name')
		);

		$this->hasOne('deal', $Crm->DealSetClassName());
	}


}


class crm_Team extends crm_TraceableRecord
{

	const SUBFOLDER = 'teams';




	/**
	 * Get the upload path for files related to this team.
	 *
	 * @return bab_Path
	 */
	public function uploadPath()
	{
		if (!isset($this->id)) {
			return null;
		}

		require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

		$path = $this->Crm()->uploadPath();
		$path->push(self::SUBFOLDER);
		$path->push($this->id);
		return $path;
	}


	/**
	 * Returns an array of members.
	 *
	 * Members can be Contacts or Organizations.
	 *
	 * @return  array
	 */
	public function getMembers()
	{
		$Crm = $this->Crm();

		$contactSet = $Crm->ContactSet();
		$linkedContacts = $contactSet->selectLinkedTo($this, 'hasMember');

		$organizationSet = $Crm->OrganizationSet();
		$linkedOrganizations = $organizationSet->selectLinkedTo($this, 'hasMember');

		$members = array();
		foreach ($linkedContacts as $linkedContact) {
			$members[$linkedContact->id] = $linkedContact->targetId;
		}
		foreach ($linkedOrganizations as $linkedOrganization) {
			$members[$linkedOrganization->id] = $linkedOrganization->targetId;
		}

		return $members;
	}


	/**
	 * Returns an array of members email addresses.
	 *
	 * @return  array
	 */
	public function getMembersEmails()
	{
		$emails = array();
		$members = $this->getMembers();
		foreach ($members as $linkId => $memberLink) {

			$member = $memberLink['member'];
			$email = $member->getMainEmail();
			if (!empty($email)) {
				$emails[] = $email;
			}
		}

		return $emails;
	}


	/**
	 * Adds a contact or organization to the members of the team.
	 *
	 * @return crm_Team
	 */
	public function addMember(crm_TraceableRecord $member, $type = 'hasMember')
	{
		$member->linkTo($this, $type);
		return $this;
	}

	/**
	 * Removes a contact or organization from the members of the team.
	 *
	 * @return crm_Team
	 */
	public function removeMember(crm_TraceableRecord $member, $type = null)
	{
		$member->unlinkFrom($this, $type);
		return $this;
	}

}


