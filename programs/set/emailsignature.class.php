<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * Email signature : each user can have a signature.
 *
 * @property ORM_StringField    $name
 * @property ORM_TextField      $signature
 * @property ORM_UserField      $userid
 */
class crm_EmailSignatureSet extends crm_RecordSet
{
    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setPrimaryKey('id');
        $this->setDescription('Email signature');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('Name'),
            ORM_TextField('signature')
                ->setDescription('Signature'),
            ORM_UserField('userid')
                ->setDescription('Associated portal user id')
        );
    }

    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isReadable()
     */
    public function isReadable()
    {
        $userId = bab_getUserId();
        return $this->userid->is($userId);
    }

    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isUpdatable()
     */
    public function isUpdatable()
    {
        $userId = bab_getUserId();
        return $this->userid->is($userId);
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        $userId = bab_getUserId();
        return $this->userid->is($userId);
    }
}



/**
 * @property string     $name
 * @property string     $signature
 * @property int        $userid
 */
class crm_EmailSignature extends crm_Record
{
}
