<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * @property ORM_StringField    $name
 * @property ORM_StringField    $fieldname
 * @property ORM_TextField      $description
 * @property ORM_EnumField      $object
 * @property ORM_StringField    $view
 * @property ORM_TextField      $fields         Comma-separated field names
 * @property ORM_StringField    $classname
 * @property ORM_StringField    $sizePolicy
 * @property ORM_EnumField      $fieldsLayout
 * @property ORM_BoolField      $foldable
 * @property ORM_BoolField      $folded
 * @property ORM_StringField    $rank
 * @property ORM_TextField      $visibilityCriteria
 *
 * @method crm_CustomSection get()
 * @method crm_CustomSection request()
 * @method crm_CustomSection[]|\ORM_Iterator  select()
 * @method crm_CustomSection newRecord()
 *
 * @method Func_Crm Crm()
 */
class crm_CustomSectionSet extends crm_TraceableRecordSet
{
    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $this->setDescription('Custom section');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('Name of section used for display'),
            ORM_StringField('sectionname')
                ->setDescription('Internal name of section'),
            ORM_TextField('description')
                ->setDescription('Optional description'),
            ORM_StringField('object')
                ->setDescription('Name of object in CRM ex : Article, Contact, Organization...'),
            ORM_StringField('view')
                ->setDescription('The specific view'),
            ORM_TextField('fields')
                ->setDescription('Contained fields and their parameters'),
            ORM_StringField('sizePolicy')
                ->setDescription('Size policy'),
            ORM_StringField('classname')
                ->setDescription('Classname'),
            ORM_EnumField('fieldsLayout', crm_CustomSection::getFieldsLayouts())
                ->setDescription('Fields layout'),
            ORM_BoolField('foldable')
                ->setDescription('Foldable'),
            ORM_BoolField('folded')
                ->setDescription('Folded'),
            ORM_BoolField('editable')
                ->setDescription('Editable'),
            ORM_IntField('rank')
                ->setDescription('Rank'),
            ORM_TextField('visibilityCriteria')
                ->setDescription('Visibility criteria')
        );
    }


    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return $this->Crm()->Access()->administer();
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        if ($this->Crm()->Access()->administer()) {
            return $this->all();
        }
        return $this->none();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @property string $name
 * @property string $sectionname
 * @property string $description
 * @property string $object
 * @property string $view
 * @property string $fields             Comma-separated field names
 * @property string	$classname
 * @property string $sizePolicy
 * @property string $fieldsLayout
 * @property bool   $foldable
 * @property bool   $folded
 * @property int    $rank
 * @property string $visibilityCriteria
 *
 * @method Func_Crm Crm()
 */
class crm_CustomSection extends crm_TraceableRecord
{

    const FIELDS_LAYOUT_VERTICAL_LABEL = 'verticalLabel';
    const FIELDS_LAYOUT_HORIZONTAL_LABEL = 'horizontalLabel';
    const FIELDS_LAYOUT_WIDE_HORIZONTAL_LABEL = 'wideHorizontalLabel';
    const FIELDS_LAYOUT_VERY_WIDE_HORIZONTAL_LABEL = 'veryWideHorizontalLabel';
    const FIELDS_LAYOUT_NO_LABEL = 'noLabel';

    /**
     * @return string[]
     */
    public static function getFieldsLayouts()
    {
        static $fieldsLayouts = null;

        if (!isset($fieldsLayouts)) {
            $fieldsLayouts = array(
                self::FIELDS_LAYOUT_VERTICAL_LABEL => crm_translate('Vertical label'),
                self::FIELDS_LAYOUT_HORIZONTAL_LABEL => crm_translate('Horizontal label'),
                self::FIELDS_LAYOUT_WIDE_HORIZONTAL_LABEL => crm_translate('Horizontal label (wide)'),
                self::FIELDS_LAYOUT_VERY_WIDE_HORIZONTAL_LABEL => crm_translate('Horizontal label (very wide)'),
                self::FIELDS_LAYOUT_NO_LABEL => crm_translate('No label'),
            );
        }
        return $fieldsLayouts;
    }

    /**
     *
     * @return string[]
     */
    public function getRawFields()
    {
        if (empty($this->fields)) {
            return array();
        }
        return explode(',', $this->fields);
    }


    /**
     * Return an array containing the fields in the section and their associated parameters.
     *
     *  [
     *      [
     *          'fieldname' => the_field_name,
     *          'parameters => [
     *              'type' => the_field_type,
     *              'label' => the_field_label,
     *              'classname' => the field_classname,
     *              ...
     *           ]
     *      ],
     *      ...
     *  ]
     *
     * @return string[][]
     */
    public function getFields()
    {
        if (empty($this->fields)) {
            return array();
        }

        $fields = array();
        $rawFields = $this->getRawFields();
        foreach ($rawFields as $rawField) {
            if (empty($rawField)) {
                continue;
            }
            $params = array();

            if (strpos($rawField, ':') === false) {
                $fieldName = $rawField;
            } else {
                list($fieldName, $parameters) = explode(':', $rawField);
                $parameters = explode(';', $parameters);
                foreach ($parameters as $parameter) {
                    list($key, $value) = explode('=', $parameter);
                    $params[$key] = $value;
                }
            }

            $fields[] = array(
                'fieldname' => $fieldName,
                'parameters' => $params
            );
        }
        return $fields;
    }


    /**
     * Return an array containing the description of the specified field in the section and its associated parameters or
     * null if the field is not present in the section.
     *
     *  [
     *      'fieldname' => the_field_name,
     *      'parameters => [
     *          'type' => the_field_type,
     *          'label' => the_field_label,
     *          'classname' => the field_classname,
     *          ...
     *      ]
     *  ],
     * @param string $searchedFieldName
     * @return NULL|array[]
     */
    public function getField($searchedFieldName)
    {
        if (empty($this->fields)) {
            return null;
        }

        $rawFields = $this->getRawFields();
        foreach ($rawFields as $rawField) {
            if (empty($rawField)) {
                continue;
            }
            $params = array();

            if (strpos($rawField, ':') === false) {
                $fieldName = $rawField;
                $parameters = '';
            } else {
                list($fieldName, $parameters) = explode(':', $rawField);
            }

            if ($fieldName !== $searchedFieldName) {
                continue;
            }

            if (!empty($parameters)) {
                $parameters = explode(';', $parameters);
                foreach ($parameters as $parameter) {
                    list($key, $value) = explode('=', $parameter);
                    $params[$key] = $value;
                }
            }

            return array(
                'fieldname' => $fieldName,
                'parameters' => $params
            );
        }
        return null;
    }


    /**
     * Appends the field to the list of contained fields.
     *
     * @param string $fieldName
     * @param array  $parameters
     */
    public function addField($fieldName, $parameters = null)
    {
        $rawFields = $this->getRawFields();

        $rawField = $fieldName;
        if (is_array($parameters)) {
            $params = array();
            foreach ($parameters as $key => $value) {
                $params[] = $key . '=' . $value;
            }
            $rawField .= ':' . implode(';', $params);
        }
        array_push($rawFields, $rawField);
        $this->fields = implode(',', $rawFields);
    }


    /**
     *
     * @param string $removedFieldName
     */
    public function removeField($removedFieldName)
    {
        $rawFields = $this->getRawFields();
        $newRawFields = array();
        foreach ($rawFields as $rawField) {
            if (empty($rawField)) {
                continue;
            }
            list($fieldName, ) = explode(':', $rawField);

            if ($removedFieldName == $fieldName) {
                continue;
            }
            $newRawFields[] = $rawField;
        }

        $this->fields = implode(',', $newRawFields);
    }



    /**
     * @param crm_Record $record
     *
     * @return bool
     */
    public function isVisibleForRecord(crm_Record $record)
    {
        if (empty($this->visibilityCriteria)) {
            return true;
        }

        $recordSet = $record->getParentSet();

        $arrCriteria = json_decode($this->visibilityCriteria, true);


        foreach ($arrCriteria as $fieldName => $condition) {
            $field = $recordSet->$fieldName;
            foreach ($condition as $op => $value) {
                $criteria = $field->$op($value);
            }
        }

        $criteria = $criteria->_AND_($recordSet->id->is($record->id));

        $records = $recordSet->select($criteria);

        return ($records->count() != 0);
    }
}
