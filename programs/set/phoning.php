<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/campaign.php';


/**
 * A mailing campaing
 * campaing recipients are stored with a sent date time
 *
 */
class crm_CampaignTypePhoning extends crm_CampaignType
{



	/**
	 * @return string
	 *
	 */
	public function getDescription()
	{
		return 'Phoning campaign';
	}



	/**
	 * Fields used in each recipient of the campaign
	 *
	 * @return crm_PhoningRecipientSet
	 */
	public function CampaignRecipientSet()
	{
		return $this->Crm()->PhoningRecipientSet();
	}


	/**
	 * list recipients of a mailing, display crm contact informations
	 *
	 * @param	int				$campaign			campaign ID
	 *
	 * @return Widget_Widget
	 */
	public function getPopulateWidget($campaign, Widget_Action $editrecipient_action)
	{
		$W = bab_functionality::get('Widgets');

		//table

//		require_once dirname(__FILE__) . '/phoning.ui.class.php';

		$this->Crm()->includeContactSet();
		$this->Crm()->includeOrganizationSet();

		$set = $this->CampaignRecipientSet();

		$set->join('contact');
		$set->join('organization');

		$iterator = $set->select($set->campaign->is($campaign));

		$table = new crm_PhoningRecipientTableView;
		$table->edit_action = $editrecipient_action;
		$table->setDataSource($iterator);

		// display campaign progress

		$set = $this->Crm()->PhoningRecipientSet();
		$set->join('organization');

		$all = $set->select($set->campaign->is($campaign));
		$all = $all->count();
		$doneIterator = $set->select($set->campaign->is($campaign)->_AND_($set->calldate->is('0000-00-00 00:00:00')->_NOT()));
		$doneIterator->orderDesc($set->calldate);

		$done = $doneIterator->count();

		if ($all > 0) {
			$percent = (int) (($done * 100) / $all);
		} else {
			$percent = 0;
		}

		$head_frame1 = $W->Frame()
			->addClass('LibCampaign_gauge')
			->addClass('LibCampaign_headitem')
			->addItem($W->Label(sprintf($this->Crm()->translate('%d / %d phone calls'), $done, $all)))
			->addItem($W->Gauge()->setProgress($percent))
		;

		$head = $W->Frame(null, $W->HBoxLayout());
		$head->addItem($head_frame1);



		$doneIterator->next();
		$lastCall = $doneIterator->current();


		if (isset($lastCall)) {

			$username = $lastCall->calluser;
			if (empty($username)) {
				$username = $this->Crm()->translate('Anonymous');
			}

			$orgname = $lastCall->organization->name;

			$head_frame2 = $W->Frame()
				->addClass('LibCampaign_headitem')
				->addItem(
					$W->HBoxItems(
						$W->Title($this->Crm()->translate('Last call : '), 4),
						$W->VBoxItems(
							$W->Label($orgname)->addClass('strong'),
							$W->Label(sprintf($this->Crm()->translate('by : %s'),  $username))
						)
					)
					->setHorizontalSpacing(1, 'em')
				)
			;

			$head->addItem($head_frame2);
		}


		return $W->VBoxItems($head, $table);
	}


	/**
	 * get a widget with additional fields for recipient
	 * each campaign type may overload this method
	 *
	 * @param	crm_CampaignRecipient | null	$recipient
	 * @return	Widget_Frame
	 */
	public function getRecipientFields($recipient)
	{
		$Crm = $this->Crm();
		$frame = parent::getRecipientFields($recipient);
		$frame->getLayout()->setVerticalSpacing(3, 'em');

		$W = bab_functionality::get('Widgets');
		$set = $this->CampaignRecipientSet();

		$dates = $W->Frame(null, $W->VBoxLayout());
		$dates->getLayout()->setVerticalSpacing(1, 'em');


		if ('0000-00-00 00:00:00' === $recipient->calldate) {
			$recipient->calldate = date('Y-m-d H:i:s');
		}


		// contact modification


		$contactSet = $Crm->ContactSet();
		$contacts = $contactSet->select($contactSet->organization->is($recipient->organization));
		$options = array();
		foreach($contacts as $contact) {
			$options[$contact->id] = $contact->lastname.' '.$contact->firstname;
		}
		$select = $W->Select()
			->setName('contact')
			->setOptions($options)
			->setValue($recipient->contact);


		$taskDateLabel = $W->Label($Crm->translate('Task date'));

		$frame
			->addItem(
				$W->HBoxItems(
					crm_dateTime($set->calldate, $recipient->calldate),
					crm_LabelledWidget($Crm->translate('Set a new contact'), $select)
				)
				->setHorizontalSpacing(5, 'em')
			)
			->addItem(
				crm_LabelledWidget($Crm->translate($set->comment->getDescription()), crm_OrmWidget($set->comment)->setValue($recipient->comment))
			)
			->addItem(
				$W->HBoxItems(
					$W->VBoxItems(
						crm_LabelledWidget($Crm->translate('Create a task for call back'), $W->Checkbox()->setName('task')),
						$W->VBoxItems($taskDateLabel, crm_dateTimeField('taskdate', $taskDateLabel))
					)
					->setVerticalSpacing(1, 'em')
					,
					crm_LabelledWidget($Crm->translate('Task description'), $W->TextEdit()->setName('taskdescription')->setColumns(51))
				)
				->setHorizontalSpacing(5, 'em')
			)
		;

		return $frame;
	}


	/**
	 * Save recipient to campaign
	 * @param	int						$campaign
	 * @param	crm_CampaignRecipient	$recipient
	 */
	public function saveRecipient($campaign, crm_CampaignRecipient $recipient)
	{
		$W = bab_functionality::get('Widgets');

		if ($calldate = bab_pp('calldate')) {
			$datepart = $W->DatePicker()->getISODate($calldate['date']);
			$timepart = $calldate['time'].':00';

			if ($datepart && '0000-00-00' !== $datepart) {
				$recipient->calldate = $datepart.' '.$timepart;
			} else {
				$recipient->calldate = '0000-00-00 00:00:00';
			}
		}



		$recipient->calluser = $GLOBALS['BAB_SESS_USER'];
		$recipient->contact = (int) bab_pp('contact');
		$recipient->comment = bab_pp('comment');

		$recipient->save();


		// create task if necessary


		if ('1' === bab_rp('task')) {

			if ($taskdate = bab_pp('taskdate')) {
				$datepart = $W->DatePicker()->getISODate($taskdate['date']);
				$timepart = $taskdate['time'].':00';

				if ($datepart) {
					$taskdate = $datepart.' '.$timepart;
				}



				$Crm = bab_functionality::get('Crm');

				$taskSet = $Crm->TaskSet();
				$task = $taskSet->newRecord();

				$task->dueDate = $taskdate;
				$task->summary = $this->Crm()->CampaignSet()->get($campaign)->name;
				$task->description = bab_pp('taskdescription');
				$task->completion = 0;
				$task->responsible = $recipient->calluser;

				$task->save();

				$task->linkTo($recipient, 'campaignRecipient');

				$contact = $Crm->contactSet()->get($recipient->contact);
				$task->linkTo($contact);
			}

		}


	}




	/**
	 * return true if the campaign recipient have a field named "contact"
	 * @return bool
	 */
	public function haveContactField()
	{
		return true;
	}


	/**
	 * return true if the campaign recipient have a field named "organization"
	 * @return bool
	 */
	public function haveOrganizationField()
	{
		return true;
	}


	/**
	 * required fileds to import recipient in the phoning campaign
	 * return list of fields to import to
	 * @return array
	 */
	public function getImportFields()
	{
		return array(

			'organization' 	=> $this->Crm()->translate('Organization UUID'),
			'contact' 		=> $this->Crm()->translate('Contact UUID'),
			'uuid'			=> $this->Crm()->translate('Universally Unique IDentifier'),
			'calldate'		=> $this->Crm()->translate('Phone call date'),
			'comment'		=> $this->Crm()->translate('Comment')
		);
	}


	public function exportRecipients($campaign)
	{

		$record = $this->Crm()->campaignSet()->get($campaign);

		$arr = array(


			'organization/name' 	=> $this->Crm()->translate('Organization'),
			'contact/lastname' 		=> $this->Crm()->translate('Lastname'),
			'contact/firstname'		=> $this->Crm()->translate('Firstname'),
			'contact/phone'			=> $this->Crm()->translate('Phone'),
			'contact/email'			=> $this->Crm()->translate('Email'),
			'contact/title'			=> $this->Crm()->translate('Title'),
			'contact/gender'		=> $this->Crm()->translate('Gender'),
			'calldate'				=> $this->Crm()->translate('Phone call date'),
			'calluser'				=> $this->Crm()->translate('Phone call user'),
			'comment'				=> $this->Crm()->translate('Comment'),
			'organization/uuid' 	=> $this->Crm()->translate('Organization UUID'),
			'contact/uuid' 			=> $this->Crm()->translate('Contact UUID'),
			'uuid' 					=> $this->Crm()->translate('Universally Unique IDentifier')

		);

		$set = $this->CampaignRecipientSet()->join('organization')->join('contact');

		$this->Crm()->exportSet($set, $arr, $record->name);

	}




	/**
	 * next recipient with a task on it or null if all is done
	 *
	 * @param	int		$campaign
	 *
	 * @return int
	 */
	public function nextTaskRecipient($campaign)
	{
		$set = $this->CampaignRecipientSet();
		$nextIterator = $set->select($set->campaign->is($campaign)->_AND_($set->calldate->is('0000-00-00 00:00:00')));
		$nextIterator->orderDesc($set->modifiedOn);

		foreach($nextIterator as $recipient) {
			return (int) $recipient->id;
		}

		return null;
	}
}
