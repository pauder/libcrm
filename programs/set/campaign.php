<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 *
 */
class crm_CampaignType
{


	private $Crm = null;


	/**
	 * @return string
	 *
	 */
	public function getDescription()
	{
		return get_class($this);
	}


	public function __construct(Func_Crm $Crm = null)
	{
		$this->setCrm($Crm);
	}


	/**
	 * Fields used in earch recipient of the campaign
	 * This method must be overloaded by inherited class
	 * @return crm_CampaignRecipientSet
	 */
	public function CampaignRecipientSet()
	{
		require_once dirname(__FILE__) . '/campaignrecipient.class.php';
		return new crm_CampaignRecipientSet;
	}


	/**
	 *
	 * @param Func_Crm $Crm
	 * @return crm_CampaignType
	 */
	public function setCrm(Func_Crm $crm = null)
	{
		$this->Crm = $Crm;
		return $this;
	}


	/**
	 *
	 * @return Func_Crm
	 */
	protected function Crm()
	{
		if (null === $this->Crm) {
			throw new Exception('missing Crm');
		}


		return $this->Crm;
	}





	/**
	 * Get widget to display in campaign list, the widget must link to the $populate_action page
	 * each campaign type may overload this method
	 * do not call this method directly for external usage
	 *
	 * @see Func_Campaign::getListWidget()			use this method to load the entire table instead
	 *
	 *
	 * @param	Widget_Action	$populate_action	page with the list of recipients
	 * @param	ORM_Record		$record				Record of the campaign
	 *
	 *
	 * @return Widget_Item
	 *
	 */
	public function getPopulateLinkWidget(Widget_Action $populate_action, ORM_Record $record)
	{
		$Crm = $this->Crm();
		$W = bab_functionality::get('Widgets');
		$W->includePhpClass('Widget_Icon');

		$icon = $W->Icon($Crm->translate('Edit recipient(s)'), Func_Icons::ACTIONS_USER_NEW);

		$set = $this->CampaignRecipientSet();
		$set = $set->join('campaign');
		$iterator = $set->select($set->campaign->is($record->id));
		$recipients = $iterator->count();

		return $W->VBoxItems(
			$W->Label(sprintf($Crm->translate('%s recipients'), $recipients)),
			$W->Link($icon, $populate_action)
		);
	}



	/**
	 * list recipients of a campaign
	 * each campaign type may overload this method
	 * the default list view print all collumns not freign keys and not primary keys
	 *
	 * @param	int				$campaign					campaign ID
	 * @param	Widget_Action	$editrecipient_action
	 *
	 * @return Widget_Widget
	 */
	public function getPopulateWidget($campaign, Widget_Action $editrecipient_action)
	{

		require_once dirname(__FILE__) . '/ui.class.php';
//		$this->Crm()->Ui()->
		$set = $this->CampaignRecipientSet();
		$iterator = $set->select($set->campaign->is($campaign));

		$table = new crm_RecipientTableView;
		$table->edit_action = $editrecipient_action;
		$table->setDataSource($iterator);

		return $table;
	}

	/**
	 * get a widget with additional fields for recipient
	 * each campaign type may overload this method
	 *
	 * @param	crm_CampaignRecipient | null	$recipient
	 * @return	Widget_Frame
	 */
	public function getRecipientFields($recipient)
	{
		$W = bab_functionality::get('Widgets');
		return $W->Frame(null, $W->VBoxLayout());
	}


	/**
	 * add a recipient to campaign
	 *
	 * @param	int		$campaign
	 * @param	int		$contact		contact id if the recipient is linked to a contact
	 * @param	int		$organization	organization id if the recipient is linked to a organization
	 *
	 * @return	crm_CampaignRecipient
	 */
	final public function addRecipient($campaign, $contact = null, $organization = null)
	{
		if (empty($contact) && empty($organization)) {
			throw new Exception('need at least one parameter, contact or organization');
			return null;
		}

		$set = $this->CampaignRecipientSet();
		$criteria = $set->campaign->is($campaign);
		$newrecipient = $set->newRecord();

		if ($this->haveOrganizationField()) {
			$criteria = $criteria->_AND_($set->organization->is($organization));
			$newrecipient->organization = $organization;
		}

		if ($this->haveContactField()) {
			$criteria = $criteria->_AND_($set->contact->is($contact));
			$newrecipient->contact = $contact;
		}

		$recipient = $set->get($criteria);

		if (null !== $recipient) {
			// a recipient already exists for this parameters
			return null;
		}


		$newrecipient->campaign = $campaign;
		$newrecipient->save();

		return $newrecipient;
	}


	/**
	 * Save recipient to campaign
	 * @param	int						$campaign
	 * @param	crm_CampaignRecipient	$recipient
	 */
	public function saveRecipient($campaign, crm_CampaignRecipient $recipient)
	{

	}




	/**
	 * Get inherited object
	 * @param	int	$campaign
	 *
	 */
	public static function type($campaign)
	{
		$C = bab_functionality::get('Campaign');

		$campaignRecord = $C->CampaignSet()->get($campaign);
		return $C->getFromType($campaignRecord);
	}


	/**
	 * return true if the campaign recipient have a field named "contact"
	 * @return bool
	 */
	public function haveContactField()
	{
		return false;
	}


	/**
	 * return true if the campaign recipient have a field named "organization"
	 * @return bool
	 */
	public function haveOrganizationField()
	{
		return false;
	}

	/**
	 * return list of fields to import to
	 * @return array
	 */
	public function getImportFields()
	{
		return array();
	}


	public function exportRecipients($campaign)
	{

	}


	/**
	 * Delete recipient row
	 *
	 */
	public function deleteRecipient($recipient)
	{
		$set = $this->CampaignRecipientSet();
		$set->delete($set->id->is($recipient));
	}

	/**
	 * next recipient with a task on it or null if all is done
	 *
	 * @param	int		$campaign
	 *
	 * @return int
	 */
	public function nextTaskRecipient($campaign)
	{
		return null;
	}
}
