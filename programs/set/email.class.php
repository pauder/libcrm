<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * @property    ORM_StringField $fromaddress
 * @property    ORM_StringField $fromname
 * @property    ORM_StringField $replyTo
 * @property    ORM_TextField   $recipients
 * @property    ORM_TextField   $ccRecipients
 * @property    ORM_TextField   $bccRecipients
 * @property    ORM_StringField $subject
 * @property    ORM_TextField   $body
 * @property    ORM_TextField   $altbody
 * @property    ORM_StringField $status
 * @property    ORM_StringField $hash
 * @property    ORM_StringField $message_id
 * @property    crm_MailBoxSet  $mailbox
 * @property    ORM_BoolField   $private
 *
 * @method crm_Email newRecord()
 * @method crm_Email get()
 * @method crm_Email[] select()
 */
class crm_EmailSet extends crm_TraceableRecordSet
{
    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setPrimaryKey('id');
        $this->setDescription('Email');

        $this->addFields(
            ORM_StringField('fromaddress'),
            ORM_StringField('fromname'),
            ORM_StringField('replyTo')
              ->setDescription('Reply to'),
            ORM_TextField('recipients')
                ->setDescription('Recipients'),
            ORM_TextField('ccRecipients')
                ->setDescription('CC Recipients'),
            ORM_TextField('bccRecipients')
                ->setDescription('BCC Recipients'),
            ORM_StringField('subject')
                ->setDescription('Subject'),
            ORM_TextField('body')
                ->setDescription('Message body'),
            ORM_TextField('altbody')
                ->setDescription('Alternate text/plain message body'),
            ORM_StringField('status')
                ->setDescription('Status'),
            ORM_StringField('hash')
                ->setDescription('Mail spooler hash'),
            ORM_StringField('message_id')
                ->setDescription('Message-ID header content'),
            ORM_BoolField('private')
                ->setDescription('Private')
        );


        if (isset($Crm->MailBox)) {
            $this->hasOne('mailbox', $Crm->MailBoxSetClassName());
        }
    }



	public function importFromImapMail(LibImap_Mail $imapMail)
	{
		/* @var $email crm_Email */
		$email = $this->get($this->message_id->is($imapMail->getMessageID()));
		if (!$email) {
			$email = $this->newRecord();
		}
		$email->setValuesFromImap($imapMail);

		$email->save(true); // do not trace

		return $email;
	}


}


/**
 * @property    string      $fromaddress
 * @property    string      $fromname
 * @property    string      $replyTo
 * @property    string      $recipients
 * @property    string      $ccRecipients
 * @property    string      $bccRecipients
 * @property    string      $subject
 * @property    string      $body
 * @property    string      $altbody
 * @property    string      $status
 * @property    string      $hash
 * @property    string      $message_id
 * @property    crm_MailBox $mailbox
 * @property    bool        $private
 */
class crm_Email extends crm_TraceableRecord
{
	public $mail;
	public $nbRecipientsByMail;

	public $lb;
	public $stack;
	public $debug;
	public $log;
	public $statusMessages;

	/**
	 * use the mail template of this skin
	 * @var string
	 */
	private $_skin_mail_template = null;

	const SUBFOLDER = 'emails';




	/**
	 * Sets the values of the email from the values of the specified imap email.
	 *
	 * @param LibImap_Mail $imapMail
	 * @return crm_Email
	 */
	public function setValuesFromImap(LibImap_Mail $imapMail)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/calincl.php';

		$this->createdOn = $imapMail->getDate();
		$this->createdBy = bab_getUserIdByEmailAndName($imapMail->getFromAddress(), $imapMail->getFromName());

		$this->modifiedOn = $this->createdOn;
		$this->modifiedBy = $this->createdBy;

		$this->fromaddress = $imapMail->getFromAddress();
		$this->fromname = $imapMail->getFromName();
		$this->replyTo = implode(', ',array_keys($imapMail->getReplyTo()));
		$this->recipients = implode(', ',array_keys($imapMail->getTo()));
		$this->ccRecipients = implode(', ',array_keys($imapMail->getCc()));
		$this->subject = $imapMail->getSubject();
		$this->body = $imapMail->getHtmlContent(); // strip embeded images
		$this->message_id = $imapMail->getMessageID();
		//		$this->mailbox = $this->id;

		$this->save(true); // do not trace

		$attachments = $imapMail->getAttachments();
		foreach ($attachments as $attachement) {
			/* @var $attachement LibImap_MailAttachment */
			$this->importAttachment($attachement->filename, $attachement->data);
		}

		return $this;
	}





	/**
	 * Use mailtemplate from skin defined in site
	 * @return crm_Email
	 */
	public function setSiteSkin()
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';
	    $settings = bab_getInstance('bab_Settings');
	    /*@var $settings bab_Settings */
	    $set = $settings->getSiteSettings();

		$this->setSkinName($set['skin']);
	}




	/**
	 * Set a skin name to use the coresponding mail template
	 * @param	string	$name
	 * @return crm_Email
	 *
	 */
	public function setSkinName($name)
	{
		$this->_skin_mail_template = $name;
		return $this;
	}


	protected function getEmailTemplate($body)
	{
		if (isset($this->_skin_mail_template))
		{
		    require_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';

			$skin = new bab_skin($this->_skin_mail_template);
			$mailer = bab_mail();

			if ($skin->isAccessValid() && $mailer)
			{
				global $babSkin,$babStyle;

				$skinname = $babSkin;
				$stylesname = $babStyle;

				// set skin
				bab_skin::applyOnCurrentPage($this->_skin_mail_template, null);

				$html = $mailer->mailTemplate($body);

				// restore skin
				if (isset($skinname)) {
				    bab_skin::applyOnCurrentPage($skinname, $stylesname);
				}

				return $html;
			}
		}



		$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
			<head></head>
			<body>
			<div align="left">' . $body . '</div>
			</body>
			</html>';


		return $html;
	}


	/**
	 * Checks if the email server has been configured on the portal.
	 *
	 * @return bool False if the email server is not configured or not activated.
	 */
	public static function emailServerIsConfigured()
	{
		global $babBody;

		if (empty($babBody->babsite['mailfunc'])) {
			return false;
		}
		return true;
	}


	/**
	 * Get the upload path for files related to this email.
	 *
	 * @return bab_Path
	 */
	public function uploadPath()
	{
		if (!isset($this->id)) {
			return null;
		}

		require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

		$path = $this->Crm()->uploadPath();
		$path->push(self::SUBFOLDER);
		$path->push($this->id);
		return $path;
	}


	/**
	 * @param ORM_RecordSet $oParentSet The set connected to this record
	 */
	function __construct(ORM_RecordSet $oParentSet)
	{
		parent::__construct($oParentSet);

		$this->nbRecipientsByMail = 1;
		$this->lb = "\n";
		$this->stack = array();
		$this->debug = array();
		$this->statusMessages = array();
		$this->log = '';
	}

	/**
	 *
	 * @param string	$emailAddress
	 * @return bool
	 */
	static public function emailAddressIsValid($emailAddress)
	{
		return (strpos($emailAddress, '@') !== false);
	}


	public function save($noTrace = false)
	{
	    // dans certains cas, on ne veut pas ecraser la variable body deja enregistree car elle n'est pas la meme que cette envoyee (mot de passe cache par des *)
	    // donc on ne modifie jamais le body si il a deja ete enregistree une fois
	    if ($this->id)
	    {
	        $this->body = null;
	    }

	    parent::save($noTrace);
	}



	/**
	 * Sends the email.
	 *
	 * @return bool
	 */
	public function send()
	{
		include_once $GLOBALS['babInstallPath'] . 'utilit/mailincl.php';

		$mailer = bab_mail();

		if (!$mailer) {
			$this->status = 'Mail server not configured / activated.';
			$this->save();
			return false;
		}

		if (!empty($this->fromaddress))
		{
			$fromname = isset($this->fromname) ? $this->fromname : '';
			$mailer->mailFrom($this->fromaddress, $fromname);

		} else if ($GLOBALS['BAB_SESS_LOGGED']) {

			$mailer->mailFrom($GLOBALS['BAB_SESS_EMAIL'], $GLOBALS['BAB_SESS_USER']);

			$this->fromaddress = $GLOBALS['BAB_SESS_EMAIL'];
			$this->fromname = $GLOBALS['BAB_SESS_USER'];
		}
		if (!empty($this->replyTo)) {
    		$mailer->clearReplyTo();
    		$mailer->mailReplyTo($this->replyTo);
		}
		$mailer->mailSubject($this->subject);

		$html = $this->getEmailTemplate($this->body);
		$mailer->mailBody($html, 'text/html');
		$mailer->mailAltBody($this->altbody);

		$mailer->clearAllRecipients();

		$recipients = preg_split('/[\s]*[,;][\s]*/', $this->recipients);
		foreach ($recipients as $address) {
			if (self::emailAddressIsValid($address)) {
				$mailer->mailTo($address);
			}
		}
		$recipients = preg_split('/[\s]*[,;][\s]*/', $this->ccRecipients);
		foreach ($recipients as $address) {
			if (self::emailAddressIsValid($address)) {
				$mailer->mailCc($address);
			}
		}
		$recipients = preg_split('/[\s]*[,;][\s]*/', $this->bccRecipients);
		foreach ($recipients as $address) {
			if (self::emailAddressIsValid($address)) {
				$mailer->mailBcc($address);
			}
		}

		if ($attachments = $this->attachments()) {
			foreach ($attachments as $attachment) {
				$mailer->mailFileAttach($attachment->getFilePath()->toString(), $attachment->toString(), '');
			}
		}



		$Spooler = @bab_functionality::get('Mailspooler');
		/*@var $Spooler Func_Mailspooler */
		if ($Spooler)
		{
			$this->hash = $Spooler->save($mailer);
		}



		$return = $mailer->send();

		if ($return) {
			$this->status = 'Sent';
			$this->message_id = $mailer->getMessageId();

		} else {
			$this->statusMessages[] = $mailer->mail->ErrorInfo;
			$this->status = implode("\n", $this->statusMessages);
		}



		$this->save();

		return $return;
	}






	/**
	 * Attach a file to the email.
	 * If the file is provided by a filepicker widget (Widget_FilePickerItem), the file will be moved into email upload path.
	 * If the file is provided with a path (bab_Path), the file will be copied into email upload path.
	 *
	 *
	 * @see Widget_FilePickerIterator
	 *
	 * @param	Widget_FilePickerItem | bab_Path	$file
	 *
	 *
	 */
	public function attachFile($file)
	{
		$uploadPath = $this->uploadPath();
		$uploadPath->createDir();

		if ($file instanceof Widget_FilePickerItem)
		{
			$original = $file->getFilePath()->toString();
			$uploadPath->push(basename($original));

			rename($original, $uploadPath->toString());

		} else if ($file instanceof bab_Path)
		{
			// encode file into uploadPath, the source path must be encoded in ovidentia database charset
			$W = bab_Widgets();
			$filePicker = $W->FilePicker()->setFolder($uploadPath);
			$filePicker->importFile($file, bab_Charset::getIso());

		} else {
			throw new ErrorException('wrong parameter type');
		}


	}

	/**
	 * Import data into an attachment
	 *
	 * @param string $filename
	 * @param string $data		Binary
	 */
	public function importAttachment($filename, $data)
	{
		$uploadPath = $this->uploadPath();

		$W = bab_Widgets();
		$filePicker = $W->FilePicker()->setFolder($uploadPath);
		$h = $filePicker->openFile($filename,bab_charset::getIso(), 'w');
		fputs($h, $data);
		fclose($h);
	}


	/**
	 * get iterator with attachements
	 * @return Widget_FilePickerIterator
	 */
	public function attachments()
	{
		$uploadPath = $this->uploadPath();
		if (!isset($uploadPath)) {
			return array();
		}
		$filePicker = bab_Widgets()->FilePicker();
		$filePickerIterator = $filePicker->getFolderFiles($uploadPath);
		return $filePickerIterator;
	}


	/**
	 * @return array
	 */
	public function getReferences()
	{
		$Crm = $this->Crm();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this);

		$ref = array();
		foreach ($links as $link) {
			/*@var $link crm_Link */
			$ref[] = $link->getSource()->getRef();
		}

		return $ref;
	}


	/**
	 * Copy links of current email
	 * @param crm_Email $record
	 */
	public function copyLinksTo(crm_Email $record)
	{
		$Crm = $this->Crm();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this);

		foreach ($links as $link) {
			/*@var $link crm_Link */

			// tester si le lien existe deja
			$exists = $linkSet->select(
				$linkSet->targetClass->is(get_class($record))
				->_AND_($linkSet->targetId->is($record->id))
				->_AND_($linkSet->sourceClass->is($link->sourceClass))
				->_AND_($linkSet->sourceId->is($link->sourceId))
				->_AND_($linkSet->type->is($link->type))
			);

			if ($exists->count() > 0)
			{
				continue;
			}


			$newLink = $linkSet->newRecord();
			/*@var $newLink crm_Link */

			$newLink->targetClass = get_class($record);
			$newLink->targetId = $record->id;
			$newLink->sourceClass = $link->sourceClass;
			$newLink->sourceId = $link->sourceId;
			$newLink->type = $link->type;

			$newLink->save();
		}
	}



	/**
	 * @return array of crm_Contact
	 */
	public function getLinkedContacts($linkType = null)
	{
		$Crm = $this->Crm();
		$Crm->includeContactSet();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this, $Crm->ContactClassName(), $linkType);

		$contacts = array();
		foreach ($links as $link) {
			$contacts[] = $link->sourceId;
		}

		return $contacts;
	}

	/**
	 * @return array of crm_Organization
	 */
	public function getLinkedOrganizations($linkType = null)
	{
		$Crm = $this->Crm();

		if (!isset($Crm->Organization))
		{
			return array();
		}

		$Crm->includeOrganizationSet();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this, $Crm->OrganizationClassName(), $linkType);

		$orgs = array();
		foreach ($links as $link) {
			if ($link->sourceId) {
				$orgs[] = $link->sourceId;
			}
		}

		return $orgs;
	}

	/**
	 * @return array of crm_Deal
	 */
	public function getLinkedDeals($linkType = null)
	{
		$Crm = $this->Crm();

		if (!isset($Crm->Deal))
		{
			return array();
		}

		$Crm->includeDealSet();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this, $Crm->DealClassName(), $linkType);

		$deals = array();
		foreach ($links as $link) {
			$deals[] = $link->sourceId;
		}

		return $deals;
	}


	/**
	 * @return array of crm_Team
	 */
	public function getLinkedTeams($linkType = 'requiresTask')
	{
		$Crm = $this->Crm();

		if (!isset($Crm->Team))
		{
			return array();
		}

		$Crm->includeTeamSet();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this, $Crm->TeamClassName(), $linkType);

		$teams = array();
		foreach ($links as $link) {
			$teams[] = $link->sourceId;
		}

		return $teams;
	}



}

