<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
* ArticleLink database
*
* @property ORM_EnumField				$type
* @property crm_ArticleSet				$article_from
* @property crm_ArticleSet				$article_to
*/
class crm_ArticleLinkSet extends crm_TraceableRecordSet
{

	public function __construct(Func_Crm $Crm)
	{
		parent::__construct($Crm);
		
		$this->setDescription('Article link');
		
		$this->addFields(
			ORM_EnumField('type', crm_ArticleLinkSet::getTypes())
		);
		
		$this->hasOne('article_from', $Crm->ArticleSetClassName());
		$this->hasOne('article_to', $Crm->ArticleSetClassName());
	}
	
	
	
	public static function getTypes()
	{
		return array(
			'I' => crm_translate('Up-sell'),//ventes incitatives
			'C' => crm_translate('Cross-sell'),//ventes croisees
			'A' => crm_translate('Related product')//apparentes
		);
	}
}



class crm_ArticleLink extends crm_TraceableRecord
{
}