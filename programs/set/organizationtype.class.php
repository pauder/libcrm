<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_OrganizationType may be a company, association...
 *
 * @property ORM_StringField    $name
 * @property ORM_BoolField      $isCustomer
 * @property ORM_BoolField      $isSupplier
 */
class crm_OrganizationTypeSet extends crm_TraceableRecordSet
{
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setDescription('Organization type');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('Name'),
            ORM_BoolField('isCustomer')
                ->setOutputOptions($Crm->translate('No'), $Crm->translate('Yes'))
                ->setDescription('Customer'),
            ORM_BoolField('isSupplier')
                ->setOutputOptions($Crm->translate('No'), $Crm->translate('Yes'))
                ->setDescription('Supplier')
        );
    }
}


/**
 * A crm_OrganizationType may be a company, association...
 *
 * @property string     $name
 * @property bool       $isCustomer
 * @property bool       $isSupplier
 */
class crm_OrganizationType extends crm_TraceableRecord
{

}
