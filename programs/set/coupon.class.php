<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/discountbase.class.php';



/**
 * Coupon set
 *
 * @property ORM_StringField				$code
 * @property ORM_BoolField					$unique
 * @property ORM_IntField                   $maxuse
 * @property ORM_StringField                $email
 * @property ORM_BoolField					$generated
 * @property ORM_IntField                   $quantity
 * @property crm_ArticlePackagingSet		$articlepackaging
 * @property crm_OrderItemSet				$orderitem
 * @property crm_ContactSet                 $contact
 * 
 * @method crm_Coupon newRecord()
 * @method crm_Coupon[] select()
 * @method crm_Coupon get()
 * @method crm_Coupon request()
 */
class crm_CouponSet extends crm_DiscountBaseSet
{

	

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Coupon');
		$this->setPrimaryKey('id');
		

		$this->addFields(
			ORM_StringField('code')
					->setDescription('Code'),
			ORM_BoolField('unique')
					->setDescription('Disable multiple use'),
		    ORM_IntField('maxuse')
		            ->setDescription('maxuse (replace unique)'),
			ORM_BoolField('generated')
					->setDescription('Automaticaly generated'),
		    ORM_IntField('quantity')
		            ->setDescription('articlepackaging quantity'),
		    ORM_StringField('email')
		            ->setDescription('Only valid for one email address')
		);
		
		if (isset($Crm->ArticlePackaging)) {
		    $this->hasOne('articlepackaging', $Crm->ArticlePackagingClassName()); 	// this coupon offer the product in shopping cart
		}
		
		$this->hasOne('orderitem', $Crm->OrderItemSetClassName()); 					// this coupon has been paid by a customer for a gift
		
		$this->hasOne('contact', $Crm->ContactSetClassName());                    // only valid for one contact
	}
	
	
	/**
	 * create a random coupon code
	 * @return string
	 */
	private function newCode()
	{
		$chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
		$max = strlen($chars)-1;
		$code = '';
		for ($i = 0; $i < 4; $i++) {
			$code .= $chars[mt_rand(0,$max)];
		}
		
		return $code;
	}

	
	/**
	 * Create a not saved random coupon based on a number of days of validity
	 * @param int $duration		validity duration in days
	 * @return crm_Coupon
	 */
	public function createRandom($duration)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		do {
			$code = $this->newCode();
			$test_record = $this->get($this->code->is($code));
		
		} while (null !== $test_record);
		
		$record = $this->newRecord();
		
		$record->code = $code;
		$record->start_date = date('Y-m-d');
		$end_date = BAB_DateTime::now();
		$end_date->add($duration, BAB_DATETIME_DAY);
		$record->end_date = $end_date->getIsoDate();
		$record->generated =1;
		
		return $record;
	}
	
	
	public function getThresholdTypes()
	{
	    $Crm = $this->Crm();
	    
	    return array(
	        'amount' => $Crm->Ui()->Euro(),
	        'weight' => 'Kg'
	    );
	}
}


/**
 * Coupon
 *
 * @property string				        $code
 * @property bool					    $unique
 * @property int					    $maxuse
 * @property string                     $email
 * @property int    					$generated
 * @property int                        $quantity
 * @property crm_ArticlePackaging		$articlepackaging
 * @property crm_OrderItem				$orderitem
 * @property crm_Contact                $contact
 */
class crm_Coupon extends crm_DiscountBase
{

    
    
    /**
     * Discount value from a product in shopping cart
     * @param crm_ShoppingCart $cart
     * @return float
     */
    protected function getProductValue(crm_ShoppingCart $cart)
    {
        $Crm = $this->Crm();
        
        // le produit offert est obligatoirement dans son conditionement principal
        // le produit doit exister dans le panier
        	
        $set = $Crm->ShoppingCartItemSet();
        $set->catalogitem();
        $set->articlepackaging();
        $set->articlepackaging->article();
        	
        $scitem = $set->get(
            $set->articlepackaging->id->is($this->articlepackaging->id)
            ->_AND_($set->articlepackaging->article->is($this->articlepackaging->article->id))
            ->_AND_($set->catalogitem->article->is($this->articlepackaging->article->id))
            ->_AND_($set->cart->is($cart->id))
            );
        	
        $value = null;
        
        if (null !== $scitem && $scitem->quantity >= 1) {
            $value = min($this->quantity, $scitem->quantity) * $scitem->articlepackaging->getUnitCostTI();
        }

        return $value;
    }
    
    
    
    
    /**
     * Add article if possible
     * @return crm_ShoppingCartItem
     */
    public function addArticleTo(crm_ShoppingCart $cart)
    {
        $id = $this->getFkPk('articlepackaging');
        
        if (!$id) {
            return null;
        }
        
        if (crm_ShoppingCart::EDIT !== (int) $cart->status) {
            return null;
        }
        
        
        $Crm = $this->Crm();
    
        $apSet = $Crm->ArticlePackagingSet();
        $apSet->article();
        $apSet->packaging();
    
        // The shopping cart is modifiable, add the product
    
        $articlePackagingRecord = $apSet->get($id);
        /*@var $articlePackagingRecord crm_ArticlePackaging */
        
        if ($cart->contains($articlePackagingRecord->article)) {
             return null;
        }
        
        $catalogItemRecord = $articlePackagingRecord->article->getCatalogItem();
    
        return $cart->addCatalogItem($catalogItemRecord, $articlePackagingRecord, $this->quantity);
    
    }
    
    
    /**
     * Get the discouted value as string
     * @return string
     */
    public function getDiscount()
    {
        $Crm = $this->Crm();
        
        if ($this->getFkPk('orderitem'))
        {
            // this coupon has been sold
            $value = $this->orderitem()->getTotalTI();
            return sprintf('%s %s', $Crm->numberFormat($value), $Crm->Ui()->Euro());
        }
        
        
        if ($this->getFkPk('articlepackaging'))
        {
            return $Crm->translate('A product');
        }
        
        return parent::getDiscount();
    }
    
    
    
    protected function testOwner(crm_ShoppingCart $cart)
    {
        $couponContact = $this->getFkPk('contact');
        $cartContact = $cart->getFkPk('contact');
        if ($couponContact && $couponContact !== $cartContact) {
            return false;
        }
        
        if ($email = $cart->getEmail()) {
            if ($email && $this->email && $email !== $this->email) {
                return false;
            }
        }
        
        return true;
    }
    
    
    
	/**
	 * Get the amount to add to the shopping cart
	 * the amount is tax included
	 * 
	 * @throws crm_CouponException
	 * 
	 * @param	crm_ShoppingCart		$cart
	 * @param	stdClass				$max		Total amount in shopping cart, preceding coupons and reductions allready included
	 *                                              contain 2 properties: total, shipping
	 * @return float (negative value)
	 */
	public function getAmount(crm_ShoppingCart $cart, stdClass $max)
	{
		$Crm = $this->Crm();
		
		if (isset($Crm->ArticlePackaging)) {
    		if (!($this->articlepackaging instanceof crm_ArticlePackaging))
    		{
    			throw new Exception('Missing join on articlepackaging');
    		}
    		
    		if (!($this->articlepackaging->article instanceof crm_Article))
    		{
    			throw new Exception('Missing join on article');
    		}
		}
		
		if (!($this->orderitem instanceof crm_OrderItem))
		{
			throw new Exception('Missing join on orderItem');
		}
		
		
		$max_coupon_value = $max->total;
		if ($this->shipping) {
		    $max_coupon_value = $max->shipping;
		}
		
		
		if ($this->orderitem->id)
		{
			// this coupon has been sold
			$value = $this->orderitem->getTotalTI();
			
		} 
		else if (isset($Crm->ArticlePackaging) && $this->articlepackaging->article->id)
		{ 
			$value = $this->getProductValue($cart);
			
			if (null === $value) {
			    throw new crm_CouponException($Crm->translate('The product does not exists in shopping cart'));
			}
			
		} else {
			
			$value = $this->getInDiscountValue($max_coupon_value);
		}
		
		if (!$this->testOwner($cart)) {
		    throw new crm_CouponException($Crm->translate('This coupon is not appliquable on your account'));
		}
		

		if (!$this->testThreshold($cart, $max)) {
		    $formatedThreshold = $Crm->numberFormat($this->threshold).bab_nbsp().$Crm->Ui()->Euro();
		    throw new crm_CouponException(sprintf($Crm->translate('This coupon is applicable only from a total amount of %s'), $formatedThreshold));
		}
		
		if ($value > $max_coupon_value) {
		    $formatedValue = $Crm->numberFormat($value).bab_nbsp().$Crm->Ui()->Euro();
		    if ($this->shipping) {
		        $message = $Crm->translate('The total of your shipping costs does not allow the use of this coupon with a value of %s');
		    } else {
		        $message = $Crm->translate('The total of your shopping cart does not allow the use of this coupon with a value of %s');
		    }
			throw new crm_CouponException(sprintf($message, $formatedValue));
		}
		
		return (-1 * $value);
	}
	
	
	
	
	/**
	 * Select consumed usages 
	 * @param string $email
	 * @return crm_CouponUsage[]
	 */
	public function selectUsages($email = null)
	{
	    $Crm = $this->Crm();
	    $set = $Crm->CouponUsageSet();
	    $set->coupon();
	    
	    $criteria = $set->coupon->id->is($this->id)->_AND_($set->consumed->is(1));
	    
	    if (isset($email)) {
	        $criteria = $criteria->_AND_($set->email->is($email));
	    }
	    
	    return $set->select($criteria);
	}

	
}



/**
 * Exception throwed when a coupon is not valid
 * Message should be used in the shopping cart UI
 */
class crm_CouponException extends crm_DiscountBaseException
{
	
}