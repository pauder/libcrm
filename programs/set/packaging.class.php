<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Packaging unit used of sellable items (crm_Article)
 *
 * @property ORM_PkField $id
 * @property ORM_StringField $name
 * @property ORM_TextField $description
 * @property ORM_IntField $min
 * 
 * @method crm_Packaging get()
 * @method crm_Packaging newRecord()
 * @method crm_Packaging[] select()
 */
class crm_PackagingSet extends crm_TraceableRecordSet
{
	public function __construct(Func_Crm $Crm)
	{
		parent::__construct($Crm);
	
		$this->setDescription('Packaging');
		$this->setPrimaryKey('id');
	
		$this->addFields(
				ORM_StringField('name')
					->setDescription('Name of packaging'),
	
				ORM_TextField('description')
					->setDescription('Generic description field'),
		    
		        ORM_IntField('min')
					->setDescription('Shopping cart item minimum quantity')
		);
	}
	
	
	
	public function getOptions()
	{
		$return = array('' => '');
		
		$res = $this->select();
		foreach($res as $packaging)
		{
			$return[$packaging->id] = bab_abbr($packaging->name, BAB_ABBR_FULL_WORDS, 50);
		}
		
		return $return;
	}
}

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $min
 */
class crm_Packaging extends crm_TraceableRecord
{
    
    /**
     * Get minimal quantity for an order in shopping cart item
     * @return int
     */
	public function getMinimalQuantity()
	{
	    return (int) $this->min;
	}
}