<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_Note is a text note that can be associated to any record.
 *
 * @property ORM_TextField		$summary
 * @property ORM_BoolField		$private
 * @property ORM_BoolField		$pinned
 */
class crm_NoteSet extends crm_TraceableRecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Note');

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_TextField('summary')
					->setDescription('Summary'),
			ORM_BoolField('private')
					->setDescription('Private'),
			ORM_BoolField('pinned')
					->setDescription('Pinned')
		);

	}


	/**
	 * Returns an iterator of notes linked to the specified source,
	 * optionally filtered on the specified link type.
	 *
	 * @return ORM_Iterator
	 */
	public function selectLinkedTo($source, $linkType = 'hasNote')
	{
		return parent::selectLinkedTo($source, $linkType);
	}
}


/**
 * A crm_Note is a text note that can be associated to any record.
 *
 * @property string		$summary
 * @property bool		$private
 * @property bool		$pinned
 */
class crm_Note extends crm_TraceableRecord
{
	const SUBFOLDER = 'notes';



	/**
	 * Get the upload path for files related to this note.
	 *
	 * @return bab_Path
	 */
	public function uploadPath()
	{
		if (!isset($this->id)) {
			return null;
		}

		require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

		$path = $this->Crm()->uploadPath();
		$path->push(self::SUBFOLDER);
		$path->push($this->id);
		return $path;
	}


	/**
	 *
	 */
	public function linkTo(crm_Record $source, $linkType = 'hasNote')
	{
		parent::linkTo($source, $linkType);
	}



	/**
	 * attach a file to note
	 * file is provided by a filepicker widget
	 *
	 * @see Widget_FilePickerIterator
	 *
	 * @param	Widget_FilePickerItem	$file
	 */
	public function attachFile(Widget_FilePickerItem $file)
	{
		$uploadPath = $this->uploadPath();
		$uploadPath->createDir();
		$original = $file->getFilePath()->toString();
		$uploadPath->push(basename($original));

		rename($original, $uploadPath->toString());
	}

	/**
	 * get iterator with attachements
	 * @return Widget_FilePickerIterator
	 */
	public function attachments()
	{
		return bab_Widgets()->FilePicker()->getFolderFiles($this->uploadPath());
	}

	/**
	 * @return array of crm_Contact
	 */
	public function getLinkedContacts($linkType = 'hasNote')
	{
		$Crm = $this->Crm();

		if (!isset($Crm->Contact))
		{
			return array();
		}

		$Crm->includeContactSet();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this, $Crm->ContactClassName(), $linkType);

		$contacts = array();
		foreach ($links as $link) {
			$contacts[] = $link->sourceId;
		}

		return $contacts;
	}

	/**
	 * @return array of crm_Organization
	 */
	public function getLinkedOrganizations($linkType = 'hasNote')
	{
		$Crm = $this->Crm();

		if (!isset($Crm->Organization))
		{
			return array();
		}

		$Crm->includeOrganizationSet();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this, $Crm->OrganizationClassName(), $linkType);

		$orgs = array();
		foreach ($links as $link) {
			if ($link->sourceId) {
				$orgs[] = $link->sourceId;
			}
		}

		return $orgs;
	}

	/**
	 * @return array of crm_Deal
	 */
	public function getLinkedDeals($linkType = 'hasNote')
	{
		$Crm = $this->Crm();

		if (!isset($Crm->Deal))
		{
			return array();
		}

		$Crm->includeDealSet();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->selectForTarget($this, $Crm->DealClassName(), $linkType);

		$deals = array();
		foreach ($links as $link) {
			$deals[] = $link->sourceId;
		}

		return $deals;
	}



	/**
	 * @return crm_Note[]
	 */
	public function getLinkedNotes($linkType = 'hasNote')
	{
	    $Crm = $this->Crm();

	    $linkSet = $Crm->LinkSet();

	    $links = $linkSet->selectForTarget($this, $Crm->NoteClassName(), $linkType);

	    $notes = array();
	    foreach ($links as $link) {
	        $notes[] = $link->sourceId;
	    }

	    return $notes;
	}


	/**
	 * @return crm_Task[]
	 */
	public function getLinkedTasks($linkType = 'hasNote')
	{
	    $Crm = $this->Crm();

	    if (!isset($Crm->Task)) {
	        return array();
	    }

	    $linkSet = $Crm->LinkSet();

	    $links = $linkSet->selectForTarget($this, $Crm->TaskClassName(), $linkType);

	    $tasks = array();
	    foreach ($links as $link) {
	        $tasks[] = $link->sourceId;
	    }

	    return $tasks;
	}


	/**
	 * {@inheritDoc}
	 * @see ORM_Record::getRecordTitle()
	 */
	public function getRecordTitle()
	{
        return bab_abbr($this->summary, BAB_ABBR_FULL_WORDS, 60);
	}
}
