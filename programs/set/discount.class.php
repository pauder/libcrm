<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__).'/discountbase.class.php';



/**
 * Discount set
 *
 * @property ORM_TextField					$description
 * @property ORM_DecimalField				$value
 * @property ORM_BoolField                  $shipping
 * @property ORM_BoolField                  $percentage
 * @property ORM_DecimalField               $threshold
 * @property ORM_DateField					$start_date
 * @property ORM_DateField					$end_date
 * 
 * @method crm_Discount newRecord()
 * @method crm_Discount get()
 * @method crm_Discount[] select()
 */
class crm_DiscountSet extends crm_DiscountBaseSet
{

	

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();
        $this->setDescription('Discount');
		
		
	}

}


/**
 * Discount
 * 
 */
class crm_Discount extends crm_DiscountBase
{
    
    
    /**
     * Get the amount to add to the shopping cart
     * the amount is tax included
     *
     * @param	crm_ShoppingCart		$cart
     * @param	stdClass				$max		Total amount in shopping cart, preceding discounts and reductions allready included
     *                                              contain 2 properties: total, shipping
     * @return float (negative value)
     */
    public function getAmount(crm_ShoppingCart $cart, stdClass $max)
    {
        $Crm = $this->Crm();
    
    
        $max_discount_value = $max->total;
        if ($this->shipping) {
            $max_discount_value = $max->shipping;
        }
    
        $value = $this->getInDiscountValue($max_discount_value);
    
    
        if (!$this->testThreshold($cart, $max)) {
            $formatedThreshold = $Crm->numberFormat($this->threshold).bab_nbsp().$Crm->Ui()->Euro();
            $message = sprintf($Crm->translate('This discount is applicable only from a total amount of %s'), $formatedThreshold);
            throw new crm_DiscountException($message);
        }
    
        if ($value > $max_discount_value) {
            $formatedValue = $Crm->numberFormat($value).bab_nbsp().$Crm->Ui()->Euro();
            if ($this->shipping) {
                $message = $Crm->translate('The total of your shipping costs does not allow the use of this discount with a value of %s');
            } else {
                $message = $Crm->translate('The total of your shopping cart does not allow the use of this discount with a value of %s');
            }
            throw new crm_DiscountException(sprintf($message, $formatedValue));
        }
    
        return (-1 * $value);
    }
    
}



/**
 * Exception throwed when a discount is not valid
 * Message should be used in the shopping cart UI
 */
class crm_DiscountException extends crm_DiscountBaseException
{
	
}