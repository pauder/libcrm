<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */








class crm_PortletDefinition_DealsSummary extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 * 
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Deals summary');
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Deals summary in CRM ' . $Crm->getAddonName());
	}

	/**
	* @return array
	*/
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();
		
		$W = bab_Widgets();
		
		$prefs = array();

		$includeRoot = false;
		$statusLevels = array('1' => 0);
		if ($includeRoot) {
			$prefs[] = array('label' => $Crm->translate('All status'), 'type' => 'boolean', 'name' => 'status_1');
			$offset = 0;
		} else {
			$offset = 1;
		}
		
		
		$statusSet = $Crm->StatusSet();
		$statuses = $statusSet->select($statusSet->id->greaterThan(1))->orderAsc($statusSet->lf);
		foreach ($statuses as $status) {
			if ($status->name == '') {
				continue;
			}
			if ($status->id_parent) {
				$statusLevel = $statusLevels[$status->id_parent] + 1;
			} else {
				$statusLevel = 0;
			}
			$statusLevels[$status->id] = $statusLevel;
			$statusName = str_repeat(bab_nbsp(), 4 * ($statusLevel - $offset)) . $status->name;
			$prefs[] = array('label' => $statusName, 'type' => 'boolean', 'name' => 'status_' . $status->id);
//			$statusSelector->addOptionClass($status->id, 'crm-status-level' . $statusLevel);
		}
		
		return $prefs;
	}
	
}




class crm_PortletUi_DealsSummary extends crm_PortletUi implements portlet_PortletInterface
{

	protected $delayedItem = null;
	
	/**
	 * @param Func_Crm $crm
	 */
	public function __construct(Func_Crm $crm)
	{
		$W = bab_Widgets();
		parent::__construct($crm);

		if (empty($this->options['nbItems'])) {
			$this->options['nbItems'] = 2;
		}

		$home = $this->Crm()->Controller()->Home();
		if (!method_exists($home, 'recentDeals'))
		{
			$this->setInheritedItem($W->Frame());
			return;
		}
		
		$crm->Ui()->includeTask();

		$this->delayedItem = $W->DelayedItem($home->recentDeals());
		$this->setInheritedItem(
			$this->delayedItem
		);
	}

	
	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		$home = $this->Crm()->Controller()->Home();
		
		$currentlySelectedStatuses = array();

		foreach ($this->options as $optionName => $optionValue) {
			if (substr($optionName, 0, strlen('status_')) === 'status_' && $optionValue != '0') {
				$statusId = substr($optionName, strlen('status_'));
				$currentlySelectedStatuses[$statusId] = $statusId;
			}
		}
		
		if (method_exists($home, 'recentDeals')) {
			$this->delayedItem->setDelayedAction($home->recentDeals(100, $currentlySelectedStatuses));
		}

		return parent::display($canvas);
	}
		
}