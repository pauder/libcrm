<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */








class crm_PortletDefinition_RecentActivity extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 * 
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Recent activity');
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Recent activity in CRM ' . $Crm->getAddonName());
	}

	/**
	* @return array
	*/
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();
	
		$Crm->Ui()->includeBase();
		
		$modesList = array(
			array('value' => crm_HistoryElement::MODE_COMPACT, 'label' => $Crm->translate('Compact')),
			array('value' => crm_HistoryElement::MODE_SHORT, 'label' => $Crm->translate('Short')),
			array('value' => crm_HistoryElement::MODE_FULL, 'label' => $Crm->translate('Full'))
		);
		

		return  array(
			array(
				'label' => $Crm->translate('Number of items to display'),
				'type' => 'int',
				'name' => 'nbItems'
			),
			array(
				'label' => $Crm->translate('Display mode'),
				'type' => 'list',
				'name' => 'mode',
				'options' => $modesList
			),
		);
	}
	
}




class crm_PortletUi_RecentActivity extends crm_PortletUi implements portlet_PortletInterface
{

	/**
	 * @param Func_Crm $crm
	 */
	public function __construct(Func_Crm $crm)
	{
		parent::__construct($crm);
		
		$W = bab_Widgets();
		$this->setInheritedItem($W->Frame());
	}

	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		if (empty($this->options['nbItems'])) {
			$this->options['nbItems'] = 2;
		}
		if (empty($this->options['mode'])) {
			$this->options['mode'] = crm_HistoryElement::MODE_SHORT;
		}
		
		$homeCtrl = $this->Crm()->Controller()->Home(false);
		if (method_exists($homeCtrl, 'globalHistoryContent')) {
			$this->setInheritedItem($homeCtrl->globalHistoryContent($this->options['nbItems'], $this->options['mode']));
		} else {
			$W = bab_Widgets();
			$this->setInheritedItem($W->Frame());
		}

		return parent::display($canvas);
	}
		
}


