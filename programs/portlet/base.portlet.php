<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */




class crm_PortletDefinition extends crm_Object
{
    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;


    /**
     * @param Func_Crm $crm
     */
    public function __construct(Func_Crm $crm = null)
    {
        parent::__construct($crm);
        $this->addon = bab_getAddonInfosInstance($crm->getAddonName());
    }



    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getIconPath();
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getIconPath();
    }

	/**
	 * Portlet definition ID
	 *
	 * @example lcrm/MyTasks
	 * @return string
	 */
	public function getId()
	{
		$Crm = $this->Crm();
		$crm_path = explode('_', get_class($Crm));
		unset($crm_path[0]); // remove Func
		unset($crm_path[1]); // remove Crm

		$portlet_path = explode('_',get_class($this));

		return implode('/', $crm_path).'/'.$portlet_path[count($portlet_path) - 1];
	}


	/**
	 * @return crm_PortletUi
	 */
	public function getPortlet()
	{
		$classname = get_class($this);
		$suffix = mb_substr($classname, mb_strlen('crm_PortletDefinition_'));
		$classname = 'crm_PortletUi_'.$suffix;

		return new $classname($this->Crm());
	}


	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		return array();
	}



	public function getConfigurationActions()
	{
		return array();
	}




	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}
}





class crm_PortletUi extends crm_UiObject
{
	protected $options = array();


	/**
	 * @return string
	 */
	public function getName()
	{
		$classname = get_class($this);
		return mb_substr($classname, 1 + mb_strlen(__CLASS__));
	}


	/**
	 *
	 * @return crm_PortletDefinition
	 */
	public function getPortletDefinition()
	{
		$Crm = $this->Crm();
		$portlet = $Crm->Portlet();
		$classname = get_class($this);
		$method = mb_substr($classname, 1 + mb_strlen(__CLASS__));
		return $portlet->$method();
	}


	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}

	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}

	public function setPortletId($id)
	{
        $this->portletId = $id;
	}
}