<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */





class crm_PortletDefinition_ShopSale extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return sprintf($Crm->translate('Online shop last months sales (%s)'), $Crm->getAddonName());
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Online shop sales by month.');
	}

	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();

		$modesList = array(
			array('value' => 'sales_volume', 'label' => $Crm->translate('Sales (volume)')),
			array('value' => 'sales_amount', 'label' => $Crm->translate('Sales (amount)'))
		);
		$nbMonthsList = array(
			array('value' => '2', 'label' => $Crm->translate('2')),
			array('value' => '3', 'label' => $Crm->translate('3')),
			array('value' => '4', 'label' => $Crm->translate('4')),
			array('value' => '5', 'label' => $Crm->translate('5')),
			array('value' => '6', 'label' => $Crm->translate('6')),
			array('value' => '7', 'label' => $Crm->translate('7')),
			array('value' => '8', 'label' => $Crm->translate('8')),
			array('value' => '9', 'label' => $Crm->translate('9')),
			array('value' => '10', 'label' => $Crm->translate('10')),
			array('value' => '11', 'label' => $Crm->translate('11')),
			array('value' => '12', 'label' => $Crm->translate('12')),
		);

		return  array(
			array('label' => $Crm->translate('Mode'), 'type' => 'list', 'name' => 'mode', 'options' => $modesList),
			array('label' => $Crm->translate('Number of months to display'), 'type' => 'list', 'name' => 'nbMonths', 'options' => $nbMonthsList)
		);
	}
}




class crm_PortletUi_ShopSale extends crm_PortletUi implements portlet_PortletInterface
{

	/**
	 * @param Func_Crm $Crm
	 */
	public function __construct(Func_Crm $Crm)
	{
		$W = bab_Widgets();
		parent::__construct($Crm);
		$this->setInheritedItem($W->Frame());
	}


	/**
	 *
	 * @param array $monthlyValues
	 * @return Widget_TableView
	 */
	protected function tableView($monthlyValues)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$tableView = $W->TableView();
		$tableView->setView(Widget_TableView::VIEW_CHART);


		$tableView->addSection('header', null, 'widget-table-header');
		$tableView->setCurrentSection('header');

		$row = 0;
		$col = 0;
		foreach ($monthlyValues as $yearMonth => $value) {
			list($year, $month) = explode('-', $yearMonth);
			$tableView->addItem($W->Label($month . '/' . substr($year, 2, 2)), $row, $col++);
		}


		$tableView->addSection('body', null, 'widget-table-body');
		$tableView->setCurrentSection('body');

		$row++;
		$col = 0;
		foreach ($monthlyValues as $yearMonth => $value) {
			$tableView->addItem($W->Label('' . $value), $row, $col++);
		}

		return $tableView;
	}



	/**
	 * @return
	 */
	public function salesVolume($nbMonths)
	{
		$Crm = $this->Crm();

		$firstOfMonth = bab_DateTime::now();
		$firstOfMonth->add(1 - $firstOfMonth->getDayOfMonth(), BAB_DATETIME_DAY);
		$firstOfMonth->add(-$nbMonths + 1, BAB_DATETIME_MONTH);

		$monthlyValues = array();
		for ($i = 0; $i < $nbMonths; $i++) {
			$yearMonth = substr($firstOfMonth->getIsoDate(), 0, 7);
			$monthlyValues[$yearMonth] = 0;
			$firstOfMonth->add(1, BAB_DATETIME_MONTH);
		}



		$set = $Crm->OrderItemSet();
		$set->parentorder();

		$set->addFields(
			$set->quantity->sum()->round()->setName('sales'),
			$set->parentorder->createdOn->left(7)->setName('yearMonth')
		);

		$end = $firstOfMonth->getIsoDate();
		$firstOfMonth->add(-$nbMonths, BAB_DATETIME_MONTH);
		$start = $firstOfMonth->getIsoDate();

		$orderItems = $set->select(
			$set->parentorder->createdOn->greaterThanOrEqual($start)
				->_AND_($set->parentorder->createdOn->lessThan($end))
		);
		$orderItems->groupBy($set->yearMonth);
		$orderItems->orderAsc($set->yearMonth);

		foreach ($orderItems as $orderItem) {
			$monthlyValues[$orderItem->yearMonth] = $orderItem->sales;
		}

		return $this->tableView($monthlyValues);
	}




	/**
	 * @return
	 */
	public function salesAmount($nbMonths)
	{
		$Crm = $this->Crm();


		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

		$firstOfMonth = bab_DateTime::now();
		$firstOfMonth->add(1 - $firstOfMonth->getDayOfMonth(), BAB_DATETIME_DAY);
		$firstOfMonth->add(-$nbMonths + 1, BAB_DATETIME_MONTH);

		$monthlyValues = array();
		for ($i = 0; $i < $nbMonths; $i++) {
			$yearMonth = substr($firstOfMonth->getIsoDate(), 0, 7);
			$monthlyValues[$yearMonth] = 0;
			$firstOfMonth->add(1, BAB_DATETIME_MONTH);
		}


		$set = $Crm->OrderSet();

		$set->addFields(
			$set->total_df->sum()->round()->setName('sales'),
			$set->createdOn->left(7)->setName('yearMonth')
		);

		$end = $firstOfMonth->getIsoDate();
		$firstOfMonth->add(-$nbMonths, BAB_DATETIME_MONTH);
		$start = $firstOfMonth->getIsoDate();

		$orders = $set->select(
			$set->createdOn->greaterThanOrEqual($start)
				->_AND_($set->createdOn->lessThan($end))
		);
		$orders->groupBy($set->yearMonth);
		$orders->orderAsc($set->yearMonth);


		foreach ($orders as $order) {
			$monthlyValues[$order->yearMonth] = $order->sales;
		}

		return $this->tableView($monthlyValues);
	}






// 	/**
// 	 * @return
// 	 */
// 	public function salesAmount($nbMonths)
// 	{
// 		$Crm = $this->Crm();
// 		$Ui = $Crm->Ui();
// 		$W = bab_Widgets();
// 		$tableView = $W->TableView();
// 		$tableView->setView(Widget_TableView::VIEW_CHART);


// 		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

// 		$firstOfMonth = bab_DateTime::now();
// 		$firstOfMonth->add(1 - $firstOfMonth->getDayOfMonth(), BAB_DATETIME_DAY);
// 		$firstOfMonth->add(-$nbMonths, BAB_DATETIME_MONTH);


// 		$row = 0;

// 		$tableView->addSection('header', null, 'widget-table-header');
// 		$tableView->setCurrentSection('header');
// 		for ($i = 0; $i < $nbMonths; $i++) {
// 			$tableView->addItem($W->Label(sprintf('%02d/%02d', $firstOfMonth->getMonth(), ($firstOfMonth->getYear() % 100))), $row, $i);
// 			$firstOfMonth->add(1, BAB_DATETIME_MONTH);
// 		}

// 		//		$firstOfMonth->add(-$nbMonths, BAB_DATETIME_MONTH);

// 		$set = $Crm->OrderSet();

// 		$set->addFields(
// 				$set->total_df->sum()->round()->setName('sales')
// 		);
// 		$set->addFields(
// 				$set->createdOn->left(7)->setName('yearMonth')
// 		);


// 		$end = $firstOfMonth->getIsoDate();
// 		$firstOfMonth->add(-$nbMonths, BAB_DATETIME_MONTH);
// 		$start = $firstOfMonth->getIsoDate();

// 		$orders = $set->select(
// 				$set->createdOn->greaterThanOrEqual($start)
// 				->_AND_($set->createdOn->lessThan($end))
// 		);
// 		$orders->groupBy($set->yearMonth);
// 		$orders->orderAsc($set->yearMonth);

// 		$values = array();

// 		foreach ($orders as $order) {
// 			$values[$order->yearMonth] = $order->sales;
// 		}

// 		$tableView->addSection('body', null, 'widget-table-body');
// 		$tableView->setCurrentSection('body');

// 		$row++;
// 		for ($i = 0; $i < $nbMonths; $i++) {

// 			$isoDate = $firstOfMonth->getIsoDate();
// 			$yearMonth = substr($isoDate, 0, 7);

// 			if (isset($values[$yearMonth])) {
// 				$sales = $values[$yearMonth];
// 			} else {
// 				$sales = 0;
// 			}

// 			//$tableView->addItem($W->Label(rand(0, 100) + $i * 10 . ''), $row, $i);
// 			$tableView->addItem($W->Label($sales), $row, $i);

// 			$firstOfMonth->add(1, BAB_DATETIME_MONTH);
// 		}

// 		return $tableView;
// 	}





	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		if (empty($this->options['mode'])) {
			$this->options['mode'] = 'sales_volume';
		}

		if (empty($this->options['nbMonths'])) {
			$this->options['nbMonths'] = 12;
		}

		$Crm = $this->Crm();
		$W = bab_Widgets();

		switch ($this->options['mode'])
		{
			case 'sales_volume':
				$this->addItem($W->Title(sprintf($Crm->translate('Last %d month sales in volume'), $this->options['nbMonths']), 4));
				$this->addItem($this->salesVolume($this->options['nbMonths']));
				break;

			case 'sales_amount':
				$this->addItem($W->Title(sprintf($Crm->translate('Last %d month sales amount'), $this->options['nbMonths']), 4));
				$this->addItem($this->salesAmount($this->options['nbMonths']));
				break;

		}

		return parent::display($canvas);
	}
}
