<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */






class crm_PortletDefinition_ShopOrder extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return sprintf($Crm->translate('Online shop order/invoices list (%s)'), $Crm->getAddonName());
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Online shop last orders or invoices, with configurable number of orders');
	}

	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();
		
		return  array(
				array('label' => $Crm->translate('Number of orders'), 'type' => 'text', 'name' => 'limit')
		);
	}
}




class crm_PortletUi_ShopOrder extends crm_PortletUi implements portlet_PortletInterface
{

	/**
	 * @param Func_Crm $Crm
	 */
	public function __construct(Func_Crm $Crm)
	{
		$W = bab_Widgets();
		parent::__construct($Crm);
		$this->setInheritedItem($W->Frame());




	}

	/**
	 * @return
	 */
	public function orders()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		
		$tableView = $Ui->OrderTableView();

		$set = $Crm->OrderSet();
		$tableView->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Number')));
		$tableView->addColumn(widget_TableModelViewColumn($set->total_ti, $Crm->translate('Total tax incl')));
		$tableView->addColumn(widget_TableModelViewColumn($set->createdOn, $Crm->translate('Date')));
		$res = $set->select();
		$res->orderDesc($set->createdOn);

		$tableView->setDataSource($res);
		$tableView->setLimit($this->options['limit']);

		return $tableView;
	}




	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{

		if (empty($this->options['limit']))
		{
			$this->options['limit'] = 10;
		}

		$Crm = $this->Crm();
		$W = bab_Widgets();

		$this->addItem($W->Title(sprintf($Crm->translate('The %d last orders/invoices'), $this->options['limit']), 4));
		$this->addItem($this->orders());
		
		if ($Crm->Access()->listOrder())
		{
			$this->addItem($W->Link($Crm->translate('View all invoices'), $Crm->Controller()->Order()->displayList())->addClass('crm-dialog-button'));
		}


		return parent::display($canvas);
	}
}