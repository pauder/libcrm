window.babAddonLibCrm = {};


window.babAddonLibCrm.orderAddressTimer = null;
window.babAddonLibCrm.orderItemCatalogItemTimer = null;


/**
 * Encodes a string in the same manner as PHP's urlencode().
 */
window.babAddonLibCrm.urlencode = function(str)
{
	str = escape(str);
	str = str.replace('+', '%2B');
	str = str.replace('%20', '+');
	str = str.replace('*', '%2A');
	str = str.replace('/', '%2F');
	str = str.replace('@', '%40');
	return str;
};

window.babAddonLibCrm.cleanStringDiacritics = function (text)
{
	try {
		text = text.replace(/%C8|%C9|%CA|%CB/g, "E");
		text = text.replace(/%E8|%E9|%EA|%EB/g, "e");

		text = text.replace(/%CC|%CD|%CE|%CF/g, "I");
		text = text.replace(/%EC|%ED|%EE|%EF/g, "i");

		text = text.replace(/%C0|%C1|%C2|%C3|%C4/g, "A");
		text = text.replace(/%E0|%E1|%E2|%E3|%E4/g, "a");

		text = text.replace(/%D2|%D3|%D4|%D5|%D6/g, "O");
		text = text.replace(/%F2|%F3|%F4|%F5|%F6/g, "o");

		text = text.replace(/%D9|%DA|%DB|%DC/g, "U");
		text = text.replace(/%F9|%FA|%FB|%FC/g, "u");

		text = text.replace(/%D1/g, "N");
		text = text.replace(/%F1/g, "n");

		text = text.replace(/%C7/g, "C");
		text = text.replace(/%E7/g, "c");
	} catch (e) {
		text = '';
	}
	return text;
};

/**
 * load organization informations if no modifications in the last second
 * this method is linked to the suggest organization widget
 */
window.babAddonLibCrm.orderAddress = function(field)
{
	var meta = window.babAddonWidgets.getMetadata(field.attr('id'));
	var contener = field.parents('.crm-loading-box');
	
	if (null != window.babAddonLibCrm.orderAddressTimer) {
		clearTimeout(window.babAddonLibCrm.orderAddressTimer);
	}
	
	window.babAddonLibCrm.orderAddressTimer = setTimeout(function() {
		
		contener.addClass('crm-loading-on');
		
		var dataurl = 'tg='+meta.tg+'&idx='+meta.action+'&type='+meta.type+'&organizationname='+field.val();
		
		if (meta.fields)
		{
			for(var i = 0; i < meta.fields.length; i++)
			{
				dataurl += '&'+meta.fields[i]+'='+jQuery('[name="order['+meta.fields[i]+']"]').val();
			}
		}

		jQuery.ajax({
		   url: meta.selfpage,
		   data: dataurl,
		   success: function(response){
			
				contener.removeClass('crm-loading-on');
				window.babAddonLibCrm.orderAddressForm(contener);
				
				if ('' == response)
				{
					return;
				}
	
				eval('var data = '+response+';');
				
				
				
				contener.find('input[name="order['+meta.type+'person]"]').val(data.recipient);
				
				for (var prop in data.address)
				{
					contener.find('[name="order['+meta.type+'address]['+prop+']"]').val(data.address[prop]);
				}
		   }
		});

	}, 1000);
};







/**
 * load catalog item informations if no modifications in the last second
 * this method is linked to the suggest catalog item widget
 */
window.babAddonLibCrm.orderItemCatalogItem = function(field)
{
	var meta = window.babAddonWidgets.getMetadata(field.attr('id'));
	var contener = field.parents('.crm-loading-box');
	
	if (null != window.babAddonLibCrm.orderItemCatalogItemTimer) {
		clearTimeout(window.babAddonLibCrm.orderItemCatalogItemTimer);
	}
	
	window.babAddonLibCrm.orderItemCatalogItemTimer = setTimeout(function() {
		
		contener.addClass('crm-loading-on');

		jQuery.ajax({
		   url: meta.selfpage,
		   data: 'tg='+meta.tg+'&idx='+meta.action+'&order='+meta.order+'&articlereference='+field.val(),
		   success: function(response){
			
				contener.removeClass('crm-loading-on');
				
				if ('' == response)
				{
					return;
				}
	
				eval('var data = '+response+';');
				
				for (var prop in data)
				{
					contener.find('[name="orderItem['+prop+']"]').val(data[prop]);
				}
		   }
		});

	}, 1000);
};











/**
 * Prepare all orderAddress forms on page load
 * the address and recipient should be locked while the organization name is empty
 */
window.babAddonLibCrm.orderAddressForms = function()
{
	
	jQuery('.crm-address-contener').each(function(i, contener) {
		window.babAddonLibCrm.orderAddressForm(jQuery(contener));
	});
};


/**
 * Set fields disabled status for one address form contener
 * 
 */
window.babAddonLibCrm.orderAddressForm = function(contener) {
	
	if (contener.find('input.crm-suggestorganization').val() == '') {
		contener.find('input, select, textarea').not('input.crm-suggestorganization').attr('disabled', true);
		
	} else {
		contener.find('input, select, textarea').not('input.crm-suggestorganization').removeAttr('disabled');
	}
	
};








/**
 * Number format according to the numberFormat method of crm object
 * 
 * @todo	implement the thousands separators
 * 
 * @param	float	num
 * @param	int		precision
 * @return string
 */
window.babAddonLibCrm.numberFormat = function(num, precision) {

    meta = window.babAddonWidgets.getMetadata(jQuery('.crm-page').attr('id'));
    
    if (null == precision) {
        precision = meta.numberFormatPrecision;
    }
    
    if (isNaN(num)) {
        return '';
    } else {
        str = (Math.round(num * Math.pow(10,precision)) / Math.pow(10,precision)).toString();
        
        var numberParts = str.split('.');
        if (numberParts.length < 2) {
            numberParts[1] = '';
        }
        numberParts[1] += '0'.repeat(precision - numberParts[1].length);
        str = numberParts[0] + meta.numberFormatSeparator + numberParts[1]
        
        return str;
    }
};




/**
 * Parse float with comma or dot
 * @param	string	value
 * @return float
 */
window.babAddonLibCrm.parseFloat = function(value) {
	value = value.replace(/,/,'.').replace(/ /,'').replace(/\xA0/,'');
	return parseFloat(value);
}








/**
 * Set events on widget tableview to compute the order cells dynamically
 */
window.babAddonLibCrm.orderEditItems = function() {
	
	var updateTable = function(event) {
		
		var field = jQuery(event.currentTarget);
		var table = field.closest('.crm-orderitem-edit-tableview');
		var sub_total_df 	= 0.0;
		var sub_total_ti 	= 0.0;
		
		table.find('.widget-table-body .widget-table-row').each(function(i, rowfield) {
			
			rowfield = jQuery(rowfield);
			
			var quantity 	= window.babAddonLibCrm.parseFloat(rowfield.find('[name$="[quantity]"]').val());
			var unit_cost 	= window.babAddonLibCrm.parseFloat(rowfield.find('[name$="[unit_cost]"]').val());
			var vat 		= window.babAddonLibCrm.parseFloat(rowfield.find('[name$="[vat]"]').val());
			var df 			= quantity * unit_cost;
			
			sub_total_df 	+= df;
			sub_total_ti	+= (df * vat / 100) + df;
			
			rowfield.find('[name$="[total_df]"]').val(window.babAddonLibCrm.numberFormat(df));
			
		});
		
		
		// sub_total_ti += window.babAddonLibCrm.parseFloat(table.find('[name="order[items][freight]"]').val());
		
		table.find('[name="order[items][total_df]"]').val(window.babAddonLibCrm.numberFormat(sub_total_df));
		table.find('[name="order[items][total_ti]"]').val(window.babAddonLibCrm.numberFormat(sub_total_ti));
	}

	jQuery('.crm-orderitem-edit-tableview input').keyup(updateTable);
	jQuery('.crm-orderitem-edit-tableview input').change(updateTable);

};










///**
// * crm_Contact
// */
//window.babAddonLibCrm.contactForm = function() {
//	
//	var formupdate = function() {
//		
//		var radioinput = jQuery('#crm_usertype input:checked');
//		
//		var userpicker = jQuery('input[name="contact[user]"]').closest('.widget-layout');
//		var extra = jQuery('#crm_contact_user_extra');
//		
//		
//		switch(jQuery(radioinput).val()) {
//			default: // nothing checked
//			case '1':
//				userpicker.hide();
//				extra.hide();
//				break;
//				
//			case '2':
//				userpicker.show();
//				extra.hide();
//				break;
//				
//			case '3':
//				userpicker.hide();
//				extra.show();
//				break;
//
//		}
//	};
//
//	
//	if (jQuery('#crm_usertype').get(0)) {
//		
//		jQuery('#crm_usertype input').change(formupdate);	
//		
//		jQuery('#crm_usertype input:checked').each(function(k, radioinput) {
//			formupdate(); 
//		});
//		
//	}
//};
















/**
 * Load images of menu 
 */
window.babAddonLibCrm.onOpenFileMenu = function(menu) {
	
	menu.find('.widget-image').each(function(k, img) {
		var metadata = window.babAddonWidgets.getMetadata(jQuery(img).attr('id'));
		jQuery(img).attr('src', metadata.src);
	});
		
};


window.babAddonLibCrm.dragDrop = function() {

	var selected = jQuery([]), offset = {top:0, left:0}; 

	if (!jQuery('.crm-draggable').draggable)
	{
		return 0;
	}
	
	// Make draggable elements.
	jQuery('.crm-draggable').draggable(
		{
			helper: 'clone',
			revert: false,
			scroll: true,
			opacity: 0.8,
		    start: function(ev, ui) {
		    	selected = jQuery('.ui-selected').each(function() {
		    		var el = jQuery(this);
		    		el.data('offset', el.offset());
		    	});

	    		jQuery(this).addClass('ui-selected');

	    		offset = jQuery(this).offset();
		    	
	        },
			drag: function(e, ui) {
				var dt = ui.position.top - offset.top, dl = ui.position.left - offset.left;

//				window.console.debug(selected);
				 // take all the elements that are selected expect $("this"), which is the element being dragged and loop through each.
                selected.each(function() {
                     // create the variable for we don't need to keep calling $("this")
                     // el = current element we are on
                     // off = what position was this element at when it was selected, before drag
                     var el = jQuery(this), off = el.data('offset');
                     el.css({position:'absolute', top: off.top + dt, left: off.left + dl});
                });

				jQuery(ui.helper).addClass('crm-draggable-dragged');
			},
			stop: function(e, ui) {
                selected.each(function() {
                    var el = jQuery(this), off = el.data('offset');
                    el.css({position:'static', top: off.top, left: off.left});
               });
				jQuery(ui.element).removeClass('crm-draggable-dragged');
			}
		}
	);
	
	jQuery('.crm-file-full-frame').selectable({
        filter: '.crm-draggable'
	});

	
	// Make droppable elements.
	jQuery('.crm-droppable').droppable(
		{
			tolerance: 'pointer',
			accept: '.crm-draggable',
			activeClass: 'crm-droppable-accepting',
			hoverClass: 'crm-droppable-hovered',
			drop: function(ev, ui) {
				var sourceId = ui.draggable.attr('id');
				var sourcePathname = window.babAddonWidgets.getMetadata(sourceId).pathname;
	
				var destId = this.id;
				var destPathname = window.babAddonWidgets.getMetadata(destId).pathname;
				var sourceFileType = window.babAddonWidgets.getMetadata(sourceId).filetype;
				
				var moveUrl = window.babAddonWidgets.getMetadata(destId).moveUrl;
				
				moveUrl = moveUrl.replace('__1__', encodeURIComponent(sourcePathname));

				window.location.href = moveUrl;
			}
		}
	);

};




/**
 * For E-mail sending screen :
 * If Javascript is activated, the code shows the input suggest e-mail and the button
 */
window.babAddonLibCrm.onLoadEmailSendingForm = function() {
	jQuery('#crm_email_assistantframe').show();
	jQuery('#crm_email_button_add_recipient').show();
	/* When we click to the button, the value of the input text is sended to the list of recipients */
	jQuery('#crm_email_button_add_recipient').click(function() {
		  newrecipient = jQuery('#crm_email_input_suggest_recipient').val();
		  if (newrecipient != '') {
			  recipients = jQuery('#crm_email_input_recipients').val();
			  if (recipients == '') {
				  jQuery('#crm_email_input_recipients').val(newrecipient);
			  } else {
				  jQuery('#crm_email_input_recipients').val(recipients+','+newrecipient);
			  }
		  }
	});
	
	jQuery('#crm_email_assistantframe_cc').show();
	jQuery('#crm_email_button_add_recipient_cc').show();
	/* When we click to the button, the value of the input text is sended to the list of recipients */
	jQuery('#crm_email_button_add_recipient_cc').click(function() {
		  newrecipient = jQuery('#crm_email_input_suggest_recipient_cc').val();
		  if (newrecipient != '') {
			  recipients = jQuery('#crm_email_input_recipients_cc').val();
			  if (recipients == '') {
				  jQuery('#crm_email_input_recipients_cc').val(newrecipient);
			  } else {
				  jQuery('#crm_email_input_recipients_cc').val(recipients+','+newrecipient);
			  }
		  }
	});
	
	jQuery('#crm_email_assistantframe_bcc').show();
	jQuery('#crm_email_button_add_recipient_bcc').show();
	/* When we click to the button, the value of the input text is sended to the list of recipients */
	jQuery('#crm_email_button_add_recipient_bcc').click(function() {
		  newrecipient = jQuery('#crm_email_input_suggest_recipient_bcc').val();
		  if (newrecipient != '') {
			  recipients = jQuery('#crm_email_input_recipients_bcc').val();
			  if (recipients == '') {
				  jQuery('#crm_email_input_recipients_bcc').val(newrecipient);
			  } else {
				  jQuery('#crm_email_input_recipients_bcc').val(recipients+','+newrecipient);
			  }
		  }
	});	
};






window.babAddonLibCrm.shoppingCart = function() {
	
	/**
	 * Replaces a formatted decimal number in the 'label' element by the 'value' string.
	 * 
	 * @var Element label
	 * @var String  value
	 * @return void
	 */
	var replaceNumeric = function (label, value)
	{
		var decimalNumberRegexp = /(\+|-)?(([0-9 \xA0]+([.,]\d+)?)|([.,]\d+))/;
		
		var labelText = label.text();
		
		if (!labelText.match(/\d/))
		{
			label.text(value+labelText);
			return;
		}
		
		var newLabelText = labelText.replace(decimalNumberRegexp, value);
		label.text(newLabelText);
		
		return;
	};
	
	
	
	var updateExtraRowVisibility = function()
	{
		// show or hide the extra rows
		
		jQuery('#crm-shopping-cart .widget-table-footer .crm-shoppingcart-extra').each(function(index, tr) {
			var tr = jQuery(tr);
			var value = tr.find('[id^=crm-shopping-cart-]').text();
			
			if (!value.match(/(\+|-)?\d+/))
			{
				tr.hide();
			} else {
				tr.show();
			}
		});
		
		
		jQuery('#crm-shopping-cart .widget-table-footer .crm-shoppingcart-coupon').each(function(index, tr) {
			var tr = jQuery(tr);
			var value = tr.find('[id^=crm-coupon-]').text();
			
			if (!value.match(/(\+|-)?\d+/))
			{
				tr.css('opacity',0.4);
			} else {
				tr.css('opacity',1);
			}
		});
	}
	
	
	
	
	
	var callAjaxUpdate = function(fieldsdata) {
		
		var querystring = '';
		
		for(var i =0; i < fieldsdata.length; i++) {
			obj = fieldsdata[i];
			querystring += '&'+obj.name+'='+obj.value;
		}
		
		var meta = window.babAddonWidgets.getMetadata('crm-shopping-cart');
		jQuery('.crm-shoppingcart-loading').addClass('crm-shoppingcart-loading-on');
		
		jQuery('.crm-shoppingcart-recalculate').attr('disabled', 'disabled');
		
		jQuery.ajax({
			url: meta.selfpage,
			data: 'tg='+meta.tg+'&idx=shoppingCart.ajaxUpdate&cart='+meta.cart+querystring,
			success: function(response) {
				if (response) {
					eval('var result = '+response+';');
					
					if (typeof result == 'string' || result instanceof String) {
						alert(result);
						return;
					}
						
					for (var id in result.item) {
						for (var field in result.item[id]) {
							label = jQuery('#crm-item-'+id+'-'+field);
							replaceNumeric(label, result.item[id][field]);
						}
					}
					
					if (null != result.extra)
					{
						for (var id in result.extra) {
							if (null === result.extra[id])
							{
								replaceNumeric(jQuery('#crm-shopping-cart-'+id), '');
							} else {
								replaceNumeric(jQuery('#crm-shopping-cart-'+id), result.extra[id]);
							}
							
							jQuery('#crm-shopping-cart').trigger('crm-shoppingcart-extra', [id, result.extra[id]]);
						}
					}
					
					if (null != result.coupon)
					{
						for (var id in result.coupon) {
							
							message = '';
							
							if (typeof result.coupon[id] == 'object')
							{
								value = result.coupon[id][0];
								message = result.coupon[id][1];

							} else {
								value = result.coupon[id];
							}
							
							
							jQuery('#crm-coupon-message-'+id).text(message);
							
							
							if (message == '')
							{
								jQuery('#crm-coupon-message-'+id).removeClass('crm-coupon-message');
							} else {
								jQuery('#crm-coupon-message-'+id).addClass('crm-coupon-message');
							}
							
							
							
							if (null === value)
							{
								replaceNumeric(jQuery('#crm-coupon-'+id), '0.00');
							}  else {
								replaceNumeric(jQuery('#crm-coupon-'+id), value);
							}
						}
					}
					
					if (null != result.marketing)
					{
						jQuery('#shoppingcart_marketing').replaceWith(result.marketing);
					}
					
					updateExtraRowVisibility();
					
				}
				
				jQuery('.crm-shoppingcart-loading').removeClass('crm-shoppingcart-loading-on');
			}
		});
	}
	
	
	
	var addPendingUpdate = function(fieldsdata) {
		
		
		
		var buttons = jQuery('.crm-shoppingcart-recalculate');
		buttons.removeAttr('disabled');
		
		var data = jQuery.data(buttons[0], 'fieldsdata');
		if (null == data)
		{
			data = new Array();
		}
		
		data = data.concat(fieldsdata);
		jQuery.data(buttons[0], 'fieldsdata', data);
		
	}
	



	window.babAddonLibCrm.shoppingCartTimer = new Object();
	
	var update = function() {
		
		var input = jQuery(this);
		
		if (null != window.babAddonLibCrm.shoppingCartTimer[input.attr('id')]) {
			clearTimeout(window.babAddonLibCrm.shoppingCartTimer[input.attr('id')]);
		}
		
		window.babAddonLibCrm.shoppingCartTimer[input.attr('id')] = setTimeout(function() {
			
			
			var fieldsdata = input.closest('.widget-table-row').find('input').serializeArray();
			
			
			if (0 == jQuery('.crm-shoppingcart-recalculate').length)
			{
				callAjaxUpdate(fieldsdata);
			} else {
				addPendingUpdate(fieldsdata);
			}

		}, 800);

	}
	
	
	
	var recalculateButton = function() {
		
		if (0 == jQuery('.crm-shoppingcart-recalculate').length)
		{
			return;
		}
		
		jQuery('.crm-shoppingcart-recalculate').click(function() {
			
			var buttons = jQuery('.crm-shoppingcart-recalculate');
			var fieldsdata = jQuery.data(buttons[0], 'fieldsdata');
			callAjaxUpdate(fieldsdata);
		}).attr('disabled', 'disabled');
		
		// verrouiller le bouton tant que le panier n'est pas a jour
		
		
		jQuery('.crm-shoppingcart-buttons .widget-link').click(function() {
			if ('disabled' != jQuery('.crm-shoppingcart-recalculate').attr('disabled'))
			{
				var meta = window.babAddonWidgets.getMetadata('crm-shopping-cart');
				alert('Recalculate'); // TODO translate
				return false;
			}
		});
		
	}
	
	
	
	jQuery('#crm-shopping-cart input[type="checkbox"]').click(update);
	jQuery('#crm-shopping-cart input[type="radio"]').change(update);
	jQuery('#crm-shopping-cart input[type="text"]').keyup(update);
	jQuery('#crm-shopping-cart input[type="number"]').keyup(update);
	jQuery('#crm-shopping-cart input[type="number"]').change(update);
	
	updateExtraRowVisibility();
	
	recalculateButton();
	
}



/**
 * Process shopping link to add a popup for modification of quantity
 */
window.babAddonLibCrm.shoppingLink = function()
{
	
	jQuery('.crm-shopping-link').click(function() {

		var link = jQuery(this);
		
		// test if there is a quantity field associated to the link
		
		var metadata = window.babAddonWidgets.getMetadata(link.attr('id'));
		
		
		if (metadata.quantityField)
		{
			var q = jQuery('#'+metadata.quantityField);
			link.attr('href', link.attr('href') + '&quantity='+q.val());
		}
		
		
		var regex = new RegExp('[\?&]catalogitem=([0-9]+)');
		var results = regex.exec(link.attr('href'));
		
		if (!results)
		{
			return true;
		}
		
		var catalogitem = parseInt(results[1]);
		
		var regex = new RegExp('[\?&]articlepackaging=([0-9]+)');
		var results = regex.exec(link.attr('href'));
		
		if (!results)
		{
			return true;
		}
		
		var articlepackaging = parseInt(results[1]);
		
		
		
		var dialog = jQuery('<div class="crm-catalogitem-option-dialog"><div class="loading"></div></div>').dialog({ width:650,  modal:true });

		
		regex = new RegExp('[\?&]tg=([^&#]+)');
		var results = regex.exec(link.attr('href'));
		var tg = results[1];
		
		var url = '?tg='+tg+'&idx=shoppingcart.catalogItemOptionsDialog&catalogitem='+catalogitem+'&articlepackaging='+articlepackaging;
		
		
		if (metadata.quantityField)
		{
			var q = jQuery('#'+metadata.quantityField);
			url += '&quantity='+q.val();
		}
		
		dialog.load(url);
		dialog.parent().addClass('crm-shopping-link-popup');
		
		return false;
	});
}



window.babAddonLibCrm.articlePackaging = function()
{
	var selectvat = jQuery('select[name="article[vat]"]');
	
	if (selectvat.length == 0)
	{
		return;
	}

	var vat = parseFloat(selectvat.find(':selected').text().replace(',','.')) / 100;
	
	var meta = window.babAddonWidgets.getMetadata(jQuery('.crm-article-packaging-edit').attr('id'));
	
	var round = function(x) {
		var y = (Math.round(x * 100) / 100);
		if (isNaN(y))
			{
			return '';
			}
		
		return y;
	}
	
	var vat = parseFloat(selectvat.find(':selected').text().replace(',','.')) / 100;
	selectvat.change(function() {
		
		vat = parseFloat(selectvat.find(':selected').text().replace(',','.')) / 100;
		
		jQuery('.crm-article-packaging-edit tr').each(function(index, tr) {
			
			var taxexcl = jQuery(tr).find('input[name$="[unit_cost]"]');
			var taxincl = jQuery(tr).find('input[name$="[unit_cost_ti]"]');
			
			if (taxexcl.length == 0 || taxincl.length == 0)
			{
				return;
			}
			
			var reduction = jQuery(tr).find('input[name$="[reduction]"]');
			var reductionincl = jQuery(tr).find('input[name$="[unit_cost_ri]"]');
			
			
			if (meta.excltaxpriority) 
			{
				var source = parseFloat(taxexcl.val().replace(',','.'));
				var ti = source * (1 + vat);
				taxincl.val(round(ti));
				reductionincl.val(round(ti * (1 - reductionvalue)));
			} else {
				var source = parseFloat(taxincl.val().replace(',','.'));
				taxexcl.val(round(source / (1 + vat)));
			}
		});
	});
	
	
	jQuery('.crm-article-packaging-edit tr').each(function(index, tr) {
		
		var taxexcl = jQuery(tr).find('input[name$="[unit_cost]"]');
		var taxincl = jQuery(tr).find('input[name$="[unit_cost_ti]"]');
		
		if (taxexcl.length == 0 || taxincl.length == 0)
		{
			return;
		}
		
		var reduction = jQuery(tr).find('input[name$="[reduction]"]');
		var reductionincl = jQuery(tr).find('input[name$="[unit_cost_ri]"]');
		
		
		
		
		taxexcl.data('target', taxincl);
		taxincl.data('target', taxexcl);
		
		taxexcl.keyup(function() {
			var source = parseFloat(jQuery(this).val().replace(',','.'));
			var ti = source * (1 + vat);
			taxincl.val(round(ti));
			var reductionvalue = parseFloat(reduction.val().replace(',','.')) / 100;
			reductionincl.val(round(ti * (1 - reductionvalue)));
		});
		
		taxincl.keyup(function() {
			var ti = parseFloat(jQuery(this).val().replace(',','.'));
			taxexcl.val(round( ti / (1 + vat)));
			var reductionvalue = parseFloat(reduction.val().replace(',','.')) / 100;
			reductionincl.val(round(ti * (1 - reductionvalue)));
		});
		
		reductionincl.keyup(function() {
            var taxinclvalue = parseFloat(taxincl.val().replace(',','.'));
            var source = parseFloat(jQuery(this).val().replace(',','.'));
            reduction.val(round(100 * (taxinclvalue - source) / taxinclvalue));
		});
		
		reduction.keyup(function() {
			var reductionvalue = parseFloat(jQuery(this).val().replace(',','.')) / 100;
			var ti = parseFloat(taxincl.val().replace(',','.'));
			reductionincl.val(round(ti * (1 - reductionvalue)));
			
		});
	});
	
}



window.babAddonLibCrm.shoppingSearch = function()
{
	var searchform = jQuery('.crm-catalogitem-search-editor');
	if (searchform.length == 0)
	{
		return;
	}
	
	var resultbar = jQuery('.crm-catalogitem-search-results');
	
	var metadata = window.babAddonWidgets.getMetadata(searchform.attr('id'));
	
	if (metadata.resultcount > 0)
	{
		searchform.hide();
		resultbar.show();
	} else {
		resultbar.hide();
	}
	
	resultbar.find('.button').click(function() {
		searchform.show();
		resultbar.hide();
	});
}


window.babAddonLibCrm.init = function() {
	window.babAddonLibCrm.orderAddressForms();
	window.babAddonLibCrm.orderEditItems();
//	window.babAddonLibCrm.contactForm();
//	window.babAddonLibCrm.instantContainer();
	window.babAddonLibCrm.dragDrop();
	window.babAddonLibCrm.onLoadEmailSendingForm();
	window.babAddonLibCrm.shoppingCart();
	window.babAddonLibCrm.shoppingLink();
	window.babAddonLibCrm.articlePackaging();
	window.babAddonLibCrm.shoppingSearch();
	
	jQuery('a.crm-codification-remove').click(function () {
		//	console.debug(jQuery(this).parents('.crm-tag'));
			var tag = jQuery(this).parents('.crm-codification');
			tag.hide('fast');
			tag.find('input').remove();
			return false;
		}
	);
}


window.bab.addInitFunction(window.babAddonLibCrm.init);


//window.bab.addInitFunction(window.babAddonLibCrm.instantContainer);


jQuery(document).ready(function() {

	jQuery('img.crm-address-map').each(function () {
		
		var imageAddressMap = this;
		var adr = jQuery(this).closest('.crm-address-form');
		
		jQuery(adr).find('textarea,input,select').change(function () {
			var street = jQuery(adr).find('.street-address').not('.resize-clone').val();
			var postalCode = jQuery(adr).find('.postal-code').val();
			var city = jQuery(adr).find('.locality').val();
			var latitude = jQuery(adr).find('.latitude').val();
			var longitude = jQuery(adr).find('.longitude').val();
			var country = jQuery(adr).find('.country option:selected').text();
			
			var zoom = 12;
			if (street == "") {
				zoom = 10;
				if (postalCode == "" && city == "") {
					zoom = 3;
					if (country == "") {
						zoom = 1;
					}
				}
			}
			street = babAddonLibCrm.cleanStringDiacritics(escape(street));
			postalCode = babAddonLibCrm.cleanStringDiacritics(escape(postalCode));
			city = babAddonLibCrm.cleanStringDiacritics(escape(city));
			country = babAddonLibCrm.cleanStringDiacritics(escape(country));
			latitude = babAddonLibCrm.cleanStringDiacritics(escape(latitude));
			longitude = babAddonLibCrm.cleanStringDiacritics(escape(longitude));

			var width = jQuery(imageAddressMap).width();
			var height = jQuery(imageAddressMap).height();
			
			if ((latitude == 'undefined' && longitude == 'undefined') || (latitude == '' && longitude == '')) {
				var url = "http://maps.google.com/maps/api/staticmap?zoom=" + zoom + "&size=" + width + "x" + height + "&maptype=roadmap&sensor=false&markers=color:green|"
					+ street + "%2C+"
					+ postalCode + "%2C+"
					+ city + "%2C+"
					+ country;
				imageAddressMap.src = url;
			} else {
				zoom = 12;
				var url = "http://maps.google.com/maps/api/staticmap?zoom=" + zoom + "&size=" + width + "x" + height + "&maptype=roadmap&sensor=false&markers=color:green|"
					+ latitude + "%2C+"
					+ longitude;
				imageAddressMap.src = url;
			}
		});
		jQuery(adr).find('textarea').change();

	});

});


//window.bab.addInitFunction(function(domNode) { jQuery(domNode).find('.crm-buttonset').buttonset(); });
