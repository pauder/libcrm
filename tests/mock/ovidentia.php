<?php


$GLOBALS['babInstallPath'] = dirname(__FILE__).'/../../vendor/ovidentia/ovidentia/ovidentia/';



// create babDB


require_once $GLOBALS['babInstallPath'].'utilit/dbutil.php';
require_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';

$GLOBALS['babDBHost'] = 'localhost';
$GLOBALS['babDBLogin'] = 'test';
$GLOBALS['babDBPasswd'] = '';
$GLOBALS['babDBName'] = 'test';

$GLOBALS['babDB'] = $babDB = new babDatabase();

// exec('mysql -u test -Nse "show tables" test | while read table; do mysql -u test -e "drop table $table" test; done');

exec('mysql -u test test < vendor/ovidentia/ovidentia/install/babinstall.sql 2>/dev/null');

$GLOBALS['babLanguage'] = 'en';



function bab_translate($str)
{
	return $str;
}


function bab_widgets()
{
    $basePath = dirname(__FILE__).'/../../vendor/ovidentia/widgets/';
    
    
    
    if (!defined('FUNC_WIDGETS_JS_PATH')) {
        define('FUNC_WIDGETS_PHP_PATH', $basePath.'programs/widgets/');
        define('FUNC_WIDGETS_JS_PATH', $basePath.'skins/ovidentia/templates/');
    }
    
    require_once $basePath.'programs/widgets.php';
    return new Func_Widgets();
}


/**
 * Returns a singleton of the specified class.
 *
 * @param string $classname
 * @return object
 */
function bab_getInstance($classname)
{
	static $instances = null;
	if (is_null($instances)) {
		$instances = array();
	}
	if (!array_key_exists($classname, $instances)) {
		$instances[$classname] = new $classname();
	}

	return $instances[$classname];
}




/**
 * return non breaking space
 * @return string
 */
function bab_nbsp()
{
    // return this fixed value for test
    return chr(160);
}




/**
 * Returns a unix timestamp corresponding to the string $time formatted as a MYSQL DATETIME
 *
 * @access  public
 *
 * @param   string	$time	(eg. '2006-03-10 17:37:02')
 *
 * @return  int	unix timestamp
 */
function bab_mktime($time)
{
    $arr = explode(" ", $time); //Split days and hours
    if ('0000-00-00' == $arr[0] || '' == $arr[0]) {
        return null;
    }
    $arr0 = explode("-", $arr[0]); //Split year, month et day
    if (isset($arr[1])) { //If the hours exist we send back days and hours
        $arr1 = explode(":", $arr[1]);
        return mktime($arr1[0], $arr1[1], $arr1[2], $arr0[1], $arr0[2], $arr0[0]);
    } else { //If the hours do not exist, we send back only days
        return mktime(0, 0, 0, $arr0[1], $arr0[2], $arr0[0]);
    }
}



/**
 * Returns a string containing the time formatted according to the user's preferences
 *
 * @access  public
 * @return  string	formatted time
 * @param   int	$time	unix timestamp
 * @param   boolean $hour	(true == '17/03/2006',
 *							false == '17/03/2006 10:11'
 */
function bab_shortDate($time, $hour = true)
{
    if (null === $time) {
        return '';
    }
    
    return date('d/m/Y' . ($hour? ' H:i' : ''), $time);
}



/**
 * Register a user
 *
 * @param	string		$firstname		mandatory firstname
 * @param	string		$lastname		mandatory lastname
 * @param	string		$middlename		The middlename can be an empty string
 * @param	string		$email			mandatory mail address
 * @param	string		$nickname		mandatory Login ID
 * @param	string		$password1		mandatory password
 * @param	string		$password2		mandatory password verification
 * @param	string		$confirmed		[1|0] 1 : the user is confirmed ; 0 : the user is not confirmed
 * @param	string		&$error			an empty string varaible, the error message will be set in this variable if the user cannot be registered
 * @param	boolean		$bgroup
 *
 * @return 	int|false
 */
function bab_registerUser( $firstname, $lastname, $middlename, $email, $nickname, $password1, $password2, $confirmed, &$error, $bgroup = true)
{
    require_once($GLOBALS['babInstallPath']."utilit/usermodifyincl.php");
    return bab_userModify::addUser( $firstname, $lastname, $middlename, $email, $nickname, $password1, $password2, $confirmed, $error, $bgroup);
}
