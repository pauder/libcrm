<?php 

require_once dirname(__FILE__).'/crm.php';
require_once dirname(__FILE__).'/../../programs/crm.php';

abstract class crm_TestCase extends PHPUnit_Framework_TestCase
{

    /**
     * Create a connexion with a test database
     * and intialize ORM
     * 
     */
    public function initTestDatabase()
    {
        
    }
    
    abstract protected function Crm();
}